//
//  AppDelegate.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "AppDelegate.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "helloTabBarController.h"
#import "CSLoginViewController.h"
#import "AvoidCrash.h"
#import "yujianniNavViewController.h"
#import "helloTabBarController.h"
#import "shawanyiyaUserSig.h"
#import "SFHFKeychainUtils.h"
#import "SELUpdateAlert.h"
//#import "GetGpsData.h"
#import <Bugly/Bugly.h>
#import "TIMComm.h"
#import <RPSDK/RPSDK.h>
@interface AppDelegate ()

{
      helloTabBarController * mainTBC;
}
@property (nonatomic ,copy) NSString *userSig;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Bugly startWithAppId:@"f22310a622"];
    [RPSDK setup];
//    [self dingweiData];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
    //第一次启动
        self.window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        self.window.backgroundColor = [UIColor whiteColor];
    }else{
    //不是第一次启动了
    }
    //辅助工具 点击视图打印出在哪一个界面
    SWIZZ_IT
    // Override point for customization after application launch.
    //获取usersig
    [[TUIKit sharedInstance] setupWithAppId:SDKAPPID];
    
    [self  LoadRequeUserSig];
    
    //设置键盘
    [self Setkeyboard];
    
    /************************ AvoidCrash ************************/
    [AvoidCrash becomeEffective];
    //监听通知:AvoidCrashNotification, 获取AvoidCrash捕获的崩溃日志的详细信息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dealwithCrashMessage:) name:AvoidCrashNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dealwithCrashMessage:) name:AvoidCrashNotification object:nil];
    //设置添加好友验证方式
    V2TIMUserFullInfo *info = [[V2TIMUserFullInfo alloc] init];
//    info.allowType = 1;
   
    [[V2TIMManager sharedInstance] setSelfInfo:info succ:nil fail:nil];
  
    //设置圆角
    [TUIKit sharedInstance].config.avatarType = TAvatarTypeRounded;
   
    //设置默认头像
    [TUIKit sharedInstance].config.defaultAvatarImage =[UIImage imageNamed:@"头像占位"] ;
    //设置默认群组图片
    [TUIKit sharedInstance].config.defaultGroupAvatarImage = [UIImage imageNamed:@"群头像占位"];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUserStatus:) name:TUIKitNotification_TIMUserStatusListener object:nil];
   
    NSString *str = [V2TIMManager sharedInstance].getVersion;
    NSLog(@"%@",str);
//    TIMManager *manager = [[TIMManager alloc]init];
    V2TIMSDKConfig *config = [[V2TIMSDKConfig alloc] init];
    // 3. 指定 log 输出级别，详情请参考 [SDKConfig](#SDKAppID)。
    config.logLevel = V2TIM_LOG_NONE;
    NSLog(@"------------%@",[self UUID]);
    
    NSLog(@"1111-------------%ld",(long)[V2TIMManager sharedInstance].getLoginStatus);
    if ( [V2TIMManager sharedInstance].getLoginStatus ==V2TIM_STATUS_LOGINED) {
        NSLog(@"1%ld",(long)[V2TIMManager sharedInstance].getLoginStatus);
        
    }
    
    NSString *loginUser = [[V2TIMManager sharedInstance] getLoginUser];
    [[UserDefaultManager shareInstance]SavaLoginLoginTengxunCode:loginUser];
    
    return YES;
}
//-(void)dingweiData{
//    [[GetGpsData shareInstance].locationManager startUpdatingLocation];//开始定位
//    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
//    if ([def objectForKey:@"lat"]==nil||[[def objectForKey:@"lat"] isEqual:@""]) {
//        return;
//    }
//    NSDictionary *dics =@{
//        @"lon":[def objectForKey:@"lon"]?:@"",
//        @"lat":[def objectForKey:@"lat"]?:@"",
//        @"shi":[def objectForKey:@"locCity"]?:@""
//    };
//
//}

//检测版本升级
- (void)LoadRequestVersions
{
   
     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appversion];
     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
     [dict setValue:@"2" forKey:@"qudaoid"];
     [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
         NSDictionary *data = dict[@"data"];
         NSDictionary *versions = data[@"versions"];
        if ([code isEqualToString:@"0"]) {
            NSString *appVername = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
            NSString *str = [appVername stringByReplacingOccurrencesOfString:@"." withString:@""];

            NSLog(@"----%@",str);
            NSDictionary* info=[[NSBundle mainBundle] infoDictionary];
            NSString* build=info[@"CFBundleVersion"];
            NSString *vername = [NSString stringWithFormat:@"%@",versions[@"vername"]];
            NSInteger verint =[versions[@"verint"] integerValue];
            NSInteger appVerint = [build integerValue];
            NSString *updatestate = [NSString stringWithFormat:@"%@",versions[@"updatestate"]];
            NSString *content =versions[@"content"];
            NSString *url =versions[@"url"];
            BOOL istype ;
            if ([updatestate isEqualToString:@"1"]) {
                istype = YES;
            }
            else
            {
                istype = NO;
            }
            if (verint>appVerint) {
                [SELUpdateAlert showUpdateAlertWithVersion:vername Description:content update:url isQiangZhi:istype];
            }
           
//            [self LoadCompareVersions:vername appversions:appVername content:versions[@"content"] updatestate:versions[@"updatestate"] url:versions[@"url"]];
            NSLog(@"%@-----%@",appVername,vername);
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadCompareVersions:(NSString *)versions appversions:(NSString *)Appversions  content:(NSString *)content  updatestate:(NSString *)updatestate url:(NSString *)url
{
    NSArray *localArray = [Appversions componentsSeparatedByString:@"."];
    NSArray *appArray = [versions componentsSeparatedByString:@"."];
    NSInteger minArrayLength = MIN(localArray.count, appArray.count);
    BOOL needUpdate = NO;
    
    for(int i=0;i<minArrayLength;i++){//以最短的数组长度为遍历次数,防止数组越界
        
        //取出每个部分的字符串值,比较数值大小
        NSString *localElement = localArray[i];
        NSString *appElement = appArray[i];
        NSInteger  localValue =  localElement.integerValue;
        NSInteger  appValue = appElement.integerValue;
        if(localValue<appValue) {
            //从前往后比较数字大小,一旦分出大小,跳出循环
            needUpdate = YES;
            break;
        }else{
            
            needUpdate = NO;
        }
    }
    if (needUpdate) {
        //弹出提示更新弹框
        [SELUpdateAlert showUpdateAlertWithVersion:versions Description:content update:url isQiangZhi:NO];
    }
    
   
}
- (NSString*)UUID {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *userName = infoDict[@"CFBundleName"];
    NSString *serviceName = infoDict[@"CFBundleIdentifier"];
    NSString *UUID = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:serviceName error:nil];
    if (UUID.length==0) {
        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SFHFKeychainUtils storeUsername:userName andPassword:UUID forServiceName:serviceName updateExisting:1 error:nil];
        
    }
    
    return UUID;

}

- (void)LoadRequeUserSig
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_getUserSig];
    [longwangRequestManage postBaseWithURL:url param:nil success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            _userSig = data[@"userSig"];
            NSString *login =  [UserDefaultManager shareInstance].GetLoginLoginAccount;
            
            [[TUILocalStorage sharedInstance] saveLogin:login  withAppId:SDKAPPID withUserSig:_userSig];
            [self LoadTXRequestLogin:login userSig:_userSig];
           
        }
        else
        {
            self.window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
            self.window.backgroundColor = [UIColor whiteColor];
        }
    } error:^(NSString *error) {
        self.window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        self.window.backgroundColor = [UIColor whiteColor];
    }];
}
- (void)LoadTXRequestLogin:(NSString *)login userSig:(NSString *)userSig
{
        [[TUIKit sharedInstance] login:login userSig:userSig succ:^{
            NSLog(@"-----> 登录成功");
            if (userSig.length>0) {
                mainTBC = [[helloTabBarController alloc]init];
                [self.window makeKeyAndVisible];
                self.window.rootViewController = [[helloTabBarController alloc]init];
                self.window.backgroundColor = [UIColor whiteColor];
                [self LoadRequestVersions];
            }
            else
            {
                self.window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
                self.window.backgroundColor = [UIColor whiteColor];
                [self LoadRequestVersions];
                
            }
    
        } fail:^(int code, NSString *msg) {
            NSLog(@"登录失败-----%@",msg);
            self.window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
            self.window.backgroundColor = [UIColor whiteColor];
            
        }];
}
void uncaughtExceptionHandler(NSException*exception){
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@",[exception callStackSymbols]);
    // Internal error reporting
}
- (void)onUserStatus:(NSNotification *)notification
{
    TUIUserStatus status = [notification.object integerValue];
    switch (status) {
        case TUser_Status_ForceOffline:
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"下线通知" message:@"您的帐号于另一台手机上登录。" delegate:self cancelButtonTitle:@"退出" otherButtonTitles:@"重新登录", nil];
            [alertView show];
        }
            break;
        case TUser_Status_ReConnFailed:
        {
            NSLog(@"连网失败");
        }
            break;
        case TUser_Status_SigExpired:
        {
            NSLog(@"userSig过期");
        }
            break;
        default:
            break;
    }
}

/**
 *强制下线后的响应函数委托
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        // 退出
        [[V2TIMManager sharedInstance] logout:^{
            NSLog(@"登出成功！");
            [[TUILocalStorage sharedInstance] logout];
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
            [[UserDefaultManager shareInstance]SavaLoginLoginAccount:@""];
        } fail:^(int code, NSString *msg) {
            NSLog(@"退出登录");
            [[TUILocalStorage sharedInstance] logout];
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
            [[UserDefaultManager shareInstance]SavaLoginLoginAccount:@""];
        }];
//        self.window.rootViewController = [self getLoginController];
    }else if(buttonIndex == 1){
        // 重新登录
     
        [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
    
        [[TUILocalStorage sharedInstance] login:^(NSString * _Nonnull identifier, NSUInteger appId, NSString * _Nonnull userSig) {
           
            [[TUIKit sharedInstance] login:@"qwer123" userSig:userSig succ:^{
                NSLog(@"-----> 登录成功");
                
                
                
            } fail:^(int code, NSString *msg) {
        //        NSLog(@"登录失败-----%@",msg);
                
            }];
        }];
    } else {
//        self.window.rootViewController = [self getLoginController];
    }
}
-(void)Setkeyboard {
    
    
  
  
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    /************************ IQKeyboardManager ************************/
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager]; // 获取类库的单例变量
    
    keyboardManager.enable = YES; // 控制整个功能是否启用
    
    keyboardManager.shouldResignOnTouchOutside = YES; // 控制点击背景是否收起键盘
    
    keyboardManager.shouldToolbarUsesTextFieldTintColor = YES; // 控制键盘上的工具条文字颜色是否用户自定义
    
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews; // 有多个输入框时，可以通过点击Toolbar 上的“前一个”“后一个”按钮来实现移动到不同的输入框
    
    keyboardManager.enableAutoToolbar = YES; // 控制是否显示键盘上的工具条
    
    keyboardManager.shouldShowToolbarPlaceholder = NO; // 是否显示占位文字
    
    keyboardManager.placeholderFont = [UIFont boldSystemFontOfSize:17]; // 设置占位文字的字体
    
    keyboardManager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
//    IQKeyboardManager.shared().isEnabled = true
}
#pragma mark - selector action
- (void)dealwithCrashMessage:(NSNotification *)note {
    //注意:所有的信息都在userInfo中
    //你可以在这里收集相应的崩溃信息进行相应的处理(比如传到自己服务器)
    NSLog(@"--------------------------异常信息------------%@",note.userInfo);
    
//    NSString * str1 = [NSString stringWithFormat:@"【AvoidCrash】崩溃名称：%@ | 导致崩溃代码所在方法：%@ | 导致崩溃的原因：%@",note.userInfo[@"errorName"],note.userInfo[@"errorPlace"],note.userInfo[@"errorReason"]];
//
//    NSString * str2 = [NSString stringWithFormat:@"AvoidCrash默认防崩溃处理：%@ | NSException：%@ | 调用栈信息：%@",note.userInfo[@"defaultToDo"],note.userInfo[@"exception"],note.userInfo[@"callStackSymbols"]];
    
//    NSException * exception = [NSException exceptionWithName:str1 reason:str2 userInfo:note.userInfo];
//    [Bugly reportException:exception];
    
}

#pragma mark - 生命周期
- (void)applicationWillEnterForeground:(UIApplication *)application{
    NSLog(@"状态** 将要进入前台");
}
- (void)applicationDidBecomeActive:(UIApplication *)application{
    NSLog(@"状态** 已经活跃");
    
    NSString *str =@"1";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"publish" object:str userInfo:nil];
}


- (void)applicationWillResignActive:(UIApplication *)application{
    NSLog(@"状态** 将要进入后台");
    
}
- (void)applicationDidEnterBackground:(UIApplication *)application{
    NSLog(@"状态** 已经进入后台");
}
- (void)applicationWillTerminate:(UIApplication *)application{
    NSLog(@"状态** 将要退出程序");

}
+ (AppDelegate* )shareAppDelegate {
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}



@end
