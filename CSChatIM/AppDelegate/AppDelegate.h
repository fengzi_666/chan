//
//  AppDelegate.h
//  IMChat
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import <UIKit/UIKit.h>
#import "suoyoudeNetworkDetection.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//判断网络状态
@property (nonatomic, strong) suoyoudeNetworkDetection *suoyoudeNetworkDetection;
+ (AppDelegate* )shareAppDelegate;
@end

