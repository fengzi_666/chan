//
//  ColorMacro.h
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#ifndef ColorMacro_h
#define ColorMacro_h



//custom
#define TXThemeColor_Cyan [UIColor colorWithRed:0 green:0.82 blue:1 alpha:1]//[UIColor colorWithRed:0.19 green:0.58 blue:0.96 alpha:1]  //主题色之青色  , Hex Color = #00D1FF
#define TXThemeColor_Gray [UIColor colorWithRed:0.52 green:0.52 blue:0.52 alpha:1]  //主题色之灰色    ,Hex Color = #848484
#define TXThemeColor_Blue [UIColor colorWithRed:0.2 green:0.59 blue:0.98 alpha:1]   //主题色之蓝色   ,Hex Color = #3396FC
#define TXThemeColor_Line [UIColor colorWithRed:0.26 green:0.32 blue:0.45 alpha:1]   //主题色之线   ,Hex Color = #435273
#define TXThemeColor_Black [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.9]  //主题色之黑色

#define TXThemeColor_White [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1]  //主题色之白色

#define TXThemeColor_DefaultImageName_bg [UIColor colorWithRed:231/255.0 green:230/255.0 blue:230/255.0 alpha:1]  //主题色之黑色

#define TXChangedSystemWhiteColor [UIColor blackColor]

#define TXLineColor [UIColor colorWithRed:207 / 255.0 green:210 /255.0 blue:213 / 255.0 alpha:0.7]

#endif /* ColorMacro_h */
