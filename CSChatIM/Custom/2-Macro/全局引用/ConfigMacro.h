//
//  ConfigMacro.h
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#ifndef ConfigMacro_h
#define ConfigMacro_h
#define HX_IS_IPHONEX (CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(375, 812)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(812, 375)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(414, 896)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(896, 414)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(360, 780)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(780, 360)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(390, 844)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(844, 390)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(428, 926)) || CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(926, 428)))
#define iOS10 ([[[UIDevice currentDevice] systemVersion] compare:@"10.0" options:NSNumericSearch] != NSOrderedAscending)
#define iOS9 ([[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] != NSOrderedAscending)
#define iOS11 ([[[UIDevice currentDevice] systemVersion] compare:@"11.0" options:NSNumericSearch] != NSOrderedAscending)
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define KScreenHeight [[UIScreen mainScreen] bounds].size.height
#define KScreenWidth  [[UIScreen mainScreen] bounds].size.width

// iPhone X
#define  TXiPhoneX (KScreenWidth == 375.f && KScreenHeight == 812.f ? YES : NO)

// Status bar height.
#define  StatusBarHeight      (IS_PhoneXAll ? 44.f : 20.f)

// 根据屏幕分辨率判断设备，是则返回YES，不是返回NO
#define isiPhone5or5sor5c ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define isiPhone6or6s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define isiPhone6plusor6splus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
#define isiPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125 , 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define isPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
// 判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
// 判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
// 判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
// 判断iPhone11
#define IS_IPHONE_11 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

#define IS_IPHONE_11_Pro ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

#define IS_IPHONE_11_Pro_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPHone12 mini
#define HX_Is_iPhoneTwelveMini ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1080, 2340), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

//判断iPHone12 和 iPHone12 Pro
#define HX_Is_iPhoneTwelvePro ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1170, 2532), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)

//判断iPHone12 ProMax
#define HX_Is_iPhoneTwelveProMax ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1284, 2778), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneX所有系列
#define IS_PhoneXAll (iPhoneX || IS_IPHONE_Xr || IS_IPHONE_Xs_Max || IS_IPHONE_Xs || IS_IPHONE_11 || IS_IPHONE_11_Pro || IS_IPHONE_11_Pro_Max||HX_Is_iPhoneTwelveMini||HX_Is_iPhoneTwelvePro||HX_Is_iPhoneTwelvePro||HX_Is_iPhoneTwelveProMax)
//边框截角
#define CORNERADIUS(sender,a)                                                    \
sender.layer.cornerRadius = a;                                                 \
sender.layer.masksToBounds = YES;                                              \
sender.layer.borderColor = [UIColor grayColor].CGColor;                        \
sender.layer.borderWidth = 0.3;

#define CORNERADIUS_white(sender,a)                                                    \
sender.layer.cornerRadius = a;                                                 \
sender.layer.masksToBounds = YES;                                              \
sender.layer.borderColor = [UIColor whiteColor].CGColor;                        \
sender.layer.borderWidth = 1;  \

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define Version [[[NSBundle mainBundle] infoDictionary] valueForKey:(NSString *)@"CFBundleShortVersionString"] //版本号
#define BANGROUPLIVE YES

#pragma mark - UIColor宏定义
#define UIColorFromRGBA(rgbValue, alphaValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:alphaValue]

#define UIColorFromRGB(rgbValue) UIColorFromRGBA(rgbValue, 1.0)
#define UICommonTableBkgColor UIColorFromRGB(0xe4e7ec)
#define UIColorFromRGB(rgbValue) UIColorFromRGBA(rgbValue, 1.0)
//工具方法
#import <JSONModel.h>                       //数据模型类
#import "Masonry.h"                         //约束
#import <SVProgressHUD/SVProgressHUD.h>     //提示性信息类
#import "MJRefresh.h"                       //上拉下拉刷新类
//#import <AliyunOSSiOS/OSSService.h>         //阿里云存储
#import "AFNetworking.h"                    //网络请求
#import "MBProgressHUD.h"
#import "Masonry.h"                         //约束
#import "URLMacro.h"

#import "NSString+LFAdd.h"
#import "UIView+Extension.h"
#import "FontMacro.h"
#import "jiaohuanqiValidate.h"
#import "UIButton+YasinTimerCategory.h"
#import "longwangRequestManage.h"
#import "UserDefaultManager.h"
#import "UIColor+LFHex.h"
#import "LFUserManager.h"
//#import "FFUIFactory.h"
#import "LFFileMgr.h"
#import "UIViewController+Swizzled.h"
#import "AppDelegate.h"
#import "UIScrollView+EmptyDataSet.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <MJExtension/MJExtension.h>
#import  <SDWebImage/SDWebImage.h>
#import "UITableView+WFEmpty.h"
#import <UINavigationController+FDFullscreenPopGesture.h>
#import "zhengtaiHttpRequest.h"
//附件引用类

// 是否为iOS7
#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)



#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)
//非空判断
static inline BOOL IsEmpty(id thing) {
    return thing == nil || [thing isEqual:[NSNull null]]
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

/**
 *    永久存储对象
 *
 *    @param    object    需存储的对象
 *    @param    key    对应的key
 */
#define DEF_PERSISTENT_SET_OBJECT(object, key)                                                                                                 \
({                                                                                                                                             \
NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];                                                                         \
[defaults setObject:object forKey:key];                                                                                                    \
[defaults synchronize];                                                                                                                    \
})

/**
 *    取出永久存储的对象
 *
 *    @param    key    所需对象对应的key
 *    @return    key所对应的对象
 */
#define DEF_PERSISTENT_GET_OBJECT(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]


/**
 *  移除永久存储的对象
 */
#define DEF_PERSISTENT_REMOVE_OBJECT(key) [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];[[NSUserDefaults standardUserDefaults] synchronize];


/******************** NSLog打印格式 ********************/
/**
 * 1: 修改默认的NSLog打印格式
 * 0: 去掉NSLog打印
 **/
#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr, "[%s:%d行] %s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__]UTF8String]);

#else
#define NSLog(FORMAT, ...) nil
#endif



#define NSLog(xx, ...)  NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
/******************************************************/

/***************空值校验*****************************/
#define VerifyValue(value)\
({id tmp;\
if ([value isKindOfClass:[NSNull class]]) {\
tmp = @"";\
}else{\
tmp = value;\
}\
tmp;\
})

/******************************************************/

/**
 检查向服务器发送的参数值是否为空
 **/
#define checkNil(y) y?y:@""


#endif /* ConfigMacro_h */
