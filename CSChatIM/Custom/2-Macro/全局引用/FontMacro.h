//
//  FontMacro.h
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#ifndef FontMacro_h
#define FontMacro_h
/**
 *  字号
 */

//cell text
static const CGFloat LFFontSize_SingleLine = 14.5;          /**< 单行cell的字体 */
static const CGFloat LFFontSize_SeveralLine = 12;           /**< 多行cell的字体 */

//按钮字体
static const CGFloat LFFontSize_SmallBtnTitle = 13;         /**< 小按钮的字体 */
static const CGFloat LFFontSize_MediumBtnTitle = 15;      /**< 中等按钮的字体 */
static const CGFloat LFFontSize_LargeBtnTitle = 18;         /**< 大按钮的字体 */

//单独label
static const CGFloat LFFontSize_Name = 15;                  /**< 单行cell的字体 */
static const CGFloat LFFontSize_Date = 12;                  /**< 单行cell的字体 */

static const CGFloat LFFontSize_Nav_Right = 13.5;             /**< 导航右按钮字体 */


#define LFFont_NavBarItem [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:15] //导航条按钮字体




static const NSInteger TXWordsNum_Name = 12;              //名称字数
static const NSInteger TXWordsNum_Phone = 11;             //手机号字数
static const NSInteger TXWordsNum_LongName = 20;          //长名称名称字数
static const NSInteger TXWordsNum_IDCard = 18;            //身份证号字数
static const NSInteger TXWordsNum_Password = 12;          //密码字数
static const NSInteger TXWordsNum_PayPassword = 6;        //支付密码字数
static const NSInteger TXWordsNum_Code = 6;               //验证码字数

static const NSInteger TXWordsNum_Sex = 1;

static NSString * const TXLength_Mobile =  @"[1][3456789][0-9]{9}";//效验手机号



#endif /* FontMacro_h */
