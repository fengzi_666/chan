//
//  URLMacro.h
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#ifndef URLMacro_h
#define URLMacro_h

//外网测试
static NSString *const CSURL_BaseUrl = @"https://www.ahjcnet.com";


#pragma mark --注册发送验证码
static NSString * const CSURL_sendRegisterSMS = @"/appregister/sendRegisterSMS";
#pragma mark --用户注册
static NSString * const CSURL_register = @"/appregister/register";
#pragma mark --用户登录
static NSString * const CSURL_login = @"/applogin/login";

#pragma mark --验证码登录
static NSString * const CSURL_loginYzm = @"/applogin/loginYzm";
#pragma mark --用户头像上传
static NSString * const CSURL_updatetouxiang = @"/appuser/updatetouxiang";

#pragma mark --用户修改背景图
static NSString * const CSURL_updateDongTaiBeiJing = @"/appuser/updateDongTaiBeiJing";
#pragma mark --用户昵称修改
static NSString * const CSURL_updateMessage = @"/appuser/updateMessage";

#pragma mark -- 获取usersig
static NSString * const CSURL_getUserSig = @"/appuser/getUserSig";

#pragma mark --用户昵称修改
static NSString * const CSURL_gappbangzhufankui = @"/appbangzhufankui/list";

#pragma mark --找回密码发送验证码
static NSString * const CSURL_zhaoHuiSendSMS = @"/appuserpass/zhaoHuiSendSMS";
#pragma mark --找回密码
static NSString * const CSURL_zhaoHuiPass = @"/appuserpass/zhaoHuiPass";
#pragma mark --添加好友申请
static NSString * const CSURL_tianjia = @"/apphaoyoushengqing/tianjia";

#pragma mark --查询添加好友列表
static NSString * const CSURL_tianjialist = @"/apphaoyou/list";

#pragma mark --查询好友详情
static NSString * const CSURL_apphaoyou = @"/apphaoyou/get";

#pragma mark --查询好友详情
static NSString * const CSURL_apphaoyoushengqing = @"/apphaoyoushengqing/chuli";

#pragma mark --修改好友备注
static NSString * const CSURL_updatebeizhu = @"/apphaoyou/updatebeizhu";

#pragma mark --删除好友
static NSString * const CSURL_apphaoyoudel = @"/apphaoyou/del";

#pragma mark --创建群聊
static NSString * const CSURL_qunchuangjian = @"/appqun/chuangjian";

#pragma mark --通过群账号查找群
static NSString * const CSURL_qunlist = @"/appqun/qunlist";
#pragma mark --上传群头像
static NSString * const CSURL_groutouxiang = @"/appqun/updatePic";
#pragma mark --修改群昵称
static NSString * const CSURL_qunupdateMessage = @"/appqun/updateMessage";
#pragma mark --修改我在群里的昵称
static NSString * const CSURL_updateQunMingPian = @"/appqunchengyuan/updateQunMingPian";
#pragma mark --查询时候加入群聊
static NSString * const CSURL_appqunjianCe = @"/appqun/jianCe";
#pragma mark --申请加入群聊
static NSString * const CSURL_qunshenqing = @"/appqunshenqing/tijiao";
#pragma mark --同意加入群聊
static NSString * const CSURL_qunshenqingchuli = @"/appqunshenqing/chuli";
#pragma mark --查询群成员列表
static NSString * const CSURL_appqunchengyuanlist = @"/appqunchengyuan/list";
#pragma mark --删除群成员
static NSString * const CSURL_quntiren = @"/appqun/quntiren";
#pragma mark --添加群成员
static NSString * const CSURL_addqunchengyuan = @"/appqunchengyuan/tianjia";
#pragma mark --解散群
static NSString * const CSURL_addqunjieSanQun = @"/appqun/jieSanQun";
#pragma mark --退群
static NSString * const CSURL_addqunjietuiqun = @"/appqun/tuiqun";
#pragma mark --设置/取消管理员
static NSString * const CSURL_updateshenfen = @"/appqun/updateshenfen";
#pragma mark --转让群主
static NSString * const CSURL_zhuanrang = @"/appqun/zhuanrang";
#pragma mark --好友中未加群聊列表
static NSString * const CSURLhaoyounotqun = @"/appqunchengyuan/haoyounotqun";
#pragma mark --设置全员禁言/解除禁言
static NSString * const CSURLappqunjinyan = @"/appqun/jinyan";

#pragma mark --禁言单个用户
static NSString * const CSURLonlyjinyan = @"/appqun/onlyjinyan";

#pragma mark --更改禁止互相添加好友状态
static NSString * const CSURL_updateTianJiaState = @"/appqun/updateTianJiaState";
#pragma mark --查看群状态
static NSString * const CSURL_updategetState = @"/appqun/getState";
#pragma mark --查看退群人员
static NSString * const CSURL_qunchengyuandellist = @"/appqunchengyuan/dellist";


#pragma mark --查看交易密码状态
static NSString * const CSURL_jiaoyistate = @"/appuser/jiaoyistate";
#pragma mark --设置交易密码发送验证码
static NSString * const CSURL_sheZhiJiaoPassSendSMS = @"/appjiaoyipass/sheZhiJiaoPassSendSMS";

#pragma mark --设置交易密码验证码校验
static NSString * const CSURL_jiaoYiPassJiaoYan = @"/appjiaoyipass/jiaoYiPassJiaoYan";

#pragma mark --交易密码设置
static NSString * const CSURL_updateJiaoYiPass = @"/appjiaoyipass/updateJiaoYiPass";

#pragma mark --发红包
static NSString * const CSURL_fahongbao = @"/apphongbao/fahongbao";

#pragma mark --红包领取状态
static NSString * const CSURL_hongBaoState = @"/apphongbao/hongBaoState";
#pragma mark --领取红包
static NSString * const CSURL_hongBaolingqu = @"/apphongbao/lingqu";
#pragma mark --领取群红包
static NSString * const CSURL_lingquzhuanshu = @"/apphongbao/lingquQun";
#pragma mark --领取记录集合
static NSString * const CSURL_hongBaolingqulist = @"/apphongbao/lingqulist";
#pragma mark --异地登录验证码
static NSString * const CSURL_sendYDYZMSMS = @"/appjiaoyipass/sendYDYZMSMS";

#pragma mark --发送登录验证码
static NSString * const CSURL_sendLoginSMS = @"/applogin/sendLoginSMS";
#pragma mark --用户收入明细列表
static NSString * const CSURL_appshourumingxi = @"/appshourumingxi/list";
#pragma mark --红包记录集合
static NSString * const CSURL_apphongbaojiLu = @"/apphongbaojiLu/list";

#pragma mark --查看群二维码
static NSString * const CSURL_qunerweima = @"/appqun/qunerweima";

#pragma mark --配置信息
static NSString * const CSURL_apppeizhi = @"/apppeizhi/info";

#pragma mark --版本升级
static NSString * const CSURL_appversion = @"/appversion/list";
#pragma mark --查看群成员邀请人信息
static NSString * const CSURL_qunYaoQingInFo = @"/appqunchengyuan/qunYaoQingInFo";
#pragma mark --通过账号查询好友列表
static NSString * const CSURL_haoyoulist = @"/apphaoyou/haoyoulist";

#pragma mark --用户个人资料
static NSString * const CSURL_appuserinfo = @"/appuser/info";
#pragma mark --商城
static NSString * const CSURL_shangcheng = @"/shop/index.html#/?";

#pragma mark --群成员禁言状态
static NSString * const CSURL_jinYanState = @"/apphongbao/jinYanState";
#pragma mark --修改群成员红包领取状态
static NSString * const CSURL_updateHongBaoState = @"/appqunchengyuan/updateHongBaoState";
#pragma mark --上传支付宝二维码
static NSString * const CSURL_uploadZhiFuBao = @"/appuser/uploadZhiFuBao";
#pragma mark --绑定支付宝
static NSString * const CSURL_BindupdateZhiFuBao = @"/appuser/updateZhiFuBao";
#pragma mark --用户提现
static NSString * const CSURL_shenQing = @"/apptixian/shenQing";
#pragma mark --用户绑定银行卡
static NSString * const CSURL_updateYinHang = @"/appuser/updateYinHang";
#pragma mark --系统消息列表
static NSString * const CSURL_appxitongxiaoxi = @"/appxitongxiaoxi/list";
#pragma mark --转账
static NSString * const CSURL_appzhuanzhang = @"/appzhuanzhang/zhuanzhang";

#pragma mark --提现计算手续费
static NSString * const CSURL_appshouxvfe = @"/apptixian/shouxvfei";

#pragma mark --好友申请列表
static NSString * const CSURL_apphaoyoushengqinglist = @"/apphaoyoushengqing/list";

#pragma mark --群聊申请列表
static NSString * const CSURL_appshengqinglist = @"/appqunshenqing/shengqinglist";

#pragma mark --普通用户邀请群成员
static NSString * const CSURL_yaoQingQunChengYuan = @"/appqunchengyuan/yaoQingQunChengYuan";


#pragma mark --动态列表
static NSString * const CSURL_appdongtai = @"appdongtai/list";

#pragma mark --他人动态列表
static NSString * const CSURL_apptaList = @"/appdongtai/taList";
#pragma mark --发布动态
static NSString * const CSURL_saveDongTai = @"appdongtai/saveDongTai";


#pragma mark --群聊发送专属红包
static NSString * const CSURL_fazhuanshuHongBao = @"/apphongbao/fazhuanshuHongBao";
#pragma mark --发送群红包
static NSString * const CSURL_fahongbaoQun = @"/apphongbao/fahongbaoQun";

#pragma mark --修改群邀请状态
static NSString * const CSURL_updateYaoQingState = @"/appqun/updateYaoQingState";

#pragma mark --群魅力值分布
static NSString * const CSURL_qunmeilizhi = @"/appqun/qunmeilizhi";
#pragma mark --发送个人红包
static NSString * const CSURL_faGeRenHongBao = @"/apphongbao/faGeRenHongBao";
#pragma mark --领取个人红包
static NSString * const CSURL_lingQuGeRen = @"/apphongbao/lingQuGeRen";

#pragma mark --飞鱼微信网页支付
static NSString * const CSURL_chongZhi = @"/appchongzhi/chongZhi";
#pragma mark --充值参数
static NSString * const CSURL_canshu = @"/appchongzhi/canshu";


#pragma mark --飞鱼支付银行编码
static NSString * const CSURL_appyinhanglist = @"/appyinhang/list";

#pragma mark --飞鱼支付城市编码
static NSString * const CSURL_apphuifucityaddress = @"/apphuifucity/address";

#pragma mark --飞鱼绑定银行卡
static NSString * const CSURL_bangDingYinHang = @"/appyinhangka/bangDingYinHang";

#pragma mark --飞鱼银行卡详情
static NSString * const CSURL_appyinhangkaget = @"/appyinhangka/get";
#pragma mark --飞鱼银行卡删除
static NSString * const CSURL_appyinhangkadel = @"/appyinhangka/del";

#pragma mark --实名认证获取token
static NSString * const CSURL_appshiminggettoken = @"/appshiming/gettoken";
#pragma mark --艾聊实名认证
static NSString * const CSURL_updateShiMing = @"/appuser/updateShiMing";
#pragma mark --艾聊银行卡列表
static NSString * const CSURL_appyinhangkalist = @"/appyinhangka/list";


#pragma mark --绑定银行卡验证码
static NSString * const CSURL_queRenBangDing = @"/appyinhangka/queRenBangDing";
#pragma mark --艾聊充值下单(第一步)
static NSString * const CSURL_chongzhione = @"/appchongzhi/chongzhione";

#pragma mark --艾聊充值下单(第二步)
static NSString * const CSURL_chongzhitwo = @"/appchongzhi/chongzhitwo";
#endif /* URLMacro_h */
