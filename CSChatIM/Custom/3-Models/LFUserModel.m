//
//  LFUserModel.m
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import "LFUserModel.h"

@implementation LFUserModel

- (id)init
{
    self = [super init];
    if (self) {
       
        self.phone = @"";
        self.userid = @"";
        self.usercode = @"";
        self.erweima = @"";
       
        self.userState = UserStateUnLogin;
        self.userType = UserTypeUnKnown;
        
    }
    return self;
}


//+(JSONKeyMapper *)keyMapper{
//    return [[JSONKeyMapper alloc]initWithDictionary:@{@"id":@"list_id"}];
//}

+ (BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}



- (id)initWithCoder:(NSCoder *)coder{
    self = [super init];
    if (self) {
       
        self.userState = [coder decodeIntegerForKey:@"userState"];
        self.userType = [coder decodeIntegerForKey:@"userType"];
        self.phone = [coder decodeObjectForKey:@"phone"];
        self.userid = [coder decodeObjectForKey:@"userid"];
        self.erweima = [coder decodeObjectForKey:@"erweima"];
        self.usercode = [coder decodeObjectForKey:@"usercode"];
        
    
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder{
    //

    
    [coder encodeObject:_phone forKey:@"phone"];
    [coder encodeInteger:_userid forKey:@"userid"];
    [coder encodeInteger:_erweima forKey:@"erweima"];
     [coder encodeObject:_usercode forKey:@"usercode"];
   
   
}

+(instancetype)modelWithDict:(NSDictionary *)dict
{
    
    LFUserModel *model = [[LFUserModel alloc] init];
    
    [model setValuesForKeysWithDictionary:dict];
    
    return model;
    
}

-(void)setValue:(id)value forKey:(NSString *)key
{
  
    
    if ([value isKindOfClass:[NSNumber class]]) {
        
        NSString *str = [value stringValue];
        
        [super setValue:str forKey:key];
        
        return;
    }
    
    [super setValue:value forKey:key];
}


-(void)setValue:(id)value forUndefinedKey:(NSString *)key{}



//大乐乐添加
- (void)updteInfoWithDic:(NSDictionary *)dic
{
    
     
    self.phone = [NSString hexStringFromString:[dic objectForKey:@"phone"]];
    self.userid = [NSString hexStringFromString:[dic objectForKey:@"userid"]];
    self.erweima = [NSString hexStringFromString:[dic objectForKey:@"erweima"]];
    self.usercode = [NSString hexStringFromString:[dic objectForKey:@"usercode"]];

}
//- (NSString *)userName {
//    if (_userName.length == 0) {
//        return _userPhone;
//    }
//    return _userName;
//}



@end
