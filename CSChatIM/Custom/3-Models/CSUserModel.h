//
//  CSUserModel.h
//  Kindergarten
//
//  Created by 梁亚飞 on 2020/2/21.
//  Copyright © 2020 梁亚飞. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSUserModel : NSObject

@property (nonatomic, copy) NSString *phone ;                  /**<  用户头像*/
@property (nonatomic, copy) NSString *userid ;
@property (nonatomic, copy) NSString *erweima ;
@property (nonatomic, copy) NSString *usercode ;

+(CSUserModel *)sharedInfo;

- (void)saveLastDisplayID:(NSString *)displayID;

- (NSString *)loadLastDisplayID;
//用于退出登陆

-(void)saveData;
//用于退出登陆
-(void)clearData;
@end

NS_ASSUME_NONNULL_END
