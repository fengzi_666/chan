//
//  CSUserModel.m
//  Kindergarten
//
//  Created by 梁亚飞 on 2020/2/21.
//  Copyright © 2020 梁亚飞. All rights reserved.
//

#import "CSUserModel.h"
#import "LFFileMgr.h"
#define LASTLOGINNAME_ARCHIVE   @"last_login_name.archive"
static CSUserModel *_mcUserInfo;
@implementation CSUserModel
+(CSUserModel *)sharedInfo
{
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken, ^{
        _mcUserInfo = [[CSUserModel alloc]init];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        _mcUserInfo.phone = [NSString hexStringFromString:[userDefaults objectForKey:@"fname"]];
        _mcUserInfo.userid = [NSString hexStringFromString:[userDefaults objectForKey:@"userid"]];
        _mcUserInfo.erweima = [NSString hexStringFromString:[userDefaults objectForKey:@"erweima"]];
        _mcUserInfo.usercode = [NSString hexStringFromString:[userDefaults objectForKey:@"usercode"]];
    
    });
    return _mcUserInfo;
}

-(void)saveData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.phone forKey:@"phone"];
    [userDefaults setObject:self.userid forKey:@"userid"];
    [userDefaults setObject:self.erweima forKey:@"erweima"];
    [userDefaults setObject:self.usercode forKey:@"usercode"];
    [userDefaults synchronize];
}

-(void)clearData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"phone"];
    [userDefaults removeObjectForKey:@"userid"];
    [userDefaults removeObjectForKey:@"erweima"];
    [userDefaults removeObjectForKey:@"usercode"];

    _mcUserInfo.phone = @"";
    _mcUserInfo.userid = @"";
    _mcUserInfo.erweima  = @"";
    _mcUserInfo.usercode     = @"";
    
}
//保存最后的登录名
- (void)saveLastDisplayID:(NSString *)displayID
{
    NSData * aData = [_userid dataUsingEncoding: NSUTF8StringEncoding];
    NSString * loginNameFile = [NSString stringWithFormat:@"%@/%@", [LFFileMgr getDocumentFile],LASTLOGINNAME_ARCHIVE];
    [LFFileMgr saveDataToFullPath:loginNameFile data:aData append:NO];
}
- (NSString *)loadLastDisplayID{
    NSString* loginNameFile = [NSString stringWithFormat:@"%@/%@", [LFFileMgr getDocumentFile],LASTLOGINNAME_ARCHIVE];
    
    if([LFFileMgr isFileExistAtPath:loginNameFile]){
        NSData* aData = [NSData dataWithContentsOfFile:loginNameFile];
        if(aData)
        {
            NSString * loginName = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
            return loginName;
        }else{
            return @"";
        }
    }else{
        return @"";
    }
}
@end
