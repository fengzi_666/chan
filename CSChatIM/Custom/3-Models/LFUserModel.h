//
//  LFUserModel.h
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  用户状态
 */
typedef NS_ENUM(NSInteger,UserState) {
    UserStateUnKnown       = 0,  //未知状态
    UserStateLogin         = 1,  //用户处于登录状态
    UserStateUnLogin       = 2,  //用户登录过，点击退出登录
};

/**
 *  用户种类
 */
typedef NS_ENUM(NSInteger,UserType) {
    UserTypeUnKnown        = 0,  //未知状态
    UserTypeCustomer       = 1,  //用户处于登录状态
    UserTypeServer         = 2,  //用户登录过，点击退出登录
};

@interface LFUserModel : NSObject

@property (nonatomic, assign) UserType   userType;                      /**< 种类 */
@property (nonatomic, assign) UserState  userState;                     /**< 状态 */
@property (nonatomic, copy) NSString *phone ;
@property (nonatomic, copy) NSString *userid ;
@property (nonatomic, copy) NSString *fsex ;
@property (nonatomic, copy) NSString *erweima ;
@property (nonatomic, copy) NSString *usercode ;



+(instancetype)modelWithDict:(NSDictionary *)dict;




- (void)updteInfoWithDic:(NSDictionary *)dic;

@end




















