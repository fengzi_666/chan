//
//  LFUserManager.h
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LFUserModel.h"

/**
 *  @brief 此类用于用户管理类
 */
@interface LFUserManager : NSObject

///当前用户信息，每次调用此类的
@property (nonatomic, retain) LFUserModel * curUserInfo;


/**
 *  @brief 单例管理
 */
+ (LFUserManager *)sharedInstance;


/**
 *  @brief 判断用户是否登录
 *  @return YES 已经登录状态  NO 未登录状态
 */
- (BOOL)isUserLogin;


/**
 *  @brief 判断是否第一次启动程序
 */
- (BOOL)isFirstStart;



/**
 Description 判断用户第一次登录
 */
-(BOOL)isFirstLogin;


/**
 *  @brief 保存用户信息，传入用户信息模型，调用此方法，将用户信息保存到单例的curUserInfo,并且用户信息保存到本地
 *         此方法在登录之后调用
 */
- (void)saveUserInfo:(LFUserModel *)userModel;


/**
 * @brief 从本地取出用户信息,将用户信息复制到单例的curUserInfo,此方法用于每次自动登录
 * @return 是否取出成功
 */
- (BOOL)loadUserInfo;


/**
 *  @brief 保存上一次登录名
 */
- (void)saveLastLoginName:(NSString *)loginName;


/**
 *  @brief 取出上一次登录名
 *  @return 返回最后一次登录名
 */
- (NSString *)loadLastLoginName;


/**
 *  @brief 恢复为注销状态
 */
- (void)restoreLogoutState;


/**
 *  @brief 跳转登录页
 */
- (void)jumpToLoginPage;


@end
