//
//  LFArchiveMgr.h
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  本类为用户对象本地序列化管理类
 */
@interface LFArchiveMgr : NSObject

/**
 *  @brief   数组做本地序列化
 *  @param   array  存放序列化对象的数组
 *  @param   archiveName   序列化名称
 */
+ (void)saveCache:(id)obj name:(NSString *)archiveName;

/**
 *  @brief   加载缓存
 *  @param   archiveName    缓存名称
 *  @return  返回数组对象
 */
+ (id)loadCache:(NSString *)archiveName;

/**
 *  @brief   判断是否存在缓存
 *  @param   archiveName   缓存名称
 *  @return  YES:存在  NO：不存在
 */
+ (BOOL)isCacheExit:(NSString *)archiveName;



@end
