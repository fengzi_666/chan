//
//  LFFileMgr.m
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import "LFFileMgr.h"

@implementation LFFileMgr

//获取沙盒Document目录
+ (NSString*)getDocumentFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

//获取libraray目录
+ (NSString*)getLibraryFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

//获取document目录下，用户的目录
+ (NSString*)getUserFile:(NSString*)userID
{
    NSString * userFile = [LFFileMgr getDocumentFile];
    userFile = [userFile stringByAppendingPathComponent:userID];
    //创建目录
    [[NSFileManager defaultManager]createDirectoryAtPath:userFile withIntermediateDirectories:YES attributes:nil error:NULL];
    return userFile;
}

//写入string, 先编码为data, 再写入
+(BOOL)saveStringToFullPath:(NSString *)fullPath string:(NSString *)string append:(BOOL)flag
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [LFFileMgr saveDataToFullPath:fullPath data:data append:flag];
}

//写入data
+(BOOL)saveDataToFullPath:(NSString *)fullPath data:(NSData *)data append:(BOOL)flag
{
    BOOL isSuccess = NO;
    
    if(!flag)
    {
        [LFFileMgr removeFile:fullPath];
        isSuccess = [data writeToFile:fullPath atomically:YES];
    }
    else
    {
        NSFileHandle * outFile;
        outFile = [NSFileHandle fileHandleForWritingAtPath:fullPath];
        if(outFile != nil)
        {
            [outFile seekToEndOfFile];
            [outFile writeData:data];
            [outFile closeFile];
            isSuccess = YES;
        }else
        {
            isSuccess = [data writeToFile:fullPath atomically:YES];
            
        }
    }
    return isSuccess;
}


//删除文件
+(BOOL)removeFile:(NSString *)fullPath
{
    BOOL isExist = FALSE;
    if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath])
    {
        isExist = [[NSFileManager defaultManager] removeItemAtPath:fullPath error:nil];
    }
    return  isExist;
}

//写入日志到文件
+ (void)writeLogToFullPath:(NSString *)fullPath string:(NSString *)string append:(BOOL)flag
{
    NSDate * date = [NSDate date];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss:SS"];
    
    NSString * tmpMsg = [NSString stringWithFormat:@"%@:%@\n", [dateFormatter stringFromDate:date],string];
    NSData * writer = [[NSData alloc] initWithData:[tmpMsg dataUsingEncoding:NSUTF8StringEncoding]];
    [LFFileMgr saveDataToFullPath:fullPath data:writer append:flag];
}

/**
 *  @brief  获得指定目录下，指定后缀名的文件列表
 *  @param  type    文件后缀名
 *  @param  dirPath     指定目录
 *  @return 文件名列表
 */
+(NSArray *)getFileArrayOfType:(NSString*)type fromFilePath:(NSString*)path
{
    NSMutableArray * fileArray = [NSMutableArray  array];
    //    NSLog(@"%@",path);
    NSArray * allFileArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    
    for (NSString * fileName in allFileArray) {
        NSString * fullpath = [path stringByAppendingPathComponent:fileName];
        if ([LFFileMgr isFileExistAtPath:fullpath]) {
            if ([[fileName pathExtension] isEqualToString:type]) {
                [fileArray  addObject:fileName];
            }
        }
    }
    return fileArray;
}

/**
 *  @brief  检查文件是否存在
 *  @param  type    文件全路径
 *  @return 是否存在
 */
+(BOOL)isFileExistAtPath:(NSString*)fileFullPath {
    BOOL isExist = NO;
    isExist = [[NSFileManager defaultManager] fileExistsAtPath:fileFullPath];
    return isExist;
}


@end
