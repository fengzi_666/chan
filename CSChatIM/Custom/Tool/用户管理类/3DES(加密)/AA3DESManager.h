//
//  AA3DESManager.h
//  AA3DESDemo
//
//  Created by Arlexovincy on 15/3/12.
//  Copyright (c) 2015年 Arlexovincy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AA3DESManager : NSObject

/**
*  3des加密
*
*  @param encryptString 待加密的string
*  @param keyString     约定的密钥
*  @param ivString      约定的密钥
*
*  @return 3des加密后的string
*/
+ (NSString*)getEncryptWithString:(NSString*)encryptString keyString:(NSString*)keyString ivString:(NSString*)ivString;

/**
 *  3des解密
 *
 *  @param decryptString 待解密的string
 *  @param keyString     约定的密钥
 *  @param ivString      约定的密钥
 *
 *  @return 3des解密后的string
 */
+ (NSString*)getDecryptWithString:(NSString*)decryptString keyString:(NSString*)keyString ivString:(NSString*)ivString;



/*
 

明文
 NSString *beMakeString = @"sxg123456";
 
 加密
 NSString *encrypptString = [AA3DESManager getEncryptWithString:beMakeString keyString:@"123456789012345678901234" ivString:@"12345678"];
 
 解密
 NSString *decryptString = [AA3DESManager getDecryptWithString:encrypptString keyString:@"123456789012345678901234" ivString:nil
 ];
 
 NSLog(@"3des加密:%@",encrypptString);
 NSLog(@"3des解密:%@",decryptString);

 
 
 
*/




@end
