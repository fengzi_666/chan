                                                                                                                                                                                                                     //
//  LFUserManager.m
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import "LFUserManager.h"
#import "LFArchiveMgr.h"
#import "LFFileMgr.h"
#import "NSString+LFAdd.h"
#import "CSLoginViewController.h"
#import "helloTabBarController.h"
#import "jiaohuanqiValidate.h"
#import "CSUserModel.h"


#define LASTLOGINNAME_ARCHIVE   @"last_login_name.archive"
#define USERINFO_ARCHIVE        @"user_info.archive"

@implementation LFUserManager

//单例
+ (LFUserManager*)sharedInstance
{
    static LFUserManager *__userMgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __userMgr = [[LFUserManager alloc] init];
    });
    return __userMgr;
}

- (id)init
{
    self = [super init];
    if (self) {
        LFUserModel *userModel = [[LFUserModel alloc] init];
        self.curUserInfo = userModel;
    }
    return self;
}

- (BOOL)isUserLogin{
    BOOL isLogin = NO;
    //判断用户是否第一次登陆
    if ([NSString isStringEffective:[NSString stringWithFormat:@"%@",self.curUserInfo.userid]] && self.curUserInfo.userState == UserStateLogin) {
        isLogin = YES;
    }
    return isLogin;
}

- (BOOL)isFirstStart{
    BOOL retVal = NO;
    //判断用户是否第一次启动
    if(![[NSUserDefaults standardUserDefaults] valueForKey:@"isFirstStart"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"isFirstStart"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //第一次启动
        retVal = YES;
    }
    return retVal;
}






/**
 *  @brief 保存用户信息，传入用户信息模型，调用此方法，将用户信息保存到单例的curUserInfo,并且用户信息保存到本地
 *         此方法在登录之后调用
 */
- (void)saveUserInfo:(LFUserModel *)userModel{
    self.curUserInfo = userModel;
    [LFArchiveMgr saveCache:userModel name:USERINFO_ARCHIVE];
}

- (void)hzy_saveUserInfo:(LFUserModel *)userModel{
    self.curUserInfo = userModel;
    [LFArchiveMgr saveCache:userModel name:USERINFO_ARCHIVE];
}

/**
 * @brief 从本地取出用户信息,将用户信息复制到单例的curUserInfo,此方法用于每次自动登录
 */
- (BOOL)loadUserInfo{
    
    BOOL retVal = NO;
    
    self.curUserInfo.phone = [CSUserModel sharedInfo].phone;
    
    if ([LFArchiveMgr isCacheExit:USERINFO_ARCHIVE]) {
        LFUserModel * userModel = [LFArchiveMgr loadCache:USERINFO_ARCHIVE];
        self.curUserInfo = userModel;
        retVal = YES;
    }
    return retVal;
}
//保存最后的登录名
- (void)saveLastLoginName:(NSString *)loginName
{
    NSData * aData = [loginName dataUsingEncoding: NSUTF8StringEncoding];
    NSString * loginNameFile = [NSString stringWithFormat:@"%@/%@", [LFFileMgr getDocumentFile],LASTLOGINNAME_ARCHIVE];
    [LFFileMgr saveDataToFullPath:loginNameFile data:aData append:NO];
}

- (NSString *)loadLastLoginName{
    NSString* loginNameFile = [NSString stringWithFormat:@"%@/%@", [LFFileMgr getDocumentFile],LASTLOGINNAME_ARCHIVE];
    
    if([LFFileMgr isFileExistAtPath:loginNameFile]){
        NSData* aData = [NSData dataWithContentsOfFile:loginNameFile];
        if(aData)
        {
            NSString * loginName = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
            return loginName;
        }else{
            return @"";
        }
    }else{
        return @"";
    }
}

//恢复为注销状态
- (void)restoreLogoutState
{
    LFUserModel * um = [[LFUserModel alloc]init];
   
    um.phone = @"";
    um.userid = @"";
    um.erweima = @"";
  
    um.usercode = @"";
    um.fsex = @"";
    
    um.userType =UserTypeUnKnown;
    um.userState =UserStateUnLogin;

   
    [[CSUserModel sharedInfo] clearData];
    [[LFUserManager sharedInstance]saveUserInfo:um];

}

//跳转登录页
- (void)jumpToLoginPage
{
    CSLoginViewController *vc = [CSLoginViewController new];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    UINavigationController * rootNav = (UINavigationController *)([UIApplication sharedApplication].delegate.window.rootViewController);
    helloTabBarController * mainTBC = (helloTabBarController *)(rootNav.viewControllers.firstObject);
    [mainTBC presentViewController:nav animated:YES completion:nil];

    
    
//    LFLoginViewController * lvc = [[LFLoginViewController alloc] init];
//    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:lvc];
//    UINavigationController * rootNav = (UINavigationController *)([UIApplication sharedApplication].delegate.window.rootViewController);
//    helloTabBarController * mainTBC = (helloTabBarController *)(rootNav.viewControllers.firstObject);
//    [mainTBC presentViewController:nav animated:YES completion:nil];
}

@end
