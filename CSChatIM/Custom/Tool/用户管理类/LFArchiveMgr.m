//
//  LFArchiveMgr.m
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import "LFArchiveMgr.h"
#import "LFUserManager.h"   
#import "LFFileMgr.h"
#import "NSString+LFAdd.h"
#import "LFUserModel.h"

@implementation LFArchiveMgr

//保存
+ (void)saveCache:(id)obj name:(NSString *)archiveName{
    
    NSData * countData = [NSKeyedArchiver archivedDataWithRootObject:obj];
    
    NSString *filePath = [self getFullPath:archiveName];
    
    [countData writeToFile:filePath atomically:YES];
}

//加载缓存
+ (id)loadCache:(NSString *)archiveName{
    
    NSString *filePath = [self getFullPath:archiveName];
    
    NSData *countData = [NSData dataWithContentsOfFile:filePath];
    
    id object = [NSKeyedUnarchiver unarchiveObjectWithData:countData];
    
    if (object) {
        return object;
    }else{
        if ([object isKindOfClass:[NSArray class]]) {
            return [[NSMutableArray alloc]init];
        }else{
            return object;
        }
    }
}

//判断是否有缓存
+ (BOOL)isCacheExit:(NSString *)archiveName{
    NSString *filePath = [self getFullPath:archiveName];
    if( [LFFileMgr isFileExistAtPath:filePath]){
        return YES;
    }else{
        return NO;
    }
}

//获取路径
+ (NSString *)getFullPath:(NSString *)archiveName
{
    NSString *filePath = @"";
    
    //    if ([archiveName isEqualToString:SEARCHARCHIVE] || [archiveName isEqualToString:CARTARCHIVE] || [archiveName isEqualToString:AREAARCHIVE] || [archiveName isEqualToString:AREAMD5STRING]) {
    //
    //        filePath = [GSFileMgr getUserFile:@"LocalUser"];
    //    }else{
    //        if ([NSString isStringEffective:[UserManager sharedInstance].curUserInfo.loginName]) {
    //
    //             filePath = [GSFileMgr getUserFile:[UserManager sharedInstance].curUserInfo.loginName];
    //        }
    //    }
    
    if ([NSString isStringEffective:[LFUserManager sharedInstance].curUserInfo.phone]) {
        filePath = [LFFileMgr getUserFile:[LFUserManager sharedInstance].curUserInfo.phone];
    }else{
        filePath = [LFFileMgr getUserFile:@"LocalUser"];
    }
    
    filePath = [filePath stringByAppendingPathComponent:archiveName];
    
    return filePath;
}


@end
