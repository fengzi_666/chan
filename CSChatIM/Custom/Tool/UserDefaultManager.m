
//
//  UserDefaultManager.m
//  FFLivingFitness
//
//  Created by Ninesday on 2017/8/25.
//  Copyright © 2017年 com.m.fearless-fitness.www. All rights reserved.
//

#import "UserDefaultManager.h"
#import "AA3DESManager.h"
#import <arpa/inet.h>
static UserDefaultManager *_manager;

@implementation UserDefaultManager

+(id)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[self alloc] init];
    });
    return _manager;
}
#pragma mark --保存腾讯code
- (void)SavaLoginLoginTengxunCode:(NSString *)loginLoginTengxunCode{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"mobile"];
    
    [defaults setValue:loginLoginTengxunCode forKey:@"mobile"];
    [defaults synchronize];
}

- (NSString *) GetLoginLoginTengxunCode{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"mobile"];
}
#pragma mark --保存用户id
- (void)SavaLoginLoginUserid:(NSString *)loginLoginUserid
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"userid"];
    
    [defaults setValue:loginLoginUserid forKey:@"userid"];
    [defaults synchronize];
}

- (NSString *) GetLoginLoginUserid
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"userid"];
}
#pragma mark --保存登录手机号
- (void)SavaLoginLoginMobile:(NSString *)loginLoginMobile{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"mobile"];
    
    [defaults setValue:loginLoginMobile forKey:@"mobile"];
    [defaults synchronize];
}

- (NSString *) GetLoginLoginMobile{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"mobile"];
}
#pragma mark --保存登录用户名
- (void)SavaLoginLoginAccount:(NSString *)loginLoginAccount{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"LoginAccount"];
    
    [defaults setValue:loginLoginAccount forKey:@"LoginAccount"];
    [defaults synchronize];
}

- (NSString *) GetLoginLoginAccount{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"LoginAccount"];
}
- (void)SavaLoginToken:(NSString *)devicetokenString
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"loginToken"];
    
    [defaults setValue:devicetokenString forKey:@"loginToken"];
    [defaults synchronize];
}
- (NSString *) GetLoginToken{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"loginToken"];

}


-(void) SaveDeviceToken:(NSString *)devicetokenString {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"DeviceTokenDtring"];
    
    [defaults setValue:devicetokenString forKey:@"DeviceTokenDtring"];
    [defaults synchronize];
}


- (NSString *) GetdevicetokenString {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"DeviceTokenDtring"];

}


//3Des 加密
-(NSString *) Make3DEString:(NSString *)passwordString {
    NSString *encrypptString = [AA3DESManager getEncryptWithString:passwordString keyString:@"123456789123456789123456" ivString:@"12345678"];
    return encrypptString;
}

-(NSString *) Get3DESString:(NSString *)DesString {
    NSString *getDesString = [AA3DESManager getDecryptWithString:DesString keyString:@"123456789123456789123456" ivString:@"12345678"];
    return getDesString;
}



-(void) SaveUserDefaulyPhonenumber:(NSString *) phoneString{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"Login_Phone"];
    
    [defaults setValue:phoneString forKey:@"Login_Phone"];
    [defaults synchronize];
    NSLog(@"%@",[defaults objectForKey:@"Login_Phone"]);

}

-(NSString *) GetUserDefaultPhoneNumber {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    return  [defaults objectForKey:@"Login_Phone"];
}


-(void) SaveCodeInfo:(id) UserObject {
    NSString *docPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
     NSString *path=[docPath stringByAppendingPathComponent:@"person.yangyang"];
    [NSKeyedArchiver archiveRootObject:UserObject toFile:path];
}


-(instancetype) GetSaveCodeIn {
     NSString *docPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
     NSString *path=[docPath stringByAppendingPathComponent:@"person.yangyang"];
     return  [NSKeyedUnarchiver unarchiveObjectWithFile:path];
}


-(void)SavePageNumber:(NSInteger)page {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"last_page"];
    [defaults setInteger:page forKey:@"last_page"];
    [defaults synchronize];
     NSLog(@"%@",[defaults objectForKey:@"last_page"]);
}

-(NSInteger)GetPageNumber {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults] ;
    NSInteger page = [defaults integerForKey:@"last_page"];
    return  page;
}



//保存数组方法

-(NSString *)bundlePath:(NSString *)fileName {
    return [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:fileName];
}

-(NSString *)documentsPath:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}

-(NSString *)documentsPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}



-(NSString * )getUDidString:(NSString *)string {
    if (string.length <32) {
        return nil;
    }else {
        NSMutableString * lastStr = nil;
        NSInteger a1 = [string characterAtIndex:7];
        NSInteger a2 = [string characterAtIndex:15];
        NSInteger a3 = [string characterAtIndex:19];
        NSInteger a4 = [string characterAtIndex:27];
        NSInteger atemp = (a1+a2+a3+a4)%7;
        
        NSInteger b1 = [string characterAtIndex:3];
        NSInteger b2 = [string characterAtIndex:11];
        NSInteger b3 = [string characterAtIndex:19];
        NSInteger btemp = (b1+b2+b3)%8;
        
        NSInteger c1 = [string characterAtIndex:8];
        NSInteger c2 = [string characterAtIndex:16];
        NSInteger c3 = [string characterAtIndex:31];
        NSInteger ctemp = (c1+c2+c3)%5;
        
        NSInteger d1 = [string characterAtIndex:0];
        NSInteger d2 = [string characterAtIndex:4];
        NSInteger d3 = [string characterAtIndex:8];
        NSInteger d4 = [string characterAtIndex:18];
        NSInteger d5 = [string characterAtIndex:30];
        NSInteger dtemp = (d1+d2+d3+d4+d5)%9;
        
        lastStr = (NSMutableString *)[NSString stringWithFormat:@"%ld%ld%ld%ld",atemp,btemp,ctemp,dtemp];
        return lastStr;
    }
}

-(NSString *)convertToJsonData:(NSDictionary *)dict

{
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
}
+ (NSString *)deviceWANIPAddress{
    
    NSURL *ipURL = [NSURL URLWithString:@"http://ip.taobao.com/service/getIpInfo.php?ip=myip"];
    NSData *data = [NSData dataWithContentsOfURL:ipURL];
    NSDictionary *ipDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    NSString *ipStr = nil;
    if (ipDic && [ipDic[@"code"] integerValue] == 0) { //获取成功
        ipStr = ipDic[@"data"][@"ip"];
    }
    return (ipStr ? ipStr : @"");
    
    return nil;
}



@end







