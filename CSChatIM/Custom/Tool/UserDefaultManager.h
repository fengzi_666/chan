//
//  UserDefaultManager.h
//  FFLivingFitness
//
//  Created by Ninesday on 2017/8/25.
//  Copyright © 2017年 com.m.fearless-fitness.www. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AA3DESManager.h"



@interface UserDefaultManager : NSObject


+(instancetype) shareInstance;
#pragma mark --保存腾讯code
- (void)SavaLoginLoginTengxunCode:(NSString *)loginLoginTengxunCode;

- (NSString *) GetLoginLoginTengxunCode;
#pragma mark --保存登录手机号
- (void)SavaLoginLoginMobile:(NSString *)loginLoginMobile;

- (NSString *) GetLoginLoginMobile;
#pragma mark --保存用户id
- (void)SavaLoginLoginUserid:(NSString *)loginLoginUserid;

- (NSString *) GetLoginLoginUserid;

#pragma mark --保存登录用户名
- (void)SavaLoginLoginAccount:(NSString *)loginLoginAccount;

- (NSString *) GetLoginLoginAccount;

#pragma mark --保存token
- (void)SavaLoginToken:(NSString *)devicetokenString;

- (NSString *) GetLoginToken;

-(void) SaveUserDefaulyPhonenumber:(NSString *) phoneString;


-(NSString *) GetUserDefaultPhoneNumber;


//3DES加密
-(NSString *) Make3DEString:(NSString *)passwordString;

//3DES 解密
-(NSString *) Get3DESString:(NSString *)DesString;


-(void) SaveCodeInfo:(id) UserObject ;


-(instancetype) GetSaveCodeIn;


-(void)SavePageNumber:(NSInteger)page;

-(NSInteger )GetPageNumber;

//处理字符串redpacket

-(NSString * )getUDidString:(NSString *)string;

//转--json字符串
-(NSString *)convertToJsonData:(NSDictionary *)dict;

+ (NSString*)deviceWANIPAddress;

@end
