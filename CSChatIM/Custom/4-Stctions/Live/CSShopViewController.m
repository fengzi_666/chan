//
//  TXShopViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/3/3.
//

#import "CSShopViewController.h"
#import <WebKit/WebKit.h>
#import "SFHFKeychainUtils.h"
@interface CSShopViewController ()<WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>
@property (nonatomic ,strong) WKWebView *webView;
@property (nonatomic ,strong) WKWebViewConfiguration *config;
@end

@implementation CSShopViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
}
// 控制器的view完全显示的时候调用
- (void)viewDidAppear:(BOOL)animated
 {
     [super viewDidAppear:animated];
     NSLog(@"控制器的view完全显示");
     self.navigationController.navigationBar.hidden = YES;
     self.tabBarController.tabBar.hidden = YES;
 }
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self LoadSetNavUI];
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.selectionGranularity = WKSelectionGranularityDynamic;
    config.allowsInlineMediaPlayback = YES;
    WKPreferences *preferences = [WKPreferences new];
    [config.userContentController addScriptMessageHandler:self name:@"backPage"];
    //是否支持JavaScript
    preferences.javaScriptEnabled = YES;
    //不通过用户交互，是否可以打开窗口
    preferences.javaScriptCanOpenWindowsAutomatically = YES;
    config.preferences = preferences;
    // 创建UserContentController（提供JavaScript向webView发送消息的方法）
    CGFloat h = StatusBarHeight;
    if (IS_PhoneXAll) {
        h =StatusBarHeight;
    }
    WKWebView *webview = [[WKWebView alloc] initWithFrame:CGRectMake(0, h, KScreenWidth, KScreenHeight-h) configuration:config];
    
    [self.view addSubview:webview];
    
    /* 加载服务器url的方法*/
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    NSString *url = [NSString stringWithFormat:@"%@%@PhoneCode=%@&token=%@",CSURL_BaseUrl,CSURL_shangcheng,[self UUID],token];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    [webview loadRequest:request];
    webview.navigationDelegate = self;
    
    webview.UIDelegate = self;
}
- (NSString*)UUID {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *userName = infoDict[@"CFBundleName"];
    NSString *serviceName = infoDict[@"CFBundleIdentifier"];
    NSString *UUID = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:serviceName error:nil];
    if (UUID.length==0) {
        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SFHFKeychainUtils storeUsername:userName andPassword:UUID forServiceName:serviceName updateExisting:1 error:nil];
        
    }
    return UUID;
    
}
- (void)LoadSetNavUI
{
//    self.view.backgroundColor = [UIColor  whiteColor];
//    UINavigationBar *navigationBar = self.navigationController.navigationBar;
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
//    [navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//    //此处使底部线条失效
//    [navigationBar setShadowImage:[UIImage new]];
    
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    if ([message.name isEqualToString:@"backPage"]) {
//        self.navigationController.navigationBar.hidden = NO;
//        self.tabBarController.tabBar.hidden = NO;
//        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.navigationController popToRootViewControllerAnimated:YES];
       
    }
}

-(void)dealloc{
    [_config.userContentController removeScriptMessageHandlerForName:@"backPage"];
}
        /*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
