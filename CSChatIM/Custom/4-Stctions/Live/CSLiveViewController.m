//
//  TXLiveViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/4/7.
//

#import "CSLiveViewController.h"
#import "CSLiveListTableViewCell.h"
#import "CSShopViewController.h"
#import "BaseshenghuoViewController.h"
@interface CSLiveViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) NSArray *imageArr;

@end
static NSString *cellid = @"cellid";
@implementation CSLiveViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
  
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self LoadNav];
    [self LoadTableView];
    _titleArr = @[@"商城",@"搜索",@"新闻",@"游戏",@"旅游"];
    _imageArr = @[@"发现_商城",@"发现_搜索",@"发现_新闻",@"发现_游戏",@"发现_旅游"];
    
    
}
-(void)LoadNav
{
   
    UIButton *title = [UIButton buttonWithType:UIButtonTypeCustom];
    [title setTitle:@"发现" forState:UIControlStateNormal];
    title.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    [title setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    UIBarButtonItem *leftItem =[[UIBarButtonItem alloc]initWithCustomView: title];
    self.navigationItem.leftBarButtonItem =leftItem;
    
}
- (void)LoadTableView
{
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSLiveListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _titleArr.count;
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSLiveListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titleLable.text = _titleArr[indexPath.row];
    cell.titleImage.image = [UIImage imageNamed:_imageArr[indexPath.row]];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        CSShopViewController *vc = [[CSShopViewController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row==1) {
        BaseshenghuoViewController *vc = [[BaseshenghuoViewController alloc]init];
        vc.webUrl = @"https://m.baidu.com";
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row==2) {
        BaseshenghuoViewController *vc = [[BaseshenghuoViewController alloc]init];
        vc.webUrl = @"https://news.baidu.com";
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row==3) {
        BaseshenghuoViewController *vc = [[BaseshenghuoViewController alloc]init];
        vc.webUrl = @"http://www.4399.com";
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row==4) {
        BaseshenghuoViewController *vc = [[BaseshenghuoViewController alloc]init];
        vc.webUrl = @"https://m.ctrip.com";
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
