//
//  TXLiveListTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2021/4/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSLiveListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@end

NS_ASSUME_NONNULL_END
