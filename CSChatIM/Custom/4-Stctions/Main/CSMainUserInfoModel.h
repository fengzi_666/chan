//
//  CSMainUserInfoModel.h
//  CSChatIM
//
//  Created by Apple on 2021/1/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMainUserInfoModel : NSObject
@property (nonatomic ,copy) NSString *usesrid;
@property (nonatomic ,copy) NSString *nicheng;
@property (nonatomic ,copy) NSString *money;
@property (nonatomic ,copy) NSString *zhifubao;
@property (nonatomic ,copy) NSString *gexingqianming;
@property (nonatomic ,copy) NSString *tengxuncode;
@property (nonatomic ,copy) NSString *zhifubaostate;
@property (nonatomic ,copy) NSString *erweima;
@property (nonatomic ,copy) NSString *userphone;
@property (nonatomic ,copy) NSString *xingming;
@property (nonatomic ,copy) NSString *createdate;
@property (nonatomic ,copy) NSString *touxiang;
@property (nonatomic ,copy) NSString *zhifubaoerweima;
@property (nonatomic ,copy) NSString *usercode;
@property (nonatomic ,copy) NSString *yinhangstate;
@property (nonatomic ,copy) NSString *yhxingming;
@property (nonatomic ,copy) NSString *yinhang;
@property (nonatomic ,copy) NSString *yinhangka;
@property (nonatomic ,copy) NSString *shimingstate;
@property (nonatomic ,copy) NSString *xsphone;
@property (nonatomic ,copy) NSString *shenfenzhenghao;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
