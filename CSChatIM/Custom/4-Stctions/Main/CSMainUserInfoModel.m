//
//  CSMainUserInfoModel.m
//  CSChatIM
//
//  Created by Apple on 2021/1/8.
//

#import "CSMainUserInfoModel.h"

@implementation CSMainUserInfoModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.usesrid = [NSString stringWithFormat:@"%@",dic[@"id"]];
        
        self.money =[NSString stringWithFormat:@"%@",dic[@"money"]];
        self.zhifubao =dic[@"zhifubao"];
 
       
        self.gexingqianming = dic[@"gexingqianming"];
        self.tengxuncode =dic[@"tengxuncode"];
        self.zhifubaostate = [NSString stringWithFormat:@"%@",dic[@"zhifubaostate"]];
        self.erweima = dic[@"erweima"];
        self.userphone = dic[@"userphone"];
        self.xingming = dic[@"xingming"];
        self.createdate = dic[@"createdate"];
        self.touxiang = dic[@"touxiang"];
        self.zhifubaoerweima = dic[@"zhifubaoerweima"];
        self.usercode = dic[@"usercode"];
        self.yhxingming = dic[@"yhxingming"];
        self.yinhang = dic[@"yinhang"];
        self.yinhangka = dic[@"yinhangka"];
        self.xsphone = dic[@"xsphone"];
        self.shenfenzhenghao = dic[@"shenfenzhenghao"];
        self.yinhangstate = [NSString stringWithFormat:@"%@",dic[@"yinhangstate"]];
        self.shimingstate = [NSString stringWithFormat:@"%@",dic[@"shimingstate"]];
        
    
    }
    return self;
}
@end
