//
//  CSMainTableViewCell.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "CSMainTableViewCell.h"

@implementation CSMainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)tagClick:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(clickHeadButton:selectButton:)]){
            [self.delegate clickHeadButton:self selectButton:sender];     //让代理调用 代理方法
        }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
