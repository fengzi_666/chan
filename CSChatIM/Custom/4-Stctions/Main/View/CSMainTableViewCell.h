//
//  CSMainTableViewCell.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSMainTableViewCell;

@protocol CSMainTableViewCellDelegate <NSObject>     //定义协议（命名：类名+Delegate）

-(void)clickHeadButton:(CSMainTableViewCell *)headView selectButton:(UIButton *)selectButton;   //代理方法 （参数：类对象+其他）

@end
@interface CSMainTableViewCell : UITableViewCell
@property(nonatomic,weak) id<CSMainTableViewCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
