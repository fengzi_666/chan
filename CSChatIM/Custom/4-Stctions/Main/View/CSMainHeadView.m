//
//  TXMainHeadView.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "CSMainHeadView.h"

@implementation TXMainHeadView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSMainHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius = 32.5;
        self.backgroundColor = [UIColor whiteColor];
        
        self.moneyView.layer.borderWidth = 1;
        self.moneyView.layer.borderColor = [UIColor colorWithHexString:@"#eeeeee"].CGColor;
        self.codeView.layer.borderWidth = 1;
        self.codeView.layer.borderColor = [UIColor colorWithHexString:@"#eeeeee"].CGColor;
    
    
       
    }
    return self;
}

@end
