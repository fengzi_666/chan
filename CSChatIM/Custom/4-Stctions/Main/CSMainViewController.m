//
//  TXMainViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "CSMainViewController.h"
#import "CSMainTableViewCell.h"
#import "CSMainHeadView.h"
#import "CSMainFoodView.h"
#import "CSMainEditViewController.h"
#import "CSEditCodeViewController.h"
#import "CSAccountViewController.h"
#import "CSMoneyViewController.h"
#import "ReactiveObjC.h"
#import "LFUserModel.h"
#import "CSMainTicklingVC.h"
#import "CSLoginViewController.h"
#import "CSProtocolViewController.h"
#import "SELUpdateAlert.h"
#import "CSPayViewController.h"
#import <RPSDK/RPSDK.h>
#import "CSMainUserInfoModel.h"
#import "CSEditIDViewController.h"
@interface CSMainViewController ()<UITableViewDelegate,UITableViewDataSource,CSMainTableViewCellDelegate>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSArray *imageArr;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) TXMainHeadView *headView;
@property V2TIMUserFullInfo *userInfo;
@property (nonatomic ,strong) NSDictionary *userDic;
@property (nonatomic ,copy) NSString *codeUrl;
@property (nonatomic ,copy) NSString *guanfangkefu;
@property (nonatomic ,strong) NSArray *msgListArr;
@property (nonatomic ,copy) NSString *phone;
@property (nonatomic ,strong) CSMainUserInfoModel *userModel;

@end
static NSString *cellid = @"cellid";
@implementation CSMainViewController
//在此处设置一次 setuoData，才能使得“我”界面消息更新。否则由于 UITabBar 的维护，“我”界面的消息将一直无法更新。
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self LoadUserLoginUserid];
    _msgListArr  =  [TIMManager sharedInstance].getConversationList;
    
    [self LoadRequestUserData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self LoadSetNavUI];
    
    [self LoadTableView];
    
    [self LoadRequsetAPP];
    
    
    
   
}
- (void)LoadRequestUserData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appuserinfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
           

            CSMainUserInfoModel *userModel = [[CSMainUserInfoModel alloc]initWithDic:data];
            _userModel = userModel;
            if ([_userModel.shimingstate isEqualToString:@"1"]) {
                [_myTableView reloadData];
            }
            
          
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadRequsetAPP
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apppeizhi];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *peizhi = data[@"peizhi"];
            _guanfangkefu = peizhi[@"guanfangkefu"];
         
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadUserLoginUserid
{
    NSString *loginUser = [[V2TIMManager sharedInstance] getLoginUser];
    if (loginUser.length > 0) {
        @weakify(self)
        [[V2TIMManager sharedInstance] getUsersInfo:@[loginUser] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
            @strongify(self)
            self.userInfo = infoList.firstObject;
            if (_userInfo) {
                NSLog(@"%@",_userInfo);
                [_headView.headImage sd_setImageWithURL:[NSURL URLWithString:_userInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
                _headView.nameLable.text = _userInfo.nickName;
                _userDic= _userInfo.customInfo;
                NSString *str = [[ NSString alloc]initWithData:_userDic[@"usercode"] encoding:NSUTF8StringEncoding];
                NSString *phone = [[ NSString alloc]initWithData:_userDic[@"phone"] encoding:NSUTF8StringEncoding];
                _phone = phone;
                _headView.IdLable.text = [NSString stringWithFormat:@"ID:   %@",str];
                _headView.signatureLable.text = [NSString stringWithFormat:@"手机号:   %@",phone];
                _codeUrl  =[[ NSString alloc] initWithData:_userDic[@"erweima"] encoding:NSUTF8StringEncoding];
                
                [[UserDefaultManager shareInstance]SavaLoginLoginMobile:phone];
//                [[UserDefaultManager shareInstance]SavaLoginLoginMobile:_userDic[@"userid"]];
            }
        } fail:nil];
    }
}
- (void)LoadSetNavUI
{
//    self.view.backgroundColor = [UIColor  whiteColor];
//    UINavigationBar *navigationBar = self.navigationController.navigationBar;
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
//    [navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//     //此处使底部线条失效
//     [navigationBar setShadowImage:[UIImage new]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"我的_编辑"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
   
    self.automaticallyAdjustsScrollViewInsets = YES;
  
    
      _imageArr = @[@"我的_钱包",@"我的_二维码",@"我的_客服",@"我的_反馈",@"我的_版本更新",@"我的_安全",@"我的_清空"];
      _titleArr = @[@"钱包",@"二维码分享",@"联系客服",@"帮助反馈",@"新版本检查",@"账号与安全",@"清空聊天记录"];
}

- (void)LoadTableView
{
  
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-44-StatusBarHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSMainTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
    
    TXMainHeadView *headView = [[TXMainHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 108+114)];
    _headView = headView;
    _myTableView.tableHeaderView.height = 108+114;
    _myTableView.tableHeaderView = headView;
    [_headView.moneyBtn addTarget:self action:@selector(moneyBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_headView.codeBtn addTarget:self action:@selector(codeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headTap:)];
    [headView addGestureRecognizer:tapGesture];
    
    CSMainFoodView *foodView = [[CSMainFoodView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
    
    [foodView.outBtn addTarget:self action:@selector(outBtnClick) forControlEvents:UIControlEventTouchUpInside];
    _myTableView.tableFooterView.height = 100;
    _myTableView.tableFooterView = foodView;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 271;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   
    cell.delegate =self;
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

- (void)codeBtnClick
{
    CSEditCodeViewController *vc = [[CSEditCodeViewController alloc]init];
    vc.codeUrl = _codeUrl;
    vc.userInfo = _userInfo;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)moneyBtnClick
{
    //钱包
    CSMoneyViewController *vc = [[CSMoneyViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)clickHeadButton:(CSMainTableViewCell *)headView selectButton:(UIButton *)selectButton
{
   
    if (selectButton.tag==0) {
        //客服
        CSProtocolViewController *vc = [[CSProtocolViewController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        vc.text = _guanfangkefu;
        vc.navigationItem.title = @"官方客服";
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (selectButton.tag==1) {
        //反馈
        CSMainTicklingVC *vc = [[CSMainTicklingVC alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (selectButton.tag==2) {
        //版本
        [self LoadRequestVersions];
    }
    if (selectButton.tag==3) {
        //账号安全
        TXAccountViewController *vc =[[TXAccountViewController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (selectButton.tag==4) {
       
        //清空聊天记录
        for (int i = 0; i<_msgListArr.count; i++) {
            TIMConversation *Conversation = _msgListArr[i];
            NSString *msgid = Conversation.getReceiver;
           
            BOOL  deleteType = [[TIMManager sharedInstance]deleteConversation:Conversation.getType receiver:msgid];
            if (deleteType ==YES) {
                [SVProgressHUD showInfoWithStatus:@"清空聊天记录成功"];
                
            }
            else
            {
                
            }
            
        }
        [SVProgressHUD showInfoWithStatus:@"清空聊天记录成功"];
        
    }
    if (selectButton.tag==5) {
//        [SVProgressHUD showWithStatus:@"<#查询中#>"];
        
        
        if ([_userModel.shimingstate isEqualToString:@"1"]) {
            CSEditIDViewController *vc = [[CSEditIDViewController alloc]init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else
        {
            NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appshiminggettoken];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
            __weak typeof(self) weakSelf = self;
            [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
                NSLog(@"%@",dict);
                NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
                NSDictionary * data = dict[@"data"];
                if ([code isEqualToString:@"0"]) {
                    NSString *bizid = data[@"bizid"];
                    NSString *token = data[@"token"];
                    [weakSelf LoadRequestToken:token bizid:bizid];
                }
                else
                {
                    [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                }
            } error:^(NSString *error) {
                [SVProgressHUD  dismiss];
            }];
        }
       
    }
}
- (void)LoadRequestToken :(NSString *)token  bizid:(NSString *)bizid
{
    //实名认证
    [RPSDK startWithVerifyToken:token
                 viewController:self
                     completion:^(RPResult * _Nonnull result) {
        // 建议接入方调用实人认证服务端接口DescribeVerifyResult，来获取最终的认证状态，并以此为准进行业务上的判断和处理。
        NSLog(@"实人认证结果：%@", result);
        //认证通过
        if (result.state==1) {
            NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateShiMing];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//            [dict setValue:_nameTF.text forKey:@"xingming"];
//            [dict setValue:_idTF.text forKey:@"shenfenzhenghao"];
            [dict setValue:bizid forKey:@"bizid"];
            [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
                NSLog(@"%@",dict);
                NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
                if ([code isEqualToString:@"0"]) {
//                    [SVProgressHUD showInfoWithStatus:@"实名认证成功"];
//                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                }
            } error:^(NSString *error) {
                [SVProgressHUD  dismiss];
            }];
        }
        //未认证
        if (result.state==-1) {
            
        }
        //认证失败
        if (result.state==2) {
            
        }
       
    }];

}
//检测版本升级
- (void)LoadRequestVersions
{
   
     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appversion];
     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
     [dict setValue:@"2" forKey:@"qudaoid"];
     [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
         NSDictionary *data = dict[@"data"];
         NSDictionary *versions = data[@"versions"];
        if ([code isEqualToString:@"0"]) {
            NSString *appVername = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
            NSString *str = [appVername stringByReplacingOccurrencesOfString:@"." withString:@""];

            NSLog(@"----%@",str);

         
            NSString *vername = [NSString stringWithFormat:@"%@",versions[@"vername"]];
            NSInteger verint =[versions[@"verint"] integerValue];
            NSDictionary* info=[[NSBundle mainBundle] infoDictionary];
            NSString* build=info[@"CFBundleVersion"];
            NSInteger appVerint = [build integerValue];
            NSString *updatestate = [NSString stringWithFormat:@"%@",versions[@"updatestate"]];
            NSString *content =versions[@"content"];
            NSString *url =versions[@"url"];
            BOOL istype ;
            if ([updatestate isEqualToString:@"1"]) {
                istype = YES;
            }
            else
            {
                istype = NO;
            }
            if (verint>appVerint) {
                [SELUpdateAlert showUpdateAlertWithVersion:vername Description:content update:url isQiangZhi:istype];
            }
            if (verint==appVerint) {
                [SVProgressHUD showInfoWithStatus:@"当前已是最新版本"];
            }
//            [self LoadCompareVersions:vername appversions:appVername content:versions[@"content"] updatestate:versions[@"updatestate"] url:versions[@"url"]];
            NSLog(@"%@-----%@",appVername,vername);
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadCompareVersions:(NSString *)versions appversions:(NSString *)Appversions  content:(NSString *)content  updatestate:(NSString *)updatestate url:(NSString *)url
{
    NSArray *localArray = [Appversions componentsSeparatedByString:@"."];
    NSArray *appArray = [versions componentsSeparatedByString:@"."];
    NSInteger minArrayLength = MIN(localArray.count, appArray.count);
    BOOL needUpdate = NO;
    
    for(int i=0;i<minArrayLength;i++){//以最短的数组长度为遍历次数,防止数组越界
        
        //取出每个部分的字符串值,比较数值大小
        NSString *localElement = localArray[i];
        NSString *appElement = appArray[i];
        NSInteger  localValue =  localElement.integerValue;
        NSInteger  appValue = appElement.integerValue;
        if(localValue<appValue) {
            //从前往后比较数字大小,一旦分出大小,跳出循环
            needUpdate = YES;
          
            break;
        }else{
            [SVProgressHUD showInfoWithStatus:@"当前已是最新版本"];
            needUpdate = NO;
        }
    }
    if (needUpdate) {
        //弹出提示更新弹框
        [SELUpdateAlert showUpdateAlertWithVersion:versions Description:content update:url isQiangZhi:NO];
    }
    
   
}

#pragma 编辑
- (void)rightBtnClick
{
    
    CSMainEditViewController *vc = [[CSMainEditViewController alloc]init];
    vc.userInfo = self.userInfo;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}
#pragma 退出账号操作
- (void)outBtnClick
{
    //初始化一个UIAlertController
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否确定退出" message:nil preferredStyle:UIAlertControllerStyleAlert];

    //创建按钮
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //点击按钮的响应事件
        NSLog(@"点击按钮的响应事件");
        [[V2TIMManager sharedInstance] logout:^{
            
            [[TUILocalStorage sharedInstance] logout];
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
            [[UserDefaultManager shareInstance]SavaLoginLoginAccount:@""];
           
        } fail:^(int code, NSString *msg) {
            NSLog(@"退出登录失败");
        }];
        }];
    //创建取消按钮
    //注意取消按钮只能添加一个
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        NSLog(@"取消");
    }];
    //添加按钮 将按钮添加到UIAlertController对象上
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    //模态弹出提示框 相当于UIAlertView show 的方法
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma 编辑个人资料
- (void)headTap:(UITapGestureRecognizer *)gesture
{
    CSMainEditViewController *vc = [[CSMainEditViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.userInfo = self.userInfo;
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
