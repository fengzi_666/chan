//
//  CSMainEditBoundCell.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMainEditBoundCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;

@end

NS_ASSUME_NONNULL_END
