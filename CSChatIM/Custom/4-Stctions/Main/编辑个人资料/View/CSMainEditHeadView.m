//
//  ZSMainEditHeadView.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "CSMainEditHeadView.h"

@implementation CSMainEditHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSMainEditHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius = 37.5;
        self.backgroundColor = [UIColor whiteColor];
    
       
    }
    return self;
}

@end
