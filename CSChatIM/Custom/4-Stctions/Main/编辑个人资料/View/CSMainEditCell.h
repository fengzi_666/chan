//
//  CSMainEditCell.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMainEditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *listTitle;
@property (weak, nonatomic) IBOutlet UILabel *listSubTitle;

@end

NS_ASSUME_NONNULL_END
