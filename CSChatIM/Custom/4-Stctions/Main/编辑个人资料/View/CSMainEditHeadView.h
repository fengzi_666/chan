//
//  CSMainEditHeadView.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMainEditHeadView : UIView
@property (weak, nonatomic) IBOutlet UIButton *headImage;

@end

NS_ASSUME_NONNULL_END
