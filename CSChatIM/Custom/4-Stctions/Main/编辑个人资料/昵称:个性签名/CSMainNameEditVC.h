//
//  CSMainNameEditVC.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMainNameEditVC : BaseViewController
@property (nonatomic ,copy) NSString *type;//1.个性签名
@end

NS_ASSUME_NONNULL_END
