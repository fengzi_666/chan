//
//  CSMainNameEditVC.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "CSMainNameEditVC.h"

@interface CSMainNameEditVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTF;

@end

@implementation CSMainNameEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    self.nameTF.delegate = self;
    [self setNav];
    if ([_type isEqualToString:@"1"]) {
        self.nameTF.placeholder = @"请编辑个性签名";
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(rightClick)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];

}
- (void)rightClick
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if ([_type isEqualToString:@"1"]) {
        [dict setValue:_nameTF.text forKey:@"gexingqianming"];
    }else
    {
        [dict setValue:_nameTF.text forKey:@"nicheng"];
    }
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        
    }];
  
}
#pragma mark - textfield action
- (void)textFieldChange:(UITextField*)textField{
    if (textField == _nameTF) {
        //        _loginButton.userInteractionEnabled = NO;
        if ([_type isEqualToString:@"1"]) {
            [jiaohuanqiValidate restoreValidTextLength:100 textField:_nameTF];
        }
        else
        {
            [jiaohuanqiValidate restoreValidTextLength:8 textField:_nameTF];
        }
      
        
    }
   
  
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _nameTF) {
        if ([_type isEqualToString:@"1"]) {
            return [jiaohuanqiValidate limitMaxLength:100 textField:textField replacementString:string];
        }
        else
        {
            return [jiaohuanqiValidate limitMaxLength:8 textField:textField replacementString:string];
        }
       
    }
    
    
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
