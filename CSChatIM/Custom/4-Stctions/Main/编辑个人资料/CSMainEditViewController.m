//
//  CSMainEditViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "CSMainEditViewController.h"
#import "CSMainEditCell.h"
#import "CSMainEditBoundCell.h"
#import "CSMainEditHeadView.h"
#import "CSMainNameEditVC.h"
#import "ZZQAvatarPicker.h"
#import "UIButton+Extension.h"
//#import <RPSDK/RPSDK.h>
@interface CSMainEditViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSArray *sectionArr1;
@property (nonatomic ,strong) NSArray *sectionArr2;
@property (nonatomic ,strong) CSMainEditHeadView *headView;

@end
static NSString *cellid = @"cellid";
static NSString *boundCellid = @"boundCellid";
@implementation CSMainEditViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self LoadUserLoginUserid];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _sectionArr1 = @[@"昵称",@"ID号",@"个性签名"];
    _sectionArr2 = @[@"实名认证",@"绑定银行卡"];
    [self LoadSav];
    
    [self LoadTableView];
}
- (void)LoadUserLoginUserid
{
    NSString *loginUser = [[V2TIMManager sharedInstance] getLoginUser];
    if (loginUser.length > 0) {
        @weakify(self)
        [[V2TIMManager sharedInstance] getUsersInfo:@[loginUser] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
            @strongify(self)
            self.userInfo = infoList.firstObject;
            if (_userInfo) {
                NSLog(@"%@",_userInfo);
                [_headView.headImage xr_setButtonImageWithUrl:_userInfo.faceURL];
                [_myTableView reloadData];
            }
        } fail:nil];
    }
}
- (void)LoadSav
{
    self.navigationItem.title = @"编辑资料";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
   
}

- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSMainEditCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSMainEditBoundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:boundCellid];
    [self.view addSubview:_myTableView];
    
    CSMainEditHeadView *headView = [[CSMainEditHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 150)];
    
//    [headView.headImage sd_setImageWithURL:[NSURL URLWithString:_userInfo.faceURL] forState:UIControlStateNormal];
    [headView.headImage xr_setButtonImageWithUrl:_userInfo.faceURL];
    _myTableView.tableHeaderView.height = 150;
    _myTableView.tableHeaderView = headView;
   
    [headView.headImage addTarget:self action:@selector(headTap:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
        return _sectionArr1.count;
    
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        CSMainEditCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.listTitle.text = _sectionArr1[indexPath.row];
        if (indexPath.row==0) {
            cell.listSubTitle.text = _userInfo.nickName;
        }
        if (indexPath.row==1) {
            NSDictionary *dict = _userInfo.customInfo;
            NSString *idname = [[ NSString alloc]initWithData:dict[@"usercode"] encoding:NSUTF8StringEncoding];
            cell.listSubTitle.text = idname;
        }
        if (indexPath.row==2) {
            cell.listSubTitle.text = _userInfo.selfSignature;
        }
        
        return cell;
    
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        if (indexPath.row==0) {
            //编辑昵称
            CSMainNameEditVC *vc = [[CSMainNameEditVC alloc]init];
            vc.navigationItem.title = @"昵称";
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        if (indexPath.row==1) {
            
            
        }
        if (indexPath.row==2) {
            CSMainNameEditVC *vc = [[CSMainNameEditVC alloc]init];
            vc.navigationItem.title = @"个性签名";
            vc.type = @"1";
            [self.navigationController pushViewController:vc animated:YES];
            //个性签名
        }
    
   
}
#pragma 编辑头像
- (void)headTap:(UIButton *)sender
{
     //处理事件
    NSLog(@"111111");
    [ZZQAvatarPicker startSelected:^(UIImage * _Nonnull image) {
        if (image) {
            [SVProgressHUD showWithStatus:@"头像上传中"];
            [self LoadRequestImage:image sender:sender];
        }
    }];
}
- (void)LoadRequestImage:(UIImage *)image sender:(UIButton *)sender
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updatetouxiang];
    [longwangRequestManage postBaseImageWithURL:url param:nil image:image success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"头像上传成功"];
            [sender setBackgroundImage:image forState:UIControlStateNormal];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
          
        }
       
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
