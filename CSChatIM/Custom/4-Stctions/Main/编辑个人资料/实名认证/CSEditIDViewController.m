//
//  CSEditIDViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSEditIDViewController.h"

@interface CSEditIDViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *idTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@end

@implementation CSEditIDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"实名认证";
    [self setNav];
    if ([_userModel.shimingstate isEqualToString:@"1"]) {
        _idTF.enabled = NO;
        _mobileTF.enabled = NO;
        _nameTF.enabled = NO;
        _saveBtn.hidden = YES;
        _nameTF.text = _userModel.xingming;
        _idTF.text = _userModel.shenfenzhenghao;
        _mobileTF.text = _userModel.xsphone;
    }
}
- (IBAction)saveBtnClick:(UIButton *)sender {
    if ([_nameTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入真实姓名"];
        return;
    }
    if ([_idTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入身份证号码"];
        return;
    }
    if ([_mobileTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"请稍后..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateShiMing];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_nameTF.text forKey:@"xingming"];
    [dict setValue:_idTF.text forKey:@"shenfenzhenghao"];
    [dict setValue:_mobileTF.text forKey:@"phone"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showInfoWithStatus:@"实名认证成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
