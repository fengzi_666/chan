//
//  CSEditIDViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "BaseViewController.h"
#import "CSMainUserInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSEditIDViewController : BaseViewController
@property (nonatomic ,strong) CSMainUserInfoModel *userModel ;
@end

NS_ASSUME_NONNULL_END
