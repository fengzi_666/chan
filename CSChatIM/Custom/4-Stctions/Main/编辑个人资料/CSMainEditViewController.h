//
//  CSMainEditViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMainEditViewController : BaseViewController
@property V2TIMUserFullInfo *userInfo;
@end

NS_ASSUME_NONNULL_END
