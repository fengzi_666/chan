//
//  CSBillingDetailsViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSBillingDetailsViewController.h"

@interface CSBillingDetailsViewController ()
//方式 提现/充值
@property (weak, nonatomic) IBOutlet UILabel *modeLable;
//钱
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
//交易成功
@property (weak, nonatomic) IBOutlet UILabel *dealLable;
//展示 余额提现/充值
@property (weak, nonatomic) IBOutlet UILabel *mode1Lable;
//提现账户银行
@property (weak, nonatomic) IBOutlet UILabel *bankLable;
//时间
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
//订单号
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLable;

@end

@implementation CSBillingDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"提现详情";
    [self setNav];
}
- (IBAction)bankBtnClick:(UIButton *)sender {
    NSLog(@"点击跳转银行");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
