//
//  CSMoneyDetailViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSMoneyDetailViewController.h"
#import "CSMoneyDetailListCell.h"
#import "CSBillingDetailsViewController.h"
#import "CSMoneyDetailListModel.h"
@interface CSMoneyDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    int page;
}
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@property (nonatomic,assign) NSInteger pageno;

@end
static NSString *cellid = @"cellid";
@implementation CSMoneyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"账单明细";
    
    [self setNav];
    
    [self LoadTableView];
    
 
    
    
}
- (void)LoadRequstData
{
    [SVProgressHUD showWithStatus:@"查询中"];
     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appshourumingxi];
     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
      [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
         NSLog(@"%@",dict);
          [SVProgressHUD dismiss];
          NSDictionary *data  =dict[@"data"];
         NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
         if ([code isEqualToString:@"0"]) {
             NSArray *list =data[@"list"];
             NSMutableArray *dataArr = [[NSMutableArray alloc]init];
            for (int i= 0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSMoneyDetailListModel *model = [[CSMoneyDetailListModel alloc]initWithDic:dic];
                [dataArr addObject:model];
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (list.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (dataArr.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
         [_myTableView reloadData];
        
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
}
- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-StatusBarHeight-40) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    _myTableView.estimatedRowHeight = 200;
   
       if (@available(iOS 11.0, *)) {
           _myTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
      } else {
           self.automaticallyAdjustsScrollViewInsets = NO;
       }
    [_myTableView registerNib:[UINib nibWithNibName:@"CSMoneyDetailListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
   
    //这种上拉刷新footer在tableview的底部
    [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        self.dataArr =[NSMutableArray array];
        [self LoadRequstData];
    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequstData];
    }];
    
    [_myTableView.mj_header beginRefreshing];
    [self.view addSubview:_myTableView];
    
  
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSMoneyDetailListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSMoneyDetailListModel *model = _dataArr [indexPath.row];
    cell.listTitle.text = model.leixingname;
    cell.listTimeLable.text = model.createdate;
    cell.moneyLable.text = model.money;
    if ( [model.zhengfustate isEqualToString:@"2"]) {
        cell.moneyLable.textColor = [UIColor colorWithHexString:@"#f79b0b"];
    }
   
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CSBillingDetailsViewController *vc = [[CSBillingDetailsViewController alloc]init];
//    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
