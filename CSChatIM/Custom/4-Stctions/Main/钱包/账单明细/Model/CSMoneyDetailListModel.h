//
//  CSMoneyDetailListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMoneyDetailListModel : NSObject
@property (nonatomic ,copy) NSString *touxiang;
@property (nonatomic ,copy) NSString *leixing;
@property (nonatomic ,copy) NSString *leixingname;
@property (nonatomic ,copy) NSString *orderid;
@property (nonatomic ,copy) NSString *createdate;
@property (nonatomic ,copy) NSString *nicheng;
@property (nonatomic ,copy) NSString *money;
@property (nonatomic ,copy) NSString *state;
@property (nonatomic ,copy) NSString *zhengfustate;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
