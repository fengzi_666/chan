//
//  CSMoneyDetailListModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/14.
//

#import "CSMoneyDetailListModel.h"
#import "NSString+LFAdd.h"
@implementation CSMoneyDetailListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.leixing = [NSString stringWithFormat:@"%@",dic[@"leixing"]];
        self.orderid =[NSString stringWithFormat:@"%@",dic[@"orderid"]];
        NSString *money = [NSString formatDecimalNumber:[NSString stringWithFormat:@"%@",dic[@"money"]]];
        self.money =[NSString stringWithFormat:@"%@元",money];
        self.state =[NSString stringWithFormat:@"%@",dic[@"state"]];
        self.zhengfustate =[NSString stringWithFormat:@"%@",dic[@"zhengfustate"]];
        self.touxiang = dic[@"touxiang"];
        self.leixingname = dic[@"leixingname"];
        self.createdate = [NSString stringWithFormat:@"%@",dic[@"createdate"]];
        self.nicheng = dic[@"nicheng"];
    }
    return self;
}
@end
