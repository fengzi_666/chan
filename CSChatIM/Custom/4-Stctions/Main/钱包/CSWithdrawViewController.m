//
//  CSWithdrawViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import "CSWithdrawViewController.h"
#import "CSChatRedPasdPopView.h"
#import "XMPayCodeView.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CSPayCodeViewController.h"
#import "CSSelectBanKViewController.h"
#import "CSBanListModel.h"
@interface CSWithdrawViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UITextField *withdrawTF;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;

@property (weak, nonatomic) IBOutlet UIButton *apayBtn;
@property (weak, nonatomic) IBOutlet UILabel *apayLable;
@property (weak, nonatomic) IBOutlet UIButton *bankBtn;
@property (weak, nonatomic) IBOutlet UILabel *bankLable;
@property (weak, nonatomic) IBOutlet UILabel *bankCard;
@property (nonatomic ,strong) CSChatRedPasdPopView *redPsdPopView;
@property (nonatomic ,strong) XMPayCodeView *codeView;
@property (weak, nonatomic) IBOutlet UILabel *tixianfeilvLable;

@property (weak, nonatomic) IBOutlet UILabel *shijidaozhangLable;
@property (weak, nonatomic) IBOutlet UILabel *shouxufeiLable;
@property (nonatomic ,copy) NSString *istype;


@property (nonatomic ,copy) NSString *paypsdStr;
@property (weak, nonatomic) IBOutlet UILabel *tixianLable;
@property (weak, nonatomic) IBOutlet UIView *bankView;
@property (nonatomic ,strong) CSBanListModel *selectModel;
@end

@implementation CSWithdrawViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self LoadRequestData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"提现";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    _istype =@"2";
   
    [_bankBtn setImage:[UIImage imageNamed:@"提现_选中"] forState:UIControlStateNormal];
    if (_money.length>0) {
        _moneyLable.text = _money;
    }
    else
    {
        _withdrawTF.text = @"";
    }
  
    
  
    
   
    [self  LoadRequsetAPP];
    
    [_withdrawTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _withdrawTF.delegate = self;
    [self LoadRequestData1];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(event:)];
    [_bankView addGestureRecognizer:tapGesture];
}
- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appuserinfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *testNum = [self formatDecimalNumber:[NSString stringWithFormat:@"%@",data[@"money"]]];
            
            _moneyLable.text = testNum;
            
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
#pragma mark --选择银行卡
- (void)event:(UITapGestureRecognizer *)gesture
{
    NSLog(@"选择银行卡");
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appyinhangkalist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSArray *list = data[@"list"];
            if (list.count>0) {
                CSSelectBanKViewController *vc = [[CSSelectBanKViewController alloc]init];
                __weak typeof(self) weakSelf = self;
                vc.bankSelectBlock = ^(CSBanListModel * _Nonnull model) {
                    weakSelf.selectModel = model;
                    _bankLable.text = model.bankname;
                    _bankCard.text = [NSString stringWithFormat:@"尾号%@",model.housiwei];
                };
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                [SVProgressHUD showInfoWithStatus:@"请先绑定银行卡"];
            }
           
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
   
}
//提现费率
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"3");//文本彻底结束编辑时调用
    

}
-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
//    [self LoadRequestMoenyData];
}
- (void)LoadRequestMoenyData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appshouxvfe];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_withdrawTF.text forKey:@"money"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
            NSDictionary *data = dict[@"data"];
            
            NSString *dakuanjine = [ self formatDecimalNumber :[NSString stringWithFormat:@"%@",data[@"dakuanjine"]]];
//            NSString *money = [NSString stringWithFormat:@"%@",data[@"money"]];
            NSString *shouxufei = [ self formatDecimalNumber :[NSString stringWithFormat:@"%@",data[@"shouxufei"]]];
            NSString *feilv = [ self formatDecimalNumber :[NSString stringWithFormat:@"%@",data[@"feilv"]]];
            _shijidaozhangLable.text =[NSString stringWithFormat:@"¥%@",dakuanjine];
            _shouxufeiLable.text = [NSString stringWithFormat:@"¥%@",shouxufei];
            NSString *str = @"%";
            _tixianfeilvLable.text =[NSString stringWithFormat:@"%@%@",str,feilv];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
            _shijidaozhangLable.text =@"";
            _shouxufeiLable.text = @"";
            _tixianfeilvLable.text =@"";
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//转换钱类型
- (NSString *)formatDecimalNumber:(NSString *)string {
    if (!string || string.length == 0) {
        return string;
    }
    
    NSNumber *number = @([string doubleValue]);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterDecimalStyle;
    formatter.positiveFormat = @"0.00";
    
    NSString *amountString = [formatter stringFromNumber:number];
    return amountString;
}
- (IBAction)tagBtnClick:(UIButton *)sender {
    
    if (sender.tag==1) {
        if ([_userModel.yinhangstate isEqualToString:@"1"]) {
        [_bankBtn setImage:[UIImage imageNamed:@"提现_选中"] forState:UIControlStateNormal];
        _istype =@"2";
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:@"请先去绑定银行卡"];
            
        }
    }
    else{
       
        
    }
}
//全部提现
- (IBAction)allBtnClick:(id)sender {
    _withdrawTF.text = _money;
    [self LoadRequestMoenyData];
}
BOOL istype = NO;
//立即提现
- (IBAction)withdrawClick:(UIButton *)sender {

    NSInteger money = [_money integerValue];
    NSInteger textMoney = [_withdrawTF.text intValue];
    if (textMoney>=money) {
        [SVProgressHUD showInfoWithStatus:@"余额不足请输入有效的金额"];
        return;
    }
    if ([_withdrawTF.text isEqualToString:@""]||_withdrawTF.text==nil) {
        [SVProgressHUD showInfoWithStatus:@"请输入提现金额"];
        return;
    }
    if ([_bankLable.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请先选择银行卡"];
        return;
    }
  
   
    [self LoadRequestTixian];
}
- (void)LoadRequestTixian
{
    
         [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;
         
             self.redPsdPopView = [[CSChatRedPasdPopView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
             self.redPsdPopView.moneyLable.text = [NSString stringWithFormat:@"¥%@",_withdrawTF.text];
             [self.redPsdPopView.deleteBtn addTarget:self action:@selector(redPopClick:) forControlEvents:UIControlEventTouchUpInside];
             [self.view addSubview:self.redPsdPopView];
             // 支付密码输入框
             XMPayCodeView *payCodeView = [[XMPayCodeView alloc]init];

             _codeView = payCodeView;
             [self.redPsdPopView.popView addSubview:payCodeView];
             [payCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.left.equalTo(self.redPsdPopView.popView.mas_left).offset(25);
                 make.right.equalTo(self.redPsdPopView.popView.mas_right).offset(-25);
                 make.bottom.equalTo(self.redPsdPopView.popView.mas_bottom).offset(-30);
                 make.height.equalTo(@55);
             
            
             payCodeView.secureTextEntry = YES;  //
             
             [payCodeView setPayBlock:^(NSString *payCode) {
                 NSLog(@"payCode==%@",payCode);
                 _paypsdStr = payCode;
                 
                 if (payCode.length<=5) {
                     istype = NO;
                 }
                 if (payCode.length==6) {
                    
                     if (istype==YES) {
                         return;
                     }
                     istype = YES;
                 
                     [SVProgressHUD showWithStatus:@"请稍后.."];
                     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_shenQing];
                     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                     [dict setValue:_withdrawTF.text forKey:@"money"];
                     [dict setValue:_paypsdStr forKey:@"zhifumima"];
                     [dict setValue:_selectModel.id forKey:@"yinhangkaid"];
                     [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
                         NSLog(@"%@",dict);
                         NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
                         if ([code isEqualToString:@"0"]) {
                             [SVProgressHUD showSuccessWithStatus:@"提现成功"];
                             [self.navigationController popViewControllerAnimated:YES];
                         }
                         else if([code isEqualToString:@"1007"])
                         {
                             [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                             CSPayCodeViewController *vc = [[CSPayCodeViewController alloc]init];
                             vc.type = @"3";
                             [self.navigationController pushViewController:vc animated:YES];
                         }
                         else
                         {
                             [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
                         }
                     } error:^(NSString *error) {
                         [SVProgressHUD  dismiss];
                     }];
                 }

             }];
              //1秒后，让密码输入成为第一响应
            [payCodeView becomeKeyBoardFirstResponder];
             
         }];
     
     
}
- (void)redPopClick:(UIButton *)sender
{
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;

    __weak typeof(self) weakSelf = self;
      //执行的动画
    [UIView animateWithDuration:0.5 animations:^{
        
        [weakSelf.redPsdPopView removeFromSuperview];
    }];
}

- (void)LoadRequsetAPP
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apppeizhi];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *peizhi = data[@"peizhi"];
            NSString *chongzhishuoming = peizhi [@"tixianshuoming"];
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[chongzhishuoming dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

            _tixianLable.attributedText =attrStr;
            _tixianLable.textColor = [UIColor colorWithHexString:@"#999999"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadRequestData1
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appuserinfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *dic = dict[@"data"];
            CSMainUserInfoModel *userModel = [[CSMainUserInfoModel alloc]initWithDic:dic];
            _userModel = userModel;
          
        }
       
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
