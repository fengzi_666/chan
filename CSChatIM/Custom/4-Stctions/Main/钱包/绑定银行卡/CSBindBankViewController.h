//
//  CSBindBankViewController.h
//  CSChatIM
//
//  Created by Apple on 2021/1/11.
//

#import "BaseViewController.h"
#import "CSBanListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSBindBankViewController : BaseViewController
@property (nonatomic ,strong) CSBanListModel *bankmodel;
@property (nonatomic ,copy) NSString *bankState;
@end

NS_ASSUME_NONNULL_END
