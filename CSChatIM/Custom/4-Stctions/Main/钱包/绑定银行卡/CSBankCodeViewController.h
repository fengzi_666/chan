//
//  CSBankCodeViewController.h
//  CSChatIM
//
//  Created by Apple on 2021/3/25.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSBankCodeViewController : BaseViewController
@property (nonatomic ,copy) NSString *bankid;
@property (nonatomic ,copy) NSString *istype;
@end

NS_ASSUME_NONNULL_END
