//
//  CSBankListViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/3/24.
//

#import "CSBankListViewController.h"
#import "CSBanKListTableViewCell.h"
#import "CSBindBankViewController.h"
#import "CSBanListModel.h"

@interface CSBankListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic ,strong) UIButton *foodBtn;

@end
static NSString *cellid = @"cellid";
@implementation CSBankListViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self LoadRequestData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"银行卡";
    
    _listArr = [[NSMutableArray alloc]init];
    [self setNav];
    
    [self LoadTableView];
    
    [self LoadFoodView];

  
    
}
- (void)LoadFoodView
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"+添加银行卡" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
    _foodBtn = btn;
    [btn addTarget:self action:@selector(addBankClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-15);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(44);
    }];
}

- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appyinhangkalist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSArray *list = data[@"list"];
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            for (int i = 0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSBanListModel *model = [CSBanListModel mj_objectWithKeyValues:dic];
                [arr addObject:model];
            }
            _listArr  = arr;
            if (arr.count>4) {
                _foodBtn.hidden = YES;
            }
            [_myTableView reloadData];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)addBankClick
{
    CSBindBankViewController *Vc = [[ CSBindBankViewController alloc]init];
    [self.navigationController pushViewController:Vc animated:YES];
}
     
- (void)LoadTableView
{
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSBanKListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
 
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
 return _listArr.count;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSBanKListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSBanListModel *model = _listArr[indexPath.row];
    cell.bankName.text = model.bankname;
    cell.nameLable.text = model.yhxingming;
    cell.bankLable.text = [NSString stringWithFormat:@"尾号%@",model.housiwei];
    
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSBanListModel *model = _listArr[indexPath.row];
    CSBindBankViewController *vc = [[CSBindBankViewController alloc]init];
    vc.bankmodel = model;
    vc.bankState = @"1";
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
