//
//  CSBankCodeViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/3/25.
//

#import "CSBankCodeViewController.h"
#import "XMPayCodeView.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
@interface CSBankCodeViewController ()
@property (nonatomic ,copy) NSString *psdStr;

@end

@implementation CSBankCodeViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
//    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"银行卡验证";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    [self setNav];
    
    [self LoadUI];
    
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;
    // 支付密码输入框
    XMPayCodeView *payCodeView = [[XMPayCodeView alloc]init];
    [self.view addSubview:payCodeView];
    [payCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(200);
        make.width.equalTo(self.view);
        make.height.equalTo(@55);
    }];
   
    payCodeView.secureTextEntry = YES;  //
    [payCodeView setPayBlock:^(NSString *payCode) {
        NSLog(@"payCode==%@",payCode);
        _psdStr = payCode;

    }];
   
    // 1秒后，让密码输入成为第一响应
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [payCodeView becomeKeyBoardFirstResponder];
    });
    
}
//确定
- (void)ensureBtnClick
{
    
    if (_psdStr .length<6) {
        [SVProgressHUD showInfoWithStatus:@"请输入6位验证码"];
        return;
    }
    if ([_istype isEqualToString:@"1"]) {
        [SVProgressHUD showWithStatus:@"充值中.."];
    }
    else
    {
        
    }
    
  
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_queRenBangDing];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if ([_istype isEqualToString:@"1"]) {
        [dict setValue:_bankid forKey:@"orderid"];
        url =[NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_chongzhitwo];
    }
    else
    {
        [dict setValue:_bankid forKey:@"yinhangkaid"];
    }
  
    [dict setValue:_psdStr forKey:@"yzmcode"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];

        if ([code isEqualToString:@"0"]) {
            if ([_istype isEqualToString:@"1"]) {
                [SVProgressHUD showSuccessWithStatus:@"充值成功"];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:@"绑定成功"];
            }
           
            
                [self jumpVC];
            
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
    
    
}
- (void)LoadUI
{
    UILabel *hitLable = [[UILabel alloc]init];
    
    
        hitLable.text = @"请输入验证码";
    
    hitLable.font = [UIFont boldSystemFontOfSize:30];
    hitLable.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.view addSubview:hitLable];
    [hitLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(13+64+StatusBarHeight);
        make.left.mas_equalTo(self.view).offset(37);

    }];
    UILabel *hitLable1 = [[UILabel alloc]init];
    
        hitLable1.text = @"请设置6位数字的验证码";
    
    
    hitLable1.font = [UIFont boldSystemFontOfSize:15];
    hitLable1.textColor = [UIColor colorWithHexString:@"#999999"];
    [self.view addSubview:hitLable1];
    [hitLable1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(hitLable.mas_bottom).offset(16);
        make.left.mas_equalTo(hitLable.mas_left);
//
    }];
    UIButton *ensureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [ensureBtn setTintColor:[UIColor whiteColor]];
    ensureBtn.backgroundColor = [UIColor colorWithHexString:@"#0183FD"];
    ensureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [ensureBtn addTarget:self action:@selector(ensureBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ensureBtn];
    [ensureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(hitLable1.mas_bottom).offset(174);
        make.left.mas_equalTo(self.view).offset(55);
        make.right.mas_equalTo(self.view).offset(-55);
        make.height.mas_equalTo(45);
    }];
    
    
}
//跳转指定控制器
- (void)jumpVC
{

    NSInteger indexValue = (NSInteger)[[self.navigationController viewControllers] indexOfObject:self];
    if (indexValue > 2) {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(indexValue - 2)] animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
