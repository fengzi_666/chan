//
//  CSChoseCityVC.m
//  相亲项目
//
//  Created by Apple on 2021/3/8.
//

#import "CSChoseCityVC.h"


@interface CSChoseCityVC ()<UIPickerViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;



@property (assign, nonatomic) NSInteger pro_index;//当前默认选中省份

@property (assign, nonatomic) NSInteger city_index;

@property (nonatomic,strong) XQChoseCityModel *cityModel;
@end

@implementation CSChoseCityVC
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
    }];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.view.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0];
    self.cityModel =[XQChoseCityModel new];
    self.cityModel =_listArr[0];
    NSArray *Arr = self.cityModel.cityList;
    NSDictionary *dict =Arr[0];
    
    self.cityModel.cityname =dict[@"cityname"];
    self.cityModel.cityid =dict[@"cityid"];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
//    [self requestData];
    // Do any additional setup after loading the view from its nib.
}





- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.listArr.count;
    } else{
        NSLog(@"--------%@",self.listArr[_pro_index]);
       return self.listArr[_pro_index].cityList.count;
    }
//    OVLocationCity *area = self.arrayList[_pro_index].city[_city_index];
//    return area.area.count;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    //设置分割线的颜色
    for (UIView *singleLine in pickerView.subviews) {
        if (singleLine.frame.size.height < 1.0) {
            singleLine.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        }
    }
    /*重新定义row 的UILabel*/
    UILabel *pickerLabel = pickerLabel = [[UILabel alloc] init];
    pickerLabel.font = [UIFont fontWithName:@"PingFang-SC-Regular" size:13.0 * KScreenWidth/375];
    pickerLabel.textAlignment = NSTextAlignmentCenter;
    pickerLabel.backgroundColor = [UIColor clearColor];
    pickerLabel.adjustsFontSizeToFitWidth = YES;
    
    pickerLabel.attributedText = [self pickerView:pickerView attributedTitleForRow:row forComponent:component];
    
    return pickerLabel;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //获取省份
    if (component == 0) {
        XQChoseCityModel *province = self.listArr[row];
        return province.name;
    } else{
        //获取城市
        XQChoseCityModel *province = self.listArr[_pro_index];
        XQChoseCityModel *city =[XQChoseCityModel mj_objectWithKeyValues:province.cityList[row]];
        return city.name;
    }
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //获取省份
    if (component == 0) {
        XQChoseCityModel *province = self.listArr[row];
        return [[NSAttributedString alloc] initWithString:province.provincename attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0 * KScreenWidth/375]}];
    } else  {
        XQChoseCityModel *province = self.listArr[_pro_index];
        XQChoseCityModel *city =[XQChoseCityModel mj_objectWithKeyValues:province.cityList[row]];
        return [[NSAttributedString alloc] initWithString:city.cityname attributes:@{NSFontAttributeName:[UIFont fontWithName:@"PingFang-SC-Regular" size:15.0 * KScreenWidth/375]}];
    }

}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        _pro_index = row;
        _city_index = 0;
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
    }
    if (component == 1) {
        _city_index = row;
    }
    
    XQChoseCityModel *city;
    NSString *stringProvince;
    NSString *stringProvinceId;
    NSString *stringCity;
    NSString *cityId;
    
    if (component == 0) {
        XQChoseCityModel *province = self.listArr[_pro_index];
        stringProvince = province.provincename;
        stringProvinceId =province.provinceid;
        city = [XQChoseCityModel mj_objectWithKeyValues:province.cityList[_city_index]];
        stringCity = city.cityname;
        cityId =city.cityid;
//        stringArea = city.area[0];
    } else if (component == 1) {
        XQChoseCityModel *selectedProvince = self.listArr[_pro_index];
        stringProvince = selectedProvince.provincename;
        stringProvinceId =selectedProvince.provinceid;
        XQChoseCityModel *selectedCity = [XQChoseCityModel mj_objectWithKeyValues:selectedProvince.cityList[_city_index]];
        stringCity = selectedCity.cityname;
        cityId =selectedCity.cityid;
//        stringArea = selectedCity.area[0];
    }
    self.cityModel =[XQChoseCityModel new];
    self.cityModel.cityname =stringCity;
    self.cityModel.cityid =cityId;
    self.cityModel.provinceid =stringProvinceId;
    
    self.cityModel.provincename =stringProvince;
}

- (IBAction)clickDisBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clickOkBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.choseCityBlock) {
        self.choseCityBlock(self.cityModel);
    }
}


@end
