//
//  CSChoseCityVC.h
//  相亲项目
//
//  Created by Apple on 2021/3/8.
//

#import <UIKit/UIKit.h>
#import "XQChoseCityModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^ChoseCityBlock)(id model);

@interface CSChoseCityVC : UIViewController


@property (nonatomic,strong) NSArray <XQChoseCityModel *>*listArr;

@property (nonatomic,strong)ChoseCityBlock choseCityBlock;
@end

NS_ASSUME_NONNULL_END
