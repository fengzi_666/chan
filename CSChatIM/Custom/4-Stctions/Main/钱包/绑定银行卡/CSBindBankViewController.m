//
//  CSBindBankViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/1/11.
//

#import "CSBindBankViewController.h"
#import "CSMainUserInfoModel.h"
#import "CustomMyPickerView.h"
#import "CustomMyPickerViewController.h"
#import "XQChoseCityModel.h"
#import "CSChoseCityVC.h"
#import "CSBankModel.h"
#import "CSBankCodeViewController.h"
@interface CSBindBankViewController ()<UITextFieldDelegate>
{
    CustomMyPickerView *picker;
    CustomMyPickerViewController *custom;
}
//持卡人姓名
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
//身份证号
@property (weak, nonatomic) IBOutlet UITextField *idTF;


@property (weak, nonatomic) IBOutlet UITextField *mobleTF;

@property (weak, nonatomic) IBOutlet UITextField *bankCardTF;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveH;
@property (nonatomic ,strong) NSMutableArray *bankArr;
@property (nonatomic ,strong) NSArray *banListArr;
@property (nonatomic ,strong) NSArray *gpsListArr;
//省list
@property (nonatomic ,strong) NSMutableArray *provinceNameArr;
@property (nonatomic ,strong) NSArray *provinceListArr;
//市去list
@property (nonatomic ,strong) NSMutableArray *cityNameArr;

@property (nonatomic ,copy) NSString *cityid;
@property (nonatomic ,copy) NSString *provinceid;
@property (nonatomic ,copy) NSString *bankid;
@property (nonatomic ,strong) CSBankModel *model;
@end

@implementation CSBindBankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"绑定银行卡";
    _nameTF.text = @"";
    _bankCardTF.text = @"";
    _idTF.text = @"";
  
    
    _nameTF.delegate = self;
    _bankCardTF.delegate = self;
    _idTF.delegate = self;
  
    _bankArr = [[NSMutableArray alloc]init];
   
    [self setNav];
   
    _nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;

    _bankCardTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _idTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    if ([_bankState isEqualToString:@"1"]) {
        self.navigationItem.title = @"银行卡";
        [self LoadRequestBankState];
        _nameTF.enabled = NO;
        _bankCardTF.enabled = NO;
        _idTF.enabled = NO;
      
        _mobleTF.enabled = NO;
        _saveBtn .hidden = YES;
        _deleteBtn.hidden = NO;
        _deleteBtn.layer.borderWidth =1;
        _deleteBtn.layer.borderColor = [UIColor colorWithHexString:@"#FF3F3A"].CGColor;
        
    }
    else
    {
        _saveBtn .hidden = NO;
        _deleteBtn.hidden = YES;
    }
  
}
- (void)LoadRequestBankState
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appyinhangkaget];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_bankmodel.id forKey:@"yinhangkaid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *yinhangka  = data[@"yinhangka"];
            CSBankModel *model  = [CSBankModel mj_objectWithKeyValues:yinhangka];
            _model = model;
            _nameTF.text = model.yhxingming;
            _idTF.text = model.shenfenzhenghao;
           
            _bankCardTF.text = model.yinhangkahao;
            _mobleTF.text = model.yhphone;
        }
        else
        {
           
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}


//提交
- (IBAction)saveClick:(UIButton *)sender {
    if ([_nameTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入真实姓名"];
        return;
    }
    if ([_idTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入身份证号"];
        return;
    }

    if ([_bankCardTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入银行卡号"];
        return;
    }
    if ([_mobleTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号"];
        return;
    }
    [SVProgressHUD showWithStatus:@"验证码发送中..."];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_bangDingYinHang];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_provinceid forKey:@"provinceid"];
//    [dict setValue:_cityid forKey:@"cityid"];
//    [dict setValue:_bankid forKey:@"yinhangid"];
    [dict setValue:_bankCardTF.text forKey:@"yinhangkahao"];
    [dict setValue:_idTF.text forKey:@"shenfenzhenghao"];
    [dict setValue:_mobleTF.text forKey:@"phone"];
    [dict setValue:_nameTF.text forKey:@"xingming"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            NSDictionary *data = dict[@"data"];
            NSString *yinhangkaid = data[@"yinhangkaid"];
            
            CSBankCodeViewController *vc = [[CSBankCodeViewController alloc]init];
            vc.bankid = yinhangkaid;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//删除

- (IBAction)deleteClick:(id)sender {
    [SVProgressHUD showWithStatus:@"删除中..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appyinhangkadel];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_model.id forKey:@"id"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"删除成功"];
            [self.navigationController popViewControllerAnimated: YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
