//
//  CSBanListModel.h
//  CSChatIM
//
//  Created by Apple on 2021/3/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSBanListModel : NSObject
@property (nonatomic,copy) NSString *shenfenzhenghao;
@property (nonatomic,copy) NSString *userid;
@property (nonatomic,copy) NSString *id;
@property (nonatomic,copy) NSString *housiwei;
@property (nonatomic,copy) NSString *yhphone;
@property (nonatomic,copy) NSString *bankname;
@property (nonatomic,copy) NSString *yinhangkahao;
@property (nonatomic,copy) NSString *bankcode;
@property (nonatomic,copy) NSString *yhxingming;
@end

NS_ASSUME_NONNULL_END
