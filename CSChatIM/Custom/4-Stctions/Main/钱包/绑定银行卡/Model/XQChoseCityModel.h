//
//  XQChoseCityModel.h
//  相亲项目
//
//  Created by Apple on 2021/3/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface XQChoseCityModel : NSObject

@property (nonatomic,strong) NSString *name;



@property (nonatomic,strong) NSArray *cityList;

@property (nonatomic,strong) NSString *cityname;
@property (nonatomic,strong) NSString *cityid;

@property (nonatomic,strong) NSString *provinceid;
@property (nonatomic,strong) NSString *provincename;

@end

NS_ASSUME_NONNULL_END
