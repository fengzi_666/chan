//
//  CSBankModel.h
//  CSChatIM
//
//  Created by Apple on 2021/3/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSBankModel : NSObject
@property (nonatomic,copy) NSString *shenfenzhenghao;
@property (nonatomic,copy) NSString *yinhangkahao;
@property (nonatomic,copy) NSString *provinceid;
@property (nonatomic,copy) NSString *yinhangcode;
@property (nonatomic,copy) NSString *yinhangid;
@property (nonatomic,copy) NSString *yinhangname;
@property (nonatomic,copy) NSString *province;
@property (nonatomic,copy) NSString *provincecode;
@property (nonatomic,copy) NSString *cityid;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *citycode;
@property (nonatomic,copy) NSString *xingming;
@property (nonatomic,copy) NSString *yinhangphone;
@property (nonatomic,copy) NSString *yhxingming;
@property (nonatomic,copy) NSString *yhphone;
@property (nonatomic,copy) NSString *id;
@end

NS_ASSUME_NONNULL_END
