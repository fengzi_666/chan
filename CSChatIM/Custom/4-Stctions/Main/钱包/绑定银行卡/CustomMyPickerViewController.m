//
//  CustomMyPickerViewController.m
//  pickerText
//
//  Created by ios3 on 16/7/8.
//  Copyright © 2016年 ios3. All rights reserved.
//

#import "CustomMyPickerViewController.h"
#import "CustomMyPickerView.h"
@interface CustomMyPickerViewController ()

@end

@implementation CustomMyPickerViewController

- (instancetype)init
{
    if (self = [super init])
    {
        
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        CustomMyPickerView *view = [[CustomMyPickerView alloc] initWithComponentDataArray:@[@"1",@"2",@"3"] titleDataArray:@[@"3",@"2",@"1"]];
        [self.view addSubview:view];
        
        
        
        
    }
    return self;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
    [self dismissViewControllerAnimated:self completion:nil];
}
@end
