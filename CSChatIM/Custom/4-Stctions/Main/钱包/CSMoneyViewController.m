//
//  CSMoneyViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSMoneyViewController.h"
#import "CSMoneyListCell.h"
#import "CSMoneyListHeadView.h"
#import "CSMoneyDetailViewController.h"
#import "CSrechargeViewController.h"
#import "CSMoneyRecordViewController.h"
#import "CSWithdrawViewController.h"
#import "CSNewRechargeViewController.h"
#import "CSMainUserInfoModel.h"
#import "BaseshenghuoViewController.h"
#import "CSBindBankViewController.h"
#import "CSBankListViewController.h"
@interface CSMoneyViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) NSArray *imageArr;
@property (nonatomic ,strong)  CSMoneyListHeadView *headView;
@property (nonatomic ,strong) CSMainUserInfoModel *userModel;
@property (nonatomic ,copy) NSString *money;
@property (nonatomic ,copy) NSString *zhifubao;


@end
static NSString *cellid = @"cellid";
@implementation CSMoneyViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNav];
    [self LoadRequestData];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]}];
    self.stateBar.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav白色"] forBarMetrics:UIBarMetricsDefault];
}
// 控制器的view完全消失的时候调用
//- (void)viewDidDisappear:(BOOL)animated
// {
//     [super viewDidDisappear:animated];
//     if (@available(iOS 13.0, *)) {
//              UIView *statusBar = [[UIView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame];
//              statusBar.backgroundColor = [UIColor whiteColor];
//              [[UIApplication sharedApplication].keyWindow addSubview:statusBar];
//           }
//     else
//     {
//         UIView* stateView = [[UIView alloc] initWithFrame:CGRectMake(0, -20, KScreenWidth, 20)];
//         [self.navigationController.navigationBar addSubview:stateView];
//         stateView.backgroundColor = [UIColor colorWithHexString:@"#ff4848"];
//     }
// }
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"钱包";
    
    _titleArr = @[@"充值",@"提现",@"红包记录",@"银行卡"];
    _imageArr = @[@"充值",@"提现",@"红包记录",@"充值"];
    [self setNav];
    
    [self LoadTableView];
  
    //接收通知
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoadRequestData) name:@"publish" object:nil];
}

- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appuserinfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *testNum = [self formatDecimalNumber:[NSString stringWithFormat:@"%@",data[@"money"]]];

            _headView.moneyLable.text = [NSString stringWithFormat:@"%@",testNum];
            CSMainUserInfoModel *userModel = [[CSMainUserInfoModel alloc]initWithDic:data];
            _userModel = userModel;

            _headView.moneyLable.text = [NSString stringWithFormat:@"%@",testNum];
            _money = testNum;
            _zhifubao =data[@"zhifubao"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//转换钱类型
- (NSString *)formatDecimalNumber:(NSString *)string {
    if (!string || string.length == 0) {
        return string;
    }
    
    NSNumber *number = @([string doubleValue]);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterDecimalStyle;
    formatter.positiveFormat = @"0.00";
    
    NSString *amountString = [formatter stringFromNumber:number];
    return amountString;
}
- (void)LoadTableView
{
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSMoneyListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
    CSMoneyListHeadView *headView = [[CSMoneyListHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 140)];
    _headView = headView;
    [headView.detailBtn addTarget:self action:@selector(detailBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    _myTableView.tableHeaderView.height = 140;
    _myTableView.tableHeaderView = headView;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
 return _titleArr.count;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSMoneyListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
   
   
   
        cell.listTitle.text = _titleArr[indexPath.row];
        cell.listImage.image = [UIImage imageNamed:_imageArr[indexPath.row]];
    
    
    
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        if (indexPath.row==0) {
            CSNewRechargeViewController *vc = [[CSNewRechargeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
        if (indexPath.row==1) {
            CSWithdrawViewController *vc = [[CSWithdrawViewController alloc]init];
            vc.money = _money;
            vc.zhifubao =_zhifubao;
            vc.userModel = _userModel;
            [self.navigationController pushViewController:vc animated:YES];
        }
        if (indexPath.row==2) {
            CSMoneyRecordViewController *vc = [[CSMoneyRecordViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
        if (indexPath.row==3) {
            CSBankListViewController *vc = [[CSBankListViewController alloc]init];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
    
    
   
   
}
#pragma mark -- 钱包明细
- (void)detailBtnClick:(UIButton *)sender
{
    NSLog(@"11");
    CSMoneyDetailViewController *vc = [[CSMoneyDetailViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
