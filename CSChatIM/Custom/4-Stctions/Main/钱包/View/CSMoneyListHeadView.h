//
//  CSMoneyListHeadView.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMoneyListHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UILabel *numberLable;

@end

NS_ASSUME_NONNULL_END
