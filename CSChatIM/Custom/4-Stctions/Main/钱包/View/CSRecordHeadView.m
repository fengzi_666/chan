//
//  CSRecordHeadView.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import "CSRecordHeadView.h"

@implementation CSRecordHeadView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSRecordHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius = 35;
    }
    return self;
}
@end
