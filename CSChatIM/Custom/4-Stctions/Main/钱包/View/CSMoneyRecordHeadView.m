//
//  CSMoneyRecordHeadView.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import "CSMoneyRecordHeadView.h"

@implementation CSMoneyRecordHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSMoneyRecordHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.receiveBtn.layer.masksToBounds = YES;
        self.receiveBtn.layer.cornerRadius = 15;
        self.emitBtn.layer.masksToBounds = YES;
        self.emitBtn.layer.cornerRadius = 15;
    }
    return self;
}

- (IBAction)receivedBtnClick:(UIButton *)sender {
   
    if (sender.tag==1) {
        [sender setBackgroundColor: [UIColor colorWithHexString:@"#ffdfb1"]];
        [sender setTitleColor:[UIColor colorWithHexString:@"#ff4848"] forState:UIControlStateNormal];
        [self.emitBtn setBackgroundColor: [UIColor clearColor]];
        [self.emitBtn setTitleColor:[UIColor colorWithHexString:@"#ffdfb1"] forState:UIControlStateNormal];
    }
    if (sender.tag ==2) {
        [sender setBackgroundColor: [UIColor colorWithHexString:@"#ffdfb1"]];
        [sender setTitleColor:[UIColor colorWithHexString:@"#ff4848"] forState:UIControlStateNormal];
        [self.receiveBtn setBackgroundColor: [UIColor clearColor]];
        [self.receiveBtn setTitleColor:[UIColor colorWithHexString:@"#ffdfb1"] forState:UIControlStateNormal];
    }
    [self.delegate HeadBtnClick:sender];

}

@end
