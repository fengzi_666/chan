//
//  CSMoneyRecordHeadView.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class TXWorldTableViewCell;
@protocol CSMoneyRecordHeadViewDelegate <NSObject>

-(void)HeadBtnClick:(UIButton *)sender;

@end
@interface CSMoneyRecordHeadView : UIView
//收到
@property (weak, nonatomic) IBOutlet UIButton *receiveBtn;
//发出
@property (weak, nonatomic) IBOutlet UIButton *emitBtn;
@property (nonatomic, assign) id<CSMoneyRecordHeadViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
