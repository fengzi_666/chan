//
//  CSMoneyListCell.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMoneyListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *listImage;
@property (weak, nonatomic) IBOutlet UILabel *listTitle;

@end

NS_ASSUME_NONNULL_END
