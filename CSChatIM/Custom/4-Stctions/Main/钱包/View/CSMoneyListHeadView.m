//
//  CSMoneyListHeadView.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSMoneyListHeadView.h"

@implementation CSMoneyListHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSMoneyListHeadView" owner:nil options:nil][0];
        self.frame =frame;
        
        self.backgroundColor = [UIColor whiteColor];
    
       
    }
    return self;
}

@end
