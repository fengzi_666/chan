//
//  CSMoneyDetailListCell.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMoneyDetailListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *listTitle;
@property (weak, nonatomic) IBOutlet UILabel *listTimeLable;
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;

@end

NS_ASSUME_NONNULL_END
