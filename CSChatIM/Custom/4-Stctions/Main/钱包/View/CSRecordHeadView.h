//
//  CSRecordHeadView.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSRecordHeadView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
//总收到金额
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
//红包数量
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
//金额 收到/发出
@property (weak, nonatomic) IBOutlet UILabel *moneyHitLable;
//个人  收到/发出
@property (weak, nonatomic) IBOutlet UILabel *numberHItLable;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@end

NS_ASSUME_NONNULL_END
