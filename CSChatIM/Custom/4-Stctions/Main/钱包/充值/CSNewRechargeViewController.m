//
//  CSNewRechargeViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/3/10.
//

#import "CSNewRechargeViewController.h"
#import "CSRechargeHeadCell.h"
#import "CSSelectBanKViewController.h"
#import "CSBanListModel.h"
#import "CSBankCodeViewController.h"
@interface CSNewRechargeViewController ()<UITableViewDelegate,UITableViewDataSource,GridCollectionViewDelegate>
{
    UIAlertView*                _alertView;
}
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,copy) NSString *zongMoney;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,copy) NSString *selectID;
@property(nonatomic, copy) NSString *tnMode;
@property (nonatomic ,strong) CSBanListModel *seletModel;


@end
static NSString *headcell = @"headcellid";
@implementation CSNewRechargeViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self LoadRequestData];
   
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title=@"充值";
    [self setNav];
    self.view.backgroundColor = [UIColor whiteColor];
    _titleArr = [[NSMutableArray alloc]init];
    [self LoadTableView];
  
    [self LoadListRequestData];
    _selectID  =@"";
    
    _tnMode = @"mode=00";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoadRequestData) name:@"publish" object:nil];
    
    
    
}

- (void)LoadListRequestData
{
//    [SVProgressHUD showWithStatus:@"<#查询中#>"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_canshu];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    [dict setValue:<#searchBar.text#> forKey:@"<#zhanghao#>"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data = dict[@"data"];
            _titleArr  = data[@"list"];

            CGFloat  w  = KScreenWidth/3-10;
            
            [_myTableView reloadData];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appuserinfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *testNum = [self formatDecimalNumber:[NSString stringWithFormat:@"%@",data[@"money"]]];
            _zongMoney = testNum;
            [_myTableView reloadData];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//转换钱类型
- (NSString *)formatDecimalNumber:(NSString *)string {
    if (!string || string.length == 0) {
        return string;
    }
    
    NSNumber *number = @([string doubleValue]);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterDecimalStyle;
    formatter.positiveFormat = @"0.00";
    
    NSString *amountString = [formatter stringFromNumber:number];
    return amountString;
}
- (void)LoadTableView
{
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    self.myTableView.estimatedRowHeight = 60;
    self.myTableView.rowHeight = UITableViewAutomaticDimension;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSRechargeHeadCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:headcell];
    [self.view addSubview:_myTableView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    CSRechargeHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:headcell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.moneLable.text = _zongMoney;
    [cell setUpFrame:_titleArr];
    if (_seletModel.bankname.length>0) {
        cell.banLable.text = _seletModel.bankname;
        
        cell.bankCard.text = [NSString stringWithFormat:@"尾号%@",_seletModel.housiwei];
    }
    
    [cell.payBtn addTarget:self action:@selector(apyBtnClick)];
    cell.gridOneView.delegate = self;
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(event:)];
    [cell.bankView addGestureRecognizer:tapGesture];
//    cell.tagView.height = 100 *(_titleArr.count/2);
    return cell;

}
- (void)event:(UITapGestureRecognizer *)gesture
{
    NSLog(@"选择银行卡");
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appyinhangkalist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSArray *list = data[@"list"];
            if (list.count>0) {
                CSSelectBanKViewController *vc = [[CSSelectBanKViewController alloc]init];
                __weak typeof(self) weakSelf = self;
                vc.bankSelectBlock = ^(CSBanListModel * _Nonnull model) {
                    weakSelf.seletModel = model;
                    [_myTableView reloadData];
                };
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                [SVProgressHUD showInfoWithStatus:@"请先绑定银行卡"];
            }
           
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
    
}
- (void)apyBtnClick
{
    if ([_selectID isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请选择要充值的金额"];
        return;
    }
    if ([_seletModel.bankname isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请选择银行卡"];
        return;
    }
    [SVProgressHUD showWithStatus:@"验证码发送中..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_chongzhione];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_seletModel.id forKey:@"yinhangkaid"];
    [dict setValue:_selectID forKey:@"chongzhiid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            NSDictionary *data = dict[@"data"];
            NSString *yinhangkaid = data[@"orderid"];
            
            CSBankCodeViewController *vc = [[CSBankCodeViewController alloc]init];
            vc.bankid = yinhangkaid;
            vc.istype = @"1";
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
    
//    [self startNetWithURL:[NSURL URLWithString:@"http://192.168.3.237:8080/apphuifuzhifu/zhifu"]];
    
}

- (void)showAlertWait
{
    [self hideAlert];
    _alertView = [[UIAlertView alloc] initWithTitle:@"正在获取请稍后.." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
    [_alertView show];
    UIActivityIndicatorView* aiv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    aiv.center = CGPointMake(_alertView.frame.size.width / 2.0f - 15, _alertView.frame.size.height / 2.0f + 10 );
    [aiv startAnimating];
    [_alertView addSubview:aiv];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    _alertView = nil;
}

- (void)hideAlert
{
    if (_alertView != nil)
    {
        [_alertView dismissWithClickedButtonIndex:0 animated:NO];
        _alertView = nil;
    }
}
-(NSString *)convertToJsonData:(NSDictionary *)dict

{

    NSError *error;

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];

    NSString *jsonString;

    if (!jsonData) {

        NSLog(@"%@",error);

    }else{

        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];

    }

    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];

    NSRange range = {0,jsonString.length};

    //去掉字符串中的空格

    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];

    NSRange range2 = {0,mutStr.length};

    //去掉字符串中的换行符

    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];

    return mutStr;

}
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }

    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
- (void)gridCollectionView:(GridCollectionView *)gridCollectionView didSelected:(CustomCollectionViewCell *)cell {
    
  
        
        [gridCollectionView.dataArray enumerateObjectsUsingBlock:^(ActivityModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
            cell.indexPath.row == idx ? (model.isSelected = YES):(model.isSelected = NO);
            if (model.isSelected ==YES) {
                _selectID = [NSString stringWithFormat:@"%@", model.tagid];
                NSLog(@"选中了第%ld个 ---id为%@",cell.indexPath.row ,_selectID);
            }
                    }];

   
         
//            NSLog(@"选中了第%ld个 ---id为%@",cell.indexPath.row ,_selectID);

        
        [gridCollectionView LoadData];
        

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
