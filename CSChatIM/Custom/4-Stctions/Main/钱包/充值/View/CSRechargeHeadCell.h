//
//  CSRechargeHeadCell.h
//  CSChatIM
//
//  Created by Apple on 2021/3/10.
//

#import <UIKit/UIKit.h>
#import "GridCollectionView.h"
#import "CustomCollectionViewCell.h"
#import "TypeCellClass.h"
#import "UIView+SetRect.h"
#import "ActivityModel.h"
#import "BusinessCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSRechargeHeadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moneLable;
@property (weak, nonatomic) IBOutlet UIView *tagView;

@property (weak, nonatomic) IBOutlet UIButton *payBtn;
//充值说明
@property (weak, nonatomic) IBOutlet UILabel *descriptionLable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagH;
@property (weak, nonatomic) IBOutlet UILabel *banLable;

@property (weak, nonatomic) IBOutlet UIView *bankView;


@property (nonatomic, strong) UIView             *backOneView;
@property (nonatomic, strong) GridCollectionView *gridOneView;
@property (weak, nonatomic) IBOutlet UILabel *bankCard;
- (void)setUpFrame :(NSArray *)arr;
@end

NS_ASSUME_NONNULL_END
