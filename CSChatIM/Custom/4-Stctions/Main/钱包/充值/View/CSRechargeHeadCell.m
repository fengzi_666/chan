//
//  CSRechargeHeadCell.m
//  CSChatIM
//
//  Created by Apple on 2021/3/10.
//

#import "CSRechargeHeadCell.h"

typedef enum : NSUInteger {
    
    kBusinessOne = 20,
    kBusinessTwo,
    
} EBusinessTag;

@interface CSRechargeHeadCell () <GridCollectionViewDelegate>




@end
@implementation CSRechargeHeadCell
- (void)setUpFrame :(NSArray *)arr
{
    NSMutableArray *mulArray = [[NSMutableArray alloc] init];
    NSArray        *array    = arr;
    for (int i = 0; i<array.count; i++) {
        NSDictionary *dict = array[i];
        ActivityModel *model = [[ActivityModel alloc] init];
        model.typeName       = dict[@"name"];
        model.tagid       = dict[@"id"];
        [mulArray addObject:model];
    }
    self.gridOneView.dataArray           = mulArray;
    [self.gridOneView LoadData];
    [self.gridOneView setUpFrame];
    CGFloat h = 65*(arr.count/2);
    self.tagH.constant =h;
    self.gridOneView.height = h;
//    [self.gridOneView  mas_makeConstraints:^(MASConstraintMaker *make) {
//
//        make.height.mas_equalTo(h);
//
//    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _bankView.layer.borderWidth = 1;
    _bankView.layer.borderColor = [UIColor colorWithHexString:@"#eeeeee"].CGColor;
    self.gridOneView   = [[GridCollectionView alloc] init];
    self.gridOneView.contentInsets       = UIEdgeInsetsMake(0, 5, 0, 5);
    self.gridOneView.HorizontalGap       = 10;
    self.gridOneView.verticalGap         = 10;
    self.gridOneView.cellHeight          = 54;
    self.gridOneView.HorizontalCellCount = 3;

    self.gridOneView.tag                 = kBusinessOne;
    
    TypeCellClass *typeCell = [[TypeCellClass alloc] init];
    typeCell.className      = [BusinessCell class];
    typeCell.registID       = @"BusinessCell";
    
    self.gridOneView.registCell = typeCell;
    self.gridOneView.delegate   = self;
    [self.gridOneView setUpFrame];
    [self.tagView addSubview:self.gridOneView];
    [self.gridOneView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.tagView.mas_top);
        make.bottom.mas_equalTo(self.tagView.mas_bottom);
        make.left.mas_equalTo(self.tagView.mas_left);
        make.right.mas_equalTo(self.tagView.mas_right);
        
    }];
 
    
}
- (void)gridCollectionView:(GridCollectionView *)gridCollectionView didSelected:(CustomCollectionViewCell *)cell {
    
    if (gridCollectionView.tag == kBusinessOne) {
        
        [gridCollectionView.dataArray enumerateObjectsUsingBlock:^(ActivityModel *model, NSUInteger idx, BOOL * _Nonnull stop) {
            
            cell.indexPath.row == idx ? (model.isSelected = YES):(model.isSelected = NO);
        }];
        
        [gridCollectionView LoadData];
        
    } else if ( gridCollectionView.tag == kBusinessTwo) {
        
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
