//
//  CSSelectBanKCell.h
//  CSChatIM
//
//  Created by Apple on 2021/3/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSSelectBanKCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bankNameLable;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *cardLable;
@property (weak, nonatomic) IBOutlet UIImageView *selectImage;

@end

NS_ASSUME_NONNULL_END
