//
//  CSSelectBanKViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/3/25.
//

#import "CSSelectBanKViewController.h"
#import "anpaifanjuMainTransferVC.h"
#import "CSSelectBanKCell.h"
#import "CSGoupListModel.h"
#import "xinxipingtaiViewController.h"
#import "CSBanListModel.h"
static NSString *cellid = @"cellid";
@interface CSSelectBanKViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) NSIndexPath *index;
@property (nonatomic ,copy) NSString *selectID;
@property (nonatomic,assign) NSInteger pageno;
@property (nonatomic ,strong)  CSBanListModel *model;

@property V2TIMGroupInfo *groupInfo;
@end
@implementation CSSelectBanKViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"选择银行卡";
   
    
    _dataArr = [[NSMutableArray alloc]init];
    
    [self setNav];
    
    [self LoadTableView];
    
    [self LoadRequestData];

}
- (void)rightClick
{
    [SVProgressHUD showWithStatus:@"转让中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_zhuanrang];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
   
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"转让成功"];
            [self jumpVC];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//跳转指定控制器
- (void)jumpVC
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[xinxipingtaiViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appyinhangkalist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSArray *list = data[@"list"];
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            for (int i = 0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSBanListModel *model = [CSBanListModel mj_objectWithKeyValues:dic];
                [arr addObject:model];
            }
            _dataArr  = arr;
            if (_dataArr.count>0) {
                _myTableView.emptyView.hidden = YES;
            }
            
            [_myTableView reloadData];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadTableView
{
    
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSSelectBanKCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    //这种上拉刷新footer在tableview的底部
    [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
//    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        _pageno =1;
//        self.dataArr =[NSMutableArray array];
//        [self LoadRequestData];
//    }];
//    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        self.pageno ++;
//        [self LoadRequestData];
//    }];
    [_myTableView.mj_header beginRefreshing];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 75;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSSelectBanKCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSBanListModel *model = _dataArr[indexPath.row];
   
    cell.nameLable.text = model.yhxingming;
    cell.bankNameLable.text  = model.bankname;
    cell.cardLable.text  = [NSString stringWithFormat:@"尾号%@",model.housiwei];
    //
    
    return cell;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *foodView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 75)];
    UIButton *foodBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [foodBtn setTitle:@"确定" forState:UIControlStateNormal];
    foodBtn.frame = CGRectMake(12, 31, KScreenWidth-24, 44);
    [foodBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    foodBtn.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
    foodBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [foodBtn addTarget:self action:@selector(foodBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [foodView addSubview:foodBtn];
    return foodView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSBanListModel *model = _dataArr[indexPath.row];
    //之前选中的，取消选择
    CSSelectBanKCell *celled = [tableView cellForRowAtIndexPath:_index];
    
    celled.selectImage.image = [UIImage imageNamed:TUIKitResource(@"icon_contact_select_selected_disable")];
    //记录当前选中的位置索引
    _index = indexPath;
    //当前选择的打勾
    CSSelectBanKCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectImage.image = [UIImage imageNamed:TUIKitResource(@"icon_contact_select_selected")];
    _selectID = model.id;
    _model = model;
}
- (void)foodBtnClick
{
    [self.navigationController popViewControllerAnimated: YES];
    if (_bankSelectBlock) {
        self.bankSelectBlock(_model);
    }
}
@end

