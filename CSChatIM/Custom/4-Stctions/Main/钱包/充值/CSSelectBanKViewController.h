//
//  CSSelectBanKViewController.h
//  CSChatIM
//
//  Created by Apple on 2021/3/25.
//

#import "BaseViewController.h"
#import "CSBanListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^bankSelectBlock)(CSBanListModel *model);
@interface CSSelectBanKViewController : BaseViewController

@property (nonatomic, copy) bankSelectBlock bankSelectBlock;
@end

NS_ASSUME_NONNULL_END
