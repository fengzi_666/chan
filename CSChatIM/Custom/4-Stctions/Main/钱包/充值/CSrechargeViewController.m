//
//  TXrecharge ViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import "CSrechargeViewController.h"

@interface CSrechargeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UIView *bankView;
@property (weak, nonatomic) IBOutlet UILabel *bankLable;
@property (nonatomic, strong) UIButton * btn;
@property (nonatomic, strong) NSMutableArray *array;
@property (weak, nonatomic) IBOutlet UILabel *chongzhiLable;

@end

@implementation CSrechargeViewController
- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray arrayWithCapacity:0];
    }
    return _array;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"充值";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self LoadRequsetAPP];
    self.bankView.layer.borderWidth =1;
    self.bankView.layer.borderColor = [UIColor colorWithHexString:@"#eeeeee"].CGColor;
    [self LoadListRequestData];
    
}
- (void)LoadListRequestData
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_canshu];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];

    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data = dict[@"data"];
            NSArray *list  = data[@"list"];
            NSMutableArray *titleArr = [[NSMutableArray alloc]init];
            for (int i=0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                NSString *title = dic[@"name"];
                NSString *tagid = dic[@"id"];
                [titleArr addObject:title];
            }
            CGFloat  w  = KScreenWidth/3-10;
            CGFloat top =  200;
            for (int i = 0; i < titleArr.count; i++) {
                for (int j = 0; j < titleArr.count/2; j++) {
                    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        //            btn.backgroundColor = [UIColor blueColor];
                    [btn setTitle:[NSString stringWithFormat:@"%@", titleArr[i]] forState:UIControlStateNormal];
                    [btn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
                    btn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
                    btn.layer.borderWidth = 1;
                    btn.layer.borderColor = [UIColor colorWithHexString:@"#eeeeee"].CGColor;
                    btn.tag = (i + (3 * j) + 1) + 100;
                    [btn addTarget:self action:@selector(clickedBtnWith:) forControlEvents:UIControlEventTouchUpInside];
                    btn.frame = CGRectMake(10 + i * w+15, top+ j * 65, w-10, 50);
                    [self.view addSubview:btn];
                    _btn = btn;
                }
            }
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)clickedBtnWith:(UIButton *)btn{
    
    //多选
//        if (!btn.selected) {
//            [btn setBackgroundColor:[UIColor brownColor]];
//            [self.array addObject:[NSNumber numberWithInteger:btn.tag - 100]];
//        }else{
//            [btn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
//            if ([self.array containsObject:[NSNumber numberWithInteger:btn.tag - 100]]) {
//                [self.array removeObject:[NSNumber numberWithInteger:btn.tag - 100]];
//            }
//        }
//        btn.selected = !btn.selected;
//    NSLog(@"%@", _array.description);
//
    
    //单选
    if(_btn== btn) {
        //上次点击过的按钮，不做处理
       
    } else{
        //本次点击的按钮设为红色
        [btn setTitleColor:[UIColor colorWithHexString:@"#32965A"] forState:UIControlStateNormal];
        btn.layer.borderWidth=1;
        btn.layer.borderColor = [UIColor colorWithHexString:@"#32965A"].CGColor;
        //将上次点击过的按钮设为黑色
        [_btn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        _btn.layer.borderWidth=1;
        _btn.layer.borderColor = [UIColor colorWithHexString:@"#eeeeee"].CGColor;

        
    }
    _btn = btn;
    
    NSLog(@"点击了第%ld 个按钮", (long)btn.tag - 100);
}
- (void)LoadRequsetAPP
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apppeizhi];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *peizhi = data[@"peizhi"];
            NSString *chongzhishuoming = peizhi [@"chongzhishuoming"];
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[chongzhishuoming dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

            _chongzhiLable.attributedText =attrStr;
            _chongzhiLable.textColor = [UIColor colorWithHexString:@"#999999"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (IBAction)payBtnClick:(UIButton *)sender {
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.tzhcwl.cn/wechat/wap?mchid=1615277914735363&notify_url=http://121.43.132.235/apptengxun/huidiao&out_trade_no=1615284295395&total_fee=1&sign=D6D0E60195C9C904666DF07E7D32C012"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
