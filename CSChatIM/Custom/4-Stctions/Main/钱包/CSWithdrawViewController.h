//
//  CSWithdrawViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import "BaseViewController.h"
#import "CSMainUserInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSWithdrawViewController : BaseViewController
@property (nonatomic ,copy) NSString *money;
@property (nonatomic ,copy) NSString *zhifubao;
@property (nonatomic ,strong) CSMainUserInfoModel *userModel;
@end

NS_ASSUME_NONNULL_END
