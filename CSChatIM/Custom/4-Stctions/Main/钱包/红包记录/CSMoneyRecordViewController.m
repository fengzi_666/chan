//
//  CSMoneyRecordViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/19.
//

#import "CSMoneyRecordViewController.h"
#import "CSRecordTableViewCell.h"
#import "CSMoneyRecordHeadView.h"
#import "CSRecordHeadView.h"
#import "CSMoneyRecordListModel.h"
#import "CSGroupOpenRedVC.h"
#import "CSRedChatData.h"
@interface CSMoneyRecordViewController ()<UITableViewDelegate,UITableViewDataSource,CSMoneyRecordHeadViewDelegate>

@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,assign) NSInteger pageno;
@property (nonatomic ,strong) NSString *selectPage;
@property (nonatomic ,strong) CSRecordHeadView *headView;
@property (nonatomic ,strong) NSDictionary *headViewData;



@end
static NSString *cellid = @"cellid";
@implementation CSMoneyRecordViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImage *backImage = [[UIImage imageNamed:@"nav_bg"] resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    //设置导航栏背景图片
    [self.navigationController.navigationBar setBackgroundImage:backImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav白色"] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]}];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"红包记录";
   
    _selectPage = @"1";
    [self setNav];
    CSMoneyRecordHeadView *headView = [[CSMoneyRecordHeadView alloc]init];
    headView.delegate =self;
    [self.view addSubview:headView];
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    
    [self LoadTableView];
    CGFloat tabTop = 64;
    if (IS_PhoneXAll) {
        tabTop = 88;
    }
   
    
  
}

- (void)LoadRequstData:(NSString *)type
{
//    [SVProgressHUD showWithStatus:@"查询中"];
     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apphongbaojiLu];
     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
   
     [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
     [dict setValue:type forKey:@"leixing"];
    
     [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data  =dict[@"data"];
            _headViewData = data;
            NSArray *list =data[@"list"];
            NSMutableArray *dataArr = [[NSMutableArray alloc]init];
            for (int i= 0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSMoneyRecordListModel *model = [[CSMoneyRecordListModel alloc]initWithDic:dic];
                
                [dataArr addObject:model];
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno ==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                
                [self.myTableView.mj_footer resetNoMoreData];
            }
            else
            {
                [_dataArr addObjectsFromArray:dataArr];
                [self.myTableView.mj_footer endRefreshing];
            }
            if (dataArr.count==0) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            [self.myTableView.mj_footer endRefreshing];
        }
         [_myTableView reloadData];
      
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
}



- (void)viewWillDisappear:(BOOL)animated
{
    self.stateBar.backgroundColor = [UIColor whiteColor];
}
- (void)LoadTableView
{
    CGFloat tabTop = 64;
    if (IS_PhoneXAll) {
        tabTop = 88;
    }
    
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, KScreenWidth, KScreenHeight-tabTop-44) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSRecordTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    //这种上拉刷新footer在tableview的底部
   
    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        self.dataArr =[NSMutableArray array];
        [self LoadRequstData:_selectPage];
    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequstData:_selectPage];
    }];
    
    [_myTableView.mj_header beginRefreshing];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 200;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CSMoneyRecordListModel *model = _dataArr[indexPath.row];
    cell.nameLable.text = model.usernicheng;
    cell.timeLable.text = model.jieshudate;
    if ([_selectPage  isEqualToString:@"2"]) {
        cell.timeLable.text =model.createdate;
    }
    cell.moneyLable.text = model.onemoney;
    
    
    return cell;

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CSRecordHeadView *headView = [[CSRecordHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 200)];
    _headView = headView;
    if ([_selectPage isEqualToString:@"1"]) {
        _headView.moneyLable.text = [NSString stringWithFormat:@"%@",_headViewData[@"money"]];
        _headView.numberLabel.text = [NSString stringWithFormat:@"%@",_headViewData[@"shuliang"]];
        _headView.moneyHitLable.text = @"收到总金额";
        _headView.numberHItLable.text = @"收到数量";
    }
    if ([_selectPage isEqualToString:@"2"]) {
        _headView.moneyLable.text = [NSString stringWithFormat:@"%@",_headViewData[@"money"]];
        _headView.numberLabel.text = [NSString stringWithFormat:@"%@",_headViewData[@"shuliang"]];
        _headView.moneyHitLable.text = @"发出总金额";
        _headView.numberHItLable.text = @"发出数量";
    }
    NSString *login = [V2TIMManager sharedInstance].getLoginUser;
    [[V2TIMManager sharedInstance]getUsersInfo:@[login] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
        V2TIMUserFullInfo *userInfo = infoList.firstObject;
        [_headView.headImage sd_setImageWithURL:[NSURL URLWithString:userInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
        _headView.nameLable.text = userInfo.nickName;
        } fail:^(int code, NSString *desc) {
            
        }];
    
   
   
    return headView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSMoneyRecordListModel *model = _dataArr[indexPath.row];
    CSGroupOpenRedVC *vc = [[CSGroupOpenRedVC alloc]init];
    CSRedChatData *cellData = [[CSRedChatData alloc]init];
    cellData.hongbaoid =model.hongbaoid;
    vc.cellData = cellData;
    if ([_selectPage isEqualToString:@"2"]) {
        vc.istype =@"3";
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)HeadBtnClick:(UIButton *)sender
{
    if (sender.tag==1) {
        NSLog(@"111111111111");
        _selectPage = @"1";
        _pageno = 1;
        [_myTableView.mj_header beginRefreshing];
        
    }
    if (sender.tag==2) {
        NSLog(@"2222222222");
        _pageno =1;
        _selectPage = @"2";
        [_myTableView.mj_header beginRefreshing];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
