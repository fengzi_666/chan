//
//  CSMoneyRecordListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSMoneyRecordListModel : NSObject
@property (nonatomic ,copy) NSString *usernicheng;//发送人昵称
@property (nonatomic ,copy) NSString *jieshudate;
@property (nonatomic ,copy) NSString *onemoney;
@property (nonatomic ,copy) NSString *createdate;
@property (nonatomic ,copy) NSString *leixing;
@property (nonatomic ,copy) NSString *hongbaoid;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
