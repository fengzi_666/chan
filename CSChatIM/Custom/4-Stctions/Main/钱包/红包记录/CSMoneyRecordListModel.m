//
//  CSMoneyRecordListModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/14.
//

#import "CSMoneyRecordListModel.h"
#import "NSString+LFAdd.h"
@implementation CSMoneyRecordListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        NSString *money = [NSString formatDecimalNumber:[NSString stringWithFormat:@"%@",dic[@"onemoney"]]];
        self.onemoney =[NSString stringWithFormat:@"%@元",money];
      
//        self.usernicheng = dic[@"usernicheng"];
        self.jieshudate = [NSString stringWithFormat:@"%@",dic[@"jieshudate"]];
        self.hongbaoid = [NSString stringWithFormat:@"%@",dic[@"hongbaoid"]];
        self.createdate = dic[@"createdate"];
        self.leixing = [NSString stringWithFormat:@"%@",dic[@"leixing"]];
        if ([self.leixing isEqualToString:@"2"]) {
            self.usernicheng = [NSString stringWithFormat:@"【随机】%@",dic[@"usernicheng"]];
        }
        if ([self.leixing isEqualToString:@"1"]) {
            self.usernicheng = [NSString stringWithFormat:@"【个人】%@",dic[@"usernicheng"]];
        }
      
    }
    return self;
}
@end
