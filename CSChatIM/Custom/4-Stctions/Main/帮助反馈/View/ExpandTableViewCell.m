//
//  ExpandTableViewCell.m
//  ExpandFrameModel
//
//  Created by 栗子 on 2017/12/8.
//  Copyright © 2017年 http://www.cnblogs.com/Lrx-lizi/. All rights reserved.
//

#import "ExpandTableViewCell.h"
#import "ListFrameModel.h"

@implementation ExpandTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self addSubviews];
    }
    return self;
}
-(void)addSubviews{
    self.questionLB = [[UILabel alloc]init];
    self.questionLB.font = [UIFont boldSystemFontOfSize:15];
    self.questionLB.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.contentView addSubview:self.questionLB];
    self.questionLB.textAlignment = NSTextAlignmentLeft;
    
    self.arrowIV = [[UIImageView alloc]init];
    self.arrowIV.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.arrowIV];
    
    self.line = [[UIView alloc]init];
    [self.contentView addSubview:self.line];
    self.line.backgroundColor = [UIColor colorWithHexString:@"f4f4f4"];
    self.bgView = [[UIView alloc]init];
    [self.contentView addSubview:self.bgView];
    
    self.answerLB = [[UILabel alloc]init];
    self.bgView.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    self.bgView.layer.masksToBounds =YES;
    self.bgView.layer.cornerRadius =5;
    [self.bgView addSubview:self.answerLB];
    
    self.answerLB.font = [UIFont systemFontOfSize:12];
    self.answerLB.textAlignment = NSTextAlignmentLeft;
    self.answerLB.numberOfLines = 0;
    self.answerLB.textColor = [UIColor colorWithHexString:@"#999999"];
    self.line1 = [[UIView alloc]init];
    [self.contentView addSubview:self.line1];
    self.line1.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
   
    
}

-(void)setFrameModel:(ListFrameModel *)frameModel{
   
        
    
    
        
   
    _frameModel = frameModel;
    self.questionLB.frame = frameModel.questionFrame;
    self.arrowIV.frame    = frameModel.arrowFrame;
    self.line.frame       = frameModel.firstLineFrame;
    self.answerLB.frame   = frameModel.answerFrame;
    self.line1.frame      = frameModel.secondLineFrame;
    self.bgView.frame     =frameModel.bgviewFrame;
    // 设置段落为左右对齐 NSTextAlignmentJustified
    NSMutableParagraphStyle *par = [[NSMutableParagraphStyle alloc]init];
        par.alignment = NSTextAlignmentJustified;
        
    NSDictionary *dic = @{NSParagraphStyleAttributeName : par, NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)};
        
    // 配置富文本
    NSAttributedString *mstr = [[NSAttributedString alloc] initWithString:frameModel.listModel.fankui attributes:dic];
    // 自适应高度
    self.answerLB.attributedText = mstr;
    self.questionLB.text = [NSString stringWithFormat:@"%@",frameModel.listModel.bangzhu];
//    self.answerLB.text = [NSString stringWithFormat:@"%@",frameModel.listModel.fankui];
    if (frameModel.listModel.isSelected) {
        self.arrowIV.image = [UIImage imageNamed:@"反馈_下箭头"];
    }else{
        self.arrowIV.image = [UIImage imageNamed:@"反馈_右箭头"];
    }
    
}


@end
