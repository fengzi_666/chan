//
//  ListModel.h
//  ExpandFrameModel
//
//  Created by 栗子 on 2017/12/6.
//  Copyright © 2017年 http://www.cnblogs.com/Lrx-lizi/. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListModel : NSObject

@property (nonatomic, copy) NSString *bangzhu;
@property (nonatomic, copy) NSString *fankui;
@property (nonatomic, assign) BOOL isSelected;

- (instancetype)initWithDic:(NSDictionary *)dic;
@end
