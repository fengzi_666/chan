//
//  ListFrameModel.m
//  ExpandFrameModel
//
//  Created by 栗子 on 2017/12/6.
//  Copyright © 2017年 http://www.cnblogs.com/Lrx-lizi/. All rights reserved.
//

#import "ListFrameModel.h"
#import "Healp.h"
@implementation ListFrameModel

-(void)setListModel:(ListModel *)listModel{
    _listModel = listModel;
    
    self.questionFrame  = CGRectMake(10, 21, KScreenWidth-20, 15);
    self.arrowFrame     = CGRectMake(KScreenWidth-30, 23,11, 11);
    self.firstLineFrame = CGRectMake(12, 56, KScreenWidth-24, 1);
    self.unExpandCellHeight = 57;
    
    CGFloat answerH     = [Healp getStringHeight:listModel.fankui andFont:15 andWidth:KScreenWidth-44];
    self.answerFrame    = CGRectMake(10, 10, KScreenWidth-44, answerH);
    self.bgviewFrame    = CGRectMake(12, 70, KScreenWidth-24, answerH+30);
    self.secondLineFrame    = CGRectMake(12, CGRectGetMaxY(self.bgviewFrame)+10, KScreenWidth-24, 1);
    
    self.expandCellHeight = CGRectGetMaxY(self.secondLineFrame);
    
}

@end
