//
//  CSEditCodeViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSEditCodeViewController : BaseViewController
@property (nonatomic ,copy) NSString *codeUrl;
@property V2TIMUserFullInfo *userInfo;
@end

NS_ASSUME_NONNULL_END
