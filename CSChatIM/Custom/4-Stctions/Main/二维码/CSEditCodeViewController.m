//
//  CSEditCodeViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSEditCodeViewController.h"

#import "SGQRCode.h"
@interface CSEditCodeViewController ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *IDLable;
@property (weak, nonatomic) IBOutlet UIImageView *codeImage;

@end

@implementation CSEditCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    self.navigationItem.title = @"分享二维码";
    [self setNav];
    /// 常规二维码
    [_codeImage sd_setImageWithURL:[NSURL URLWithString:_codeUrl]];
    _headImage.layer.masksToBounds = YES;
    _headImage.layer.cornerRadius = 32.5;
    [_headImage sd_setImageWithURL:[NSURL URLWithString:_userInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    _nameLable.text = _userInfo.nickName;
    NSDictionary *dict = _userInfo.customInfo;
    NSString *idname =[[ NSString alloc]initWithData:dict[@"usercode"] encoding:NSUTF8StringEncoding];
    _IDLable.text = [NSString stringWithFormat:@"ID:  %@",idname];
      
   
    
}
//截图功能
-(UIImage *)captureImageFromView:(UIView *)view
{
    CGRect screenRect = [view bounds];
    UIGraphicsBeginImageContext(screenRect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (IBAction)saveImageClick:(UIButton *)sender {
    UIImage * image = [self captureImageFromView:self.view];
       
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}
/**
 保存图片操作之后就会调用
 */
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) { // 保存失败
        
        [SVProgressHUD showInfoWithStatus:@"保存失败"];
    } else { // 保存成功
        [SVProgressHUD showInfoWithStatus:@"保存成功"];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
