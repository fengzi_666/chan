//
//  CSRedCodeView.h
//  CSChatIM
//
//  Created by Apple on 2020/12/13.
//

#import <UIKit/UIKit.h>
typedef void (^CodeBlock)(UIButton* _Nullable sender);
NS_ASSUME_NONNULL_BEGIN

@interface CSRedCodeView : UIView
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIButton *quedingBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end

NS_ASSUME_NONNULL_END
