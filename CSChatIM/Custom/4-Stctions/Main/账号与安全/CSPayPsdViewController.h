//
//  CSPayPsdViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSPayPsdViewController : BaseViewController
@property (nonatomic ,copy) NSString *tokenpass;//手机唯一标识
@property (nonatomic ,copy) NSString *moble;//手机号
@property (nonatomic ,copy) NSString *istype;//是设置密码还是支付
@property (nonatomic ,copy) NSString *money;//金额
@property (nonatomic ,copy) NSString *number;//数量
@property (nonatomic ,copy) NSString *jieshoutxcode;//群id 个人id
@property (nonatomic ,copy) NSString *name;//红包名
@property (nonatomic ,copy) NSString *chatType;//类型 1.群 2.个人
//@property (nonatomic ,copy) NSString *chatType;//类型 1.群 2.个人
@end

NS_ASSUME_NONNULL_END
