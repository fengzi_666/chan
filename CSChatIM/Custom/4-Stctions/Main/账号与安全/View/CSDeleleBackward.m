//
//  CSDeleleBackward.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSDeleleBackward.h"

@implementation CSDeleleBackward

- (void)deleteBackward {
   // ！！！这里要调用super方法，要不然删不了东西
    [super deleteBackward];
    
    if ([self.yn_delegate respondsToSelector:@selector(ynTextFieldDeleteBackward:)]) {
        [self.yn_delegate ynTextFieldDeleteBackward:self];
    }
}


@end
