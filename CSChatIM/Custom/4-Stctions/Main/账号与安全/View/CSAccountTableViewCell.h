//
//  CSAccountTableViewCell.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSAccountTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@end

NS_ASSUME_NONNULL_END
