//
//  CSDeleleBackward.h
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSDeleleBackward;



@protocol YNTextFieldDelegate <NSObject>

- (void)ynTextFieldDeleteBackward:(CSDeleleBackward *)textField;

@end
@interface CSDeleleBackward : UITextField
@property (nonatomic, assign) id <YNTextFieldDelegate> yn_delegate;
@end

NS_ASSUME_NONNULL_END
