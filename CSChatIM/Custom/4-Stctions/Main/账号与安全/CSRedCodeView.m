//
//  CSRedCodeView.m
//  CSChatIM
//
//  Created by Apple on 2020/12/13.
//

#import "CSRedCodeView.h"

@implementation CSRedCodeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSRedCodeView" owner:nil options:nil][0];
        self.frame =frame;
        UIColor *color = [UIColor blackColor];
        self.backgroundColor = [color colorWithAlphaComponent:0.5];
       
    }
    return self;
}
@end
