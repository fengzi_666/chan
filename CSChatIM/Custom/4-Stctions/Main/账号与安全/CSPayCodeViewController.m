//
//  CSPayCodeViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSPayCodeViewController.h"
#import "CSPayPsdViewController.h"

@interface CSPayCodeViewController ()<UITextFieldDelegate>
//修改/设置支付密码
@property (weak, nonatomic) IBOutlet UILabel *hitPayLable;
//请输入以下信息进行修改/设置
@property (weak, nonatomic) IBOutlet UILabel *hitLable;
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@end

@implementation CSPayCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    self.mobileTF.delegate = self;
    
    self.codeTF.delegate = self;
//    _mobileTF.clearButtonMode = UITextFieldViewModeWhileEditing;
  
    _codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _mobileTF.text = [UserDefaultManager shareInstance].GetLoginLoginMobile;
    _mobileTF.enabled=NO;
   
    
    if ([_type isEqualToString:@"1"]) {
        self.hitPayLable.text  = @"设置支付密码";
        self.hitLable.text = @"请输入以下信息进行设置";
    }
    [_codeTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [self LoadRequestUserData];
}
- (void)LoadRequestUserData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appuserinfo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            _mobileTF.text = data[@"userphone"];
            
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
 - (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
     if (textField==_mobileTF) {
         return NO;
     }
     return YES;

 }
- (IBAction)codeClick:(UIButton *)sender {
    NSString *mobileRegex = TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    if ([_mobileTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号码"];
        return;
    }
    if (!_mobileTF.text || ![mobilePredicate evaluateWithObject:_mobileTF.text]){
        [SVProgressHUD showInfoWithStatus:@"请输入正确的手机号"];
        return;
    }
   
    [SVProgressHUD showWithStatus:@"发送中..."];
    [sender startCountDownTime:60 withCountDownBlock:^{
        NSLog(@"开始倒计时");
        //此处发送验证码等操作
        //................
        [self LoadRequestCode];
        
    }];
   
}
- (void)LoadRequestCode
{
//    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sheZhiJiaoPassSendSMS];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_mobileTF.text forKey:@"loginname"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}

//下一步

- (IBAction)nextBtnClick:(UIButton *)sender {
 
    if ([_codeTF.text isEqualToString:@""])
    {
        [SVProgressHUD showInfoWithStatus:@"验证码为空"];
        return;
    }
    if (_codeTF.text.length < 4)
    {
        [SVProgressHUD showInfoWithStatus:@"请输入正确的验证码"];
        return;
    }
//    [SVProgressHUD showWithStatus:@"<#查询中#>"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_jiaoYiPassJiaoYan];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_mobileTF.text forKey:@"loginname"];
    [dict setValue:_codeTF.text forKey:@"code"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *tokenpass =data[@"tokenpass"];
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            CSPayPsdViewController *vc = [[CSPayPsdViewController alloc]init];
            vc.tokenpass = tokenpass;
            vc.moble = _mobileTF.text;
            vc.istype = _type;
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
 
}

#pragma mark - textfield action
- (void)textFieldChange:(UITextField*)textField{
   
    if (textField == _mobileTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Phone textField:_mobileTF];
        
    }
    if (textField == _codeTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Code textField:_codeTF];
        
    }
   
  
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _mobileTF) {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Phone textField:textField replacementString:string];
    }
   
 
    else if (textField == _codeTF)
    {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Code textField:textField replacementString:string];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
