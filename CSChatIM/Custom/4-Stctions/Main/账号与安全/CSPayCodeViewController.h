//
//  CSPayCodeViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSPayCodeViewController : BaseViewController
@property (nonatomic ,copy) NSString *type;//聊天界面跳转

@end

NS_ASSUME_NONNULL_END
