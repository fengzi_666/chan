//
//  CSPayPsdViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSPayPsdViewController.h"
#import "VerifyCodeView.h"
#import "CSMainViewController.h"
#import "XMPayCodeView.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "SFHFKeychainUtils.h"
#import "liaotianshiViewController.h"
#import "CSRedCodeView.h"
#import "CSChatRedViewController.h"
#import "CSWithdrawViewController.h"
//#import "TXCharmRecordController.h"
@interface CSPayPsdViewController ()
@property (nonatomic ,copy) NSString *psdStr;
/// 支付密码输入框
@property (weak, nonatomic) XMPayCodeView *payCodeView;
@property (nonatomic ,strong)  CSRedCodeView *foodView;
@end

@implementation CSPayPsdViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNav];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]}];
    self.stateBar.backgroundColor = [UIColor colorWithHexString:@"#ff4848"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self LoadPayView];
    [self LoadUI];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;
   
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
}
- (void)LoadUI
{
    UILabel *hitLable = [[UILabel alloc]init];
    
    if ([_istype isEqualToString:@"1"]) {
        hitLable.text = @"请输入密码";
    }
    else
    {
        hitLable.text = @"设置支付密码";
    }
    hitLable.font = [UIFont boldSystemFontOfSize:30];
    hitLable.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.view addSubview:hitLable];
    [hitLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(13+64+StatusBarHeight);
        make.left.mas_equalTo(self.view).offset(37);

    }];
    UILabel *hitLable1 = [[UILabel alloc]init];
    if ([_istype isEqualToString:@"1"]) {
        hitLable1.text = @"请输入6位数字的支付密码";
    }
    else
    {
        hitLable1.text = @"请设置6位数字的支付密码";
    }
    
    hitLable1.font = [UIFont boldSystemFontOfSize:15];
    hitLable1.textColor = [UIColor colorWithHexString:@"#999999"];
    [self.view addSubview:hitLable1];
    [hitLable1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(hitLable.mas_bottom).offset(16);
        make.left.mas_equalTo(hitLable.mas_left);
//
    }];
    UIButton *ensureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [ensureBtn setTintColor:[UIColor whiteColor]];
    ensureBtn.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
    ensureBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [ensureBtn addTarget:self action:@selector(ensureBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ensureBtn];
    [ensureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(hitLable1.mas_bottom).offset(174);
        make.left.mas_equalTo(self.view).offset(55);
        make.right.mas_equalTo(self.view).offset(-55);
        make.height.mas_equalTo(45);
    }];
    
    
}

- (void)LoadPayView
{
    // 支付密码输入框
    XMPayCodeView *payCodeView = [[XMPayCodeView alloc]init];
    [self.view addSubview:payCodeView];
    [payCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(200);
        make.width.equalTo(self.view);
        make.height.equalTo(@55);
    }];
   
    payCodeView.secureTextEntry = YES;  //
    [payCodeView setPayBlock:^(NSString *payCode) {
        NSLog(@"payCode==%@",payCode);
        _psdStr = payCode;

    }];
   
    // 1秒后，让密码输入成为第一响应
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [payCodeView becomeKeyBoardFirstResponder];
    });
    
}
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    NSLog(@"11111");
//}
- (NSString*)UUID {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *userName = infoDict[@"CFBundleName"];
    NSString *serviceName = infoDict[@"CFBundleIdentifier"];
    NSString *UUID = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:serviceName error:nil];
    if (UUID.length==0) {
        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SFHFKeychainUtils storeUsername:userName andPassword:UUID forServiceName:serviceName updateExisting:1 error:nil];
        
    }
    
    return UUID;

}
//确定
- (void)ensureBtnClick
{
    
    if (_psdStr .length<6) {
        [SVProgressHUD showInfoWithStatus:@"请输入6位交易密码"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"设置中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateJiaoYiPass];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_moble forKey:@"loginname"];
    [dict setValue:_psdStr forKey:@"newpass"];
    [dict setValue:_tokenpass forKey:@"tokenpass"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];

        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            if ([_istype  isEqualToString:@"1"]) {
                [self jumpVC];
            }
            else if([_istype isEqualToString:@"3"])
            {
                [self jumpVC1];
            }
            else
            {
                NSInteger indexValue = (NSInteger)[[self.navigationController viewControllers] indexOfObject:self];
                if (indexValue > 2) {
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(indexValue - 2)] animated:YES];
                }
            }

        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
    
    
}

//跳转指定控制器
- (void)jumpVC
{

    NSInteger indexValue = (NSInteger)[[self.navigationController viewControllers] indexOfObject:self];
    if (indexValue > 2) {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(indexValue - 2)] animated:YES];
    }
}
- (void)jumpVC1
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSWithdrawViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
