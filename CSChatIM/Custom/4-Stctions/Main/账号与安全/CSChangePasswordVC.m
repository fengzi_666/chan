//
//  CSChangePasswordVC.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2021/3/1.
//

#import "CSChangePasswordVC.h"

@interface CSChangePasswordVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;

@property (weak, nonatomic) IBOutlet UITextField *psdTF;
@end

@implementation CSChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    self.mobileTF.delegate = self;
    self.psdTF.delegate = self;
    self.codeTF.delegate = self;
    _mobileTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _psdTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    NSString *str = [UserDefaultManager shareInstance].GetLoginLoginMobile;
    _mobileTF.text = str;
    _mobileTF.enabled=NO;
    [_mobileTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [_psdTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [_codeTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)codeClick:(UIButton *)sender {
    NSString *mobileRegex = TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    
    if ([_mobileTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号码"];
        return;
    }
    if (!_mobileTF.text || ![mobilePredicate evaluateWithObject:_mobileTF.text]){
        [SVProgressHUD showInfoWithStatus:@"请输入正确的手机号"];
        return;
    }
    [SVProgressHUD showWithStatus:@"发送中..."];
    [sender startCountDownTime:60 withCountDownBlock:^{
        NSLog(@"开始倒计时");
        //此处发送验证码等操作
        //................
        [self LoadRequestCode];
        
    }];
}

- (void)LoadRequestCode
{
    NSDictionary *dict = @{@"loginname":_mobileTF.text};
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_zhaoHuiSendSMS];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
        
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
- (IBAction)affirmBtnClick:(UIButton *)sender {
    NSString *mobileRegex = TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    
    if ([_mobileTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号码"];
        return;
    }
    if (!_mobileTF.text || ![mobilePredicate evaluateWithObject:_mobileTF.text]){
        [SVProgressHUD showInfoWithStatus:@"请输入正确的手机号"];
        return;
    }
    if ([_codeTF.text isEqualToString:@""])
    {
        [SVProgressHUD showInfoWithStatus:@"验证码为空"];
        return;
    }
    if (_codeTF.text.length < 4)
    {
        [SVProgressHUD showInfoWithStatus:@"请输入正确的验证码"];
        return;
    }
    if ([_psdTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"密码不能为空"];
        return;
    }
    if (_psdTF.text.length < TXWordsNum_PayPassword) {
        [SVProgressHUD showInfoWithStatus:@"密码不可以少于6位"];
        return;
    }
    if (_psdTF.text.length >TXWordsNum_Password) {
        [SVProgressHUD showInfoWithStatus:@"密码不可以多于12位"];
        return;
    }
    [self LoadRequestRegister];
}
- (void)LoadRequestRegister
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_mobileTF.text forKey:@"loginname"];
    [dict setValue:_psdTF.text forKey:@"newpass"];
    [dict setValue:_codeTF.text forKey:@"code"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_zhaoHuiPass];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
       
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark - textfield action
- (void)textFieldChange:(UITextField*)textField{
    NSString *mobileRegex =TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    if (textField == _mobileTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Phone textField:_mobileTF];
        
    }
    if (textField == _codeTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Code textField:_codeTF];
        
    }
    if (textField == _psdTF)
    {
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Password textField:_psdTF];
        if([mobilePredicate evaluateWithObject:_mobileTF.text] ) {
    
        }
    }
  
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _mobileTF) {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Phone textField:textField replacementString:string];
    }
    else if (textField == _psdTF)
    {
        NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL isRange = [jiaohuanqiValidate limitMaxLength:TXWordsNum_Password textField:textField replacementString:string];
        if (isRange) {
            textField.text = updatedString;
            [self textFieldChange:_psdTF];
            return NO;
        }
        [self textFieldChange:_psdTF];
        return isRange;
    }
 
    else if (textField == _codeTF)
    {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Code textField:textField replacementString:string];
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
