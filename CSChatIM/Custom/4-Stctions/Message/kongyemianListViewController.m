//
//  kongyemianListViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/1/9.
//

#import "kongyemianListViewController.h"

@interface kongyemianListViewController ()

@end

@implementation kongyemianListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
    self.tableView.frame =CGRectMake(0, 0, KScreenWidth, KScreenHeight-StatusBarHeight-44);
    @weakify(self)
    __weak typeof(self) weakSelf = self;
    [RACObserve(self.viewModel, dataList) subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if (weakSelf.viewModel.dataList.count>0) {
            weakSelf.tableView.emptyView.hidden = YES;
            

        }
        [weakSelf.tableView reloadData];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
