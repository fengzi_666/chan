//
//  CSContactListPicker.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSContactListPicker.h"

@implementation CSContactListPicker

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    self.backgroundColor = [UIColor redColor];

    return self;
}
@end
