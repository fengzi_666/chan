//
//  CSContactSelectController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSContactSelectController.h"

@interface CSContactSelectController ()

@end

@implementation CSContactSelectController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"选择联系人";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClick)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
   
        
}
- (void)backBarButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
