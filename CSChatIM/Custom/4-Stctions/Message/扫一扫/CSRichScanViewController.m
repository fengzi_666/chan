//
//  CSRichScanViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSRichScanViewController.h"
#import "SGQRCode.h"
#import "CSUserDataViewController.h"
#import "liaotianxinxiProfileController.h"
#import "THelper.h"
#import "CSGroupInfoVC.h"
#import "CSApplyAddFrindViewController.h"
@interface CSRichScanViewController ()
{
    SGQRCodeObtain *obtain;
}
@property (nonatomic, strong) SGQRCodeScanView *scanView;
@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, assign) BOOL stop;
@end

@implementation CSRichScanViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_stop) {
        [obtain startRunningWithBefore:nil completion:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.scanView addTimer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.scanView removeTimer];
}

- (void)dealloc {
    NSLog(@"WBQRCodeVC - dealloc");
    [self removeScanningView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"扫一扫";
    [self setNav];
    self.view.backgroundColor = [UIColor whiteColor];
    obtain = [SGQRCodeObtain QRCodeObtain];
    
    [self setupQRCodeScan];
    [self setupNavigationBar];
    [self.view addSubview:self.scanView];
    [self.view addSubview:self.promptLabel];
}

- (void)setupQRCodeScan {
  

    SGQRCodeObtainConfigure *configure = [SGQRCodeObtainConfigure QRCodeObtainConfigure];
    configure.openLog = YES;
    configure.rectOfInterest = CGRectMake(0.05, 0.2, 0.7, 0.6);
    // 这里只是提供了几种作为参考（共：13）；需什么类型添加什么类型即可
    NSArray *arr = @[AVMetadataObjectTypeQRCode,AVMetadataObjectTypeAztecCode];
    configure.metadataObjectTypes = arr;
    
    [obtain establishQRCodeObtainScanWithController:self configure:configure];
    [obtain startRunningWithBefore:^{
//        [MBProgressHUD SG_showMBProgressHUDWithModifyStyleMessage:@"正在加载..." toView:weakSelf.view];
    } completion:^{
//        [MBProgressHUD SG_hideHUDForView:weakSelf.view];
    }];
    __weak typeof(self) weakSelf = self;
    [obtain setBlockWithQRCodeObtainScanResult:^(SGQRCodeObtain *obtain, NSString *result) {
        if (result) {
            [obtain stopRunning];
            weakSelf.stop = YES;
            [obtain playSoundName:@"SGQRCode.bundle/sound.caf"];
            NSLog(@"%@",result);
            NSDictionary *dict = [weakSelf dictionaryWithJsonString:result];
            NSLog(@"%@",dict);
            NSString *leixing = [NSString stringWithFormat:@"%@",dict[@"leixing"]];
            if ( [leixing isEqualToString:@"1"]) {
                [[V2TIMManager sharedInstance] getFriendsInfo:@[dict[@"tengxuncode"]] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
                    V2TIMFriendInfoResult *result = resultList.firstObject;
                    if (result.relation == V2TIM_FRIEND_RELATION_TYPE_IN_MY_FRIEND_LIST || result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY) {
                            CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
                                vc.friendProfile = result.friendInfo;
                        vc.result = result;
                                [weakSelf.navigationController pushViewController:vc animated:YES];
                        
                    } else {
                        
                        [[V2TIMManager sharedInstance] getUsersInfo:@[dict[@"tengxuncode"]] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                            
                            liaotianxinxiProfileController *vc = [[liaotianxinxiProfileController alloc]init];
                                vc.userFullInfo = infoList.firstObject;
                                if ([vc.userFullInfo.userID isEqualToString:[[V2TIMManager sharedInstance] getLoginUser]]) {
                                    vc.actionType = PCA_NONE;
                                } else {
                                    vc.actionType = PCA_ADD_FRIEND;
                                    vc.type = @"1";
                                }
                            [weakSelf.navigationController pushViewController:vc animated:YES];
                            
                        } fail:^(int code, NSString *msg) {
                            [THelper makeToastError:code msg:msg];
                        }];
                    }
                } fail:^(int code, NSString *msg) {
                    [THelper makeToastError:code msg:msg];
                }];
            }
            
            if ([leixing isEqualToString:@"2"]) {
                [weakSelf LoadIsAddGroup:dict[@"qunid"] tengxuncode:dict[@"tengxuncode"]];
            }

        }
    }];
}
#pragma mark--  查询时候加入群聊
- (void)LoadIsAddGroup :(NSString *)groupid  tengxuncode:(NSString *)tengxuncode
{
    [SVProgressHUD showWithStatus:@"......"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appqunjianCe];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:groupid forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        [SVProgressHUD dismiss];
        NSDictionary *data = dict[@"data"];
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD dismiss];
            NSString *state = [NSString stringWithFormat:@"%@",data[@"state"]];
            if ([state isEqualToString:@"1"]) {
                CSGroupInfoVC *vc = [[CSGroupInfoVC alloc]init];
                vc.groupId = tengxuncode;
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
//
                CSApplyAddFrindViewController *vc = [[CSApplyAddFrindViewController alloc]init];
                vc.isGroup = @"1";
                vc.groupid =groupid;
                [self.navigationController pushViewController:vc animated:YES];
            }
           
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
   if (jsonString == nil) {
       return nil;
   }

   NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
   NSError *err;
   NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                       options:NSJSONReadingMutableContainers
                                                         error:&err];
   if(err)
   {
       NSLog(@"json解析失败：%@",err);
       return nil;
   }
   return dic;
}
- (void)setupNavigationBar {
    self.navigationItem.title = @"扫一扫";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"相册" style:(UIBarButtonItemStyleDone) target:self action:@selector(rightBarButtonItenAction)];
}

- (void)rightBarButtonItenAction {
    __weak typeof(self) weakSelf = self;

    [obtain establishAuthorizationQRCodeObtainAlbumWithController:nil];
    if (obtain.isPHAuthorization == YES) {
        [self.scanView removeTimer];
    }
    [obtain setBlockWithQRCodeObtainAlbumDidCancelImagePickerController:^(SGQRCodeObtain *obtain) {
        [weakSelf.view addSubview:weakSelf.scanView];
    }];
    [obtain setBlockWithQRCodeObtainAlbumResult:^(SGQRCodeObtain *obtain, NSString *result) {
        if (result == nil) {
            NSLog(@"暂未识别出二维码");
        } else {
            if (result ) {
                [obtain stopRunning];
                weakSelf.stop = YES;
                [obtain playSoundName:@"SGQRCode.bundle/sound.caf"];
                NSLog(@"%@",result);
                NSDictionary *dict = [weakSelf dictionaryWithJsonString:result];
                NSLog(@"%@",dict);
                NSString *leixing = [NSString stringWithFormat:@"%@",dict[@"leixing"]];
                if ( [leixing isEqualToString:@"1"]) {
                    NSString *tengxuncode =dict[@"tengxuncode"];
                    [[V2TIMManager sharedInstance] getFriendsInfo:@[tengxuncode] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
                        V2TIMFriendInfoResult *result = resultList.firstObject;
                        if (result.relation == V2TIM_FRIEND_RELATION_TYPE_IN_MY_FRIEND_LIST || result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY) {
                                CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
                                    vc.friendProfile = result.friendInfo;
                            vc.result = result;
                                    [weakSelf.navigationController pushViewController:vc animated:YES];
                            
                        } else {
                            
                            [[V2TIMManager sharedInstance] getUsersInfo:@[dict[@"tengxuncode"]] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                                
                                liaotianxinxiProfileController *vc = [[liaotianxinxiProfileController alloc]init];
                                    vc.userFullInfo = infoList.firstObject;
                                    if ([vc.userFullInfo.userID isEqualToString:[[V2TIMManager sharedInstance] getLoginUser]]) {
                                        vc.actionType = PCA_NONE;
                                    } else {
                                        vc.actionType = PCA_ADD_FRIEND;
                                        vc.type = @"1";
                                    }
                                [weakSelf.navigationController pushViewController:vc animated:YES];
                                
                            } fail:^(int code, NSString *msg) {
                                [THelper makeToastError:code msg:msg];
                            }];
                        }
                    } fail:^(int code, NSString *msg) {
                        [THelper makeToastError:code msg:msg];
                    }];
                }
                
                if ([leixing isEqualToString:@"2"]) {
                    [weakSelf LoadIsAddGroup:dict[@"qunid"] tengxuncode:dict[@"tengxuncode"]];
                }

            } else {
//                ScanSuccessJumpVC *jumpVC = [[ScanSuccessJumpVC alloc] init];
//                jumpVC.comeFromVC = ScanSuccessJumpComeFromWB;
//                jumpVC.jump_bar_code = result;
//                [weakSelf.navigationController pushViewController:jumpVC animated:YES];
            }
        }
    }];
}

- (SGQRCodeScanView *)scanView {
    if (!_scanView) {
        _scanView = [[SGQRCodeScanView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        // 静态库加载 bundle 里面的资源使用 SGQRCode.bundle/QRCodeScanLineGrid
        // 动态库加载直接使用 QRCodeScanLineGrid
        _scanView.scanImageName = @"SGQRCode.bundle/QRCodeScanLineGrid";
        _scanView.scanAnimationStyle = ScanAnimationStyleGrid;
        _scanView.cornerLocation = CornerLoactionOutside;
        _scanView.cornerColor = [UIColor orangeColor];
    }
    return _scanView;
}
- (void)removeScanningView {
    [self.scanView removeTimer];
    [self.scanView removeFromSuperview];
    self.scanView = nil;
}

- (UILabel *)promptLabel {
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc] init];
        _promptLabel.backgroundColor = [UIColor clearColor];
        CGFloat promptLabelX = 0;
        CGFloat promptLabelY = 0.73 * self.view.frame.size.height;
        CGFloat promptLabelW = self.view.frame.size.width;
        CGFloat promptLabelH = 25;
        _promptLabel.frame = CGRectMake(promptLabelX, promptLabelY, promptLabelW, promptLabelH);
        _promptLabel.textAlignment = NSTextAlignmentCenter;
        _promptLabel.font = [UIFont boldSystemFontOfSize:13.0];
        _promptLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _promptLabel.text = @"将二维码/条码放入框内, 即可自动扫描";
    }
    return _promptLabel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
