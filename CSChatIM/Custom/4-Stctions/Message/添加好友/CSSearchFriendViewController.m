//
//  CSSearchFriendViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSSearchFriendViewController.h"
#import "CSSearchTableViewCell.h"
#import <SDWebImage/SDWebImage.h>
#import "V2TIMManager.h"
#import "CSApplyAddFrindViewController.h"
#import "CSAddFriendListModel.h"
#import "CSAddGoupListModel.h"
#import "CSGroupInfoVC.h"
@interface CSSearchFriendViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic,assign) NSInteger pageno;
@property (nonatomic,assign) NSInteger pageno1;
//是否加入群聊
@property (nonatomic ,copy) NSString *state;
@property (nonatomic ,copy) NSString *searchStr;

@end
static NSString *cellid = @"cellid";
@implementation CSSearchFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor  = [UIColor whiteColor];
    
    _listArr = [[NSMutableArray alloc]init];
    [self setNav];
    
    [self loadSearcView];
    
    [self LoadTableView];
}
- (void)loadSearcView
{
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 52)];
    myView.backgroundColor = [UIColor whiteColor];
    myView.userInteractionEnabled = YES;
    [self.view addSubview:myView];
    UIView *searchView = [[UIView alloc]init];
    searchView.backgroundColor = [UIColor whiteColor];
    searchView.layer.masksToBounds = YES;
    searchView.layer.cornerRadius = 5;
    searchView.userInteractionEnabled = YES;
    [myView addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(3);
        make.height.mas_equalTo(32);
        make.left.mas_equalTo(myView.mas_left).offset(12);
        make.right.mas_equalTo(myView.mas_right).offset(-12);
    }];
   
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    searchBar.frame = CGRectMake(0, 0, KScreenWidth-24, 32); // 设置位置和大小
//    oneSearchBar.keyboardType = UIKeyboardTypeEmailAddress; // 设置弹出键盘的类型
    [searchBar setImage:[UIImage imageNamed:@"search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    if (@available(iOS 13.0, *)) {
        UITextField *txfSearchField = (UITextField*)searchBar.searchTextField;
        txfSearchField.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    } else {
        // Fallback on earlier versions
        NSArray *subviewArr = self.view.subviews;
        for(int i = 0; i < subviewArr.count ; i++) {
            UIView *viewSub = [subviewArr objectAtIndex:i];
            NSArray *arrSub = viewSub.subviews;
            for (int j = 0; j < arrSub.count ; j ++) {
                id tempId = [arrSub objectAtIndex:j];
                if([tempId isKindOfClass:[UITextField class]]) {
                    UITextField *searchFiled = (UITextField *)tempId;
                    searchFiled.font = [UIFont systemFontOfSize:14];
                }
            }
        }
      
     
       
    }
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault; // 设置UISearchBar的样式
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
//    searchBar.showsCancelButton = YES;
//    searchBar.tintColor = [UIColor redColor]; // 设置UISearchBar的颜色 使用clearColor就是去掉背景
    searchBar.placeholder = @"对方账号"; // 设置提示文字
    [searchView addSubview:searchBar];
    
}
- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 52, KScreenWidth, KScreenHeight-52) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSSearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    
    [self.view addSubview:_myTableView];
//    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        _pageno =1;
//        self.dataArr =[NSMutableArray array];
//        [self LoadRequstData];
//    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
       
        if (_segIndex ==1) {
            //查找群
            self.pageno ++;
            [self LoadRequestSearchGroup:_searchStr];
        }
        else
        {
            self.pageno1 ++;
            //查找用户
            [self LoadRequestSearch:_searchStr];
        }
        
    }];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CSSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (_segIndex==1) {
        CSAddGoupListModel *info = _listArr[indexPath.row];
        NSLog(@"%@",info.qunpic);
        [cell.headImage sd_setImageWithURL:[NSURL URLWithString:info.qunpic] placeholderImage:[UIImage imageNamed:@"头像占位"]];
        
        cell.nameLable.text = info.qunming;
    }else
    {
        CSAddFriendListModel *info = _listArr[indexPath.row];
        NSLog(@"%@",info.touxiang);
        [cell.headImage sd_setImageWithURL:[NSURL URLWithString:info.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
        
        cell.nameLable.text = info.nicheng;
    }
  
   
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_segIndex==1) {
        CSAddGoupListModel *model = _listArr[indexPath.row];
        [self LoadIsAddGroup:model.listid idx:indexPath];
        
        
    }
    else
    {
        CSAddFriendListModel *info = _listArr[indexPath.row];
        CSApplyAddFrindViewController *vc = [[CSApplyAddFrindViewController alloc]init];
        vc.info = info;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
    
}
#pragma mark--  查询是否加入群聊
- (void)LoadIsAddGroup :(NSString *)groupid idx:(NSIndexPath *)index
{
    [SVProgressHUD showWithStatus:@"......"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appqunjianCe];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:groupid forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        [SVProgressHUD dismiss];
        NSDictionary *data = dict[@"data"];
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD dismiss];
            _state = [NSString stringWithFormat:@"%@",data[@"state"]];
            CSAddGoupListModel *model = _listArr[index.row];
            if ([_state isEqualToString:@"1"]) {
                CSGroupInfoVC *vc = [[CSGroupInfoVC alloc]init];
                vc.groupId = model.txquncode;
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                
                CSApplyAddFrindViewController *vc = [[CSApplyAddFrindViewController alloc]init];
                vc.groupModel = model;
                vc.isGroup = @"1";
                vc.groupid = model.listid;
                [self.navigationController pushViewController:vc animated:YES];
            }
           
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    _searchStr =searchBar.text;
    if (_segIndex ==1) {
        //查找群
        _pageno =1;
        [self LoadRequestSearchGroup:_searchStr];
    }
    else
    {
        //查找用户
        _pageno1 =1;
        [self LoadRequestSearch:_searchStr];
    }
   
//    NSString *inputStr = searchBar.text ;
//    NSLog(@"serach==== %@", inputStr);
//    [[V2TIMManager sharedInstance] getUsersInfo:@[inputStr] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
//        NSLog(@"%@",infoList);
//        _listArr =infoList;
//        [_myTableView reloadData];
//    } fail:^(int code, NSString *msg) {
//
//    }];
}
- (void)LoadRequestSearchGroup :(NSString *)searchBar
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunlist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString *inputStr = searchBar ;
    [dict setValue:inputStr forKey:@"quncode"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
       
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data =dict[@"data"];
            NSArray *list = data[@"list"];
            NSMutableArray *arr = [NSMutableArray array];
            for (int i=0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSAddGoupListModel *model = [[CSAddGoupListModel alloc]initWithDic:dic];
                [arr addObject:model];
            }
            if (_pageno==1) {
                [_listArr removeAllObjects];
                [_listArr addObjectsFromArray:arr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (_listArr.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (list.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_listArr addObjectsFromArray:arr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
        }
        else
        {
            
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
        [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [_myTableView.mj_footer endRefreshing];
    }];
}
- (void)LoadRequestSearch :(NSString *)searchBar
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_tianjialist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString *inputStr = searchBar ;
    [dict setValue:inputStr forKey:@"zhanghao"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno1] forKey:@"pageNo"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data =dict[@"data"];
            NSArray *list = data[@"list"];
            NSMutableArray *arr = [NSMutableArray array];
            for (int i=0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSAddFriendListModel *model = [[CSAddFriendListModel alloc]initWithDic:dic];
                [arr addObject:model];
            }
            if (_pageno1==1) {
                [_listArr removeAllObjects];
                [_listArr addObjectsFromArray:arr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (_listArr.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (list.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_listArr addObjectsFromArray:arr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
        }
        else
        {
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
        [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [_myTableView.mj_footer endRefreshing];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
