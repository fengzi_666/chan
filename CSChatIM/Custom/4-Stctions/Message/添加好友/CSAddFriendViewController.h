//
//  CSAddFriendViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/25.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSAddFriendViewController : BaseViewController
@property (nonatomic,assign)NSInteger segIndex;
@end

NS_ASSUME_NONNULL_END
