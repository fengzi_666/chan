//
//  CSApplyAddFrindViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSApplyAddFrindViewController.h"
#import "CSAddFrindTextViewController.h"
#import "liaotianshiViewController.h"
@interface CSApplyAddFrindViewController ()
@property (weak, nonatomic) IBOutlet UIButton *addBtnClicl;
@property (nonatomic ,copy) NSString *qunid;
@property (nonatomic ,copy) NSString *quncode;

@end

@implementation CSApplyAddFrindViewController
{
    V2TIMUserFullInfo *_userFullInfo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    
    [self setNav];
    
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = 31;
    
    if ([_isGroup isEqualToString:@"1"]) {
        self.nameLable.text = _groupModel.qunming;
        self.synopsis.text = [NSString stringWithFormat:@"公告:  %@",_groupModel.gonggao];
        self.IDLable.text = [NSString stringWithFormat:@"ID:  %@",_groupModel.txquncode];
        [self.headImage sd_setImageWithURL:[NSURL URLWithString:_groupModel.qunpic] placeholderImage:[UIImage imageNamed:@"头像占位"]];
        [self.addBtnClicl setTitle:@"申请加入群聊" forState:UIControlStateNormal];
        self.navigationItem.title = @"群详情";
        [self LoadRequestCodeData];
    }
    else
    {
        self.nameLable.text = _info.nicheng;
        self.synopsis.text = [NSString stringWithFormat:@"个性签名:  %@",_info.gexingqianming];
        self.IDLable.text = [NSString stringWithFormat:@"ID:  %@",_info.tengxuncode];
        [self.headImage sd_setImageWithURL:[NSURL URLWithString:_info.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
        self.navigationItem.title = @"用户详情";
        [self LoadRequestData];
        
    }
}
- (void)LoadRequestCodeData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunerweima];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupid forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data=dict[@"data"];
            self.nameLable.text = data[@"qunming"];
            self.synopsis.text = [NSString stringWithFormat:@"公告:  %@",data[@"gonggao"]];
            self.IDLable.text = [NSString stringWithFormat:@"ID:  %@",data[@"quncode"]];
            [self.headImage sd_setImageWithURL:[NSURL URLWithString:data[@"qunerweima"]] placeholderImage:[UIImage imageNamed:@"头像占位"]];
            _qunid =[NSString stringWithFormat:@"%@",data[@"qunid"]];
            _quncode =data[@"txquncode"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadRequestData
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apphaoyou];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_info.listid forKey:@"haoyouid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data = dict[@"data"];
            NSDictionary *dic = data[@"haoyou"];
            NSString *isFrind = [NSString stringWithFormat:@"%@",data[@"shifouhaoyou"]];
            self.IDLable.text = [NSString stringWithFormat:@"ID:  %@",dic[@"usercode"]];
            if ([isFrind isEqualToString:@"1"]) {
                [self.addBtnClicl setTitle:@"聊天" forState:UIControlStateNormal];
            }
        }
        else
        {

        }
    } error:^(NSString *error) {

    }];
}
//申请添加好友
- (IBAction)addFriendClick:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:@"聊天"]) {
        TUIConversationCellData *data = [[TUIConversationCellData alloc] init];
        data.conversationID = [NSString stringWithFormat:@"c2c_%@",self.info.tengxuncode];
        data.userID = self.info.tengxuncode;
        data.title = self.info.nicheng;
        liaotianshiViewController *chat = [[liaotianshiViewController alloc] init];
        chat.conversationData = data;
        [self.navigationController pushViewController:chat animated:YES];
        
    }
    else
    {
        CSAddFrindTextViewController *vc = [[CSAddFrindTextViewController alloc]init];
        //判断是群还是用户
        if ([_isGroup isEqualToString:@"1"]) {
            vc.listid = _qunid;
            vc.gourpid = _quncode;
            vc.isGroup = @"1";
            vc.navigationItem.title =@"申请加入群组";
        }
        else
        {
            vc.listid = _info.listid;
            vc.navigationItem.title =@"申请加好友";
        }
        [self.navigationController pushViewController:vc animated:YES];
       
      
    }
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
