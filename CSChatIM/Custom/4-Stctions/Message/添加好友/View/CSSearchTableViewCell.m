//
//  CSSearchTableViewCell.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSSearchTableViewCell.h"

@implementation CSSearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = 22.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
