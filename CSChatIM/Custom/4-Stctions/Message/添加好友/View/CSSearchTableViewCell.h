//
//  CSSearchTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@end

NS_ASSUME_NONNULL_END
