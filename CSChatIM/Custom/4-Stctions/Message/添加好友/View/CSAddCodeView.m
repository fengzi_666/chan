//
//  CSAddCodeView.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "CSAddCodeView.h"

@implementation CSAddCodeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSAddCodeView" owner:nil options:nil][0];
        self.frame =frame;
       
}
    return self;
}
@end
