//
//  CSAddGoupListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSAddGoupListModel : NSObject
@property (nonatomic ,copy) NSString *jinyan;
@property (nonatomic ,copy) NSString *qunerweima;
@property (nonatomic ,copy) NSString *qunpic;
@property (nonatomic ,copy) NSString *listid;
@property (nonatomic ,copy) NSString *txquncode;
@property (nonatomic ,copy) NSString *createdate;
@property (nonatomic ,copy) NSString *gonggao;
@property (nonatomic ,copy) NSString *quncode;
@property (nonatomic ,copy) NSString *qunming;

- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
