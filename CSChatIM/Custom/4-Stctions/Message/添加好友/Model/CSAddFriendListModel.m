//
//  CSAddFriendListModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import "CSAddFriendListModel.h"

@implementation CSAddFriendListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.tengxuncode = dic[@"tengxuncode"];
        self.touxiang = dic[@"touxiang"];
        self.erweima = dic[@"erweima"];
        self.listid = dic[@"id"];
        self.createdate = dic[@"createdate"];
        self.nicheng = dic[@"nicheng"];
        self.usercode = dic[@"usercode"];
        self.money = [NSString stringWithFormat:@"%@",dic[@"money"]];
        self.userphone = dic[@"userphone"];
        self.gexingqianming = dic[@"gexingqianming"];
    }
    return self;
}
@end
