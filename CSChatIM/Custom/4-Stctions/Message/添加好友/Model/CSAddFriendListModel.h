//
//  CSAddFriendListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSAddFriendListModel : NSObject
@property (nonatomic ,copy) NSString *tengxuncode;

@property (nonatomic ,copy) NSString *touxiang;
@property (nonatomic ,copy) NSString *erweima;
@property (nonatomic ,copy) NSString *listid;
@property (nonatomic ,copy) NSString *createdate;
@property (nonatomic ,copy) NSString *nicheng;
@property (nonatomic ,copy) NSString *usercode;
@property (nonatomic ,copy) NSString *money;
@property (nonatomic ,copy) NSString *userphone;
@property (nonatomic ,copy) NSString *gexingqianming;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
