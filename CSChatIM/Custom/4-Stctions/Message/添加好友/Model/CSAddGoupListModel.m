//
//  CSAddGoupListModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import "CSAddGoupListModel.h"

@implementation CSAddGoupListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.jinyan = [NSString stringWithFormat:@"%@",dic[@"tengxuncode"]];
        self.qunerweima = dic[@"qunerweima"];
        self.qunpic = dic[@"qunpic"];
        self.listid = dic[@"id"];
        self.txquncode = dic[@"txquncode"];
        self.createdate = dic[@"createdate"];
        self.gonggao = dic[@"gonggao"];
        self.quncode = [NSString stringWithFormat:@"%@",dic[@"quncode"]];
        self.qunming = dic[@"qunming"];
     
    }
    return self;
}
@end
