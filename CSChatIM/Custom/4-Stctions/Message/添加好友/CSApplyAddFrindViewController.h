//
//  CSApplyAddFrindViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "BaseViewController.h"
#import "CSAddFriendListModel.h"
#import "CSAddGoupListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSApplyAddFrindViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *synopsis;

@property (weak, nonatomic) IBOutlet UILabel *IDLable;
@property (nonatomic ,strong) CSAddFriendListModel *info;
@property (nonatomic ,strong) CSAddGoupListModel *groupModel;
@property (nonatomic ,copy) NSString *groupid;
@property (nonatomic ,copy) NSString *isGroup;


//1.会话进入
@property (nonatomic ,copy) NSString *isChat;
@end

NS_ASSUME_NONNULL_END
