//
//  CSAddFriendViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/25.
//

#import "CSAddFriendViewController.h"
#import "CSAddCodeView.h"
#import "CSSearchFriendViewController.h"
#import "CSRichScanViewController.h"
@interface CSAddFriendViewController ()<UISearchBarDelegate>


@end

@implementation CSAddFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [self LoadSegment];
    
    [self loadSearcView];
    
    CSAddCodeView *codeView = [[CSAddCodeView alloc]init];
    [self.view addSubview:codeView];
    [codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(62);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(65);
    }];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(codeTop:)];
    [codeView addGestureRecognizer:tapGesture];
    
}
- (void)loadSearcView
{
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 52)];
    myView.backgroundColor = [UIColor whiteColor];
    myView.userInteractionEnabled = YES;
    [self.view addSubview:myView];
    UIView *searchView = [[UIView alloc]init];
    searchView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    searchView.layer.masksToBounds = YES;
    searchView.layer.cornerRadius = 5;
    searchView.userInteractionEnabled = YES;
    [myView addSubview:searchView];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchTop:)];
    [searchView addGestureRecognizer:tapGesture];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(3);
        make.height.mas_equalTo(32);
        make.left.mas_equalTo(myView.mas_left).offset(12);
        make.right.mas_equalTo(myView.mas_right).offset(-12);
    }];
    UIImageView *searchImage = [[UIImageView alloc]init];
    searchImage.image = [UIImage imageNamed:@"search"];
    [searchView addSubview:searchImage];
    [searchImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(14, 14));
        make.centerY.mas_equalTo(searchView.mas_centerY);
    }];
    
    UILabel *searchLable = [[UILabel alloc]init];
    searchLable.text = @"搜索";
    searchLable.textColor = [UIColor colorWithHexString:@"#999999"];
    searchLable.font = [UIFont systemFontOfSize:14];
    [searchView addSubview:searchLable];
    [searchLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchImage.mas_right).offset(5);
        make.centerY.mas_equalTo(searchImage.mas_centerY);
    }];
    
}
- (void)LoadSegment
{
    UISegmentedControl *seg = [[UISegmentedControl alloc] init];
    seg.frame= CGRectMake(10, 0, 125, 30);
    [seg insertSegmentWithTitle:@"找人" atIndex:0 animated:YES];
    
    [seg insertSegmentWithTitle:@"找群" atIndex:1 animated:YES];
    [seg setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#999999"]} forState:UIControlStateNormal];
    [seg setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#333333"]} forState:UIControlStateSelected];
    seg.layer.masksToBounds = YES;               //    默认为no，不设置则下面一句无效
    
    seg.layer.cornerRadius = 15;               //    设置圆角大小，同UIView
    
    seg.layer.borderWidth = 1.5;                   //    边框宽度，重新画边框，若不重新画，可能会出现圆角处无边框的情况
    seg.layer.borderColor = [UIColor colorWithHexString:@"#f4f4f4"].CGColor; //     边框颜色
    seg.selectedSegmentIndex = _segIndex;
    [seg addTarget:self action:@selector(segchange:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = seg;
}
- (void)segchange:(UISegmentedControl *)Seg
{
    NSInteger Index = Seg.selectedSegmentIndex;
    NSLog(@"%ld", (long)Index);
    _segIndex = Index;

}
//搜索
- (void)searchTop:(UITapGestureRecognizer *)gesture
{
    CSSearchFriendViewController *vc = [[CSSearchFriendViewController alloc]init];
    vc.segIndex = _segIndex;
    if (_segIndex == 0) {
        vc.navigationItem.title = @"添加好友";
    }
    if (_segIndex == 1) {
        vc.navigationItem.title = @"添加群聊";
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}
//扫一扫
- (void)codeTop:(UITapGestureRecognizer *)gesture
{
    NSLog(@"扫一扫啊");
    CSRichScanViewController *vc  =[[CSRichScanViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
