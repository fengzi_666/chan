//
//  xinxipingtaiViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "BaseViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface xinxipingtaiViewController : BaseViewController
/**
 *跳转到对应的聊天界面
 */
- (void)pushToChatViewController:(NSString *)groupID userID:(NSString *)userID;

@end

NS_ASSUME_NONNULL_END
