//
//  liaotianshiViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "liaotianshiViewController.h"
#import "GroupInfoController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "TUIVideoMessageCell.h"
#import "TUIFileMessageCell.h"
#import "TUITextMessageCell.h"
#import "TUISystemMessageCell.h"
#import "TUIVoiceMessageCell.h"
#import "TUIImageMessageCell.h"
#import "TUIFaceMessageCell.h"
#import "TUIVideoMessageCell.h"
#import "TUIFileMessageCell.h"
#import "TUIGroupLiveMessageCell.h"
#import "liaotianxinxiProfileController.h"
#import "TUIKit.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "MMLayout/UIView+MMLayout.h"
#import "MyCustomCell.h"
#import "TCUtil.h"
#import "THelper.h"
#import "TCConstants.h"

#import "V2TIMManager.h"

#import "shawanyiyaUserSig.h"
#import "Toast.h"
#import "tianjiashiwuPopViewController.h"
#import "CSApplyAddFrindViewController.h"
#import "NSObject+RACPropertySubscribing.h"
#import "CSUserDataViewController.h"

#import "CSGroupInfoVC.h"
#import "TUIImageViewController.h"
#import "TUIMessageCell.h"
#import "CSChatRedViewController.h"
#import "CSChatRedCell.h"
#import "CSRedChatData.h"
#import "aihexiwangImageViewController.h"
#import "TUICallUtils.h"
#import "CSChatRedHitCell.h"
#import "CSChatRedHitData.h"
#import "CSGroupOpenRedVC.h"
#import "CSGroupRedHitListVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CSTransferAccountsViewController.h"
#import "CSTransferAccountsData.h"
#import "CSTransferAccountsCell.h"
// MLeaksFinder 会对这个类误报，这里需要关闭一下
@implementation UIImagePickerController (Leak)

- (BOOL)willDealloc {
    return NO;
}

@end
@interface liaotianshiViewController ()<TUIChatControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate,TMessageControllerDelegate,V2TIMGroupListener
>
@property (nonatomic, strong) tianjiashiwuPopViewController *chat;
@property (nonatomic ,strong)V2TIMGroupInfo *groupInfo;
@property (nonatomic ,copy )NSString    *msgstr;
@property (nonatomic ,strong) V2TIMFriendCheckResult *result;
@end

@implementation liaotianshiViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNav];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav白色"] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]}];
    __weak typeof(self) weakSelf = self;
    if (_conversationData.userID.length>0) {
        [[V2TIMManager sharedInstance]checkFriend:_conversationData.userID succ:^(V2TIMFriendCheckResult *result) {
            weakSelf.result = result;
            
            
                } fail:^(int code, NSString *desc) {
                    
                    
                }];
    }
    
    [IQKeyboardManager sharedManager].enable = NO;
   
 
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [IQKeyboardManager sharedManager].enable = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
   
}

- (NSData *)compactFormatDataForDictionary:(NSDictionary *)dicJson

{

    if (![dicJson isKindOfClass:[NSDictionary class]]) {

        return nil;

    }

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dicJson options:0 error:nil];

    if (![jsonData isKindOfClass:[NSData class]]) {

        return nil;

    }

    return jsonData;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    _chat = [[tianjiashiwuPopViewController alloc] initWithConversation:self.conversationData];
    _chat.delegate = self;
    _chat.messageController.delegate = self;
    [self addChildViewController:_chat];
    [self.view addSubview:_chat.view];

  
    RAC(self, title) = [RACObserve(_conversationData, title) distinctUntilChanged];
    [self checkTitle];

    NSMutableArray *moreMenus = [NSMutableArray arrayWithArray:_chat.moreMenus];
   
       
        [moreMenus addObject:({
            TUIInputMoreCellData *data = [TUIInputMoreCellData new];
            data.image = [UIImage imageNamed:@"消息_红包"];
            data.title = @"红包";
            data;
        })];
    
    if (_conversationData.userID.length>0)  {
        [moreMenus addObject:({
            TUIInputMoreCellData *data = [TUIInputMoreCellData new];
            data.image = [UIImage imageNamed:@"消息_转账"];
            data.title = @"转账";
            data;
        })];
    }
  
    _chat.moreMenus = moreMenus;

    [self setupNavigator];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onRefreshNotification:)
                                                 name:TUIKitNotification_TIMRefreshListener_Changed
                                               object:nil];

    //添加未读计数的监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onChangeUnReadCount:)
                                                 name:TUIKitNotification_onChangeUnReadCount
                                               object:nil];
    
    __weak typeof(self) weakSelf = self;
    [[V2TIMManager sharedInstance] getGroupsInfo:@[_conversationData.groupID] succ:^(NSArray<V2TIMGroupInfoResult *> *groupResultList) {
     
        V2TIMGroupInfoResult *result = groupResultList.firstObject;
        if (result.info && result.info.groupName.length > 0) {
            weakSelf.groupInfo = result.info;
        }
    } fail:nil];


}

- (void)checkTitle {
    
    if (_conversationData.title.length == 0) {
        if (_conversationData.userID.length > 0) {
            _conversationData.title = _conversationData.userID;
          
//             @weakify(self)
            __weak typeof(self) weakSelf = self;
            [[V2TIMManager sharedInstance] getFriendsInfo:@[_conversationData.userID] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
                
                V2TIMFriendInfoResult *result = resultList.firstObject;
                if (result.friendInfo && result.friendInfo.friendRemark.length > 0) {
                    weakSelf.conversationData.title = result.friendInfo.friendRemark;
                } else {
                    [[V2TIMManager sharedInstance] getUsersInfo:@[self.conversationData.userID] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                        V2TIMUserFullInfo *info = infoList.firstObject;
                        if (info && info.nickName.length > 0) {
                            weakSelf.conversationData.title = info.nickName;
                        }
                    } fail:nil];
                }
            } fail:nil];
        }
        if (_conversationData.groupID.length > 0) {
            _conversationData.title = _conversationData.groupID;
            __weak typeof(self) weakSelf = self;
            [[V2TIMManager sharedInstance] getGroupsInfo:@[_conversationData.groupID] succ:^(NSArray<V2TIMGroupInfoResult *> *groupResultList) {
                
                V2TIMGroupInfoResult *result = groupResultList.firstObject;
                if (result.info && result.info.groupName.length > 0) {
                    weakSelf.conversationData.title = result.info.groupName;
                }
            } fail:nil];
        }
    }
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    if (parent == nil) {
        [_chat saveDraft];
    }
}

// 聊天窗口标题由上层维护，需要自行设置标题
- (void)onRefreshNotification:(NSNotification *)notifi
{
    NSArray<V2TIMConversation *> *convs = notifi.object;
    for (V2TIMConversation *conv in convs) {
        if ([conv.conversationID isEqualToString:self.conversationData.conversationID]) {
            self.conversationData.title = conv.showName;
            break;
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"-------成功释放");
}

- (void)setupNavigator
{
    //left
    _unRead = [[TUnReadView alloc] init];

    //_unRead.backgroundColor = [UIColor grayColor];//可通过此处将未读标记设置为灰色，类似微信，但目前仍使用红色未读视图
    UIBarButtonItem *urBtn = [[UIBarButtonItem alloc] initWithCustomView:_unRead];
    self.navigationItem.leftBarButtonItems = @[urBtn];
    
    //既显示返回按钮，又显示未读视图
//    self.navigationItem.leftItemsSupplementBackButton = YES;
   
//    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self resetBarItemSpacesWithController:self];
    //right，根据当前聊天页类型设置右侧按钮格式
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton addTarget:self action:@selector(rightBarButtonClick) forControlEvents:UIControlEventTouchUpInside];
    if(_conversationData.userID.length > 0){
        [rightButton setImage:[UIImage tk_imageNamed:@"person_nav"] forState:UIControlStateNormal];
        //[rightButton setImage:[UIImage tk_imageNamed:@"person_nav_hover"] forState:UIControlStateHighlighted];
    }
    else if(_conversationData.groupID.length > 0){
        [rightButton setImage:[UIImage tk_imageNamed:(@"group_nav")] forState:UIControlStateNormal];
        //[rightButton setImage:[UIImage tk_imageNamed:(@"group_nav_hover")] forState:UIControlStateHighlighted];
    }
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItems = @[rightItem];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    
    
}

-(void)leftBarButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBarButtonClick
{
    [self.view endEditing:YES];
    //当前为用户和用户之间通信时，右侧按钮响应为用户信息视图入口
    if (_conversationData.userID.length > 0) {
        @weakify(self)
        [[V2TIMManager sharedInstance] getFriendsInfo:@[_conversationData.userID] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
            V2TIMFriendInfoResult *result = resultList.firstObject;
            if (result.relation == V2TIM_FRIEND_RELATION_TYPE_IN_MY_FRIEND_LIST || result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY) {
                @strongify(self)
                
                CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
                vc.friendProfile = result.friendInfo;
                vc.result = result;
                vc.groupId = _groupInfo.groupID;
                vc.msgid = _conversationData.conversationID;
                [self.navigationController pushViewController:vc animated:YES];
                
            } else {
                
                [[V2TIMManager sharedInstance] getUsersInfo:@[_conversationData.userID] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                    @strongify(self)
                    liaotianxinxiProfileController *vc = [[liaotianxinxiProfileController alloc]init];
                        vc.userFullInfo = infoList.firstObject;
                        if ([vc.userFullInfo.userID isEqualToString:[[V2TIMManager sharedInstance] getLoginUser]]) {
                            vc.actionType = PCA_NONE;
                            return;
                        } else {
                            vc.actionType = PCA_ADD_FRIEND;
                        }
                        vc.type=@"1";
                        vc.info = _groupInfo;
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    
                } fail:^(int code, NSString *msg) {
                    [THelper makeToastError:code msg:msg];
                }];
            }
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];

    //当前为群组通信时，右侧按钮响应为群组信息入口
    } else {
        CSGroupInfoVC *groupInfo = [[CSGroupInfoVC alloc] init];
        groupInfo.groupId = _conversationData.groupID;
        groupInfo.chatId = _conversationData.conversationID;
        groupInfo.quntouxiang = _conversationData.avatarImage;
        [self.navigationController pushViewController:groupInfo animated:YES];
    }
}

- (void)chatController:(TUIChatController *)controller didSendMessage:(TUIMessageCellData *)msgCellData
{
 
//    if (_conversationData.userID.length>0) {
//        NSLog(@"11111");
//        if (_result.relationType==0) {
//            [SVProgressHUD  showInfoWithStatus:@"对方不是您的好友,请先添加好友"];
//            return;
//            NSLog(@"不是好友");
//        }
//
//    }
    if ([_conversationData.groupID isEqualToString:@"im_demo_admin"] || [_conversationData.userID isEqualToString:@"im_demo_admin"]) {
        [TCUtil report:Action_Sendmsg2helper actionSub:@"" code:@(0) msg:@"sendmsg2helper"];
    }
    else if ([_conversationData.groupID isEqualToString:@"@TGS#33NKXK5FK"] || [_conversationData.userID isEqualToString:@"@TGS#33NKXK5FK"]) {
        [TCUtil report:Action_Sendmsg2defaultgrp actionSub:@"" code:@(0) msg:@"sendmsg2defaultgrp"];
    }
    if ([msgCellData isKindOfClass:[TUITextMessageCellData class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendtext code:@(0) msg:@"sendtext"];
    }
    else if ([msgCellData isKindOfClass:[TUIVoiceMessageCellData class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendaudio code:@(0) msg:@"sendaudio"];
    }
    else if ([msgCellData isKindOfClass:[TUIFaceMessageCellData class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendface code:@(0) msg:@"sendface"];
    }
    else if ([msgCellData isKindOfClass:[TUIImageMessageCellData class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendpicture code:@(0) msg:@"sendpicture"];
    }
    else if ([msgCellData isKindOfClass:[TUIVideoMessageCellData class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendvideo code:@(0) msg:@"sendvideo"];
    }
    else if ([msgCellData isKindOfClass:[TUIFileMessageCellData class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendfile code:@(0) msg:@"sendfile"];
    }
    else if ([msgCellData isKindOfClass:[TUIGroupLiveMessageCell class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendgrouplive code:@(0) msg:@"sendgrouplive"];
    }
    else if ([msgCellData isKindOfClass:[TUIGroupLiveMessageCell class]]) {
        [TCUtil report:Action_SendMsg actionSub:Action_Sub_Sendcustom code:@(0) msg:@"sendcustom"];
    }
}
#pragma --mark  在此回调特指您自定义的“更多”单元。
- (void)chatController:(TUIChatController *)chatController onSelectMoreCell:(TUIInputMoreCell *)cell
{
#pragma mark -- 红包入口
    __weak typeof(self) weakSelf = self;
    if (_conversationData.groupID.length>0) {
        if ([cell.data.title isEqualToString:@"红包"]) {
            [self LoadRequestRedState];
        }
       
    }
    if (_conversationData.userID.length>0) {
        [[V2TIMManager sharedInstance] getFriendsInfo:@[_conversationData.userID] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
            V2TIMFriendInfoResult *result = resultList.firstObject;
            if (result.relation == V2TIM_FRIEND_RELATION_TYPE_NONE  ) {
                if ([cell.data.title isEqualToString:@"红包"]) {
                    [SVProgressHUD showInfoWithStatus:@"陌生人不能发送"];
                    return;
                }
                if ([cell.data.title isEqualToString:@"转账"]) {
                    [SVProgressHUD showInfoWithStatus:@"陌生人不能转账"];
                    return;
                }
               
            } else {
                [[V2TIMManager sharedInstance] getFriendsInfo:@[_conversationData.userID] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
                    V2TIMFriendInfoResult *result = resultList.firstObject;
                    if (result.relation == V2TIM_FRIEND_RELATION_TYPE_NONE  ) {
                        if ([cell.data.title isEqualToString:@"红包"]) {
                            [SVProgressHUD showInfoWithStatus:@"陌生人不能发送"];
                            return;
                        }
                        if ([cell.data.title isEqualToString:@"转账"]) {
                            [SVProgressHUD showInfoWithStatus:@"陌生人不能转账"];
                            return;
                        }
                       
                    } else {
                        if ([cell.data isKindOfClass:[TUIInputMoreCellData class]]) {
                           
                                if ([cell.data.title isEqualToString:@"红包"]) {
                                    NSLog(@"单聊会话");
                                    CSChatRedViewController *vc = [[CSChatRedViewController alloc]init];
                                    vc.istype = @"1";
                                    vc.userid = _conversationData.userID;
                                    [weakSelf.navigationController pushViewController:vc animated:YES];
                                }
                                if ([cell.data.title isEqualToString:@"转账"]) {
                                    NSLog(@"单聊会话");
                                    CSTransferAccountsViewController *vc = [[CSTransferAccountsViewController alloc]init];
                                    vc.userid = _conversationData.userID;
                                    [weakSelf.navigationController pushViewController:vc animated:YES];

                                }
                               
                            
                        }
                    }
                } fail:^(int code, NSString *msg) {
                    [THelper makeToastError:code msg:msg];
                }];
               
                
            }
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];
    }
}
//获取成员引言状态  判断是否可以可以领取红包
- (void)LoadRequestRedState
{
    __weak typeof(self) weakSelf = self;
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_jinYanState];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupInfo.groupID forKey:@"txquncode"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
//        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *state = [NSString stringWithFormat:@"%@",data[@"state"]];
            if ([state isEqualToString:@"1"]) {
              
                if (_conversationData.groupID.length>0) {
                    NSLog(@"群会话");
                    CSChatRedViewController *vc = [[CSChatRedViewController alloc]init];
                    vc.istype = @"2";
                    vc.goupid = _conversationData.groupID;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }
               
               
            }
            
            
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadChatList:(V2TIMMessage *)msg
{
  
    [[V2TIMManager sharedInstance]findMessages:@[msg.msgID] succ:^(NSArray<V2TIMMessage *> *msgs) {
        NSDictionary *param = [TCUtil jsonData2Dictionary:msg.customElem.data];
        NSLog(@"%@",param);
        } fail:^(int code, NSString *desc) {
            
        }];
}
#pragma  mark -- 加载自定义的cell
- (TUIMessageCellData *)chatController:(TUIChatController *)controller onNewMessage:(V2TIMMessage *)msg
{
    if (msg.elemType == V2TIM_ELEM_TYPE_CUSTOM) {
        NSDictionary *param = [TCUtil jsonData2Dictionary:msg.customElem.data];
        if (param != nil) {
            NSInteger version = [param[@"version"] integerValue];
            //1.加载红包 2.加载群聊界面红包提示信息 4.加载群操作提示信息 5.加载单聊红包提示消息
            NSString *xiaoxileixing =[NSString stringWithFormat:@"%@",param[@"xiaoxileixing"]];
            NSString *tengxuncode   =param[@"tengxuncode"];
            if ([xiaoxileixing  isEqualToString:@"4"])
            {
                TUISystemMessageCellData *cellData = [[TUISystemMessageCellData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                cellData.innerMessage = msg;
                cellData.msgID = msg.msgID;
                cellData.content =param[@"content"];
                return cellData;
            }
          

            //加载群聊会话界面红包提示信息
            if ([xiaoxileixing isEqualToString:@"2"]) {
                if (version <= TextLink_Version) {
                    NSString *loginUser = [[V2TIMManager sharedInstance] getLoginUser];                    if ([loginUser isEqualToString:tengxuncode]) {
//                        NSLog(@"1111");
                        CSChatRedHitData *cellData = [[CSChatRedHitData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                        cellData.innerMessage = msg;
                        cellData.msgID = msg.msgID;
                        cellData.content =param[@"content"];
                        cellData.identifier =param[@"quncode"];
                        cellData.hongbaoid = [NSString stringWithFormat:@"%@",param[@"hongbaoid"]];
                        cellData.tengxuncode = tengxuncode;
                        cellData.xiaoxileixing =param[@"xiaoxileixing"];
                     
                        return cellData;
                    }
                    else
                    {
                        CSChatRedHitData *cellData = [[CSChatRedHitData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                        cellData.innerMessage = msg;
                        cellData.msgID = msg.msgID;
                        cellData.content =@"";
                        cellData.tengxuncode = tengxuncode;
                        cellData.xiaoxileixing =param[@"xiaoxileixing"];
                        
                        return cellData;
                    }
                   
                }
            }
            //加载单聊会话界面红包提示信息
            if ([xiaoxileixing isEqualToString:@"7"]) {
                if (version <= TextLink_Version) {
                    TUISystemMessageCellData *cellData = [[TUISystemMessageCellData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                    cellData.innerMessage = msg;
                    cellData.msgID = msg.msgID;

                    NSString *str =[V2TIMManager sharedInstance].getLoginUser;
                    NSLog(@"1. -----%@-----2----------%@",str,msg.userID);
                    if ([param[@"tengxuncode"] isEqualToString:str]) {
                        cellData.content =param[@"content"];
                    }

                    return cellData;
                }
            }
            //转账UI
            if ([xiaoxileixing isEqualToString:@"6"]) {
                CSTransferAccountsData *cellData = [[CSTransferAccountsData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                cellData.innerMessage = msg;
                cellData.msgID = msg.msgID;
                cellData.touxiang = param[@"touxiang"];
                cellData.tengxuncode = param[@"tengxuncode"];
                cellData.datetime = [NSString stringWithFormat:@"%@",param[@"datetime"]];
                cellData.jishouid = [NSString stringWithFormat:@"%@",param[@"jishouid"]];
                cellData.hongbaoid =[NSString stringWithFormat:@"%@",param[@"hongbaoid"]];
                cellData.jishouid = [NSString stringWithFormat:@"%@",param[@"jishouid"]];
                cellData.xiaoxileixing = param[@"xiaoxileixing"];
                cellData.nicheng = param[@"nicheng"];
                cellData.money = [NSString stringWithFormat:@"%@",param[@"money"]];
                return cellData;
                
            }
            
            NSString *name = param[@"name"];
            NSString *hongbaid = [NSString stringWithFormat:@"%@",param[@"hongbaoid"]];
            if (name.length == 0 || hongbaid.length ==0) {
                return nil;
            }
            //加载红包
            if ([xiaoxileixing isEqualToString:@"1"]) {
                
                if (version <= TextLink_Version) {
                    CSRedChatData *cellData = [[CSRedChatData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                    cellData.innerMessage = msg;
                    cellData.msgID = msg.msgID;
                    cellData.titleName = param[@"name"];
                    cellData.hongbaoid = param[@"hongbaoid"];
                    cellData.avatarUrl = [NSURL URLWithString:msg.faceURL];
                    cellData.touxiang = param[@"touxiang"];
                    cellData.tengxuncode = param[@"tengxuncode"];
                    cellData.nicheng = param[@"nicheng"];
                    cellData.jieshouid = param[@"jishouid"];
                    cellData.yilingqu = [NSString stringWithFormat:@"%d",msg.localCustomInt];
                    cellData.identifier =param[@"tengxuncode"];
                    cellData.datetime = [NSString stringWithFormat:@"%@",param[@"datetime"]];
                    cellData.name = msg.nickName;
                    [self LoadChatList:msg];
                    return cellData;
                }
            } else {
                // 兼容下老版本
                CSRedChatData *cellData = [[CSRedChatData alloc] initWithDirection:msg.isSelf ? MsgDirectionOutgoing : MsgDirectionIncoming];
                cellData.innerMessage = msg;
                cellData.msgID = msg.msgID;
                cellData.titleName = param[@"name"];
                cellData.hongbaoid = param[@"hongbaoid"];
                cellData.avatarUrl = [NSURL URLWithString:msg.faceURL];
                cellData.touxiang = param[@"touxiang"];
                cellData.tengxuncode = param[@"tengxuncode"];
                cellData.nicheng = param[@"nicheng"];
                cellData.jieshouid = param[@"jishouid"];
                cellData.identifier =param[@"tengxuncode"];
                cellData.datetime = [NSString stringWithFormat:@"%@",param[@"datetime"]];
                cellData.yilingqu = [NSString stringWithFormat:@"%d",msg.localCustomInt];
                cellData.name = msg.nickName;
                return cellData;
            }
        }
    }
    return nil;
}

- (TUIMessageCell *)chatController:(TUIChatController *)controller onShowMessageData:(TUIMessageCellData *)data
{
    if ([data isKindOfClass:[CSRedChatData class]]) {
        CSChatRedCell *myCell = [[CSChatRedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell"];
        [myCell fillWithData:(CSRedChatData *)data];
        return myCell;
    }
    if ([data isKindOfClass:[TUISystemMessageCellData class]]) {
        TUISystemMessageCell *myCell = [[TUISystemMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell1"];
        [myCell fillWithData:(TUISystemMessageCellData *)data];
        return myCell;
    }
    if ([data isKindOfClass:[CSChatRedHitData class]]) {
        CSChatRedHitCell *myCell = [[CSChatRedHitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell2"];
        
        [myCell fillWithData:(CSChatRedHitData *)data];
        __weak typeof(self) weakSelf = self;
        myCell.messageLabel.actionBlock = ^{
            NSLog(@"1111");
            CSGroupRedHitListVC *vc = [[CSGroupRedHitListVC alloc]init];
            vc.hitCellData = data;
            [weakSelf.navigationController pushViewController:vc animated:YES];
           
        };
        return myCell;
    }
    
    if ([data isKindOfClass:[CSTransferAccountsData class]]) {
        CSTransferAccountsCell *myCell = [[CSTransferAccountsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyCell2"];
        
        [myCell fillWithData:(CSTransferAccountsData *)data];
       
        return myCell;
    }
    return nil;
}


#pragma mark -- 点击消息头像回调
- (void)chatController:(TUIChatController *)controller onSelectMessageAvatar:(TUIMessageCell *)cell;

{
    [self.view endEditing:YES];
    //群聊
    if (_conversationData.groupID.length>0) {
        NSLog(@"群会话");
        @weakify(self)
        __weak typeof(self) weakSelf = self;
        [[V2TIMManager sharedInstance] getFriendsInfo:@[cell.messageData.identifier] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
            V2TIMFriendInfoResult *result = resultList.firstObject;
            if (result.relation == V2TIM_FRIEND_RELATION_TYPE_IN_MY_FRIEND_LIST || result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY) {
               
                
                CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
                vc.friendProfile = result.friendInfo;
                vc.groupId = _groupInfo.groupID;
                vc.result = result;
                vc.istype=@"1";
//                vc.msgid = _conversationData.conversationID;
                [weakSelf.navigationController pushViewController:vc animated:YES];
                
            } else {
                
                [[V2TIMManager sharedInstance] getUsersInfo:@[cell.messageData.identifier] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                   
                    liaotianxinxiProfileController *vc = [[liaotianxinxiProfileController alloc]init];
                        vc.userFullInfo = infoList.firstObject;
                        if ([vc.userFullInfo.userID isEqualToString:[[V2TIMManager sharedInstance] getLoginUser]]) {
                            vc.actionType = PCA_NONE;
                            return;
                            
                        } else {
                            vc.actionType = PCA_ADD_FRIEND;
                        }
                        vc.info = _groupInfo;
                    vc.type =@"2";
                    
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                } fail:^(int code, NSString *msg) {
                    [THelper makeToastError:code msg:msg];
                }];
            }
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];
    }
    // 单聊
    if(_conversationData.userID.length>0)
    {
        NSLog(@"单聊");
        @weakify(self)
        __weak typeof(self) weakSelf = self;
        [[V2TIMManager sharedInstance] getFriendsInfo:@[cell.messageData.identifier] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
            V2TIMFriendInfoResult *result = resultList.firstObject;
            if (result.relation == V2TIM_FRIEND_RELATION_TYPE_IN_MY_FRIEND_LIST || result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY) {
               
                
                CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
                vc.friendProfile = result.friendInfo;
                vc.result = result;
                vc.groupId = _groupInfo.groupID;
                vc.msgid = _conversationData.conversationID;
                [weakSelf.navigationController pushViewController:vc animated:YES];
                
            } else {
                
                [[V2TIMManager sharedInstance] getUsersInfo:@[cell.messageData.identifier] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                   
                    liaotianxinxiProfileController *vc = [[liaotianxinxiProfileController alloc]init];
                        vc.userFullInfo = infoList.firstObject;
                        if ([vc.userFullInfo.userID isEqualToString:[[V2TIMManager sharedInstance] getLoginUser]]) {
                            vc.actionType = PCA_NONE;
                            return;
                        } else {
                            vc.actionType = PCA_ADD_FRIEND;
                        }
                        vc.type=@"1";
                        vc.info = _groupInfo;
                    
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                } fail:^(int code, NSString *msg) {
                    [THelper makeToastError:code msg:msg];
                }];
            }
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];
    }
  
}

- (void)chatController:(TUIChatController *)controller onSelectMessageContent:(TUIMessageCell *)cell
{
    NSLog(@"11111");
}
- (void)messageController:(TUIMessageController *)controller onSelectMessageContent:(TUIMessageCell *)cell
{
    NSLog(@"11111");
    if ([cell isKindOfClass:[CSChatRedCell class]]) {
        CSRedChatData *cellData = [(CSChatRedCell *)cell redData];
            NSLog(@"---------点击了红包");
    }
}

- (void) onChangeUnReadCount:(NSNotification *)notifi{
    NSMutableArray *convList = (NSMutableArray *)notifi.object;
    int unReadCount = 0;
    for (V2TIMConversation *conv in convList) {
        // 忽略当前会话的未读数
        if (![conv.conversationID isEqual:self.conversationData.conversationID]) {
            unReadCount += conv.unreadCount;
        }
    }
    [_unRead setNum:unReadCount];
}

///此处可以修改导航栏按钮的显示位置，但是无法修改响应位置，暂时不建议使用
- (void)resetBarItemSpacesWithController:(UIViewController *)viewController {
    CGFloat space = 16;
    for (UIBarButtonItem *buttonItem in viewController.navigationItem.leftBarButtonItems) {
        buttonItem.image = [UIImage imageNamed:@"返回"];
        if (buttonItem.customView == nil) { continue; }
        /// 根据实际情况(自己项目UIBarButtonItem的层级)获取button
        UIButton *itemBtn = nil;
        if ([buttonItem.customView isKindOfClass:[UIButton class]]) {
            itemBtn = (UIButton *)buttonItem.customView;
        }
        [itemBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
        /// 设置button图片/文字偏移
        itemBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -space,0, 0);
        itemBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -space,0, 0);
        itemBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -space,0, 0);
        /// 改变button事件响应区域
        // itemBtn.hitEdgeInsets = UIEdgeInsetsMake(0, -space, 0, space);
    }
    for (UIBarButtonItem *buttonItem in viewController.navigationItem.rightBarButtonItems) {
        if (buttonItem.customView == nil) { continue; }
        UIButton *itemBtn = nil;
        if ([buttonItem.customView isKindOfClass:[UIButton class]]) {
            itemBtn = (UIButton *)buttonItem.customView;
        }
        itemBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0,0, -space);
        itemBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0,0, -space);
        itemBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0,0, -space);
        //itemBtn.hitEdgeInsets = UIEdgeInsetsMake(0, space, 0, -space);
    }
}

- (void)sendMessage:(TUIMessageCellData*)msg {
    [_chat sendMessage:msg];
}

@end
