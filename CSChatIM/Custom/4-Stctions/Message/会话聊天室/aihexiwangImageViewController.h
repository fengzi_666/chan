//
//  aihexiwangImageViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/2.
//

#import "BaseViewController.h"
#import "TUIImageMessageCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface aihexiwangImageViewController : BaseViewController
@property (nonatomic, strong) TUIImageMessageCellData *data;
@end

NS_ASSUME_NONNULL_END
