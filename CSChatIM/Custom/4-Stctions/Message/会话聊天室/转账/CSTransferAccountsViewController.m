//
//  CSTransferAccountsViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "CSTransferAccountsViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CSRedCodeView.h"
#import "SFHFKeychainUtils.h"
#import "liaotianshiViewController.h"
#import "CSChatRedPasdPopView.h"
#import "XMPayCodeView.h"
#import "CSPayCodeViewController.h"
@interface CSTransferAccountsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *naleLable;
@property (weak, nonatomic) IBOutlet UITextField *moneyTF;
@property (nonatomic ,strong) CSChatRedPasdPopView *redPsdPopView;
@property (nonatomic ,strong) CSRedCodeView *foodView;
@property (nonatomic ,strong)  XMPayCodeView *codeView;
@property (nonatomic ,copy) NSString *paypsdStr;

@end

@implementation CSTransferAccountsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNav];
    self.navigationItem.title = @"向好友转账";
    
   
    [[V2TIMManager sharedInstance] getUsersInfo:@[_userid] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
       
        V2TIMUserFullInfo *userInfo = infoList.firstObject;
        if (userInfo) {
            NSLog(@"%@",userInfo);
            [_headImage sd_setImageWithURL:[NSURL URLWithString:userInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
            _naleLable.text = userInfo.nickName;
            NSDictionary *dic =userInfo.customInfo;
//            _userid = [[ NSString alloc]initWithData:dic[@"userid"] encoding:NSUTF8StringEncoding];
            
        }
    } fail:nil];
    [[V2TIMManager sharedInstance] getUsersInfo:@[_userid] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
        
        V2TIMUserFullInfo *userInfo = infoList.firstObject;
        NSDictionary *dic =userInfo.customInfo;
        _userid = [[ NSString alloc]initWithData:dic[@"userid"] encoding:NSUTF8StringEncoding];
        
} fail:nil];

    _headImage.layer.masksToBounds = YES;
    _headImage.layer.cornerRadius = _headImage.height/2;
    
    _moneyTF.text = @"";
    
    
}
- (IBAction)sendClick:(UIButton *)sender {
    
    if ([_moneyTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入转账金额"];
        return;
    }
    [self LoadRequstData];
    
}
BOOL istype2 = NO;
//发送红包之前先检测是否设置过支付密码
- (void)LoadRequstData
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_jiaoyistate];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    __weak typeof(self) weakSelf = self;
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *jiaoyistate = [NSString stringWithFormat:@"%@",data[@"jiaoyistate"]];
            if ([jiaoyistate isEqualToString:@"1"]) {
                [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;
                    weakSelf.redPsdPopView = [[CSChatRedPasdPopView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
                    weakSelf.redPsdPopView.moneyLable.text = [NSString stringWithFormat:@"¥%@",_moneyTF.text];
                    [weakSelf.redPsdPopView.deleteBtn addTarget:self action:@selector(redPopClick:) forControlEvents:UIControlEventTouchUpInside];
                    [weakSelf.view addSubview:weakSelf.redPsdPopView];
                    // 支付密码输入框
                    XMPayCodeView *payCodeView = [[XMPayCodeView alloc]init];

                    _codeView = payCodeView;
                    [weakSelf.redPsdPopView.popView addSubview:payCodeView];
                    [payCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(weakSelf.redPsdPopView.popView.mas_left).offset(25);
                        make.right.equalTo(weakSelf.redPsdPopView.popView.mas_right).offset(-25);
                        make.bottom.equalTo(weakSelf.redPsdPopView.popView.mas_bottom).offset(-30);
                        make.height.equalTo(@55);
                    
                   
                    payCodeView.secureTextEntry = YES;  //
                    
                    [payCodeView setPayBlock:^(NSString *payCode) {
                        NSLog(@"payCode==%@",payCode);
                        _paypsdStr = payCode;
                        
                        if (payCode.length<=5) {
                            istype2 = NO;
                        }
                        if (payCode.length==6) {
                           
                            if (istype2==YES) {
                                return;
                            }
                            istype2 = YES;
                          
                            [self LoadRequestPostSendRedData:payCode istype:@""];
                            
                          
                        }

                    }];
                     //1秒后，让密码输入成为第一响应
                   
                        [payCodeView becomeKeyBoardFirstResponder];
                    
                }];
            }
            if ([jiaoyistate isEqualToString:@"2"]) {
               [SVProgressHUD showInfoWithStatus:@"请先设置支付密码"];
                CSPayCodeViewController *vc = [[CSPayCodeViewController alloc]init];
                vc.type = @"1";
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }
        else
        {
//            [SVProgressHUD showSuccessWithStatus:dict[@"<#msg#>"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}

- (void)LoadRequestPostSendRedData:(NSString *)zhifumima istype:(NSString *)type
{
    
    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appzhuanzhang];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString *uuid= [self UUID];
    [dict setValue:_moneyTF.text forKey:@"money"];
    [dict setValue:_userid forKey:@"haoyouid"];
    [dict setValue:zhifumima forKey:@"zhifumima"];
    
    
    __weak typeof(self) weakSelf = self;
   
    [longwangRequestManage postBaseRedWithURL:url param:dict uuid:uuid success:^(NSDictionary *dict) {
        NSString *code= [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD dismiss];
            if ([type isEqualToString:@"1"]) {
                [_foodView removeFromSuperview];
            }
//            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            
            [self  jumpVC];
            
        }
//        else if ([code isEqualToString:@"1008"])
//        {
//            //异地登录验证手机号验证码
//
//            [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
//            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
//
//            [self.view endEditing:YES];
//            CSRedCodeView *foodView = [[CSRedCodeView alloc]init];
////            weakSelf.foodView = foodView;
//
////                foodView.backgroundColor = [UIColor whiteColor];
//            [foodView.codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            [foodView.quedingBtn addTarget:self action:@selector(quedingClick:) forControlEvents:UIControlEventTouchUpInside];
//            [foodView.deleteBtn addTarget:self action:@selector(deletaBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            foodView.mobileTF.text = [UserDefaultManager shareInstance].GetLoginLoginMobile;
//            foodView.mobileTF.enabled=NO;
//            [weakSelf.redPsdPopView addSubview:foodView];
//            self.foodView  = foodView;
//
//            [foodView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.bottom.mas_equalTo(weakSelf.redPsdPopView.mas_bottom);
//                make.height.mas_equalTo(258);
//                make.right.mas_equalTo(weakSelf.redPsdPopView.mas_right);
//                make.left.mas_equalTo(weakSelf.redPsdPopView.mas_left);
//            }];
//
//            NSNotificationCenter * center=[NSNotificationCenter defaultCenter];
//            [center addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
////            [[[UIApplication  sharedApplication ]keyWindow ] addSubview : foodView ] ;
//
//        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
          
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
- (void)redPopClick:(UIButton *)sender
{
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;

    __weak typeof(self) weakSelf = self;
      //执行的动画
    [UIView animateWithDuration:1 animations:^{
        
        [weakSelf.redPsdPopView removeFromSuperview];
    }];
}
- (void)quedingClick :(UIButton *)sender
{
    if (_paypsdStr.length==6) {
        [self LoadRequestPostSendRedData:_paypsdStr istype:@"1"];
        
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@"请输入6位支付密码"];
    }
 
}
- (void)deletaBtnClick:(UIButton *)sender
{
//    [self.foodView removeFromSuperview];
    [self.redPsdPopView removeFromSuperview];
}
- (NSString*)UUID {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *userName = infoDict[@"CFBundleName"];
    NSString *serviceName = infoDict[@"CFBundleIdentifier"];
    NSString *UUID = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:serviceName error:nil];
    if (UUID.length==0) {
        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SFHFKeychainUtils storeUsername:userName andPassword:UUID forServiceName:serviceName updateExisting:1 error:nil];
        
    }
    
    return UUID;

}
//跳转指定控制器
- (void)jumpVC
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[liaotianshiViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
- (void)codeBtnClick :(UIButton *)sender
{
    [sender startCountDownTime:60 withCountDownBlock:^{
        NSLog(@"开始倒计时");
        //此处发送验证码等操作
        //................
        [self LoadRequestCode];
        
    }];
}
- (void)LoadRequestCode
{
//    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sendYDYZMSMS];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_foodView.mobileTF.text forKey:@"loginname"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
-(void)keyboardWillChange:(NSNotification *)note
{
        //获取键盘弹起时的frame值
        CGRect rect=[note.userInfo[UIKeyboardFrameEndUserInfoKey]CGRectValue];
        //获取键盘弹起时的Y值
        CGFloat keyboardY=rect.origin.y;
        //计算view要上移动多少就是键盘的y值减去屏幕的高度就能得到需要向上移动多少，最后y值变回和屏幕高度一样就又回来了，注意这里不能说我之前比如说变了-100，
         //要想变回来变成正100那是错了，要回到原点才是对的。
        CGFloat transformY=keyboardY-self.view.frame.size.height;
        self.foodView.transform=CGAffineTransformMakeTranslation(0, transformY);

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
