//
//  CSTransferAccountsCell.m
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "CSTransferAccountsCell.h"

@implementation CSTransferAccountsCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.myView = [[UIView alloc]init];
        self.myView.backgroundColor = [UIColor colorWithHexString:@"#FB9F3C"];
        self.myView.layer.masksToBounds = YES;
        self.myView.layer.cornerRadius = 5;
        [self.container addSubview:self.myView];
        [self.myView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(8);
            make.left.mas_equalTo(5);
            make.bottom.mas_equalTo(-8);
            make.right.mas_equalTo(-8);
        }];
        self.redImage= [[UIImageView alloc]init];
        self.redImage.image = [UIImage imageNamed:@"会话_转账成功"];
        [self.myView addSubview:self.redImage];
        [self.redImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.top.mas_equalTo(self.myView.mas_top).offset(20);
            make.size.mas_equalTo(CGSizeMake(27, 27));
        }];
        self.titleLable = [[UILabel alloc]init];
        self.titleLable.textColor = [UIColor whiteColor];
        self.titleLable.font = [UIFont boldSystemFontOfSize:13];
        [self.myView addSubview:self.titleLable];
        [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.redImage.mas_right).offset(10);
            make.top.mas_equalTo(self.redImage.mas_top);
            make.height.mas_equalTo(14);
            make.right.mas_equalTo(self.myView.mas_right).offset(-10);
        }];
        self.line = [[UILabel alloc]init];
        self.line.backgroundColor = [UIColor whiteColor];
        [self.myView addSubview:self.line];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.myView.mas_left);
            make.right.mas_equalTo(self.myView.mas_right);
            make.height.mas_equalTo(20);
            make.bottom.mas_equalTo(self.myView.mas_bottom);
        }];
        
      
        self.hitLable1 = [[UILabel alloc]init];
        self.hitLable1.text = @"转账成功";
        self.hitLable1.textColor = [UIColor whiteColor];
        self.hitLable1.font = [UIFont systemFontOfSize:10];
        [self.myView addSubview:_hitLable1];
        [_hitLable1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleLable.mas_left);
            make.top.mas_equalTo(self.titleLable.mas_bottom).offset(5);
            make.height.mas_equalTo(10);
        }];
        self.hitLable = [[UILabel alloc]init];
        self.hitLable.text = @"余额转账";
        self.hitLable.textColor = [UIColor colorWithHexString:@"#999999"];
        self.hitLable.font = [UIFont systemFontOfSize:10];
        [self.myView addSubview:_hitLable];
        [_hitLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.myView.mas_left).offset(10);
            make.bottom.mas_equalTo(self.myView.mas_bottom).offset(-3);
            make.height.mas_equalTo(10);
        }];
        self.hitLable1.hidden = NO;
    }
    return self;
}
- (void)fillWithData:(CSTransferAccountsData *)data;
{
    [super fillWithData:data];
    self.data = data;
    self.titleLable.text = [NSString stringWithFormat:@"¥%@",data.money];
    //获取当前时间
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:data.innerMessage.faceURL]];
  
   
}

-(NSString *)timewithIntervasting:(NSString*)string{
    
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[string doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];//更改自己想要的时间格式
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
    
    
}
//比较两个日期大小
-(int)compareDate:(NSString*)startDate withDate:(NSString*)endDate{
    
    int comparisonResult;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] init];
    date1 = [formatter dateFromString:startDate];
    date2 = [formatter dateFromString:endDate];
    NSComparisonResult result = [date1 compare:date2];
//    NSLog(@"result==%ld",(long)result);
    switch (result)
    {
            //date02比date01大
        case NSOrderedAscending:
            comparisonResult = 1;
            break;
            //date02比date01小
        case NSOrderedDescending:
            comparisonResult = -1;
            break;
            //date02=date01
        case NSOrderedSame:
            comparisonResult = 0;
            break;
        default:
            NSLog(@"erorr dates %@, %@", date1, date2);
            break;
    }
    return comparisonResult;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
