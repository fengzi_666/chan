//
//  CSTransferAccountsCell.h
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "TUIMessageCell.h"
#import "CSTransferAccountsData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSTransferAccountsCell : TUIMessageCell

@property (nonatomic ,strong) UIView *myView;
@property (nonatomic ,strong) UILabel *titleLable;//红包标题
@property (nonatomic ,strong) UILabel *line;//线条
@property (nonatomic ,strong) UIImageView *redImage;//图片
@property (nonatomic ,strong) UILabel *hitLable;
@property (nonatomic ,strong) UILabel *hitLable1;

@property CSTransferAccountsData *data;
- (void)fillWithData:(CSTransferAccountsData *)data;
@end
NS_ASSUME_NONNULL_END
