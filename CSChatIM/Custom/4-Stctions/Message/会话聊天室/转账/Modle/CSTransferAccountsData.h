//
//  CSTransferAccountsData.h
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "TUIMessageCellData.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSTransferAccountsData : TUIMessageCellData
@property NSString *touxiang;
@property NSString *tengxuncode;
@property NSString *datetime;
@property NSString *jishouid;
@property NSString *xiaoxileixing;
@property NSString *nicheng;
@property NSString *hongbaoid;
@property NSString *money;
@end

NS_ASSUME_NONNULL_END
