//
//  liaotianshiViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "BaseViewController.h"
#import "TUIChatController.h"
#import "TUnReadView.h"
NS_ASSUME_NONNULL_BEGIN
@class TUIMessageCellData;
@interface liaotianshiViewController : BaseViewController
@property (nonatomic, strong) TUIConversationCellData *conversationData;
@property (nonatomic, strong) TUnReadView *unRead;
- (void)sendMessage:(TUIMessageCellData*)msg;
@end

NS_ASSUME_NONNULL_END
