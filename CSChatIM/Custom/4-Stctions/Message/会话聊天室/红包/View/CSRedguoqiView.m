//
//  CSRedguoqiView.m
//  CSChatIM
//
//  Created by Apple on 2020/12/26.
//

#import "CSRedguoqiView.h"

@implementation CSRedguoqiView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSRedguoqiView" owner:nil options:nil][0];
        self.frame =frame;
        self.bgView.layer.masksToBounds = YES;
        self.bgView.layer.cornerRadius =5;
        UIColor *color = [UIColor blackColor];
        self.backgroundColor = [color colorWithAlphaComponent:0.5];
    }
    return self;
}
- (IBAction)cancelBtnClick:(UIButton *)sender {
    [self removeFromSuperview];
}

@end
