//
//  CSChatRedHitCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/21.
//

#import "CSChatRedHitCell.h"
#import "THeader.h"
#import "MMLayout/UIView+MMLayout.h"
#import "UIColor+TUIDarkMode.h"

@interface CSChatRedHitCell ()
@property (nonatomic, strong) GHAttributesLabel *messageLabel;
@property CSChatRedHitData *systemData;
@end

@implementation CSChatRedHitCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _messageLabel = [[GHAttributesLabel alloc]init];
        _messageLabel.font = [UIFont systemFontOfSize:13];
        _messageLabel.textColor = [UIColor d_systemGrayColor];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.numberOfLines = 0;
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.layer.cornerRadius = 3;
        _messageLabel.userInteractionEnabled = YES;
      
        [_messageLabel.layer setMasksToBounds:YES];
       
        self.container.userInteractionEnabled = YES;
        self.contentView.userInteractionEnabled = YES;
        self.userInteractionEnabled = YES;
        self.contentView.userInteractionEnabled = YES;
        [self.container addSubview:_messageLabel];
    }
    return self;
}

- (void)fillWithData:(CSChatRedHitData*)data;
{
    [super fillWithData:data];
    self.systemData = data;
    //set data
//    self.messageLabel.text = data.content;
    self.messageLabel.textColor = data.contentColor;
    NSString *text = data.content;
    NSRange hightlightTextRange = [text rangeOfString:@"红包"];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attrStr addAttribute: NSForegroundColorAttributeName value: [UIColor d_systemGrayColor]
                                                       range: NSMakeRange(0, text.length)];
    
    [attrStr addAttribute:NSLinkAttributeName
                    value:@"红包"
                    range:hightlightTextRange];
   
   
    
    [_messageLabel setAttributesText:attrStr actionText:@"红包"];
    
    _messageLabel.textColor = [UIColor redColor];
     
   

    
    self.nameLabel.hidden = YES;
    self.avatarView.hidden = YES;
    self.retryView.hidden = YES;
    [self.indicator stopAnimating];
    [self setNeedsLayout];
    
    


}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.container.mm_center();
    self.messageLabel.mm_fill();
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
