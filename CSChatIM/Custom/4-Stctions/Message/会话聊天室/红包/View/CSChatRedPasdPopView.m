//
//  CSChatRedPasdPopView.m
//  CSChatIM
//
//  Created by Apple on 2020/12/19.
//

#import "CSChatRedPasdPopView.h"

@implementation CSChatRedPasdPopView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSChatRedPasdPopView" owner:nil options:nil][0];
        self.frame =frame;
        self.popView.layer.masksToBounds = YES;
        self.popView.layer.cornerRadius = 5;
        UIColor *color = [UIColor blackColor];
        self.backgroundColor = [color colorWithAlphaComponent:0.5];
    }
    return self;
}
@end
