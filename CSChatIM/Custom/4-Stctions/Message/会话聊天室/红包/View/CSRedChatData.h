//
//  CSRedChatData.h
//  CSChatIM
//
//  Created by Apple on 2020/12/10.
//

#import "TUIMessageCellData.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSRedChatData : TUIMessageCellData
@property NSString *titleName;
@property NSString *hongbaoid;
@property NSString *touxiang;
@property NSString *nicheng;
@property NSString *jieshouid;
@property NSString *tengxuncode;
@property NSString *yilingqu;
@property NSString *datetime;
@end

NS_ASSUME_NONNULL_END
