//
//  CSGroupRedHeadView.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSGroupRedHeadView;
typedef void (^walletBtnClick)(CSGroupRedHeadView *view);
@interface CSGroupRedHeadView : UIView
//头像
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
//昵称
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
//红包标题
@property (weak, nonatomic) IBOutlet UILabel *redHitLable;
//钱数
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
//钱包按钮
@property (weak, nonatomic) IBOutlet UIButton *walletBtn;
//点击钱包回调
@property (weak, nonatomic) IBOutlet UILabel *moneyHit;
@property (weak, nonatomic) IBOutlet UILabel *yilingquLable;
@property(nonatomic, copy) walletBtnClick walletBtnClick;
@end

NS_ASSUME_NONNULL_END
