//
//  CSChatRedHitData.h
//  CSChatIM
//
//  Created by Apple on 2020/12/21.
//

#import "TUIMessageCellData.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSChatRedHitData : TUIMessageCellData
/**
 *  系统消息内容，例如“您撤回了一条消息。”
 */
@property (nonatomic, strong) NSString *content;

/**
 *  内容字体
 *  系统消息显示时的 UI 字体。
 */
@property UIFont *contentFont;

/**
 *  内容颜色
 *  系统消息显示时的 UI 颜色。
 */
@property UIColor *contentColor;

@property NSString *groupId;
@property NSString *hongbaoid;
@property NSString *lingqucode;
@property NSString *xiaoxileixing;
@property NSString *quncode;
@property NSString *tengxuncode;

@end

NS_ASSUME_NONNULL_END
