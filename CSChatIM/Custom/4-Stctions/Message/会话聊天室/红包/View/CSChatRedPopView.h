//
//  CSChatRedPopView.h
//  CSChatIM
//
//  Created by Apple on 2020/12/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSChatRedPopView;
typedef void (^openTapClick)(CSChatRedPopView *view);
@interface CSChatRedPopView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
//文字输入
@property(nonatomic, copy) openTapClick openTapClick;
@end

NS_ASSUME_NONNULL_END
