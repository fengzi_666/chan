//
//  CSGroupRedHeadView.m
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "CSGroupRedHeadView.h"

@implementation CSGroupRedHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSGroupRedHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius = self.headImage.width/2;
    
    }
    return self;
}
- (IBAction)walletBtnClick:(UIButton *)sender {
    if (self.walletBtnClick) {
        self.walletBtnClick(self);
    }
}

@end
