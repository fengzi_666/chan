//
//  CSGroupRedListCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "CSGroupRedListCell.h"

@implementation CSGroupRedListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
