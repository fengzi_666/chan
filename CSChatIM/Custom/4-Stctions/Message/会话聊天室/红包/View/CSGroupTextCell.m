//
//  CSGroupTextCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "CSGroupTextCell.h"

@implementation CSGroupTextCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.container.backgroundColor = [UIColor colorWithHexString:@"#FB9F3C"];
        
        self.titleLable = [[UILabel alloc]init];
        self.titleLable.textColor = [UIColor whiteColor];
        self.titleLable.frame = CGRectMake(0, 0, KScreenWidth, 20);
        self.titleLable.font = [UIFont boldSystemFontOfSize:14];
        [self.container addSubview:self.titleLable];
       
    }
    return self;
}
- (void)fillWithData:(CSGroupTextData *)data;
{
    [super fillWithData:data];
    self.redData = data;
    self.titleLable.text = data.name;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
