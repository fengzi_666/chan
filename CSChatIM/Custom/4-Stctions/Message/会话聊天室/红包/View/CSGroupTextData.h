//
//  CSGroupTextData.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "TUIMessageCellData.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupTextData : TUIMessageCellData
@property NSString *name;
@end

NS_ASSUME_NONNULL_END
