//
//  CSGroupRedListCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupRedListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UIImageView *huangguanImage;
@property (weak, nonatomic) IBOutlet UILabel *optimumLable;

@end

NS_ASSUME_NONNULL_END
