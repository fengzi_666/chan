//
//  CSChatRedPopView.m
//  CSChatIM
//
//  Created by Apple on 2020/12/10.
//

#import "CSChatRedPopView.h"

@implementation CSChatRedPopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSChatRedPopView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius = self.headImage.width/2;
        UIColor *color = [UIColor blackColor];
        self.backgroundColor = [color colorWithAlphaComponent:0.5];
    }
    return self;
}
- (IBAction)openBtnClick:(UIButton *)sender {
    if (self.openTapClick) {
        self.openTapClick(self);
    }
}
- (IBAction)cancelBtnClick:(UIButton *)sender {
    
    [self removeFromSuperview];
  
}
@end
