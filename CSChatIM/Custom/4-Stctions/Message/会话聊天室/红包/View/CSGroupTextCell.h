//
//  CSGroupTextCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "TUIMessageCell.h"
#import "CSGroupTextData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSGroupTextCell : TUIMessageCell
@property (nonatomic ,strong) UILabel *titleLable;
@property CSGroupTextData *redData;
- (void)fillWithData:(CSGroupTextData *)data;
@end

NS_ASSUME_NONNULL_END
