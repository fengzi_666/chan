//
//  CSChatRedCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "TUIMessageCell.h"
#import "CSRedChatData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSChatRedCell : TUIMessageCell
@property (nonatomic ,strong) UILabel *nameTitle;
@property (nonatomic ,strong) UIView *myView;
@property (nonatomic ,strong) UILabel *titleLable;//红包标题
@property (nonatomic ,strong) UILabel *line;//线条
@property (nonatomic ,strong) UIImageView *redImage;//图片
@property (nonatomic ,strong) UILabel *hitLable;
@property (nonatomic ,strong) UILabel *hitLable1;

@property CSRedChatData *redData;
- (void)fillWithData:(CSRedChatData *)data;

@end

NS_ASSUME_NONNULL_END
