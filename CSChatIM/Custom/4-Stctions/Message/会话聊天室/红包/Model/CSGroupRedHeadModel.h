//
//  CSGroupRedHeadModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupRedHeadModel : NSObject
@property (nonatomic ,copy) NSString *touxiang;
@property (nonatomic ,copy) NSString *yilingqu;
@property (nonatomic ,copy) NSString *zong;
@property (nonatomic ,copy) NSString *nicheng;
@property (nonatomic ,copy) NSString *onemoney;
@property (nonatomic ,copy) NSString *money;
@property (nonatomic ,copy) NSString *hongbaoname;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
