//
//  CSGroupRedListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupRedListModel : NSObject
@property (nonatomic ,copy) NSString *userid;
@property (nonatomic ,copy) NSString *lingqutouxiang;
@property (nonatomic ,copy) NSString *lingqunicheng;
@property (nonatomic ,copy) NSString *onemoney;
@property (nonatomic ,copy) NSString *jieshudate;
@property (nonatomic ,copy) NSString *shifouzuijia;
@property (nonatomic ,copy) NSString *zongmoney;
@property (nonatomic ,copy) NSString *shuliang;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
