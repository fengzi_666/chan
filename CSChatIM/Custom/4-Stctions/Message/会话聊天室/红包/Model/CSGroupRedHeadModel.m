//
//  CSGroupRedHeadModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "CSGroupRedHeadModel.h"

@implementation CSGroupRedHeadModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.zong = [NSString stringWithFormat:@"%@",dic[@"zong"]];
        self.onemoney =[NSString stringWithFormat:@"%@",dic[@"onemoney"]];
        self.touxiang = dic[@"usertouxiang"];
        self.nicheng = dic[@"usernicheng"];
        self.money = [NSString stringWithFormat:@"%@",dic[@"money"]];
        self.hongbaoname = dic[@"hongbaoname"];
    }
    return self;
}
@end
