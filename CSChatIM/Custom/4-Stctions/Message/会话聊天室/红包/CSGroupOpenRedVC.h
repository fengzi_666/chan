//
//  CSGroupOpenRedVC.h
//  CSChatIM
//
//  Created by Apple on 2020/12/11.
//

#import "BaseViewController.h"
#import "CSRedChatData.h"
#import "CSGroupRedHeadModel.h"
#import "CSChatRedHitData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSGroupOpenRedVC : BaseViewController
@property (nonatomic ,strong) CSRedChatData *cellData;
@property (nonatomic ,strong) CSChatRedHitData *hitCellData;
@property (nonatomic ,strong) CSGroupRedHeadModel *headModel;
@property (nonatomic ,copy) NSString *istype;
@property (nonatomic ,copy) NSString *danliao;


@end

NS_ASSUME_NONNULL_END
