//
//  CSGroupGQRedVC.m
//  CSChatIM
//
//  Created by Apple on 2020/12/26.
//

#import "CSGroupGQRedVC.h"

@interface CSGroupGQRedVC ()
@property (weak, nonatomic) IBOutlet UIImageView *headImgae;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *redcontLable;

@end

@implementation CSGroupGQRedVC
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImage *backImage = [[UIImage imageNamed:@"nav_bg"] resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    //设置导航栏背景图片
    [self.navigationController.navigationBar setBackgroundImage:backImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];

    
  
}
- (void)viewDidDisappear:(BOOL)animated
{
    
    [super viewDidDisappear:animated];
   
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav白色"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor blackColor]}];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    
    self.view.backgroundColor = [UIColor whiteColor];
   
    
    UIImageView *headImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 80)];
    headImage.image = [UIImage imageNamed:@"红包_headbg"];
    headImage.contentMode= UIViewContentModeScaleAspectFill;
    [self.view addSubview:headImage];
    
    _headImgae.layer.masksToBounds = YES;
    _headImgae.layer.cornerRadius =_headImgae.width/2;
       
    
    
    
    [self LoadRequestData];
    
    
}

- (void)LoadRequestData
{
    //    [SVProgressHUD showWithStatus:@"...."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_hongBaolingqulist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_cellData.hongbaoid forKey:@"hongbaoid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            [_headImgae sd_setImageWithURL:[NSURL URLWithString:data[@"touxiang"]] placeholderImage:[UIImage imageNamed:@"头像占位"]];
            _nameLable.text = [NSString stringWithFormat:@"%@的红包",data[@"nicheng"]];
            _titleLable.text = _cellData.titleName;
            _redcontLable.text = [NSString stringWithFormat:@"红包金额%@元，等待对方领取",data[@"money"]];
        }
        
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
