//
//  CSGroupRedHitListVC.m
//  CSChatIM
//
//  Created by Apple on 2020/12/21.
//

#import "CSGroupRedHitListVC.h"
#import "CSGroupRedHeadView.h"
#import "CSGroupRedListCell.h"
#import "CSGroupRedListModel.h"
#import "NSString+LFAdd.h"
#import "CSMoneyViewController.h"
@interface CSGroupRedHitListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;

@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) CSGroupRedHeadView *headView;
@end
static NSString *cellid = @"cellid";
@implementation CSGroupRedHitListVC
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImage *backImage = [[UIImage imageNamed:@"nav_bg"] resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    //设置导航栏背景图片
    [self.navigationController.navigationBar setBackgroundImage:backImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    
    self.view.backgroundColor = [UIColor whiteColor];
    _dataArr = [[NSMutableArray alloc]init];
    
    UIImageView *headImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, KScreenWidth, 80)];
    headImage.image = [UIImage imageNamed:@"红包_headbg"];
    headImage.contentMode= UIViewContentModeScaleAspectFill;
    [self.view addSubview:headImage];
    
    [self LoadTableView];
    
    [self LoadRequestData];
    
    
}

- (void)LoadRequestData
{
    //    [SVProgressHUD showWithStatus:@"...."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_hongBaolingqulist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_hitCellData.hongbaoid forKey:@"hongbaoid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            
            [_headView.headImage sd_setImageWithURL:[NSURL URLWithString:data[@"touxiang"]] placeholderImage:[UIImage imageNamed:@"头像占位"]];
            _headView.nameLable.text = data[@"nicheng"];
            _headView.redHitLable.text = data[@"hongbaoname"];
            
            NSString *money = [NSString formatDecimalNumber:[NSString stringWithFormat:@"%@",data[@"lingqumoney"]]];
            if ([money isEqualToString:@"0.00"]) {
                _headView.moneyLable.hidden = YES;
                _headView.moneyHit.hidden = YES;
                _headView.walletBtn.hidden = YES;
            }
            _headView.moneyLable.text = [NSString stringWithFormat:@"%@",money];
            NSString *zong  = [NSString stringWithFormat:@"%@",data[@"zong"]];
            NSString *yilingqu  = [NSString stringWithFormat:@"%@",data[@"yilingqu"]];
            NSString *money1 = [NSString formatDecimalNumber:[NSString stringWithFormat:@"%@",data[@"money"]]];
            _headView.yilingquLable.text  = [NSString stringWithFormat:@"已领取%@/%@，共%@元",yilingqu,zong,money1];
            
            NSArray *list = data[@"list"];
            NSMutableArray  *dataArr = [[NSMutableArray alloc]init];
            for (int i=0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSGroupRedListModel *model = [[CSGroupRedListModel alloc]initWithDic:dic];
                [dataArr  addObject:model];
            }
            [_dataArr addObjectsFromArray:dataArr];
            [_myTableView reloadData];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadTableView
{
    
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 80, KScreenWidth, KScreenHeight-80) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor whiteColor];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSGroupRedListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
    CSGroupRedHeadView *headView = [[CSGroupRedHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 150)];
    _headView = headView;
    [_headView.walletBtn addTarget:self action:@selector(moneyClick:) forControlEvents:UIControlEventTouchUpInside];
 
    
    _myTableView.tableHeaderView.height = 150;
    _myTableView.tableHeaderView = headView;
    headView.backgroundColor =[UIColor whiteColor];
#pragma mark -- 跳转我的钱包查看
    headView.walletBtnClick = ^(CSGroupRedHeadView * _Nonnull view) {
        
        
    };
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CSGroupRedListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSGroupRedListModel *model = _dataArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.lingqutouxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = model.lingqunicheng;
    cell.timeLable.text = model.jieshudate;
    cell.moneyLable.text = model.onemoney;
    cell.huangguanImage.hidden = YES;
    cell.optimumLable.hidden = YES;
    if ([model.shifouzuijia isEqualToString:@"1"]) {
        cell.huangguanImage.hidden = NO;
        cell.optimumLable.hidden = NO;
    }
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)moneyClick:(UIButton *)sender
{
    CSMoneyViewController *vc = [[CSMoneyViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
