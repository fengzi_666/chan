//
//  CSChatRedViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "CSChatRedViewController.h"
#import "CSPayPsdViewController.h"
#import "CSPayCodeViewController.h"
#import "CSChatRedPasdPopView.h"
#import "XMPayCodeView.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "CSRedCodeView.h"
#import "SFHFKeychainUtils.h"
#import "liaotianshiViewController.h"
@interface CSChatRedViewController ()<UITextFieldDelegate>
//输入钱
@property (weak, nonatomic) IBOutlet UITextField *moneyTF;
//输入数量
@property (weak, nonatomic) IBOutlet UITextField *numberTF;
@property (weak, nonatomic) IBOutlet UITextField *redTitleTF;
@property (weak, nonatomic) IBOutlet UILabel *moneyLable;
@property (weak, nonatomic) IBOutlet UIButton *RedBtn;
@property (weak, nonatomic) IBOutlet UIView *numberView;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numberH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headViewH;
@property (nonatomic ,strong) CSChatRedPasdPopView *redPsdPopView;
@property (nonatomic ,strong) CSRedCodeView *foodView;
@property (nonatomic ,strong)  XMPayCodeView *codeView;
@property (nonatomic ,copy) NSString *paypsdStr;
//@property (nonatomic ,copy) NSString *userid;
@property (nonatomic ,copy) NSString *qunid;
@property V2TIMUserFullInfo *userInfo;

@end

@implementation CSChatRedViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImage *backImage = [[UIImage imageNamed:@"nav_bg"] resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
    //设置导航栏背景图片
    [self.navigationController.navigationBar setBackgroundImage:backImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
    //点击编辑的时候清空
    _redTitleTF.clearsOnBeginEditing = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //设置导航栏背景图片为一个空的image，这样就透明了
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    
    
    [self setNav];
    
    self.navigationItem.title = @"发红包";
  
    int money = [self.moneyTF.text intValue];
    int number = [self.numberTF.text intValue];
    int zonhe = money *number;
   
    [self.moneyTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.numberTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _moneyTF.delegate = self;
    //点击编辑的时候清空
    
    if ([_istype isEqualToString:@"1"]) {
        [_numberH setConstant:1];
        _numberView.hidden = YES;
        [_headViewH setConstant:160];
    }
    if (_goupid.length>0) {
        //获取群组资料自定义字段
        [[TIMGroupManager sharedInstance]getGroupInfo:@[self.goupid] succ:^(NSArray *groupList) {
           
            TIMGroupInfo *info =groupList[0];
            NSDictionary *dict = info.customInfo;
//            NSString *quncode = [[ NSString alloc]initWithData:dict[@"quncode"] encoding:NSUTF8StringEncoding];
//            NSString *qunerweima = [[ NSString alloc]initWithData:dict[@"qunerweima"] encoding:NSUTF8StringEncoding];
            NSString *qunid = [[ NSString alloc]initWithData:dict[@"qunid"] encoding:NSUTF8StringEncoding];
            _qunid = qunid;
            NSLog(@"%@",info.customInfo);
          
            } fail:^(int code, NSString *msg) {
                
            }];
    }
    if (_userid.length>0) {
        [[V2TIMManager sharedInstance] getUsersInfo:@[_userid] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
            
            self.userInfo = infoList.firstObject;
            NSDictionary *dic =_userInfo.customInfo;
            _userid = [[ NSString alloc]initWithData:dic[@"userid"] encoding:NSUTF8StringEncoding];
            
    } fail:nil];
    }
   
}
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField ==_numberTF) {
        int money = [self.moneyTF.text intValue];
        int number = [self.numberTF.text intValue];
        int zonghe = money *number;
        NSLog(@"%d",zonghe);
//        _moneyLable.text = [NSString stringWithFormat:@"%d",money];
//        NSLog(@"%@")
    }
    if (textField ==_moneyTF) {
        int money = [self.moneyTF.text intValue];
        int number = [self.numberTF.text intValue];
        int zonghe = money *number;
        NSLog(@"%d",zonghe);
        float fValue =round(money*100)/100;
//        NSString *valueStr = [NSString stringWithFormat:@"%.2f",fValue];
//        _moneyLable.text = valueStr;
//        NSLog(@"%@")
    }
  
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text             = _moneyTF.text;
    NSString *decimalSeperator = @".";
    NSCharacterSet *charSet    = nil;
    NSString *numberChars      = @"0123456789";

    if ([string isEqualToString:decimalSeperator] && [text length] == 0) {
        return NO;
    }

    NSRange decimalRange = [text rangeOfString:decimalSeperator];
    BOOL isDecimalNumber = (decimalRange.location != NSNotFound);
    if (isDecimalNumber) {
        charSet = [NSCharacterSet characterSetWithCharactersInString:numberChars];
        if ([string rangeOfString:decimalSeperator].location != NSNotFound) {
            return NO;
        }
    }
    else {
        numberChars = [numberChars stringByAppendingString:decimalSeperator];
        charSet = [NSCharacterSet characterSetWithCharactersInString:numberChars];
    }

    NSCharacterSet *invertedCharSet = [charSet invertedSet];
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:invertedCharSet];
    text = [text stringByReplacingCharactersInRange:range withString:trimmedString];

    if (isDecimalNumber) {
        NSArray *arr = [text componentsSeparatedByString:decimalSeperator];
        if ([arr count] == 2) {
            if ([arr[1] length] > 2) {
                return NO;
            }
        }
    }

    textField.text = text;
    int money = [self.moneyTF.text intValue];
    int number = [self.numberTF.text intValue];
    int zonghe = money *number;
    _moneyLable.text =[NSString stringWithFormat:@"%@",text];
    NSLog(@"%d",zonghe);
   
    
    return NO;
}


- (IBAction)redBtn:(UIButton *)sender {
    if ([_moneyTF.text intValue]<1) {
        [SVProgressHUD showInfoWithStatus:@"金额不能少于0"];
        return;
    }
    if ([_moneyTF.text intValue]>99999999) {
        [SVProgressHUD showInfoWithStatus:@"金额过大"];
        return;
    }
    
    
    
    [self LoadRequstData];
    
}
BOOL istype1 = NO;
//发送红包之前先检测是否设置过支付密码
- (void)LoadRequstData
{
//    [SVProgressHUD showWithStatus:@""];
    if ([self.redTitleTF.text isEqualToString: @""]||self.redTitleTF.text==nil){
        _redTitleTF.text= @"恭喜发财，大吉大利";
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_jiaoyistate];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    __weak typeof(self) weakSelf = self;
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *jiaoyistate = [NSString stringWithFormat:@"%@",data[@"jiaoyistate"]];
            if ([jiaoyistate isEqualToString:@"1"]) {
                [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;
            
            

                    weakSelf.redPsdPopView = [[CSChatRedPasdPopView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
                    weakSelf.redPsdPopView.moneyLable.text = [NSString stringWithFormat:@"¥%@",_moneyLable.text];
                    [weakSelf.redPsdPopView.deleteBtn addTarget:self action:@selector(redPopClick:) forControlEvents:UIControlEventTouchUpInside];
                    [weakSelf.view addSubview:weakSelf.redPsdPopView];
                    // 支付密码输入框
                    XMPayCodeView *payCodeView = [[XMPayCodeView alloc]init];

                    _codeView = payCodeView;
                    [weakSelf.redPsdPopView.popView addSubview:payCodeView];
                    [payCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(weakSelf.redPsdPopView.popView.mas_left).offset(25);
                        make.right.equalTo(weakSelf.redPsdPopView.popView.mas_right).offset(-25);
                        make.bottom.equalTo(weakSelf.redPsdPopView.popView.mas_bottom).offset(-30);
                        make.height.equalTo(@55);
                    
                   
                    payCodeView.secureTextEntry = YES;  //
                    
                    [payCodeView setPayBlock:^(NSString *payCode) {
                        NSLog(@"payCode==%@",payCode);
                        _paypsdStr = payCode;
                        
                        if (payCode.length<=5) {
                            istype1 = NO;
                        }
                        if (payCode.length==6) {
                           
                            if (istype1==YES) {
                                return;
                            }
                            istype1 = YES;
                          
                            
                            if ([_istype isEqualToString:@"2"]) {
                                [self LoadRequestPostSendRedData:payCode istype:@""];
                            }
                            if ([_istype isEqualToString:@"1"]) {
                                [self LoadRequestPostSendRedData1:payCode istype:@""];
                            }
                           
                            
                          
                        }

                    }];
                     //1秒后，让密码输入成为第一响应
                   
                        [payCodeView becomeKeyBoardFirstResponder];
                    
                }];
            }
            if ([jiaoyistate isEqualToString:@"2"]) {
               [SVProgressHUD showInfoWithStatus:@"请先设置支付密码"];
                CSPayCodeViewController *vc = [[CSPayCodeViewController alloc]init];
                vc.type = @"1";
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }
        else
        {
//            [SVProgressHUD showSuccessWithStatus:dict[@"<#msg#>"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
#pragma mark -- 1V1发送红包
- (void)LoadRequestPostSendRedData1:(NSString *)zhifumima istype:(NSString *)type
{
    
    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_faGeRenHongBao];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString *uuid= [self UUID];
    [dict setValue:_redTitleTF.text forKey:@"name"];
    
    [dict setValue:_moneyLable.text forKey:@"money"];
    [dict setValue:_userid forKey:@"jieshouid"];
    [dict setValue:zhifumima forKey:@"zhifumima"];
   
    __weak typeof(self) weakSelf = self;
   
    [longwangRequestManage postBaseRedWithURL:url param:dict uuid:uuid success:^(NSDictionary *dict) {
        NSString *code= [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD dismiss];
            
//            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            
            [self  jumpVC];
            
        }
//        else if ([code isEqualToString:@"1008"])
//        {
//            //异地登录验证手机号验证码
//
//            [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
//            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
//
//            [self.view endEditing:YES];
//            CSRedCodeView *foodView = [[CSRedCodeView alloc]init];
////            weakSelf.foodView = foodView;
//
////                foodView.backgroundColor = [UIColor whiteColor];
//            [foodView.codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            [foodView.quedingBtn addTarget:self action:@selector(quedingClick:) forControlEvents:UIControlEventTouchUpInside];
//            [foodView.deleteBtn addTarget:self action:@selector(deletaBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            foodView.mobileTF.text = [UserDefaultManager shareInstance].GetLoginLoginMobile;
//            foodView.mobileTF.enabled=NO;
//            [weakSelf.redPsdPopView addSubview:foodView];
//            self.foodView  = foodView;
//
//            [foodView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.bottom.mas_equalTo(weakSelf.redPsdPopView.mas_bottom);
//                make.height.mas_equalTo(258);
//                make.right.mas_equalTo(weakSelf.redPsdPopView.mas_right);
//                make.left.mas_equalTo(weakSelf.redPsdPopView.mas_left);
//            }];
//
//            NSNotificationCenter * center=[NSNotificationCenter defaultCenter];
//            [center addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
////            [[[UIApplication  sharedApplication ]keyWindow ] addSubview : foodView ] ;
//
//        }
        else
        {
            [_redPsdPopView removeFromSuperview];
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
          
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
- (void)LoadRequestPostSendRedData:(NSString *)zhifumima istype:(NSString *)type
{
    
    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_fahongbaoQun];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString *uuid= [self UUID];
    [dict setValue:_redTitleTF.text forKey:@"name"];
    
    [dict setValue:_moneyLable.text forKey:@"money"];
    [dict setValue:_qunid forKey:@"qunid"];
    [dict setValue:zhifumima forKey:@"zhifumima"];
    if ([_istype isEqualToString:@"1"]) {
        [dict setValue:@"1" forKey:@"shulang"];
    }
    else
    {
        [dict setValue:_numberTF.text forKey:@"shuliang"];
    }
   
    __weak typeof(self) weakSelf = self;
   
    [longwangRequestManage postBaseRedWithURL:url param:dict uuid:uuid success:^(NSDictionary *dict) {
        NSString *code= [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD dismiss];
            if ([type isEqualToString:@"1"]) {
                [_foodView removeFromSuperview];
            }
//            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
            
            [self  jumpVC];
            
        }
//        else if ([code isEqualToString:@"1008"])
//        {
//            //异地登录验证手机号验证码
//
//            [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;
//            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
//
//            [self.view endEditing:YES];
//            CSRedCodeView *foodView = [[CSRedCodeView alloc]init];
////            weakSelf.foodView = foodView;
//
////                foodView.backgroundColor = [UIColor whiteColor];
//            [foodView.codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            [foodView.quedingBtn addTarget:self action:@selector(quedingClick:) forControlEvents:UIControlEventTouchUpInside];
//            [foodView.deleteBtn addTarget:self action:@selector(deletaBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//            foodView.mobileTF.text = [UserDefaultManager shareInstance].GetLoginLoginMobile;
//            foodView.mobileTF.enabled=NO;
//            [weakSelf.redPsdPopView addSubview:foodView];
//            self.foodView  = foodView;
//
//            [foodView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.bottom.mas_equalTo(weakSelf.redPsdPopView.mas_bottom);
//                make.height.mas_equalTo(258);
//                make.right.mas_equalTo(weakSelf.redPsdPopView.mas_right);
//                make.left.mas_equalTo(weakSelf.redPsdPopView.mas_left);
//            }];
//
//            NSNotificationCenter * center=[NSNotificationCenter defaultCenter];
//            [center addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
////            [[[UIApplication  sharedApplication ]keyWindow ] addSubview : foodView ] ;
//
//        }
        else
        {
            [_redPsdPopView removeFromSuperview];
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
          
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
- (void)deletaBtnClick:(UIButton *)sender
{
//    [self.foodView removeFromSuperview];
    [self.redPsdPopView removeFromSuperview];
}
- (void)codeBtnClick :(UIButton *)sender
{
    [sender startCountDownTime:60 withCountDownBlock:^{
        NSLog(@"开始倒计时");
        //此处发送验证码等操作
        //................
        [self LoadRequestCode];
        
    }];
}
- (void)LoadRequestCode
{
//    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sendYDYZMSMS];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_foodView.mobileTF.text forKey:@"loginname"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)quedingClick :(UIButton *)sender
{
    if (_paypsdStr.length==6) {
        [self LoadRequestPostSendRedData:_paypsdStr istype:@"1"];
        
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@"请输入6位支付密码"];
    }
 
}
-(void)keyboardWillChange:(NSNotification *)note
{
        //获取键盘弹起时的frame值
        CGRect rect=[note.userInfo[UIKeyboardFrameEndUserInfoKey]CGRectValue];
        //获取键盘弹起时的Y值
        CGFloat keyboardY=rect.origin.y;
        //计算view要上移动多少就是键盘的y值减去屏幕的高度就能得到需要向上移动多少，最后y值变回和屏幕高度一样就又回来了，注意这里不能说我之前比如说变了-100，
         //要想变回来变成正100那是错了，要回到原点才是对的。
        CGFloat transformY=keyboardY-self.view.frame.size.height;
        self.foodView.transform=CGAffineTransformMakeTranslation(0, transformY);

}
- (NSString*)UUID {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *userName = infoDict[@"CFBundleName"];
    NSString *serviceName = infoDict[@"CFBundleIdentifier"];
    NSString *UUID = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:serviceName error:nil];
    if (UUID.length==0) {
        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SFHFKeychainUtils storeUsername:userName andPassword:UUID forServiceName:serviceName updateExisting:1 error:nil];
        
    }
    
    return UUID;

}
- (void)redPopClick:(UIButton *)sender
{
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=NO;

    __weak typeof(self) weakSelf = self;
      //执行的动画
    [UIView animateWithDuration:2 animations:^{
        
        [weakSelf.redPsdPopView removeFromSuperview];
    }];
}
//跳转指定控制器
- (void)jumpVC
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[liaotianshiViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
//移除通知
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
