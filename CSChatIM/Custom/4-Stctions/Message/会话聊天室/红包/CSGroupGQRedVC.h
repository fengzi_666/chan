//
//  CSGroupGQRedVC.h
//  CSChatIM
//
//  Created by Apple on 2020/12/26.
//

#import "BaseViewController.h"
#import "CSRedChatData.h"
#import "CSGroupRedHeadModel.h"
#import "CSChatRedHitData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSGroupGQRedVC : BaseViewController
@property (nonatomic ,strong) CSRedChatData *cellData;
@property (nonatomic ,strong) CSChatRedHitData *hitCellData;
@property (nonatomic ,strong) CSGroupRedHeadModel *headModel;
@end

NS_ASSUME_NONNULL_END
