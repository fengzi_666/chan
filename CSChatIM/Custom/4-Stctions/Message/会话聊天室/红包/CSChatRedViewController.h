//
//  CSChatRedViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSChatRedViewController : BaseViewController
@property (nonatomic ,copy) NSString *istype;//类型 1.单 2.群
@property (nonatomic ,copy) NSString *goupid;//类型 .群id
@property (nonatomic ,copy) NSString *userid;//类型 .个人id
@property (nonatomic ,copy) NSString *chatid;//会话id
@end

NS_ASSUME_NONNULL_END
