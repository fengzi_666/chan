//
//  tianjiashiwuPopViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/26.
//

#import "tianjiashiwuPopViewController.h"
#import "TUIImageMessageCell.h"
#import "aihexiwangImageViewController.h"
#import "TUIGroupPendencyViewModel.h"
#import "CSRedChatData.h"
#import "CSChatRedCell.h"
#import "CSChatRedPopView.h"
#import "CSGroupOpenRedVC.h"
#import "CSGroupRedHeadModel.h"
#import "CSRedguoqiView.h"
#import "CSGroupGQRedVC.h"
#import "TUISelectMemberViewController.h"
#import "NSBundle+TUIKIT.h"
@interface tianjiashiwuPopViewController ()<TMessageControllerDelegate,TMessageCellDelegate,V2TIMGroupListener>
@property (nonatomic, strong) TUIConversationCellData *conversationData;
@property (nonatomic, strong) TUIGroupPendencyViewModel *pendencyViewModel;
@property (nonatomic ,strong) CSChatRedPopView *popView;
@property (nonatomic, strong) NSMutableArray<UserModel *> *atUserList;
@end

@implementation tianjiashiwuPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.messageController.delegate = self;
    self.atUserList = [[NSMutableArray alloc]init];
}
- (void)onReceiveRESTCustomData:(NSString *)groupID data:(NSData *)data
{
    NSLog(@"1111");
}

 - (instancetype)initWithConversation:(TUIConversationCellData *)conversationData
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.conversationData = conversationData;
        NSMutableArray *moreMenus = [NSMutableArray array];
        [moreMenus addObject:[TUIInputMoreCellData photoData]];
        [moreMenus addObject:[TUIInputMoreCellData pictureData]];
        [moreMenus addObject:[TUIInputMoreCellData videoData]];
        [moreMenus addObject:[TUIInputMoreCellData fileData]];
       
        self.moreMenus = moreMenus;

        if (self.conversationData.groupID.length > 0) {
            _pendencyViewModel = [TUIGroupPendencyViewModel new];
            _pendencyViewModel.groupId = _conversationData.groupID;
        }
        
    }
    return self;
}

- (void)inputControllerDidInputAt:(TUIInputController *)inputController
{
    // 检测到 @ 字符的输入
    if (_conversationData.groupID.length > 0) {
        TUISelectMemberViewController *vc = [[TUISelectMemberViewController alloc] init];
        vc.groupId = _conversationData.groupID;
        vc.name = TUILocalizableString(TUIKitAtSelectMemberTitle); // @"选择群成员";
        vc.optionalStyle = TUISelectMemberOptionalStyleAtAll;
        vc.selectedFinished = ^(NSMutableArray<UserModel *> * _Nonnull modelList) {
            NSMutableString *atText = [[NSMutableString alloc] init];
            for (int i = 0; i < modelList.count; i++) {
                UserModel *model = modelList[i];
                [self.atUserList addObject:model];
                if (i == 0) {
                    [atText appendString:[NSString stringWithFormat:@"%@ ",model.name]];
                } else {
                    [atText appendString:[NSString stringWithFormat:@"@%@ ",model.name]];
                }
            }
            NSString *inputText = self.inputController.inputBar.inputTextView.text;
            self.inputController.inputBar.inputTextView.text = [NSString stringWithFormat:@"%@%@ ",inputText,atText];
            [self.inputController.inputBar updateTextViewFrame];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)messageController:(TUIMessageController *)controller onSelectMessageContent:(TUIMessageCell *)cell
{
    [self.view endEditing:YES];
    if ([cell isKindOfClass:[TUIImageMessageCell class]]) {
        [self showImageMessage:(TUIImageMessageCell *)cell];
    }
    //红包回调
    
    if ([cell isKindOfClass:[CSChatRedCell class]]) {
        CSRedChatData *cellData = [(CSChatRedCell *)cell redData];
        
        //获取当前时间
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
        
        NSString *endtime = [self timewithIntervasting:cellData.datetime];
        
        int comparisonResult = [self compareDate:endtime withDate:currentDateStr];
        
       if(comparisonResult >0){
         //endDate 大
           CSRedguoqiView *guoqiView = [[CSRedguoqiView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
           [guoqiView.headImage sd_setImageWithURL:[NSURL URLWithString:cellData.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
           guoqiView.nameLable.text = cellData.nicheng;
           guoqiView.contentLable.text = @"该红包已超过24小时。\n如已领取可在“红包记录”中\n查看详情。";
           [self.view addSubview:guoqiView];
          
       }
        else
        {
            __weak typeof(self) weakSelf = self;
            
            //查询红包是否开启状态
            NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_hongBaoState];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setValue:cellData.hongbaoid forKey:@"hongbaoid"];
            [dict setValue:cellData.jieshouid forKey:@"qunid"];
            [SVProgressHUD showWithStatus:nil];
            [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
                [SVProgressHUD dismiss];
                NSLog(@"%@",dict);
                NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
                if ([code isEqualToString:@"0"]) {
                    
                    NSDictionary *data = dict[@"data"];
                    NSString *state = [NSString stringWithFormat:@"%@",data[@"state"]];
                    //1.待领取 2.已领取 3.红包已抢完
                    if ([state isEqualToString:@"1"]) {
                        CSChatRedPopView *popView = [[CSChatRedPopView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
                        popView.titleLable.text = cellData.name;
                        popView.nameLable.text = cellData.nicheng;
                        _popView = popView;
                        [popView.headImage sd_setImageWithURL:[NSURL URLWithString:cellData.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
                       
                        popView.openTapClick = ^(CSChatRedPopView * _Nonnull view) {
                            NSLog(@"开启红包");
                            [weakSelf.popView removeFromSuperview];
                            [weakSelf LoadRequestOpenRed:cellData.jieshouid redid:cellData.hongbaoid cellData:cellData cell:cell];
                        };
                
                        [self.view addSubview:popView];
                    }
                    if ([state isEqualToString:@"2"]) {
                        NSLog(@"红包已领取");
                        CSGroupOpenRedVC *vc = [[CSGroupOpenRedVC alloc]init];
                        vc.istype = @"2";
                        vc.cellData = cellData;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                       
                    }
                    if ([state isEqualToString:@"3"]) {
                        NSLog(@"红包已抢完");
                        [weakSelf LoadRedOver:cell data:cellData];
                    }
                    
                    if ([state isEqualToString:@"4"]) {
                        CSGroupGQRedVC *vc = [[CSGroupGQRedVC alloc]init];
                       
                        vc.cellData = cellData;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                       
                    }
                }
                else
                {
    //
                }
            } error:^(NSString *error) {
                [SVProgressHUD  dismiss];
            }];
                  
        }
     }
    
}
-(NSString *)timewithIntervasting:(NSString*)string{
    
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[string doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];//更改自己想要的时间格式
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
    
    
}
//比较两个日期大小
-(int)compareDate:(NSString*)startDate withDate:(NSString*)endDate{
    
    int comparisonResult;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] init];
    date1 = [formatter dateFromString:startDate];
    date2 = [formatter dateFromString:endDate];
    NSComparisonResult result = [date1 compare:date2];
//    NSLog(@"result==%ld",(long)result);
    switch (result)
    {
            //date02比date01大
        case NSOrderedAscending:
            comparisonResult = 1;
            break;
            //date02比date01小
        case NSOrderedDescending:
            comparisonResult = -1;
            break;
            //date02=date01
        case NSOrderedSame:
            comparisonResult = 0;
            break;
        default:
            NSLog(@"erorr dates %@, %@", date1, date2);
            break;
    }
    return comparisonResult;
}
- (void)LoadRedOver:(CSChatRedCell *)cell data:(CSRedChatData *)data
{
    CSGroupOpenRedVC *vc = [[CSGroupOpenRedVC alloc]init];
    vc.istype = @"3";
    vc.cellData = data;
    CSRedChatData *cellData = [(CSChatRedCell *)cell redData];
    cellData.yilingqu = @"1";
    cellData.innerMessage.localCustomInt =1;
    cell.redImage.image = [UIImage imageNamed:@"红包_已开启"];
    [cell fillWithData:cellData];
    if (_conversationData.userID.length>0) {
        vc.danliao = @"1";
    }
    [cellData.innerMessage localCustomData];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)LoadRequestOpenRed:(NSString *)groupind redid:(NSString *)redid cellData:(CSRedChatData *)cellData  cell:(CSChatRedCell *)cell
{
    [SVProgressHUD showWithStatus:@"开启红包"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lingquzhuanshu];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
  
    if (_conversationData.userID>0) {
        url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lingQuGeRen];
        [dict setValue:redid forKey:@"hongbaoid"];
    }
    if (_conversationData.groupID>0) {
        [dict setValue:groupind forKey:@"qunid"];
        [dict setValue:redid forKey:@"hongbaoid"];
    }
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSDictionary *data = dict[@"data"];
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        
        
      
        if ([code isEqualToString:@"0"]) {
           
            [SVProgressHUD dismiss];
            CSRedChatData *cellData = [(CSChatRedCell *)cell redData];
            cellData.yilingqu = @"1";
            cellData.innerMessage.localCustomInt =1;
            cell.redImage.image = [UIImage imageNamed:@"红包_已开启"];
            [cell fillWithData:cellData];
            [cellData.innerMessage localCustomData];
            
            CSGroupRedHeadModel *model = [[CSGroupRedHeadModel alloc]initWithDic:data];
            CSGroupOpenRedVC *vc = [[CSGroupOpenRedVC alloc]init];
            vc.cellData = cellData;
            vc.headModel = model;
            if (_conversationData.userID.length>0) {
                vc.danliao = @"1";
            }
            vc.istype = @"1";
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}

- (void)showImageMessage:(TUIImageMessageCell *)cell
{
    NSLog(@"11111");
    aihexiwangImageViewController *image = [[aihexiwangImageViewController alloc] init];
    image.data = [cell imageData];
    [self.navigationController pushViewController:image animated:YES];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"-------成功释放");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
