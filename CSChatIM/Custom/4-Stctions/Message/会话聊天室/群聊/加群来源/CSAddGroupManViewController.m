//
//  CSAddGroupManViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/16.
//

#import "CSAddGroupManViewController.h"

@interface CSAddGroupManViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *IDLable;
@property (weak, nonatomic) IBOutlet UILabel *jianjie;

@end

@implementation CSAddGroupManViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"邀请人信息";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self LoadRequestData];
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.width/2;
}
- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunYaoQingInFo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_quntxcode forKey:@"quntxcode"];
    [dict setValue:_tengxuncode forKey:@"tengxuncode"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *user = data[@"user"];
            [_headImage sd_setImageWithURL:[NSURL URLWithString:user[@"touxiang"]] placeholderImage:[UIImage imageNamed:@"头像占位"]];
            _nameLable.text =user[@"nicheng"];
            _IDLable.text = [NSString stringWithFormat:@"ID:    %@",user[@"usercode"]];
            
//            [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
