//
//  CSForbidRedViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "CSForbidRedViewController.h"
#import "CSForbidTableViewCell.h"
#import "CSGoupListModel.h"
#import "xinxipingtaiViewController.h"
static NSString *cellid = @"cellid";
@interface CSForbidRedViewController ()<UITableViewDelegate,UITableViewDataSource,CSForbidTableViewCellDelegate>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) NSIndexPath *index;
@property (nonatomic ,copy) NSString *selectID;
@property (nonatomic ,strong) NSMutableArray *selectUserArr;//选中的userid数组
@property V2TIMGroupInfo *groupInfo;
@property (nonatomic,assign) NSInteger pageno;
@end

@implementation CSForbidRedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"红包领取权限";
    
    //初始化多选数组
    
    _dataArr = [[NSMutableArray alloc]init];
    
    [self setNav];
    
    [self LoadTableView];
   

}

- (void)LoadRequestData
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appqunchengyuanlist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString *shenfen ;
    if (_mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_ADMIN)
    {
        shenfen = @"3";
    }
   
    if (_mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_SUPER)
    {
        shenfen = @"2,3";
    }
    [dict setValue:_groupid forKey:@"qunid"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [dict setValue:shenfen forKey:@"shenfen"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        NSMutableArray *dataArr = [[NSMutableArray alloc]init];
        
        if ([code isEqualToString:@"0"]) {
//                        [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
            NSDictionary *data  = dict[@"data"];
            NSArray *list = data[@"list"];
            for (int i =0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSGoupListModel *model = [[CSGoupListModel alloc]initWithDic:dic];
               [dataArr addObject:model];
            
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (list.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (dataArr.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
         [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
    
}

- (void)LoadTableView
{
    
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSForbidTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    //这种上拉刷新footer在tableview的底部
    [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        self.dataArr =[NSMutableArray array];
        [self LoadRequestData];
    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequestData];
    }];
   [_myTableView.mj_header beginRefreshing];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSForbidTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSGoupListModel *model = _dataArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = model.nicheng;
    if ([model.shenfen isEqualToString:@"1"]) {
        cell.hitLable.text = @"群主";
    }
    else if([model.shenfen isEqualToString:@"2"])
    {
        cell.hitLable.text = @"管理员";
    }
    else
    {
        cell.hitLable.hidden = YES;
        cell.hitLayerbg.hidden = YES;
    }
    if ([model.hongbaostate  isEqualToString:@"1"]) {
        cell.switchBtn.on=YES;
    }else
    {
        cell.switchBtn.on=NO;
    }
    cell.delegate = self;
    cell.indexPasth = indexPath;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)switchBtnClickCell:(CSForbidTableViewCell *)cell tag:(NSString *)tag swich:(UISwitch *)swich
{
    CSGoupListModel *model = _dataArr[cell.indexPasth.row];
    if ([model.hongbaostate isEqualToString:@"1"]) {
    //如果是开启状态  设置关闭状态
        [self LoadRequestSwichData:@"2" model:model swich:swich];

    }
    if ([model.hongbaostate isEqualToString:@"2"]) {
    //如果是关闭状态 设置开启状态

        [self LoadRequestSwichData:@"1" model:model swich:swich];
    }
}
- (void)LoadRequestSwichData:(NSString *)jinyan  model:(CSGoupListModel *)model swich:(UISwitch *)swich
{
    [SVProgressHUD showWithStatus:@"设置中"];
    
     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateHongBaoState];
     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
     [dict setValue:model.userid forKey:@"userid"];
    [dict setValue: jinyan forKey:@"state"];
    [dict setValue:_groupid forKey:@"qunid"];
     [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            if ([jinyan isEqualToString:@"1"]) {
                model.hongbaostate =@"1";
                swich.on=YES;
            }
            if ([jinyan isEqualToString:@"2"]) {
                swich.on=NO;
                model.hongbaostate =@"2";
            }
            }

        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
//         [_myTableView reloadData];

    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
