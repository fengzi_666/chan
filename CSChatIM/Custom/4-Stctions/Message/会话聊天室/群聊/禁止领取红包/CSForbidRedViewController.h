//
//  CSForbidRedViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSForbidRedViewController : BaseViewController
@property (nonatomic ,copy) NSString *groupid;
@property (nonatomic ,copy) NSString *groupId;
@property (nonatomic, strong) V2TIMGroupMemberFullInfo *mainInfo;
@end

NS_ASSUME_NONNULL_END
