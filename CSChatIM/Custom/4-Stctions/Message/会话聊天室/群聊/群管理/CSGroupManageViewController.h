//
//  CSGroupManageViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/2.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupManageViewController : BaseViewController
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic ,copy) NSString *groupCode;
@property (nonatomic, strong) V2TIMGroupMemberFullInfo *selfInfo;
@end

NS_ASSUME_NONNULL_END
