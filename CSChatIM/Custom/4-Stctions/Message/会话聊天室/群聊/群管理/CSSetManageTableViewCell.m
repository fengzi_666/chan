//
//  CSSetManageTableViewCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/8.
//

#import "CSSetManageTableViewCell.h"

@implementation CSSetManageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.height/2;
}
- (IBAction)cellBtnClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickButtonCell:index:)]) {
            [self.delegate didClickButtonCell:self.canBtn index:_index];
        }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
