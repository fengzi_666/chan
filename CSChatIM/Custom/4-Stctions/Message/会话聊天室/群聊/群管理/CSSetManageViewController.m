//
//  CSSetManageViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/8.
//

#import "CSSetManageViewController.h"
#import "CSGroupListTableViewCell.h"
#import "CSGoupListModel.h"
#import "CSSetManageTableViewCell.h"
@interface CSSetManageViewController ()<UITableViewDelegate,UITableViewDataSource,CSSetManageTableViewCellDelele>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,assign) NSInteger pageno;

@end
static NSString *cellid = @"cellid";
@implementation CSSetManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"管理员设置";
    _dataArr = [[NSMutableArray alloc]init];
    
    [self setNav];
    
    [self LoadTableView];
}
- (void)LoadRequestData
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appqunchengyuanlist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupCode forKey:@"qunid"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        NSMutableArray *dataArr = [[NSMutableArray alloc]init];
        
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data  = dict[@"data"];
            NSArray *list = data[@"list"];
            for (int i =0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSGoupListModel *model = [[CSGoupListModel alloc]initWithDic:dic];
                if ([model.shenfen isEqualToString:@"1"]) {
                }
                else
                {
                    [dataArr addObject:model];
                    
                }
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (list.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (dataArr.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
        [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
    
}

- (void)LoadTableView
{

    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSSetManageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    
     //这种上拉刷新footer在tableview的底部
     [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
     _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         _pageno =1;
         self.dataArr =[NSMutableArray array];
         [self LoadRequestData];
     }];
     self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
         self.pageno ++;
         [self LoadRequestData];
     }];
    [_myTableView.mj_header beginRefreshing];
    [self.view addSubview:_myTableView];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSSetManageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSGoupListModel *model = _dataArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.index = indexPath;
    cell.nameLable.text = model.nicheng;
    cell.delegate = self;
    if ([model.shenfen isEqualToString:@"2"]) {
        [cell.canBtn setTitle:@"取消管理员" forState:UIControlStateNormal];
        [cell.canBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        cell.canBtn.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        [cell.canBtn setTitle:@"设置管理员" forState:UIControlStateNormal];
        [cell.canBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.canBtn.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
    }
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}
- (void)didClickButtonCell:(UIButton *)button index:(NSIndexPath *)index
{
    NSLog(@"-----%ld",(long)index.row);
    CSGoupListModel *model = _dataArr[index.row];
    if ([model.shenfen isEqualToString:@"2"]) {
        [self LoadSetManageRequest:_groupCode userid:model.userid shenfen:@"3" sender:button model:model];
        
    }
    if ([model.shenfen isEqualToString:@"3"]) {
        [self LoadSetManageRequest:_groupCode userid:model.userid shenfen:@"2" sender:button model:model];
    }
//    [button setTitle:@"取消管理员" forState:UIControlStateNormal];
  
}
- (void)LoadSetManageRequest :(NSString *)qunid userid:(NSString *)userid shenfen:(NSString *)shenfen sender:(UIButton *)sender model :(CSGoupListModel *)model
{
    
    [SVProgressHUD showWithStatus:@"请稍后"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateshenfen];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:qunid forKey:@"qunid"];
    [dict setValue:userid forKey:@"userid"];
    [dict setValue:shenfen forKey:@"shenfen"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            if ([shenfen isEqualToString:@"3"]) {
                [SVProgressHUD showSuccessWithStatus:@"取消管理员成功"];
                [sender setTitle:@"设置管理员" forState:UIControlStateNormal];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                sender.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
                model.shenfen=@"3";
                [_myTableView reloadData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:@"设置管理员成功"];
                [sender setTitle:@"取消管理员" forState:UIControlStateNormal];
                [sender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                sender.backgroundColor = [UIColor whiteColor];
                model.shenfen=@"2";
            }
           
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
/*
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
