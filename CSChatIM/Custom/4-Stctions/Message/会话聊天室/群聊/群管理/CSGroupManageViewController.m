//
//  CSGroupManageViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/2.
//

#import "CSGroupManageViewController.h"
#import "CSCommonTextCell.h"
#import "CSCommonSwitchCell.h"
#import "TUILocalStorage.h"
#import "THelper.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "CSSetManageViewController.h"
#import "anpaifanjuMainTransferVC.h"
#import "wenrouderenListViewController.h"
#import "CSOutGroupViewController.h"
#import "CSForbidRedViewController.h"
@interface CSGroupManageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong)UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *data;
@property CSCommonTextCellData *groupMembersCountCellData;
@property CSCommonTextCellData *addOptionData;
@property TUIProfileCardCellData *profileCellData;
@property CSCommonTextCellData *groupNickNameCellData;
@property (nonatomic ,copy) NSString *jinyan;
@property (nonatomic ,copy) NSString *tianjiahaoyou;
@property (nonatomic ,copy) NSString *qunid;
@property (nonatomic ,copy) NSString *yaoqingstate;
@end

@implementation CSGroupManageViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[TIMGroupManager sharedInstance]getGroupInfo:@[self.groupId] succ:^(NSArray *groupList) {
       
        TIMGroupInfo *info =groupList[0];
        NSDictionary *dict = info.customInfo;
        NSString *quncode = [[ NSString alloc]initWithData:dict[@"quncode"] encoding:NSUTF8StringEncoding];
        NSString *qunerweima = [[ NSString alloc]initWithData:dict[@"qunerweima"] encoding:NSUTF8StringEncoding];
        NSString *qunid = [[ NSString alloc]initWithData:dict[@"qunid"] encoding:NSUTF8StringEncoding];
        _qunid = qunid;
        NSString *pingbistate= [[ NSString alloc]initWithData:dict[@"pingbistate"] encoding:NSUTF8StringEncoding];
        _tianjiahaoyou = pingbistate;
        
        NSString *yaoqingstate= [[ NSString alloc]initWithData:dict[@"yaoqingstate"] encoding:NSUTF8StringEncoding];
        _yaoqingstate = yaoqingstate;
        NSString *jinyanstate= [[ NSString alloc]initWithData:dict[@"jinyanstate"] encoding:NSUTF8StringEncoding];
        _jinyan = jinyanstate;
        NSLog(@"%@",info.customInfo);
      
        [self setupData];
        
        } fail:^(int code, NSString *msg) {
            
        }];
  
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    self.navigationItem.title = @"群管理";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupData];
   
    [self LoadTableView];
    [self LoadRequestAddSate];
    //获取群组资料自定义字段
    
    
    
}
//请求开关状态
- (void)LoadRequestAddSate
{

}
#pragma mark -- 列表加载数据
- (void)setupData
{
    _data = [NSMutableArray array];
    NSMutableArray *personalArray = [NSMutableArray array];
    //section=0,row0
    if (_selfInfo.role ==V2TIM_GROUP_MEMBER_ROLE_SUPER||_selfInfo.role ==V2TIM_GROUP_MEMBER_ROLE_ADMIN) {
        CSCommonSwitchCellData *switchData = [[CSCommonSwitchCellData alloc] init];
        if ([_yaoqingstate isEqualToString:@"1"]) {
            switchData.on = YES;
        }
        else
        {
            switchData.on = NO;
        }
            
        switchData.title = @"群邀请确认";
        switchData.isType=@"1";
        switchData.subTitle = @"阿斯达多";
        switchData.cswitchSelector = @selector(didSelectOnTopRow1:);
        [personalArray addObject:switchData];
    }
   
    
    //section=0,row1
    if (_selfInfo.role ==V2TIM_GROUP_MEMBER_ROLE_SUPER) {
        CSCommonTextCellData *skillData0 = [[CSCommonTextCellData alloc] init];
        skillData0.key = @"群主管理权授予";
        skillData0.showAccessory = YES;
        skillData0.cselector = @selector(didSelectOnTopRow2:);
        [personalArray addObject:skillData0];
        
        
        CSCommonTextCellData *skillData = [[CSCommonTextCellData alloc] init];
        skillData.key = @"群主管理转让";
        skillData.showAccessory = YES;
        skillData.cselector = @selector(didSelectOnTopRow3:);
        [personalArray addObject:skillData];
        
        [self.data addObject:personalArray];
      
    }
   
    
    
    
    //section=0,row2
    
   
    
   
    
    //section=1,row0
    NSMutableArray *groupInfoArray = [NSMutableArray array];
    CSCommonSwitchCellData *switchData3 = [[CSCommonSwitchCellData alloc] init];
    if ([[[TUILocalStorage sharedInstance] topConversationList] containsObject:[NSString stringWithFormat:@"group_%@",self.groupId]]) {
       
        
       
    }
    if ([_jinyan isEqualToString:@"1"]) {
        switchData3.on = YES;
    }else
    {
        switchData3.on = NO;
    }
    switchData3.title = @"群成员禁言（除群主和管理员)";
    switchData3.cswitchSelector = @selector(didSelectOnTopRow4:);
    [groupInfoArray addObject:switchData3];
    
    //section=1,row1
   
    CSCommonSwitchCellData *switchData4 = [[CSCommonSwitchCellData alloc] init];
    if ([[[TUILocalStorage sharedInstance] topConversationList] containsObject:[NSString stringWithFormat:@"group_%@",self.groupId]]) {
        
        switchData4.on = YES;
    }
    if ([_tianjiahaoyou isEqualToString:@"1"]) {
        switchData4.on = YES;
    }else
    {
        switchData4.on = NO;
    }
    switchData4.title = @"群成员禁止互相添加好友（屏蔽账号）";
    switchData4.cswitchSelector = @selector(didSelectOnTopRow5:);
    [groupInfoArray addObject:switchData4];
        
    //section=1,row2
    CSCommonTextCellData *textData1 = [[CSCommonTextCellData alloc] init];
    textData1.key = @"群成员禁言名单";
    textData1.cselector = @selector(didSelectOnTopRow6:);
    textData1.showAccessory = YES;
    [groupInfoArray addObject:textData1];
    [self.data addObject:groupInfoArray];
    
    //section=2,row0
    NSMutableArray *listArray = [NSMutableArray array];
    //section=2,row1
    CSCommonTextCellData *textData3 = [[CSCommonTextCellData alloc] init];
    textData3.key = @"查看退群列表";
    textData3.cselector = @selector(didSelectOnTopRow8:);
    textData3.showAccessory = YES;
    [listArray addObject:textData3];
    //section=2,row2
    CSCommonTextCellData *textData4 = [[CSCommonTextCellData alloc] init];
    textData4.key = @"禁止领取红包";
    textData4.cselector = @selector(didSelectOnTopRow9:);
    textData4.showAccessory = YES;
    [listArray addObject:textData4];
    [self.data addObject:listArray];
   
    
    [self.myTableView reloadData];
    
}
- (void)LoadTableView
{
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];

    [self.view addSubview:_myTableView];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _data.count;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSMutableArray *array = _data[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *array = _data[indexPath.section];
    NSObject *data = array[indexPath.row];
    if([data isKindOfClass:[CSCommonTextCellData class]]){
        CSCommonTextCell *cell = [tableView dequeueReusableCellWithIdentifier:TKeyValueCell_ReuseId];
        if(!cell){
            cell = [[CSCommonTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TKeyValueCell_ReuseId];
        }
        [cell fillWithData:(CSCommonTextCellData *)data];
        return cell;
    }
    else if([data isKindOfClass:[CSCommonSwitchCellData class]]){
        CSCommonSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:TSwitchCell_ReuseId];
        if(!cell){
            cell = [[CSCommonSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TSwitchCell_ReuseId];
            cell.switcher.onTintColor = [UIColor colorWithHexString:@"#32965A"];
        }
        [cell fillWithData:(CSCommonSwitchCellData *)data];
        return cell;
    }
    
    return nil;
}
#pragma -- mark  群邀请状态
- (void)didSelectOnTopRow1:(CSCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        [SVProgressHUD showWithStatus:@"请稍后"];
        NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateYaoQingState];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:_qunid forKey:@"qunid"];
        [dict setValue:@"1" forKey:@"yaoqingstate"];
        [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
            if ([code isEqualToString:@"0"]) {
                [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
            }
        } error:^(NSString *error) {
            [SVProgressHUD  dismiss];
        }];

    } else {
        [SVProgressHUD showWithStatus:@"请稍后"];
        NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateYaoQingState];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:_qunid forKey:@"qunid"];
        [dict setValue:@"2" forKey:@"yaoqingstate"];
        [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
            if ([code isEqualToString:@"0"]) {
                [SVProgressHUD showSuccessWithStatus:@"设置成功"];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
            }
        } error:^(NSString *error) {
            [SVProgressHUD  dismiss];
        }];
    }
}
#pragma -- mark  群主管理权限授予
- (void)didSelectOnTopRow2:(CSCommonSwitchCell *)cell
{
    CSSetManageViewController *vc = [[CSSetManageViewController alloc]init];
    vc.groupInfo = _selfInfo;
    vc.groupId = _groupId;
    vc.groupCode  = _groupCode;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark --群主转让
- (void)didSelectOnTopRow3:(CSCommonTextCell *)cell
{
    NSLog(@"群主管理转让");
    anpaifanjuMainTransferVC *vc = [[anpaifanjuMainTransferVC alloc]init];
    vc.groupid = self.groupCode;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didSelectOnTopRow4:(CSCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        NSLog(@"开");
        [self LoadRequestSwitcherJY:@"1"];
    } else {
        NSLog(@"关");
        [self LoadRequestSwitcherJY:@"2"];
    }
}
- (void)LoadRequestSwitcherJY :(NSString *)jinyan
{
//    [SVProgressHUD showWithStatus:@""];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURLappqunjinyan];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupCode forKey:@"qunid"];
    [dict setValue:jinyan forKey:@"jinyan"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
#pragma mark --群成员禁止添加好友
- (void)didSelectOnTopRow5:(CSCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        //开启
        [self LoadRequestDataAddState:@"1"];
    } else {
        //关闭
        [self LoadRequestDataAddState:@"2"];
    }
}

- (void)LoadRequestDataAddState:(NSString *)jinzhi
{
    [SVProgressHUD showWithStatus:@"设置中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateTianJiaState];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:self.groupCode forKey:@"qunid"];
    [dict setValue:jinzhi forKey:@"state"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"设置成功"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
#pragma mark -- 群成员禁言名单
- (void)didSelectOnTopRow6:(CSCommonTextCell *)cell
{
    NSLog(@"群成员禁言名单");
    wenrouderenListViewController  *vc = [[wenrouderenListViewController alloc]init];
    vc.groupid = self.groupCode;
    vc.mainInfo = _selfInfo;
    vc.groupId = _groupId;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didSelectOnTopRow7:(CSCommonTextCell *)cell
{
    NSLog(@"不活跃成员");
}
- (void)didSelectOnTopRow8:(CSCommonTextCell *)cell
{
    NSLog(@"查看退群列表");
    CSOutGroupViewController *vc = [[CSOutGroupViewController alloc]init];
    vc.groupCode = self.groupCode;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didSelectOnTopRow9:(CSCommonTextCell *)cell
{
    NSLog(@"禁止领红包");
    CSForbidRedViewController *vc = [[CSForbidRedViewController alloc]init];
    vc.groupid = self.groupCode;
    vc.mainInfo = _selfInfo;
    vc.groupId = _groupId;
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
