//
//  CSSetManageViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/8.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSSetManageViewController : BaseViewController
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic ,strong) NSString *groupCode;
@property (nonatomic, strong) V2TIMGroupMemberFullInfo *groupInfo;
@end

NS_ASSUME_NONNULL_END
