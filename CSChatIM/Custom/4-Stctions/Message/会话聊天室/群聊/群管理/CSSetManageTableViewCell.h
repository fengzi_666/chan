//
//  CSSetManageTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CSSetManageTableViewCellDelele <NSObject>


- (void)didClickButtonCell:(UIButton *)button index:(NSIndexPath *)index;
@end
@interface CSSetManageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;

@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@property (weak, nonatomic) IBOutlet UIButton *canBtn;
@property (nonatomic ,strong) NSIndexPath *index;
@property(nonatomic,weak) id<CSSetManageTableViewCellDelele> delegate;
@end

NS_ASSUME_NONNULL_END
