//
//  CSGroupCodeViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/15.
//

#import "CSGroupCodeViewController.h"

@interface CSGroupCodeViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *IDLable;
@property (weak, nonatomic) IBOutlet UIImageView *codeImage;
@end

@implementation CSGroupCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    self.navigationItem.title = @"群二维码";
    [self setNav];
    self.IDLable .text = [NSString stringWithFormat:@"ID   %@",_groupCode];
    self.nameLable.text = _groupInfo.groupName;
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.width/2;
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:_groupInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    [self LoadRequestCodeData];
}
- (void)LoadRequestCodeData
{
//    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunerweima];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupId forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"查询成功"];
            NSDictionary *data=dict[@"data"];
            [_codeImage sd_setImageWithURL:[NSURL URLWithString:data[@"qunerweima"]]];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//截图功能
-(UIImage *)captureImageFromView:(UIView *)view
{
    CGRect screenRect = [view bounds];
    UIGraphicsBeginImageContext(screenRect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:ctx];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (IBAction)saveBtnClik:(UIButton *)sender {
    
    UIImage * image = [self captureImageFromView:self.view];
       
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

}
/**
 保存图片操作之后就会调用
 */
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) { // 保存失败
        
        [SVProgressHUD showInfoWithStatus:@"保存失败"];
    } else { // 保存成功
        [SVProgressHUD showInfoWithStatus:@"保存成功"];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
