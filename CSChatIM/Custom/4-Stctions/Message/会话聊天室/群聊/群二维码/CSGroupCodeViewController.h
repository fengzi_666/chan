//
//  CSGroupCodeViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/15.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupCodeViewController : BaseViewController
@property (nonatomic ,strong) V2TIMGroupInfo *groupInfo;

@property (nonatomic ,copy) NSString *groupId;
@property (nonatomic ,copy) NSString *groupCode;
@end

NS_ASSUME_NONNULL_END
