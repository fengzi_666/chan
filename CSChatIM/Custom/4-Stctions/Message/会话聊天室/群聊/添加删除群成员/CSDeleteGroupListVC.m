
#import "CSDeleteGroupListVC.h"
#import "chengshihuaMainTransferCell.h"
#import "CSGoupListModel.h"
#import "xinxipingtaiViewController.h"
static NSString *cellid = @"cellid";
@interface CSDeleteGroupListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) NSIndexPath *index;
@property (nonatomic ,copy) NSString *selectID;
@property (strong, nonatomic) NSMutableArray  *selectIndexs; //多选选中的行
@property (nonatomic ,strong) NSMutableArray *selectUserArr;//选中的userid数组
@property V2TIMGroupInfo *groupInfo;
@property (nonatomic,assign) NSInteger pageno;

@end

@implementation CSDeleteGroupListVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"删除群成员";
    
    //初始化多选数组
    
    _selectUserArr = [[NSMutableArray alloc]init];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"删除" style:UIBarButtonItemStylePlain target:self action:@selector(rightClick)];
    _dataArr = [[NSMutableArray alloc]init];
    
    [self setNav];
    
    [self LoadTableView];
    
}
- (void)rightClick
{
    for (int i= 0; i<_selectIndexs.count; i++) {
        NSString *str = _selectIndexs[i];
        if ([str isEqualToString:@"1"]) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            CSGoupListModel *model = _dataArr[indexPath.row];
            [_selectUserArr addObject:model.userid];
        }
    }
    
    
    NSString *useridList = [_selectUserArr componentsJoinedByString:@","];
    [SVProgressHUD showWithStatus:@"删除中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_quntiren];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:self.groupid forKey:@"qunid"];
    [dict setValue:useridList forKey:@"userid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"删除成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}



- (void)LoadRequestData
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appqunchengyuanlist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupid forKey:@"qunid"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        NSMutableArray *dataArr = [[NSMutableArray alloc]init];
        
        if ([code isEqualToString:@"0"]) {
//                        [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
            NSDictionary *data  = dict[@"data"];
            NSArray *list = data[@"list"];
            for (int i =0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSGoupListModel *model = [[CSGoupListModel alloc]initWithDic:dic];
                if (![model.shenfen isEqualToString:@"1"]) {
                    if (_mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_ADMIN) {
                        if (![model.shenfen isEqualToString:@"2"]) {
                            [dataArr addObject:model];
                        }
                    }
                    else
                    {
                        [dataArr addObject:model];
                    }
                    
                }
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (list.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (dataArr.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
           
            _selectIndexs = [[NSMutableArray alloc]initWithCapacity:0];
            for (int i = 0; i < _dataArr.count; i ++) {
                [_selectIndexs addObject:@"0"];
            }
           
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
        [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
    
}

- (void)LoadTableView
{
    
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"chengshihuaMainTransferCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    //这种上拉刷新footer在tableview的底部
    [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        self.dataArr =[NSMutableArray array];
        [self LoadRequestData];
    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequestData];
    }];
   [_myTableView.mj_header beginRefreshing];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    chengshihuaMainTransferCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSGoupListModel *model = _dataArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = model.nicheng;
    if ([model.shenfen isEqualToString:@"1"]) {
        cell.hitLable.text = @"群主";
    }
    else if([model.shenfen isEqualToString:@"2"])
    {
        cell.hitLable.text = @"管理员";
    }
    else
    {
        cell.hitLable.hidden = YES;
        cell.hitLayerbg.hidden = YES;
    }
    
    
    if ([_selectIndexs[indexPath.row] isEqualToString:@"0"]) {
        cell.selectImage.image = [UIImage imageNamed:TUIKitResource(@"icon_contact_select_normal")];
    }else
    {
        cell.selectImage.image = [UIImage imageNamed:TUIKitResource(@"icon_contact_select_selected")];
        //           cell.tintColor = BLUECOLOR;
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSGoupListModel *model = _dataArr[indexPath.row];
    chengshihuaMainTransferCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *stateStr = _selectIndexs[indexPath.row];
    
    //连续点击的时候，两种状态进行切换
    NSString *picName = [stateStr isEqualToString:@"0"]? TUIKitResource(@"icon_contact_select_selected"):TUIKitResource(@"icon_contact_select_normal");
    NSString *changeStateStr = [stateStr isEqualToString:@"0"]?@"1":@"0";
    cell.selectImage.image = [UIImage imageNamed:picName];
    
    [_selectIndexs replaceObjectAtIndex:indexPath.row withObject:changeStateStr];
    
    
}
@end
