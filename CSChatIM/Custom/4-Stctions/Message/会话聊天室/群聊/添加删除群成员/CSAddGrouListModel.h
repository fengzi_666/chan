//
//  CSAddGrouListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSAddGrouListModel : NSObject
@property (nonatomic ,copy) NSString *haoyoucode;
@property (nonatomic ,copy) NSString *haoyouid;
@property (nonatomic ,copy) NSString *haoyounicheng;
@property (nonatomic ,copy) NSString *haoyouphone;
@property (nonatomic ,copy) NSString *haoyoutouxiang;
@property (nonatomic ,copy) NSString *listid;
@property (nonatomic ,copy) NSString *tengxuncode;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
