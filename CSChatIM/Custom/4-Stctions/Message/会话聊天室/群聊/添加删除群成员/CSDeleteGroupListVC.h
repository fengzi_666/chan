//
//  CSDeleteGroupListVC.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSDeleteGroupListVC : BaseViewController
@property (nonatomic ,copy) NSString *groupid;
@property (nonatomic, strong) V2TIMGroupMemberFullInfo *mainInfo;
@property (nonatomic ,strong) NSString *istype;
@end

NS_ASSUME_NONNULL_END
