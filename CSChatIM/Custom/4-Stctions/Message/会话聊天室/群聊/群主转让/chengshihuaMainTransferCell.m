//
//  chengshihuaMainTransferCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "chengshihuaMainTransferCell.h"

@implementation chengshihuaMainTransferCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.width/2;
    self.hitLayerbg.layer.masksToBounds = YES;
    self.hitLayerbg.layer.cornerRadius = 3;
    self.hitLayerbg.layer.borderWidth=1;
    self.hitLayerbg.layer.borderColor = [UIColor colorWithHexString:@"#f79b0b"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
