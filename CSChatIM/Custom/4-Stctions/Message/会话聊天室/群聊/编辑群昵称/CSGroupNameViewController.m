//
//  CSGroupNameViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "CSGroupNameViewController.h"
#import "THelper.h"
@interface CSGroupNameViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameLable;

@end

@implementation CSGroupNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
   
}
- (void)rightBtnClick
{
    if ([_isType isEqualToString:@"1"]) {
        __weak typeof(self) weakSelf = self;
        [SVProgressHUD showWithStatus:@"修改中"];
        NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updateQunMingPian];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:_grouid forKey:@"qunid"];
    //    [dict setValue:@"" forKey:@"gonggao"];
        [dict setValue:_nameLable.text forKey:@"qunmingpian"];
        [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
            if ([code isEqualToString:@"0"]) {
                [SVProgressHUD showSuccessWithStatus:@"修改成功"];
                [weakSelf.navigationController popViewControllerAnimated:YES];
               
                weakSelf.callBackBlock(_nameLable.text);
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
            }
        } error:^(NSString *error) {
            [SVProgressHUD  dismiss];
        }];
    }
    else
    {
        [SVProgressHUD showWithStatus:@"修改中"];
        if (_nameLable.text.length>10) {
            [SVProgressHUD showInfoWithStatus:@"群名称不能大于10个字"];
            return;
        }
        NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunupdateMessage];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:_grouid forKey:@"qunid"];
    //    [dict setValue:@"" forKey:@"gonggao"];
        [dict setValue:_nameLable.text forKey:@"qunming"];
        [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
            if ([code isEqualToString:@"0"]) {
                [SVProgressHUD showSuccessWithStatus:@"修改成功"];
                [self.navigationController popViewControllerAnimated:YES];
               
                self.callBackBlock(_nameLable.text);
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
            }
        } error:^(NSString *error) {
            [SVProgressHUD  dismiss];
        }];
    }
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
