//
//  CSGroupNameViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^CallBackBlcok) (NSString *text);
@interface CSGroupNameViewController : BaseViewController
@property (nonatomic ,copy) NSString *grouid;
@property (nonatomic ,strong)V2TIMGroupInfo *info;
@property (nonatomic,copy)CallBackBlcok callBackBlock;
//1.我在群里的昵称修改
@property (nonatomic ,copy) NSString *isType;
@end

NS_ASSUME_NONNULL_END
