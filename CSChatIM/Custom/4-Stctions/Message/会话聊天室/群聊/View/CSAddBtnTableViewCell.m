//
//  CSAddBtnTableViewCell.m
//  IMChat
//
//  Created by Apple on 2020/11/27.
//

#import "CSAddBtnTableViewCell.h"

@implementation CSAddBtnTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        
        self.button.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
        self.button.titleLabel.font = [UIFont boldSystemFontOfSize:17];
       
    }
    return self;
}
- (void)fillWithData:(TUIButtonCellData *)data
{
    [super fillWithData:data];
    self.buttonData = data;
    self.button.layer.masksToBounds = YES;
    self.button.layer.cornerRadius = 1;
    [self.button setTitle:data.title forState:UIControlStateNormal];
    self.button.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
    self.button.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [self.button setBackgroundImage:[self imageWithColor:[UIColor colorWithHexString:@"#32965A"]] forState:UIControlStateHighlighted];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
