//
//  CSCommonSwitchCell.m
//  IMChat
//
//  Created by Apple on 2020/12/2.
//

#import "CSCommonSwitchCell.h"
#import "MMLayout/UIView+MMLayout.h"
#import "THeader.h"
#import "UIColor+TUIDarkMode.h"

@implementation CSCommonSwitchCellData
- (instancetype)init {
    self = [super init];
    _margin = 15;
    return self;
}

@end

@interface CSCommonSwitchCell()
@property CSCommonSwitchCellData *switchData;
@end

@implementation CSCommonSwitchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        _titleLabel = self.textLabel;
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLabel.font = [UIFont boldSystemFontOfSize:15];
        
        _switcher = [[UISwitch alloc] init];
        //将开关开启时的颜色改为蓝色
        _switcher.onTintColor = [UIColor colorWithHexString:@"#32965A"];

        self.accessoryView = _switcher;
        [self.contentView addSubview:_switcher];
        [_switcher addTarget:self action:@selector(switchClick) forControlEvents:UIControlEventValueChanged];

        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *lable = [[UILabel alloc]init];
        lable.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
        [self.contentView addSubview:lable];
        [lable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.height.mas_equalTo(1);
            make.left.mas_equalTo(self.contentView.mas_left).offset(15);
            make.right.mas_equalTo(self.contentView.mas_right).offset(-15);
        }];
        
    }
    return self;
}


- (void)fillWithData:(CSCommonSwitchCellData *)switchData
{
    [super fillWithData:switchData];
    if ([switchData.isType isEqualToString:@"1"]) {
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).offset(7);
            make.left.mas_equalTo(self.contentView.mas_left).offset(15);
        }];
        self.subTitleLabel = [[UILabel alloc]init];
        self.subTitleLabel.text = @"启用后，群成员需群管理确认才能邀请朋友进群";
        self.subTitleLabel.font = [UIFont systemFontOfSize:11];
        self.subTitleLabel.textColor = [UIColor colorWithHexString:@"#999999"];
        [self.contentView addSubview:self.subTitleLabel];
        [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-9);
            make.left.mas_equalTo(self.titleLabel.mas_left);
        }];
    }
    else{
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.left.mas_equalTo(self.contentView.mas_left).offset(15);
        }];
    }
   
    self.switchData = switchData;

    _titleLabel.text = switchData.title;
    [_switcher setOn:switchData.isOn];

    _titleLabel.mm_sizeToFit().mm_left(switchData.margin).mm__centerY(self.contentView.mm_h / 2);
}

- (void)switchClick
{
    if (self.switchData.cswitchSelector) {
        UIViewController *vc = self.mm_viewController;
        if ([vc respondsToSelector:self.switchData.cswitchSelector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [vc performSelector:self.switchData.cswitchSelector withObject:self];
#pragma clang diagnostic pop
        }
    }

}
@end

