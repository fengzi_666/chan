//
//  CSProfileCardCell.m
//  IMChat
//
//  Created by Apple on 2020/11/27.
//

#import "CSProfileCardCell.h"
#import "MMLayout/UIView+MMLayout.h"
@implementation CSProfileCardCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        
        self.avatar.layer.masksToBounds = YES;
        self.avatar.layer.cornerRadius=30;
        self.name.textColor = [UIColor colorWithHexString:@"#333333"];
        self.name.font = [UIFont boldSystemFontOfSize:17];
        self.identifier.font = [UIFont systemFontOfSize:11];
        self.identifier.textColor = [UIColor colorWithHexString:@"#999999"];
        
        self.signature.font = [UIFont systemFontOfSize:11];
        self.signature.textColor = [UIColor colorWithHexString:@"#999999"];
        self.signature.numberOfLines =2;
       
        [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).offset(12);
            make.size.mas_equalTo(CGSizeMake(60, 60));
            make.top.mas_equalTo(self.mas_top).offset(20);
        }];
        [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.avatar.mas_right).offset(10);
            make.top.mas_equalTo(self.avatar.mas_top);
            make.right.mas_equalTo(self.contentView.mas_right).offset(-12);
        }];
        [self.identifier mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.name.mas_bottom).offset(8);
            make.left.mas_equalTo(self.name.mas_left);
            make.height.mas_equalTo(11);
        }];
        
        [self.signature mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.name.mas_left);
            make.right.mas_equalTo(self.contentView.mas_right).offset(-12);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-10);
        }];
//        self.signature.mm_sizeToFitThan(KScreenWidth-90, self.avatar.mm_h/3).mm_bottom(self.avatar.mm_b).mm_left(self.name.mm_x);
    }
    return self;
}
- (void)layoutSubviews
{
    
    self.name.mm_sizeToFitThan(KScreenWidth-90, self.avatar.mm_h/3).mm_top(self.avatar.mm_y).mm_left(self.avatar.mm_maxX + TPersonalCommonCell_Margin);
    self.identifier.mm_sizeToFitThan(80, self.avatar.mm_h/3).mm__centerY(self.avatar.mm_centerY).mm_left(self.name.mm_x);
    self.signature.mm_sizeToFitThan(KScreenWidth-90, self.avatar.mm_h/3).mm_bottom(self.avatar.mm_b).mm_left(self.name.mm_x);
    //iCon大小 = 字体*0.9，视觉上最为自然
    self.genderIcon.mm_sizeToFitThan(self.name.font.pointSize * 0.9, self.name.font.pointSize * 0.9).mm__centerY(self.name.mm_centerY).mm_left(self.name.mm_x + self.name.mm_w + TPersonalCommonCell_Margin);
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
