//
//  CSCommonSwitchCell.h
//  IMChat
//
//  Created by Apple on 2020/12/2.
//

#import "TCommonCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSCommonSwitchCellData : TCommonCellData

@property NSString *title;
@property NSString *subTitle;
@property NSString *isType;
@property (getter=isOn) BOOL on;
@property CGFloat margin;
@property SEL cswitchSelector;

@end
@interface CSCommonSwitchCell : TCommonTableViewCell
@property UILabel *titleLabel;
@property UILabel *subTitleLabel;
@property UISwitch *switcher;

@property (readonly) CSCommonSwitchCellData *switchData;

- (void)fillWithData:(CSCommonSwitchCellData *)data;
@end

NS_ASSUME_NONNULL_END
