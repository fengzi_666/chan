//
//  CSUIGroupMemberCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "CSUIGroupMemberCell.h"
#import "THeader.h"
#import "TUIKit.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "MMLayout/UIView+MMLayout.h"
#import "SDWebImage/UIImageView+WebCache.h"
@implementation CSGroupMemberCellData

@end

@implementation CSUIGroupMemberCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
        [self defaultLayout];
       
    }
    return self;
}

- (void)setupViews
{
    _head = [[UIImageView alloc] init];
    _head.contentMode =UIViewContentModeScaleAspectFill;
    _head.layer.cornerRadius = 5;
    [_head.layer setMasksToBounds:YES];
    [self addSubview:_head];

    _name = [[UILabel alloc] init];
    [_name setFont:[UIFont systemFontOfSize:11]];
    [_name setTextColor:[UIColor colorWithHexString:@"#999999"]];
    _name.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_name];
}

- (void)defaultLayout
{
   
    CGSize headSize = [[self class] getSize];
    _head.frame = CGRectMake(0, 0, 40, 40);
    [_head mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    _name.frame = CGRectMake(0, _head.frame.origin.y + _head.frame.size.height + TGroupMemberCell_Margin, self.contentView.frame.size.width, 16);
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.head.mas_centerX);
        make.top.mas_equalTo(self.head.mas_bottom).offset(8);
        make.width.mas_equalTo(self.contentView.mas_width);
    }];
    
    if ([TUIKit sharedInstance].config.avatarType == TAvatarTypeRounded) {
        _head.layer.masksToBounds = YES;
        _head.layer.cornerRadius = _head.frame.size.height / 2;
    } else if ([TUIKit sharedInstance].config.avatarType == TAvatarTypeRadiusCorner) {
        _head.layer.masksToBounds = YES;
        _head.layer.cornerRadius = [TUIKit sharedInstance].config.avatarCornerRadius;
    }
}
- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}
- (void)setData:(CSGroupMemberCellData *)data
{

    NSLog(@"%@",data.name);
   
    __weak typeof(self) weakSelf = self;
     
           if ([data.name isEqualToString:@"添加成员"]&&data.identifier==nil) {
               self.name.textColor = [UIColor colorWithHexString:@"#32965A"];
               self.head.image = [UIImage imageNamed:@"group_添加成员"];
               self.name.text = data.name;
           }
    else if ([data.name isEqualToString:@"删除成员"]&&data.identifier==nil) {
               self.name.textColor = [UIColor colorWithHexString:@"#32965A"];
               self.head.image = [UIImage imageNamed:@"group_删除成员"];
               self.name.text = data.name;
           }
    else
    {
        self.name.textColor = [UIColor colorWithHexString:@"#999999"];
        self.name.text = data.name;
        [self.head sd_setImageWithURL:[NSURL URLWithString:data.faceUrl]];
    }
    
//    if ([data.name isEqualToString:@"添加成员"]&&data.identifier==nil) {
//        self.name.textColor = [UIColor colorWithHexString:@"#02cdcb"];
//        self.head.image = [UIImage imageNamed:@"group_添加成员"];
//        self.name.text = data.name;
//    }else if ([data.name isEqualToString:@"删除成员"]&&data.identifier==nil) {
//        self.name.textColor = [UIColor colorWithHexString:@"#02cdcb"];
//        self.head.image = [UIImage imageNamed:@"group_删除成员"];
//        self.name.text = data.name;
//    }else{
        
//        if (data.identifier) {
//            __weak typeof(self) weakSelf = self;
//            [[V2TIMManager sharedInstance] getUsersInfo:@[data.identifier] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
//                if (infoList.firstObject) {
//                    [weakSelf.head sd_setImageWithURL:[NSURL URLWithString:infoList.firstObject.faceURL] placeholderImage:DefaultAvatarImage];
//                };
//            } fail:nil];
//        }else{
//            self.head.image = [UIImage imageNamed:@"group_删除成员"];
//        }
      
       
//    }
    
    
    [self defaultLayout];
}

+ (CGSize)getSize {
    CGSize headSize = TGroupMemberCell_Head_Size;
    
    if (headSize.width * 6 + 10 * (6 + 1) > Screen_Width) {
        CGFloat wd = (Screen_Width - (10 * (6 + 1))) / 6;
        headSize = CGSizeMake(wd, wd);
    }
    return CGSizeMake(headSize.width+5, headSize.height + 15 + TGroupMemberCell_Margin+5);
}
@end
