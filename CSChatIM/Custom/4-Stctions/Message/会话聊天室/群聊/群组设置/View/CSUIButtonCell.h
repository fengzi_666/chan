//
//  CSUIButtonCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "TCommonCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ButtonGreen1,
    ButtonWhite1,
    ButtonRedText1,
    ButtonBule1,
} CSUIButtonStyle;

@interface CSUIButtonCellData : TCommonCellData
@property (nonatomic, strong) NSString *title;
@property SEL cbuttonSelector;
@property CSUIButtonStyle style;
@end

@interface CSUIButtonCell : TCommonTableViewCell
@property (nonatomic, strong) UIButton *button;
@property CSUIButtonCellData *buttonData;

- (void)fillWithData:(CSUIButtonCellData *)data;
@end

NS_ASSUME_NONNULL_END
