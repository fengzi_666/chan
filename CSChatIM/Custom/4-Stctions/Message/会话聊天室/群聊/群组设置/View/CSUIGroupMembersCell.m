//
//  CSUIGroupMembersCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "CSUIGroupMembersCell.h"
#import "CSUIGroupMemberCell.h"
#import "THeader.h"
#import "UIColor+TUIDarkMode.h"

@implementation CSGroupMembersCellData
@end

@interface CSUIGroupMembersCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) NSMutableDictionary *cellDic;
@end

@implementation CSUIGroupMembersCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setupViews];
        self.cellDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)setupViews
{
    self.backgroundColor = [UIColor d_colorWithColorLight:TCell_Nomal dark:TCell_Nomal_Dark];
    _memberFlowLayout = [[UICollectionViewFlowLayout alloc] init];
//    _memberFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _memberFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    CGSize cellSize = [CSUIGroupMemberCell getSize];
    _memberFlowLayout.itemSize = cellSize;
    _memberFlowLayout.minimumInteritemSpacing = (Screen_Width - cellSize.width * 6 - 2*5) / (6 - 1);
    _memberFlowLayout.minimumLineSpacing = 5;
    _memberFlowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);

    _memberCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_memberFlowLayout];
    [_memberCollectionView registerClass:[CSUIGroupMemberCell class] forCellWithReuseIdentifier:TGroupMemberCell_ReuseId];
    _memberCollectionView.collectionViewLayout = _memberFlowLayout;
    _memberCollectionView.delegate = self;
    _memberCollectionView.dataSource = self;
    _memberCollectionView.showsHorizontalScrollIndicator = NO;
    _memberCollectionView.showsVerticalScrollIndicator = NO;
    _memberCollectionView.alwaysBounceHorizontal = YES;
    _memberCollectionView.alwaysBounceVertical = NO;
    _memberCollectionView.backgroundColor =[UIColor colorWithHexString:@"#f4f4f4"];
    [self.contentView addSubview:_memberCollectionView];
    [self setSeparatorInset:UIEdgeInsetsMake(10, TGroupMembersCell_Margin, 0, 0)];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)updateLayout
{
    CGFloat height = [CSUIGroupMembersCell getHeight:_data];
    _memberCollectionView.frame = CGRectMake(0, 0, Screen_Width, height);
    
}

- (void)setData:(CSGroupMembersCellData *)data
{
    _data = data;
    [_memberCollectionView reloadData];
    [self updateLayout];
   
}

+ (CGFloat)getHeight:(CSGroupMembersCellData *)data{
    NSInteger row = ceil(data.members.count * 1.0 / 6);
    if(row > 1){
        row = 1;
    }
    CGFloat height = row * [CSUIGroupMemberCell getSize].height + (row + 1) * 6;
    return height;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _data.members.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CSUIGroupMemberCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TGroupMemberCell_ReuseId forIndexPath:indexPath];
    CSGroupMemberCellData *data = nil;
    
    data = _data.members[indexPath.item];
    [cell setData:data];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(groupMembersCell:didSelectItemAtIndex:)]){
        [_delegate groupMembersCell:self didSelectItemAtIndex:indexPath.section * 6 + indexPath.row];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [CSUIGroupMemberCell getSize];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
