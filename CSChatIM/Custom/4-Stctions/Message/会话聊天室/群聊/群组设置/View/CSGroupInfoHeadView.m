//
//  CSGroupInfoHeadView.m
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "CSGroupInfoHeadView.h"

@implementation CSGroupInfoHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSGroupInfoHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius =28 ;
        self.backgroundColor = [UIColor whiteColor];
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(event:)];
        [self.headImage addGestureRecognizer:tapGesture];
        UITapGestureRecognizer * tapheadHitImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(event:)];
        [self.headHitImage addGestureRecognizer:tapheadHitImage];
    }
    return self;
}
- (void)event:(UITapGestureRecognizer *)gesture
{
    
     //处理事件
    if(self.delegate && [self.delegate respondsToSelector:@selector(HeadImageTap:image:)]){
        [_delegate HeadImageTap:self image:self.headImage];
        
    }
}
- (IBAction)editNameClick:(UIButton *)sender {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(HeadEditNameClick:sender:)]){
        [_delegate HeadEditNameClick:self sender:sender];
        
    }
}
@end
