//
//  CSGroupInfoHeadView.h
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSGroupInfoHeadView;
@protocol CSGroupInfoHeadViewDelegate <NSObject>
- (void)HeadEditNameClick:(CSGroupInfoHeadView *)headView sender:(UIButton *)sender;
- (void)HeadImageTap:(CSGroupInfoHeadView *)headView image:(UIImageView *)image;
@end
@interface CSGroupInfoHeadView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UIImageView *headHitImage;

@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *IDlable;
@property (weak, nonatomic) IBOutlet UIButton *EditNameBtn;
@property (nonatomic, weak) id<CSGroupInfoHeadViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
