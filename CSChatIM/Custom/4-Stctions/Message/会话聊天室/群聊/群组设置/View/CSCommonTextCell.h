//
//  CSCommonTextCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "TCommonCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSCommonTextCellData : TCommonCellData

@property NSString *key;
@property NSString *value;
@property BOOL showAccessory;

@end
@interface CSCommonTextCell : TCommonTableViewCell

@property UILabel *keyLabel;
@property UILabel *valueLabel;
@property (readonly) CSCommonTextCellData *textData;

- (void)fillWithData:(CSCommonTextCellData *)data;
@end

NS_ASSUME_NONNULL_END
