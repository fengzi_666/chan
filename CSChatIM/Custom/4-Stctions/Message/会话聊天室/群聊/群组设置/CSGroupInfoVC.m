//
//  CSGroupInfoVC.m
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "CSGroupInfoVC.h"
#import "CSGroupInfoHeadView.h"
#import "TUIGroupInfoController.h"
#import "GroupMemberController.h"
#import "TUIGroupMemberCell.h"
#import "TUIContactSelectController.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "Toast/Toast.h"
#import "THelper.h"
#import <ImSDK/ImSDK.h>
#import "TUIProfileCardCell.h"
#import "TUIGroupMembersCell.h"
#import "TUIGroupMemberCell.h"
#import "TUIButtonCell.h"
#import "TCommonSwitchCell.h"
#import "THeader.h"
#import "TUIGroupMemberController.h"
#import "TModifyView.h"
#import "TAddCell.h"
#import "TUILocalStorage.h"
#import "UIImage+TUIKIT.h"
#import "TCommonTextCell.h"
#import "TUIKit.h"
#import "MMLayout/UIView+MMLayout.h"
#import "TIMGroupInfo+DataProvider.h"
#import "TUIAvatarViewController.h"
#import "UIColor+TUIDarkMode.h"
#import "NSBundle+TUIKIT.h"
#import "CSUIGroupMembersCell.h"
#import "CSUIGroupMemberCell.h"
#import "CSCommonTextCell.h"
#import "CSUIButtonCell.h"
#import "ZZQAvatarPicker.h"
#import "CSGroupNameViewController.h"
#import "CSRequestTextViewController.h"
#import "CSGroupManageViewController.h"
#import "CSGroupListViewController.h"
#import "CSUIContactSelectController.h"
#import "xinxipingtaiViewController.h"
#import "CSDeleteGroupListVC.h"
#import "CSAddGroupListViewController.h"
#import "CSGroupCodeViewController.h"
#import "xinxipingtaiViewController.h"
#import "CSCharmDistributionViewController.h"
#define ADD_TAG @"-1"
#define DEL_TAG @"-2"

@import ImSDK;
@interface CSGroupInfoVC ()<UITableViewDelegate,UITableViewDataSource,CSGroupInfoHeadViewDelegate,CSGroupMembersCellDelegate>
@property (nonatomic ,strong)UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) NSMutableArray *memberData;
@property (nonatomic, strong) NSMutableArray *groupList;
@property (nonatomic, strong) V2TIMGroupInfo *groupInfo;
@property (nonatomic, strong) V2TIMGroupMemberFullInfo *mainInfo;
@property V2TIMGroupMemberInfo *selfInfo;
//群成员列表data
@property CSGroupMembersCellData *groupMembersCellData;

@property CSCommonTextCellData *groupMembersCountCellData;
@property CSCommonTextCellData *addOptionData;
@property TUIProfileCardCellData *profileCellData;
@property CSCommonTextCellData *groupNickNameCellData;
@property CSCommonTextCellData *groupNameData;
@property (nonatomic ,strong) CSGroupInfoHeadView *headView;
//后台自定义展示id
@property (nonatomic ,copy) NSString *groupCode;
//向后台传送id
@property (nonatomic ,copy) NSString *qunid;
@property (nonatomic ,copy) NSString *groupid;

//是否需要管理员同意入群
@property (nonatomic ,copy) NSString *yaoqingstate;
@end

@implementation CSGroupInfoVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    [self updateData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"群组设置";
    
    [self setNav];
    //拉取数据
    [self LoadTableView];
 
}
- (void)updateData
{
    @weakify(self)
    
    _memberData = [NSMutableArray array];
    _groupList = [NSMutableArray array];
    //获取群组资料自定义字段
    [[TIMGroupManager sharedInstance]getGroupInfo:@[self.groupId] succ:^(NSArray *groupList) {
       
        TIMGroupInfo *info =groupList[0];
        NSDictionary *dict = info.customInfo;
//        NSString *quncode = [[ NSString alloc]initWithData:dict[@"quncode"] encoding:NSUTF8StringEncoding];
//        NSString *qunerweima = [[ NSString alloc]initWithData:dict[@"qunerweima"] encoding:NSUTF8StringEncoding];
        NSString *qunid = [[ NSString alloc]initWithData:dict[@"qunid"] encoding:NSUTF8StringEncoding];
        _qunid = qunid;
//        NSString *pingbistate= [[ NSString alloc]initWithData:dict[@"pingbistate"] encoding:NSUTF8StringEncoding];
        
        NSString *yaoqingstate= [[ NSString alloc]initWithData:dict[@"yaoqingstate"] encoding:NSUTF8StringEncoding];
        _yaoqingstate = yaoqingstate;
        NSLog(@"%@",info.customInfo);
      

        
        } fail:^(int code, NSString *msg) {
            
        }];
    [[V2TIMManager sharedInstance] getGroupsInfo:@[_groupId] succ:^(NSArray<V2TIMGroupInfoResult *> *groupResultList) {
        NSLog(@"%@",groupResultList);
        @strongify(self)
        if(groupResultList.count == 1){
            _groupInfo = groupResultList[0].info;
           
            
            [self setupData];
        }
    } fail:^(int code, NSString *msg) {
        [THelper makeToastError:code msg:msg];
    }];
    /**
     *  3.2 指定的群成员资料
     */
    [[V2TIMManager sharedInstance] getGroupMembersInfo:self.groupId memberList:@[[V2TIMManager sharedInstance].getLoginUser] succ:^(NSArray<V2TIMGroupMemberFullInfo *> *memberList) {
        NSLog(@"%@",memberList);
        if(memberList.count == 1){
            V2TIMGroupMemberFullInfo *info =memberList[0];
            _mainInfo = info;
            [self setupData];
        }
           
    
      
        } fail:^(int code, NSString *desc) {
            
        }];
  
    [[V2TIMManager sharedInstance] getGroupMemberList:self.groupId filter:V2TIM_GROUP_MEMBER_FILTER_ALL  nextSeq:0 succ:^(uint64_t nextSeq, NSArray<V2TIMGroupMemberFullInfo *> *memberList) {
        @strongify(self)
        for (V2TIMGroupMemberFullInfo *fullInfo in memberList) {
            if([fullInfo.userID isEqualToString:[V2TIMManager sharedInstance].getLoginUser]){
                self.selfInfo = fullInfo;
            }
            CSGroupMemberCellData *data = [[CSGroupMemberCellData alloc] init];
            data.identifier = fullInfo.userID;
            data.name = fullInfo.userID;
            if (fullInfo.nameCard.length > 0) {
                data.name = fullInfo.nameCard;
            } else if (fullInfo.friendRemark.length > 0) {
                data.name = fullInfo.friendRemark;
            } else if (fullInfo.nickName.length > 0) {
                data.name = fullInfo.nickName;
            }
            data.faceUrl= fullInfo.faceURL;
            
            [self.memberData addObject:data];
            
        }
        [self setupData];
    } fail:^(int code, NSString *msg) {
        [THelper makeToastError:code msg:msg];
    }];
}

- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-64) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
//    [_myTableView registerNib:[UINib nibWithNibName:@"CSMainEditCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
//    [_myTableView registerNib:[UINib nibWithNibName:@"CSMainEditBoundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:boundCellid];
    [self.view addSubview:_myTableView];
    
   _headView = [[CSGroupInfoHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 80)];
    _headView.delegate = self;
    
    _myTableView.tableHeaderView.height = 80;
    _myTableView.tableHeaderView = _headView;
    
}
#pragma mark -- 在此处更改群组设置列表
- (void)setupData
{
    _data = [NSMutableArray array];
    if (_mainInfo.role == 300||_mainInfo.role == 400) {
        _headView.headImage.userInteractionEnabled = YES;
    }
    else
    {
        _headView.headImage.userInteractionEnabled = NO;
    }
    if (self.groupInfo) {
        //判断是否是群管理
        if ([self.groupInfo isMeOwner]) {
           
        }
        else
        {
            _headView.EditNameBtn.hidden = YES;
            _headView.headHitImage.hidden = YES;
        }
        [_headView.headImage sd_setImageWithURL:[NSURL URLWithString:_groupInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
        _headView.nameLable.text = _groupInfo.groupName;
        NSArray *testArray = [_groupInfo.introduction componentsSeparatedByString:@","];
        if (testArray.count>0) {
            _groupid = testArray[0];
            _groupCode = testArray[1];
        }
    
        _headView.IDlable.text = [NSString stringWithFormat:@"ID:  %@",_groupCode];

        //设置群成员显示人数
        NSMutableArray *memberArray = [NSMutableArray array];
        CSCommonTextCellData *countData = [[CSCommonTextCellData alloc] init];
        countData.key = [NSString stringWithFormat:@"群成员  (%u 人)", self.groupInfo.memberCount];
//        countData.value = [NSString stringWithFormat:TUILocalizableString(TUIKitGroupProfileMemberCount), self.groupInfo.memberCount];
        countData.cselector = @selector(didSelectMembers);
        countData.showAccessory = YES;
        self.groupMembersCountCellData = countData;
        [memberArray addObject:countData];

        //群成员列表展示
        NSMutableArray *tmpArray = [self getShowMembers:self.memberData];
        CSGroupMembersCellData *membersData = [[CSGroupMembersCellData alloc] init];
        membersData.members = tmpArray;
        [memberArray addObject:membersData];
        self.groupMembersCellData = membersData;
        [self.data addObject:memberArray];


        //.群名称
        //判断群主/管理
        if (_mainInfo.role == 300||_mainInfo.role == 400)
        {
            NSMutableArray *groupName = [NSMutableArray array];
            CSCommonTextCellData *typeData = [[CSCommonTextCellData alloc] init];
            typeData.key = @"群聊名称";
            typeData.value = self.groupInfo.groupName;
            typeData.cselector = @selector(editGroupName:);
            typeData.showAccessory = YES;;
            self.groupNameData = typeData;
            [groupName addObject:typeData];
           
//            //.我的群昵称
//
//            CSCommonTextCellData *myGroupNamData = [[CSCommonTextCellData alloc] init];
//            myGroupNamData.key = @"我的群昵称";
//
//            myGroupNamData.value = self.selfInfo.nameCard;
//            myGroupNamData.cselector = @selector(myEditGroupName:);
//            myGroupNamData.showAccessory = YES;;
//            self.groupNickNameCellData = myGroupNamData;
//            [groupName addObject:myGroupNamData];
//

            //群公告
            
            CSCommonTextCellData *addOptionData = [[CSCommonTextCellData alloc] init];
            addOptionData.key = @"群公告";
            addOptionData.value = self.groupInfo.notification;
            addOptionData.cselector = @selector(grouPannouncement);
            addOptionData.showAccessory = YES;;
            self.addOptionData = addOptionData;
            [groupName addObject:addOptionData];
           
            
            //群成员管理
            CSCommonTextCellData *groupManage = [[CSCommonTextCellData alloc] init];
            groupManage.key = @"群管理";
            groupManage.cselector = @selector(selectGroupManage);
            groupManage.showAccessory = YES;;
        
            [groupName addObject:groupManage];
            [self.data addObject:groupName];

        }
//        NSMutableArray *fenbuArr = [NSMutableArray array];
//
//
//        CSCommonTextCellData *meilihzidata = [[CSCommonTextCellData alloc] init];
//        meilihzidata.key = @"魅力值分布";
//        meilihzidata.value = @"";
//        meilihzidata.cselector = @selector(meilizhifenbuClick);
//        meilihzidata.showAccessory = YES;;
//        [fenbuArr addObject:meilihzidata];
//
//
//        [self.data addObject:fenbuArr];

       
        //personal info
        NSMutableArray *emptyArray = [NSMutableArray array];
        
       
            CSCommonTextCellData *myGroupNamData1 = [[CSCommonTextCellData alloc] init];
            myGroupNamData1.key = @"群二维码";
            myGroupNamData1.value = @"";
            myGroupNamData1.cselector = @selector(groupCodeClick);
            myGroupNamData1.showAccessory = YES;;
            [emptyArray addObject:myGroupNamData1];
        
//        if (self.mainInfo.role == 200) {
//            CSCommonTextCellData *myGroupNamData = [[CSCommonTextCellData alloc] init];
//            myGroupNamData.key = @"我的群昵称";
//            myGroupNamData.value = self.selfInfo.nameCard;
//            myGroupNamData.cselector = @selector(myEditGroupName:);
//            myGroupNamData.showAccessory = YES;;
//            [emptyArray addObject:myGroupNamData];
//        }
        //顶置聊天
        TCommonSwitchCellData *switchData = [[TCommonSwitchCellData alloc] init];
        if ([[[TUILocalStorage sharedInstance] topConversationList] containsObject:[NSString stringWithFormat:@"group_%@",self.groupId]]) {
            switchData.on = YES;
        }
        switchData.title = TUILocalizableString(TUIKitGroupProfileStickyOnTop);
        switchData.cswitchSelector = @selector(didSelectOnTop:);
        [emptyArray addObject:switchData];
        //消息免打扰
        TCommonSwitchCellData *botherSwitch = [[TCommonSwitchCellData alloc] init];
        if (_groupInfo.recvOpt==2||_groupInfo.recvOpt==1) {
            botherSwitch.on = YES;
        }
        botherSwitch.title = @"消息免打扰";
        botherSwitch.cswitchSelector = @selector(botherSelectOnTop:);
        [emptyArray addObject:botherSwitch];
        
        CSCommonTextCellData *emptyChat = [[CSCommonTextCellData alloc] init];
        emptyChat.key = @"清空聊天记录";
        emptyChat.cselector = @selector(onSelectEmptyChat);
        emptyChat.showAccessory = YES;;
        [emptyArray addObject:emptyChat];
        [self.data addObject:emptyArray];

        NSMutableArray *buttonArray = [NSMutableArray array];

        //群删除按钮
        if (_mainInfo.role != V2TIM_GROUP_MEMBER_ROLE_SUPER ) {
            CSUIButtonCellData *quitButton = [[CSUIButtonCellData alloc] init];
            quitButton.title = TUILocalizableString(TUIKitGroupProfileDeleteAndExit);
            quitButton.style = ButtonRedText;
            quitButton.cbuttonSelector = @selector(deleteGroup:);
            [buttonArray addObject:quitButton];
        }
        

        //群解散按钮
        if ([self.groupInfo canDelete]) {
              CSUIButtonCellData *Deletebutton = [[CSUIButtonCellData alloc] init];
              Deletebutton.title = TUILocalizableString(TUIKitGroupProfileDissolve);
              Deletebutton.style = ButtonRedText;
              Deletebutton.cbuttonSelector = @selector(deleteGroup:);
              [buttonArray addObject:Deletebutton];
        }

        [self.data addObject:buttonArray];

        [self.myTableView reloadData];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _data.count;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSMutableArray *array = _data[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *array = _data[indexPath.section];
    NSObject *data = array[indexPath.row];
    if([data isKindOfClass:[TUIProfileCardCellData class]]){
        return [(TUIProfileCardCellData *)data heightOfWidth:Screen_Width];
    }
    else if([data isKindOfClass:[CSGroupMembersCellData class]]){
        return [CSUIGroupMembersCell getHeight:(CSGroupMembersCellData *)data];
    }
    else if([data isKindOfClass:[CSUIButtonCellData class]]){
        return [(CSUIButtonCellData *)data heightOfWidth:Screen_Width];;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *array = _data[indexPath.section];
    NSObject *data = array[indexPath.row];
    if([data isKindOfClass:[CSCommonTextCellData class]]){
        CSCommonTextCell *cell = [tableView dequeueReusableCellWithIdentifier:TKeyValueCell_ReuseId];
        if(!cell){
            cell = [[CSCommonTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TKeyValueCell_ReuseId];
        }
        [cell fillWithData:(CSCommonTextCellData *)data];
        return cell;
    }
    else if([data isKindOfClass:[CSGroupMembersCellData class]]){
        CSUIGroupMembersCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSUIGroupMembersCell"];
        if(!cell){
            cell = [[CSUIGroupMembersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CSUIGroupMembersCell"];
            cell.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
            cell.delegate = self;
        }
        [cell setData:(CSGroupMembersCellData *)data];
        return cell;
    }
    else if([data isKindOfClass:[TCommonSwitchCellData class]]){
        TCommonSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:TSwitchCell_ReuseId];
        if(!cell){
            cell = [[TCommonSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TSwitchCell_ReuseId];
            cell.switcher.onTintColor = [UIColor colorWithHexString:@"#32965A"];
            
        }
        [cell fillWithData:(TCommonSwitchCellData *)data];
        return cell;
    }
    else if([data isKindOfClass:[CSUIButtonCellData class]]){
        CSUIButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:TButtonCell_ReuseId];
        if(!cell){
            cell = [[CSUIButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TButtonCell_ReuseId];
        }
        [cell fillWithData:(CSUIButtonCellData *)data];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (void)leftBarButtonClick:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 消息免打扰
- (void)botherSelectOnTop:(TCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        [[V2TIMManager sharedInstance]setReceiveMessageOpt:_groupId opt:2 succ:^{
            NSLog(@"11111");
            } fail:^(int code, NSString *desc) {
                NSLog(@"222222");
            }];
    }
    else
    {
        [[V2TIMManager sharedInstance]setReceiveMessageOpt:_groupId opt:0 succ:^{
            NSLog(@"11111");
            } fail:^(int code, NSString *desc) {
                NSLog(@"222222");
            }];
    }
   
}

#pragma mark -- 魅力值分布
- (void)meilizhifenbuClick
{
    CSCharmDistributionViewController *vc = [[CSCharmDistributionViewController alloc]init];
    vc.qunid = _qunid;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark --查看群二维码
- (void)groupCodeClick
{
    CSGroupCodeViewController *vc = [[CSGroupCodeViewController alloc]init];
    vc.groupInfo = self.groupInfo;
    vc.groupId = self.groupid;
    vc.groupCode = self.groupCode;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- 查看群成员
- (void)didSelectMembers
{
    NSLog(@"查看群成员");
    CSGroupListViewController *membersController = [[CSGroupListViewController alloc] init];
    membersController.groupId = _groupid;
//    membersController.groupCode = _groupCode;
    membersController.title = [NSString stringWithFormat:@"群成员(%lu)",(unsigned long)self.groupInfo.memberCount];// @"群成员";
    membersController.groupInfo = _groupInfo;
    membersController.groupCode = _groupId;
    [self.navigationController pushViewController:membersController animated:YES];
}
#pragma mark -- 编辑群昵称
- (void)editGroupName:(TCommonTableViewCell *)cell
{
    NSLog(@"编辑群昵称");
    CSGroupNameViewController *vc = [[CSGroupNameViewController alloc]init];
    vc.navigationItem.title = @"设置群昵称";
    vc.info = self.groupInfo;
    vc.grouid = self.groupid;
    vc.callBackBlock = ^(NSString * _Nonnull text) {
        self.headView.nameLable.text = text;
        self.groupNameData.value = text;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- 编辑我的群昵称
- (void)myEditGroupName:(TCommonTableViewCell *)cell
{
    NSLog(@"编辑我的群昵称");
    CSGroupNameViewController *vc = [[CSGroupNameViewController alloc]init];
    vc.navigationItem.title = @"设置在群里的昵称";
    vc.grouid = self.groupid;
    vc.isType = @"1";
    vc.callBackBlock = ^(NSString * _Nonnull text) {
        self.groupNickNameCellData.value =text;
    };
    [self.navigationController pushViewController:vc animated:YES];
    
}
#pragma mark -- 群公告
- (void)grouPannouncement
{
    NSLog(@"群公告");
    CSRequestTextViewController *vc = [[CSRequestTextViewController alloc]init];
    vc.navigationItem.title = @"请设置群公告";
    vc.groupid = self.groupid;
    vc.callBackBlock = ^(NSString * _Nonnull text) {
        self.addOptionData.value = text;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark --群管理
- (void)selectGroupManage
{
    NSLog(@"群管理");
    CSGroupManageViewController *vc = [[CSGroupManageViewController alloc]init];
    vc.groupId = self.groupId;
    vc.groupCode = self.groupid;
    vc.selfInfo = self.mainInfo;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma  mark -- 清空聊天记录
- (void)onSelectEmptyChat
{
    
    NSLog(@"清空聊天记录");
    
    [[V2TIMManager sharedInstance]deleteConversation:_chatId succ:^{
        NSLog(@"");
        [SVProgressHUD showSuccessWithStatus:@"清空成功"];
        [self jumpVC1];
        } fail:^(int code, NSString *desc) {
            
        }];
//    [ self LoadRequestMessageList];
}
//跳转指定控制器
- (void)jumpVC1
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[xinxipingtaiViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
- (void)LoadRequestMessageList
{
    V2TIMMessage *msgForGet;
    int msgCount = 20;
    @weakify(self)
    NSMutableArray *msgList = [NSMutableArray arrayWithCapacity:1];
    //查询群历史聊天记录
    [[V2TIMManager sharedInstance] getGroupHistoryMessageList:self.groupId count:msgCount lastMsg:msgForGet succ:^(NSArray<V2TIMMessage *> *msgs) {
        @strongify(self)
        [msgList addObject:msgs];
        [[V2TIMManager sharedInstance]deleteMessages:msgs succ:^{
            NSLog(@"删除成功");
                } fail:^(int code, NSString *desc) {
                    
                }];
        
    } fail:^(int code, NSString *msg) {
        @strongify(self)
//        self.isLoadingMsg = NO;
//        [THelper makeToastError:code msg:msg];
    }];
    
}
- (void)DeletaMessageList:(NSArray<V2TIMMessage *>*)msgs;
{
    @weakify(self)
    [[V2TIMManager sharedInstance] deleteMessages:msgs succ:^{
        @strongify(self)
        
    } fail:^(int code, NSString *msg) {
        NSLog(@"remove msg failed!");
    }];
}
- (void)didSelectAddOption:(UITableViewCell *)cell
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:TUILocalizableString(TUIKitGroupProfileJoinType) preferredStyle:UIAlertControllerStyleActionSheet];

    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(TUIKitGroupProfileJoinDisable) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setGroupAddOpt:V2TIM_GROUP_ADD_FORBID];
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(TUIKitGroupProfileAdminApprove) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setGroupAddOpt:V2TIM_GROUP_ADD_AUTH];
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(TUIKitGroupProfileAutoApproval) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setGroupAddOpt:V2TIM_GROUP_ADD_ANY];
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(Cancel) style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:ac animated:YES completion:nil];
}

- (void)setGroupAddOpt:(V2TIMGroupAddOpt)opt
{
    @weakify(self)
    V2TIMGroupInfo *info = [[V2TIMGroupInfo alloc] init];
    info.groupID = _groupId;
    info.groupAddOpt = opt;
    [[V2TIMManager sharedInstance] setGroupInfo:info succ:^{
        @strongify(self)
        self.addOptionData.value = [self.groupInfo showAddOption:opt];
    } fail:^(int code, NSString *msg) {
        [THelper makeToastError:code msg:msg];
    }];
}

- (void)didSelectGroupNick:(CSCommonTextCell *)cell
{
    TModifyViewData *data = [[TModifyViewData alloc] init];
    data.title = TUILocalizableString(TUIKitGroupProfileEditAlias);
    TModifyView *modify = [[TModifyView alloc] init];
    modify.tag = 2;
    modify.delegate = self;
    [modify setData:data];
    [modify showInWindow:self.view.window];
}

- (void)didSelectCommon
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    if ([self.groupInfo isPrivate] || [self.groupInfo isMeOwner]) {
        [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(TUIKitGroupProfileEditGroupName) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            TModifyViewData *data = [[TModifyViewData alloc] init];
            data.title = TUILocalizableString(TUIKitGroupProfileEditGroupName);
            TModifyView *modify = [[TModifyView alloc] init];
            modify.tag = 0;
            modify.delegate = self;
            [modify setData:data];
            [modify showInWindow:self.view.window];

        }]];
    }
    if ([self.groupInfo isMeOwner]) {
        [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(TUIKitGroupProfileEditAnnouncement) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            TModifyViewData *data = [[TModifyViewData alloc] init];
            data.title = TUILocalizableString(TUIKitGroupProfileEditAnnouncement);
            TModifyView *modify = [[TModifyView alloc] init];
            modify.tag = 1;
            modify.delegate = self;
            [modify setData:data];
            [modify showInWindow:self.view.window];
        }]];
    }

//    if ([self.delegate respondsToSelector:@selector(groupInfoController:didSelectChangeAvatar:)]) {
//        if ([self.groupInfo isMeOwner]) {
//            [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(TUIKitGroupProfileEditAvatar) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self.delegate groupInfoController:self didSelectChangeAvatar:self.groupId];
//            }]];
//        }
//    }
    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(Cancel) style:UIAlertActionStyleCancel handler:nil]];

    [self presentViewController:ac animated:YES completion:nil];
}

- (void)didSelectOnTop:(TCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        [[TUILocalStorage sharedInstance] addTopConversation:[NSString stringWithFormat:@"group_%@",_groupId]];
    } else {
        [[TUILocalStorage sharedInstance] removeTopConversation:[NSString stringWithFormat:@"group_%@",_groupId]];
    }
}


- (void)modifyView:(TModifyView *)modifyView didModiyContent:(NSString *)content
{
    @weakify(self)
    if(modifyView.tag == 0){
        V2TIMGroupInfo *info = [[V2TIMGroupInfo alloc] init];
        info.groupID = _groupId;
        info.groupName = content;
        [[V2TIMManager sharedInstance] setGroupInfo:info succ:^{
            @strongify(self)
            self.profileCellData.name = content;
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];
    }
    else if(modifyView.tag == 1){
        V2TIMGroupInfo *info = [[V2TIMGroupInfo alloc] init];
        info.groupID = _groupId;
        info.notification = content;
        [[V2TIMManager sharedInstance] setGroupInfo:info succ:^{
            @strongify(self)
            self.profileCellData.signature = content;
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];
    }
    else if(modifyView.tag == 2){
        NSString *user = [V2TIMManager sharedInstance].getLoginUser;
        V2TIMGroupMemberFullInfo *info = [[V2TIMGroupMemberFullInfo alloc] init];
        info.userID = user;
        info.nameCard = content;
        [[V2TIMManager sharedInstance] setGroupMemberInfo:_groupId info:info succ:^{
            @strongify(self)
            self.groupNickNameCellData.value = content;
        } fail:^(int code, NSString *msg) {
            [THelper makeToastError:code msg:msg];
        }];
    }
}

- (void)deleteGroup:(CSUIButtonCell *)cell
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil message:TUILocalizableString(TUIKitGroupProfileDeleteGroupTips) preferredStyle:UIAlertControllerStyleActionSheet];

    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(Confirm) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {

        @weakify(self)
        if ([self.groupInfo canDelete]) {
            [self LoadRequestDissolve];
//            [[V2TIMManager sharedInstance] dismissGroup:self.groupId succ:^{
//                @strongify(self)
//                [self.navigationController popViewControllerAnimated:YES];
//            } fail:^(int code, NSString *msg) {
//                [THelper makeToastError:code msg:msg];
//            }];
        } else {
            [self LoadRequestOut];
            
//            [[V2TIMManager sharedInstance] quitGroup:self.groupId succ:^{
//                @strongify(self)
//                [self.navigationController popViewControllerAnimated:YES];
//            } fail:^(int code, NSString *msg) {
//                [THelper makeToastError:code msg:msg];
//            }];
        }
    }]];

    [ac addAction:[UIAlertAction actionWithTitle:TUILocalizableString(Cancel) style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:ac animated:YES completion:nil];
}
- (void)LoadRequestDissolve
{
    [SVProgressHUD showWithStatus:@"解散中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_addqunjieSanQun];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupid forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"解散成功"];
            [self jumpVC];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadRequestOut
{
    [SVProgressHUD showWithStatus:@"退出中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_addqunjietuiqun];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupid forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"退出成功"];
            [self jumpVC];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//跳转指定控制器
- (void)jumpVC
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[xinxipingtaiViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
- (void)groupMembersCell:(CSUIGroupMembersCell *)cell didSelectItemAtIndex:(NSInteger)index
{
    CSGroupMemberCellData *mem = self.groupMembersCellData.members[index];
#pragma mark -- 添加群成员
    if(mem.tag == 1){
        //add
        CSAddGroupListViewController *vc = [[CSAddGroupListViewController alloc]init];
        vc.groupid = self.groupid;
        vc.mainInfo = self.mainInfo;
        [self.navigationController pushViewController:vc animated:YES];
//        TUIContactSelectController *vc = [[TUIContactSelectController alloc] initWithNibName:nil bundle:nil];
//        vc.title = NSLocalizedString(@"GroupAddFirend", nil); // @"添加联系人";
//        vc.viewModel.disableFilter = ^BOOL(TCommonContactSelectCellData *data) {
//            for (TGroupMemberCellData *cd in _memberData) {
//                if ([cd.identifier isEqualToString:data.identifier])
//                    return YES;
//            }
//            return NO;
//        };
//        @weakify(self)
//        [self.navigationController pushViewController:vc animated:YES];
//        //添加成功后默认返回群组聊天界面
//        vc.finishBlock = ^(NSArray<TCommonContactSelectCellData *> *selectArray) {
//            @strongify(self)
//            NSMutableArray *list = @[].mutableCopy;
//            for (TCommonContactSelectCellData *data in selectArray) {
//                [list addObject:data.identifier];
//            }
//
//
//            NSString *useridList = [list componentsJoinedByString:@","];
//            [self LoadAddGroupStaff:useridList list:list];
//        };
    }
#pragma mark -- 删除群成员
    else if(mem.tag == 2) {
        //delete
        CSDeleteGroupListVC *vc = [[CSDeleteGroupListVC alloc]init];
        vc.groupid = self.groupid;
        vc.mainInfo = self.mainInfo;
        [self.navigationController pushViewController:vc animated:YES];
//        CSUIContactSelectController *vc = [[CSUIContactSelectController alloc] initWithNibName:nil bundle:nil];
//        vc.title = NSLocalizedString(@"GroupDeleteFriend", nil); // @"删除联系人";
//        vc.viewModel.avaliableFilter = ^BOOL(TCommonContactSelectCellData *data) {
//
//            for (TGroupMemberCellData *cd in _memberData) {
//                if ([cd.identifier isEqualToString:data.identifier])
//                    return YES;
//            }
//            return NO;
//        };
//        @weakify(self)
//        [self.navigationController pushViewController:vc animated:YES];
//        vc.finishBlock = ^(NSArray<TCommonContactSelectCellData *> *selectArray) {
//            @strongify(self)
//            NSMutableArray *list = @[].mutableCopy;
//            for (TCommonContactSelectCellData *data in selectArray) {
//                [list addObject:data.identifier];
//            }
//            //取出选中的list用户id传给后台
//
//
//            NSString *useridList = [list componentsJoinedByString:@","];
//
//            [self LoadDeleleGroupStaff:useridList list:list];
//
//        };
    }
    else
    {
        // TODO:
    }
}

- (void)LoadDeleleGroupStaff:(NSString *)userid list:(NSArray *)list
{
    [SVProgressHUD showWithStatus:@"删除中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_quntiren];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:self.groupid forKey:@"qunid"];
    [dict setValue:userid forKey:@"userid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"删除成功"];
            [self.navigationController popToViewController:self animated:YES];
            [self deleteGroupId:self.groupId memebers:list controller:self];
            [self.myTableView reloadData];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadAddGroupStaff:(NSString *)userid list:(NSArray *)list
{
    [SVProgressHUD showWithStatus:@"添加中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_addqunchengyuan];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:self.groupid forKey:@"qunid"];
    [dict setValue:userid forKey:@"userid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"添加成功"];
            [self.navigationController popToViewController:self animated:YES];
//            [self addGroupId:self.groupId memebers:list controller:self];
            [self updateData];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)addMembers:(NSArray *)members
{
    for (TAddCellData *addMember in members) {
        CSGroupMemberCellData *data = [[CSGroupMemberCellData alloc] init];
        data.identifier = addMember.identifier;
        data.name = addMember.name;
        [_memberData addObject:data];
    }

    self.groupMembersCountCellData.value = [NSString stringWithFormat:TUILocalizableString(TUIKitGroupProfileMemberCountlu), (unsigned long)_memberData.count];
    self.groupMembersCellData.members = [self getShowMembers:_memberData];

    [self.myTableView reloadData];
}

- (void)deleteMembers:(NSArray *)members
{
    NSMutableArray *delArray = [NSMutableArray array];
    for (TAddCellData *delMember in members) {
        for (CSGroupMemberCellData *member in _memberData) {
            if([delMember.identifier isEqualToString:member.identifier]){
                [delArray addObject:member];
            }
        }
    }
    [_memberData removeObjectsInArray:delArray];

    self.groupMembersCountCellData.value = [NSString stringWithFormat:TUILocalizableString(TUIKitGroupProfileMemberCountlu), (unsigned long)_memberData.count];
    self.groupMembersCellData.members = [self getShowMembers:_memberData];

    [self.myTableView reloadData];
}
#pragma mark --在此设置群成员数据
- (NSMutableArray *)getShowMembers:(NSMutableArray *)members
{
    int maxCount = TGroupMembersCell_Column_Count * TGroupMembersCell_Row_Count;
    if ([self.groupInfo canRemoveMember]) maxCount--;
    if ([self.groupInfo canRemoveMember]) maxCount--;
    NSMutableArray *tmpArray = [NSMutableArray array];

    
//    if(self.mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_SUPER||self.mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_ADMIN) {
    if ([_yaoqingstate isEqualToString:@"1"]||self.mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_SUPER||self.mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_ADMIN) {
        CSGroupMemberCellData *add = [[CSGroupMemberCellData alloc] init];
        add.avatarImage = [UIImage imageNamed:@"group_添加成员"];
        add.tag = 1;
        add.name = @"添加成员";
        [tmpArray addObject:add];
    }
    if(self.mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_SUPER||self.mainInfo.role==V2TIM_GROUP_MEMBER_ROLE_ADMIN) {
        CSGroupMemberCellData *delete = [[CSGroupMemberCellData alloc] init];
        delete.avatarImage = [UIImage imageNamed:@"group_删除成员"];
        delete.tag = 2;
        delete.name = @"删除成员";
        [tmpArray addObject:delete];
    }
    for (NSInteger i = 0; i < members.count && i < maxCount; ++i) {
        
        [tmpArray addObject:members[i]];
    }
    return tmpArray;
}

/**
 *  点击头像查看大图的委托实现。
 */
-(void)didTapOnAvatar:(TUIProfileCardCell *)cell{
    TUIAvatarViewController *image = [[TUIAvatarViewController alloc] init];
    image.avatarData = cell.cardData;
    [self.navigationController pushViewController:image animated:YES];
}
#pragma  mark -- 设置群头像
- (void)HeadImageTap:(CSGroupInfoHeadView *)headView image:(UIImageView *)image
{
    
    NSLog(@"设置群头像");
//    UIImageView *imageView = image;
    __weak typeof(self) weakSelf = self;
    [ZZQAvatarPicker startSelected:^(UIImage * _Nonnull image) {
        if (image) {
            
            [weakSelf LoadRequestImage:image];
        }
    }];
}
- (void)LoadRequestImage :(UIImage *)image
{
    [SVProgressHUD showWithStatus:@"上传中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_groutouxiang];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:self.groupid forKey:@"qunid"];
    [longwangRequestManage postBaseImageWithURL:url param:dict image:image success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"上传成功"];
            _headView.headImage.image = image;
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
     
}
#pragma mark -- 设置群昵称
- (void)HeadEditNameClick:(CSGroupInfoHeadView *)headView sender:(UIButton *)sender
{
    NSLog(@"设置群昵称");
    CSGroupNameViewController *vc = [[CSGroupNameViewController alloc]init];
    vc.navigationItem.title = @"设置群昵称";
    vc.info = self.groupInfo;
    vc.grouid = self.groupid;
    vc.callBackBlock = ^(NSString * _Nonnull text) {
        self.headView.nameLable.text = text;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addGroupId:(NSString *)groupId memebers:(NSArray *)members controller:(TUIGroupMemberController *)controller
{
    [[V2TIMManager sharedInstance] inviteUserToGroup:_groupId userList:members succ:^(NSArray<V2TIMGroupMemberOperationResult *> *resultList) {
        [THelper makeToast:NSLocalizedString(@"add_success", nil)];
        [controller updateData];
    } fail:^(int code, NSString *desc) {
        [THelper makeToastError:code msg:desc];
    }];
}

- (void)deleteGroupId:(NSString *)groupId memebers:(NSArray *)members controller:(TUIGroupMemberController *)controller
{
    [[V2TIMManager sharedInstance] kickGroupMember:groupId memberList:members reason:@"" succ:^(NSArray<V2TIMGroupMemberOperationResult *> *resultList) {
//        [THelper makeToast:NSLocalizedString(@"delete_success", nil)];
        [controller updateData];
    } fail:^(int code, NSString *desc) {
        [THelper makeToastError:code msg:desc];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
