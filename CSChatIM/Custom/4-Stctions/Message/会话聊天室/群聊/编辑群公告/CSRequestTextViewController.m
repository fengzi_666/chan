//
//  CSRequestTextViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "CSRequestTextViewController.h"
#import "PlaceholderTextView.h"
@interface CSRequestTextViewController ()
@property (nonatomic ,strong) PlaceholderTextView *textView;

@end

@implementation CSRequestTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNav];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    PlaceholderTextView *textView = [[PlaceholderTextView alloc]init];
    _textView = textView;
    textView.placeholderLabel.font = [UIFont systemFontOfSize:15];
    textView.placeholder = @"请编辑群公告...";
    textView.font = [UIFont systemFontOfSize:15];
    textView.frame = CGRectMake(0, 10, KScreenWidth, 155);
    textView.maxLength = 300;
//    textView.layer.cornerRadius = 5.f;
//    textView.layer.borderColor = [[UIColor grayColor]colorWithAlphaComponent:0.3].CGColor;
//    textView.layer.borderWidth = 0.5f;
    [self.view addSubview:textView];
    
    [textView didChangeText:^(PlaceholderTextView *textView) {
        NSLog(@"%@",textView.text);
        
        
    }];
   
}
#pragma  mark - 滚动scrollView 键盘退下
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    // 取消键盘
    [self.view endEditing:YES];
    
}
- (void)rightBtnClick
{
    [SVProgressHUD showWithStatus:@"修改中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunupdateMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupid forKey:@"qunid"];

    [dict setValue:_textView.text forKey:@"gonggao"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
            self.callBackBlock(_textView.text);
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
