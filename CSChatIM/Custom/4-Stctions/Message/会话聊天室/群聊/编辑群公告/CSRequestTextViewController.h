//
//  CSRequestTextViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^CallBackBlcok) (NSString *text);
@interface CSRequestTextViewController : BaseViewController
@property (nonatomic ,copy) NSString *groupid;
@property (nonatomic,copy)CallBackBlcok callBackBlock;
@end

NS_ASSUME_NONNULL_END
