//
//  CSGroupListTableViewCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import "CSGroupListTableViewCell.h"

@implementation CSGroupListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.width/2;
    self.layerbg.layer.masksToBounds = YES;
    self.layerbg.layer.cornerRadius = 3;
    self.layerbg.layer.borderWidth=1;
    self.layerbg.layer.borderColor = [UIColor colorWithHexString:@"#f79b0b"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
