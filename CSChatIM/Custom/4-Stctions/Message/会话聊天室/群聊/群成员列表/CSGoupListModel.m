//
//  CSGoupListModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import "CSGoupListModel.h"

@implementation CSGoupListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.miandarao = [NSString stringWithFormat:@"%@",dic[@"miandarao"]];
        self.qunid =[NSString stringWithFormat:@"%@",dic[@"qunid"]];
        self.qunmingpian = dic[@"qunmingpian"];
        self.listid = [NSString stringWithFormat:@"%@",dic[@"id"]];
        self.qunming = dic[@"qunming"];
        self.qunpic = dic[@"qunpic"];
        self.tengxuncode = dic[@"tengxuncode"];
        self.shenfen = [NSString stringWithFormat:@"%@",dic[@"shenfen"]];
        self.userid =[NSString stringWithFormat:@"%@",dic[@"userid"]];
        self.quncode = dic[@"quncode"];
        self.jinyan =[NSString stringWithFormat:@"%@",dic[@"jinyan"]];
        self.hongbaostate =[NSString stringWithFormat:@"%@",dic[@"hongbaostate"]];
        self.touxiang = dic[@"touxiang"];
        self.nicheng = dic[@"nicheng"];
    }
    return self;
}
@end
