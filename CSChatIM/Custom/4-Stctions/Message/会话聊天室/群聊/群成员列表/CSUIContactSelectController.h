//
//  CSUIContactSelectController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import "BaseViewController.h"
#import "TCommonContactSelectCellData.h"
#import "TContactSelectViewModel.h"

typedef void(^ContactSelectFinishBlock)(NSArray<TCommonContactSelectCellData *> * _Nonnull selectArray);
NS_ASSUME_NONNULL_BEGIN

@interface CSUIContactSelectController : BaseViewController

@property (nonatomic ,copy) NSString *goupid;
/**
 * 选择结束回调
 */
@property (nonatomic, copy) ContactSelectFinishBlock finishBlock;

/**
 * 最多选择个数
 */
@property NSInteger maxSelectCount;

@property (nonatomic) TContactSelectViewModel *viewModel;

/**
 * 自定义的数据列表
 */
@property NSArray *sourceIds;
@end

NS_ASSUME_NONNULL_END
