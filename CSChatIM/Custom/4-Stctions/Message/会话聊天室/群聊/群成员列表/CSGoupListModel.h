//
//  CSGoupListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGoupListModel : NSObject
@property (nonatomic ,copy) NSString *miandarao;
@property (nonatomic ,copy) NSString *qunid;
@property (nonatomic ,copy) NSString *qunmingpian;
@property (nonatomic ,copy) NSString *listid;
@property (nonatomic ,copy) NSString *qunming;
@property (nonatomic ,copy) NSString *qunpic;
@property (nonatomic ,copy) NSString *tengxuncode;
@property (nonatomic ,copy) NSString *shenfen;
@property (nonatomic ,copy) NSString *userid;
@property (nonatomic ,copy) NSString *quncode;
@property (nonatomic ,copy) NSString *jinyan;
@property (nonatomic ,copy) NSString *hongbaostate;
@property (nonatomic ,copy) NSString *touxiang;
@property (nonatomic ,copy) NSString *nicheng;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
