//
//  CSGroupMemberTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import "TCommonCell.h"
#import "TCommonContactSelectCellData.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupMemberTableViewCell : TCommonTableViewCell

@property UIButton *selectButton;
@property UIImageView *avatarView;
@property UILabel *titleLabel;

@property  TCommonContactSelectCellData *selectData;

- (void)fillWithData:(TCommonContactSelectCellData *)selectData;

@end

NS_ASSUME_NONNULL_END
