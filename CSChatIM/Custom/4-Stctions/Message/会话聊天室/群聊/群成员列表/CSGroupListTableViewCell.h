//
//  CSGroupListTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *hitLable;
@property (weak, nonatomic) IBOutlet UIView *layerbg;

@end

NS_ASSUME_NONNULL_END
