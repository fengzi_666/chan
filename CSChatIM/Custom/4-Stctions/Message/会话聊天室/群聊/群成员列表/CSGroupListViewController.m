//
//  CSGroupListViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "CSGroupListViewController.h"
#import "CSGroupListTableViewCell.h"
#import "CSGoupListModel.h"
#import "CSUserDataViewController.h"
#import "liaotianxinxiProfileController.h"
#import "THelper.h"
@interface CSGroupListViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,assign) NSInteger pageno;
@property (nonatomic ,copy) NSString *searchStr;
@end
static NSString *cellid = @"cellid";
@implementation CSGroupListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dataArr = [[NSMutableArray alloc]init];
    
    [self setNav];
    [self loadSearcView];
    
    [self LoadTableView];
    
   
}
- (void)loadSearcView
{
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 52)];
    myView.backgroundColor = [UIColor whiteColor];
    myView.userInteractionEnabled = YES;
    [self.view addSubview:myView];
    UIView *searchView = [[UIView alloc]init];
    searchView.backgroundColor = [UIColor whiteColor];
    searchView.layer.masksToBounds = YES;
    searchView.layer.cornerRadius = 5;
    searchView.userInteractionEnabled = YES;
    [myView addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(3);
        make.height.mas_equalTo(32);
        make.left.mas_equalTo(myView.mas_left).offset(12);
        make.right.mas_equalTo(myView.mas_right).offset(-12);
    }];
   
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    searchBar.frame = CGRectMake(0, 0, KScreenWidth-24, 32); // 设置位置和大小
//    oneSearchBar.keyboardType = UIKeyboardTypeEmailAddress; // 设置弹出键盘的类型
    [searchBar setImage:[UIImage imageNamed:@"search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    if (@available(iOS 13.0, *)) {
        UITextField *txfSearchField = (UITextField*)searchBar.searchTextField;
        txfSearchField.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    } else {
        // Fallback on earlier versions
        NSArray *subviewArr = self.view.subviews;
        for(int i = 0; i < subviewArr.count ; i++) {
            UIView *viewSub = [subviewArr objectAtIndex:i];
            NSArray *arrSub = viewSub.subviews;
            for (int j = 0; j < arrSub.count ; j ++) {
                id tempId = [arrSub objectAtIndex:j];
                if([tempId isKindOfClass:[UITextField class]]) {
                    UITextField *searchFiled = (UITextField *)tempId;
                    searchFiled.font = [UIFont systemFontOfSize:14];
                }
            }
        }
      
     
       
    }
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault; // 设置UISearchBar的样式
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
//    searchBar.showsCancelButton = YES;
//    searchBar.tintColor = [UIColor redColor]; // 设置UISearchBar的颜色 使用clearColor就是去掉背景
    searchBar.placeholder = @"对方账号"; // 设置提示文字
    [searchView addSubview:searchBar];
    
}
- (void)LoadRequestData: (NSString *)str
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appqunchengyuanlist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupId forKey:@"qunid"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [dict setValue:str forKey:@"nichengorcode"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        NSMutableArray *dataArr = [[NSMutableArray alloc]init];
        
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
            NSDictionary *data  = dict[@"data"];
            NSArray *list = data[@"list"];
            for (int i =0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSGoupListModel *model = [[CSGoupListModel alloc]initWithDic:dic];
                [dataArr addObject:model];
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (list.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (dataArr.count==0) {
                    
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
            
//            self.navigationItem.title = [NSString stringWithFormat:@"群成员(%lu)人",(unsigned long)list.count];
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
         [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
    
}

- (void)LoadTableView
{
  
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 52, KScreenWidth, KScreenHeight-52-64) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSGroupListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
     //这种上拉刷新footer在tableview的底部
     [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
     _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         _pageno =1;
         self.dataArr =[NSMutableArray array];
         [self LoadRequestData:_searchStr];
     }];
     self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
         self.pageno ++;
         [self LoadRequestData:_searchStr];
     }];
    [_myTableView.mj_header beginRefreshing];
     
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSGroupListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSGoupListModel *model = _dataArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = model.nicheng;
    if ([model.shenfen isEqualToString:@"1"]) {
        cell.hitLable.text = @"群主";
    }
    else if([model.shenfen isEqualToString:@"2"])
    {
        cell.hitLable.text = @"管理员";
    }
    else
    {
        cell.hitLable.hidden = YES;
        cell.layerbg.hidden = YES;
    }
    
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSGoupListModel *model = _dataArr[indexPath.row];
    @weakify(self)
    [[V2TIMManager sharedInstance] getFriendsInfo:@[model.tengxuncode] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
        V2TIMFriendInfoResult *result = resultList.firstObject;
        if (result.relation == V2TIM_FRIEND_RELATION_TYPE_IN_MY_FRIEND_LIST || result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY) {
            @strongify(self)
            
            CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
            vc.friendProfile = result.friendInfo;
            vc.result = result;
            vc.groupId = _groupInfo.groupID;
            vc.istype=@"1";
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            
            [[V2TIMManager sharedInstance] getUsersInfo:@[model.tengxuncode] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
                @strongify(self)
                liaotianxinxiProfileController *vc = [[liaotianxinxiProfileController alloc]init];
                    vc.userFullInfo = infoList.firstObject;
                    if ([vc.userFullInfo.userID isEqualToString:[[V2TIMManager sharedInstance] getLoginUser]]) {
                        vc.actionType = PCA_NONE;
                        return;
                        
                    } else {
                        vc.actionType = PCA_ADD_FRIEND;
                    }
                    vc.info = _groupInfo;
                vc.type =@"2";
                
                [self.navigationController pushViewController:vc animated:YES];
                
            } fail:^(int code, NSString *msg) {
                [THelper makeToastError:code msg:msg];
            }];
        }
    } fail:^(int code, NSString *msg) {
        [THelper makeToastError:code msg:msg];
    }];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //查找用户
    _searchStr = searchBar.text;
    _pageno =1;
        [self LoadRequestData:_searchStr];
    
}

/*
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
