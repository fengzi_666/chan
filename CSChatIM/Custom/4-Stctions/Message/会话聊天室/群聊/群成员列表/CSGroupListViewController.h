//
//  CSGroupListViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/1.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupListViewController : BaseViewController
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic ,strong) NSString *groupCode;
@property V2TIMGroupInfo *groupInfo;
@end

NS_ASSUME_NONNULL_END
