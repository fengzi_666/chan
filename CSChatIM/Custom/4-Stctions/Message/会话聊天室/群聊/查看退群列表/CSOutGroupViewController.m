//
//  CSOutGroupViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "CSOutGroupViewController.h"
#import "CSGroupListTableViewCell.h"
#import "CSOutGoupListModel.h"
@interface CSOutGroupViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property V2TIMGroupInfo *groupInfo;
@property (nonatomic,assign) NSInteger pageno;
@end
static NSString *cellid = @"cellid";
@implementation CSOutGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"查看退群人员";
    _dataArr = [[NSMutableArray alloc]init];
    
    [self setNav];
    
    [self LoadTableView];

}
- (void)LoadRequestData
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunchengyuandellist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupCode forKey:@"qunid"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        NSMutableArray *dataArr = [[NSMutableArray alloc]init];
        
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
            NSDictionary *data  = dict[@"data"];
            NSArray *list = data[@"list"];
            for (int i =0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSOutGoupListModel *model = [[CSOutGoupListModel alloc]initWithDic:dic];
                [dataArr addObject:model];
            }
            
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
                if (list.count==0) {
                    _myTableView.emptyView.hidden = NO;
                }
            }
            else
            {
                if (dataArr.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
            
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
         [_myTableView reloadData];
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
    
}

- (void)LoadTableView
{
  
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [_myTableView registerNib:[UINib nibWithNibName:@"CSGroupListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    
     //这种上拉刷新footer在tableview的底部
     [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
     _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         _pageno =1;
         self.dataArr =[NSMutableArray array];
         [self LoadRequestData];
     }];
     self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
         self.pageno ++;
         [self LoadRequestData];
     }];
    [_myTableView.mj_header beginRefreshing];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSGroupListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSOutGoupListModel *model = _dataArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = model.userphone;
   
    cell.hitLable.hidden = YES;
    cell.layerbg.hidden = YES;
    
    
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
