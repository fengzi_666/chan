//
//  CSOutGoupListModel.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSOutGoupListModel : NSObject
@property (nonatomic ,copy) NSString *tengxuncode;
@property (nonatomic ,copy) NSString *touxiang;
@property (nonatomic ,copy) NSString *usercode;
@property (nonatomic ,copy) NSString *userid;
@property (nonatomic ,copy) NSString *userphone;

- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
