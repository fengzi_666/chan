//
//  CSOutGroupViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSOutGroupViewController : BaseViewController
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic ,strong) NSString *groupCode;
@end

NS_ASSUME_NONNULL_END
