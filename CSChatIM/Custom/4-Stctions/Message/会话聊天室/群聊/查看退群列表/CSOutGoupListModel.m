//
//  CSOutGoupListModel.m
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "CSOutGoupListModel.h"

@implementation CSOutGoupListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
      
        self.tengxuncode = dic[@"tengxuncode"];
        self.touxiang = [NSString stringWithFormat:@"%@",dic[@"touxiang"]];
        self.userid =[NSString stringWithFormat:@"%@",dic[@"userid"]];
        self.usercode = dic[@"usercode"];
        self.userphone =[NSString stringWithFormat:@"%@",dic[@"userphone"]];
    }
    return self;
}
@end
