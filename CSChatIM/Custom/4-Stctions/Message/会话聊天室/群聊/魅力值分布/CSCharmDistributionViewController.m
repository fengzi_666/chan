//
//  CSCharmDistributionViewController.m
//  CSChatIM
//
//  Created by Apple on 2021/2/19.
//

#import "CSCharmDistributionViewController.h"
#import "JGProgressView.h"
@interface CSCharmDistributionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *qunwaimeilizhi;
@property (weak, nonatomic) IBOutlet UIView *qunwaiView;
@property (weak, nonatomic) IBOutlet UILabel *qunwaimeilizhizhanbi;
@property (weak, nonatomic) IBOutlet UILabel *daituimeilizhi;
@property (weak, nonatomic) IBOutlet UIView *daituiView;
@property (weak, nonatomic) IBOutlet UILabel *daituimeilizhizhanbi;
@property (weak, nonatomic) IBOutlet UILabel *qunneimeilizhi;
@property (weak, nonatomic) IBOutlet UILabel *qunneimeilizhizhanbi;
@property (weak, nonatomic) IBOutlet UIView *qunneiView;
@property (weak, nonatomic) IBOutlet UIView *shuomingView;
@property (nonatomic ,strong)JGProgressView *progressView1;
@property (nonatomic ,strong)JGProgressView *progressView2;
@property (nonatomic ,strong)JGProgressView *progressView3;

@end

@implementation CSCharmDistributionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    self.navigationItem.title = @"魅力值分布";
    // Do any additional setup after loading the view from its nib.
    _progressView1 = [[JGProgressView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth-24, 24)];
    _progressView1.progress = 0.5;
    [self.qunwaiView addSubview:_progressView1];
    self.qunwaiView.layer.masksToBounds = YES;
    self.qunwaiView.layer.cornerRadius = self.qunwaiView.height/2;
    _progressView2 = [[JGProgressView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth-24, 24)];
    _progressView2.progress = 0.5;
    [self.daituiView addSubview:_progressView2];
    self.daituiView.layer.masksToBounds = YES;
    self.daituiView.layer.cornerRadius = self.daituiView.height/2;
    _progressView3 = [[JGProgressView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth-24, 24)];
    _progressView3.progress = 0.5;
    [self.qunneiView addSubview:_progressView3];
    self.qunneiView.layer.masksToBounds = YES;
    self.qunneiView.layer.cornerRadius = self.qunneiView.height/2;
    
    self.shuomingView.layer.masksToBounds = YES;
    self.shuomingView.layer.cornerRadius = 10;
    
    [self LoadRequestData];
  
}
- (void)LoadRequestData
{
//    [SVProgressHUD showWithStatus:@"<#查询中#>"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunmeilizhi];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_qunid forKey:@"qunid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            _daituimeilizhi.text =[NSString stringWithFormat:@"%@",data[@"zongmeilizhi"]];
            _qunneimeilizhi.text =[NSString stringWithFormat:@"%@",data[@"zongmeilizhi"]];
            _qunwaimeilizhi.text =[NSString stringWithFormat:@"%@",data[@"zongmeilizhi"]];
            NSInteger a=[data[@"zongmeilizhi"] integerValue];
            NSInteger b=[data[@"qunneimeilizhi"] integerValue];
            NSInteger c=[data[@"daituimeilizhi"] integerValue];
            NSInteger d=[data[@"qunwaimeilizhi"] integerValue];
           
            NSString *str = [NSString stringWithFormat:@"%.2f",(float)a/b];
            NSString *str1 = [NSString stringWithFormat:@"%.2f",(float)a/c];
            NSString *str2 = [NSString stringWithFormat:@"%.2f",(float)a/d];
            
            NSString *str3 = [NSString stringWithFormat:@"%.2f",(float)a/b*100];
            NSString *str4 = [NSString stringWithFormat:@"%.2f",(float)a/c*100];
            NSString *str5 = [NSString stringWithFormat:@"%.2f",(float)a/d*100];
            NSString *str6 = @"%";
            if (b>0) {
                _qunwaimeilizhizhanbi.text = [NSString stringWithFormat:@"%@%@",str3,str6];
            }
            else
            {
                _qunwaimeilizhizhanbi.text = @"0.00%";
            }
            if (c>0) {
                _daituimeilizhizhanbi.text = [NSString stringWithFormat:@"%@%@",str4,str6];
            }
            else
            {
                _daituimeilizhizhanbi.text = @"0.00%";
            }
            if (d>0) {
                _qunneimeilizhizhanbi.text = [NSString stringWithFormat:@"%@%@",str5,str6];
            }
            else
            {
                _qunneimeilizhizhanbi.text = @"0.00%";
            }
           
           
          
            
            _progressView1.progress = [str integerValue];
            _progressView2.progress = [str1 integerValue];
            _progressView3.progress = [str2 integerValue];
            
            
            
//            _progressView1.progress =
            NSLog(@"%.2f",(float)b/a);//ok
           
            
//            [SVProgressHUD showSuccessWithStatus:@"<#查询成功#>"];
        }
        else
        {
//            [SVProgressHUD showSuccessWithStatus:dict[@"<#msg#>"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
