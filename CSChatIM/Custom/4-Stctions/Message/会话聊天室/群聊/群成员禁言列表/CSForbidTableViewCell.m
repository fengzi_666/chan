//
//  CSForbidTableViewCell.m
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import "CSForbidTableViewCell.h"

@implementation CSForbidTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _headImage.layer.masksToBounds = YES;
    _headImage.layer.cornerRadius = _headImage.height/2;
    // Initialization code
}
- (IBAction)switchClicj:(UISwitch *)sender {
    if (sender.on==YES)
    {
        NSLog( @"开启");
        NSString *str = @"1";
        if (_delegate&&[_delegate respondsToSelector:@selector(switchBtnClickCell:tag:swich:)]) {
            [_delegate switchBtnClickCell:self tag:str swich:sender];
        }
    }
    else
    {
        NSLog( @"关闭");
        NSString *str = @"2";
        if (_delegate&&[_delegate respondsToSelector:@selector(switchBtnClickCell:tag:swich:)]) {
            [_delegate switchBtnClickCell:self tag:str  swich:sender];
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
