//
//  CSForbidTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSForbidTableViewCell;

@protocol CSForbidTableViewCellDelegate <NSObject>


- (void)switchBtnClickCell:(CSForbidTableViewCell *)cell tag:(NSString *)tag swich:(UISwitch *)swich;
@end
@interface CSForbidTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UISwitch *switchBtn;
@property (weak, nonatomic) IBOutlet UILabel *hitLable;
@property (weak, nonatomic) IBOutlet UIView *hitLayerbg;
@property (nonatomic, weak) id<CSForbidTableViewCellDelegate> delegate;
@property (nonatomic ,strong) NSIndexPath *indexPasth;
@end

NS_ASSUME_NONNULL_END
