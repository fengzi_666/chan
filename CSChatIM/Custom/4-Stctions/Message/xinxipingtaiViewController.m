//
//  xinxipingtaiViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "xinxipingtaiViewController.h"
#import "TUIConversationListController.h"
#import "liaotianshiViewController.h"
#import "TPopView.h"
#import "TPopCell.h"
#import "THeader.h"
#import "Toast/Toast.h"
#import "CSPopView.h"
#import "TPopCell.h"
#import "TUIContactSelectController.h"
#import "TCUtil.h"
#import "THelper.h"
#import "SearchFriendViewController.h"
#import "CSAddFriendViewController.h"
#import "CSRichScanViewController.h"
#import "CSContactSelectController.h"
#import "kongyemianListViewController.h"
#import "CSSystemMessageVC.h"
@interface xinxipingtaiViewController ()<TUIConversationListControllerDelegate,TPopViewDelegate,V2TIMGroupListener>

@end

@implementation xinxipingtaiViewController


- (void)viewDidLoad{
    [super viewDidLoad];
//    self.navigationItem.title = @"消息";
    kongyemianListViewController *conv = [[kongyemianListViewController alloc] init];
    conv.delegate = self;
    
    
    //如果不加这一行代码，依然可以实现点击反馈，但反馈会有轻微延迟，体验不好。
    conv.tableView.delaysContentTouches = NO;
    
    [self addChildViewController:conv];
    [self.view addSubview:conv.view];

    //如果不加这一行代码，依然可以实现点击反馈，但反馈会有轻微延迟，体验不好。
    conv.tableView.delaysContentTouches = NO;
    
    [self LoadNav];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRecvMessageReceipts:) name:TUIKitNotification_onRecvMessageReceipts object:nil];
    
}
- (void) didRecvMessageReceipts:(NSNotification *)noti{
    NSArray *receiptsArray = noti.object;
    if(!receiptsArray.count){
        NSLog(@"Receipt Data Error");
        return;
    }
    V2TIMMessageReceipt *receipt = receiptsArray[0];
    time_t receiptTime = receipt.timestamp;
//    if(receipt.userID.length > 0 && [receipt.userID isEqualToString:self.conversationData.userID]){
//        //性能优化
//        for(int i = 0;i < _uiMsgs.count;i++){
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_uiMsgs.count - 1 - i inSection:0];
//            TUIMessageCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            //通过回调时间戳判定当前的未读是否需要改为已读
//            time_t msgTime = [cell.messageData.innerMessage.timestamp timeIntervalSince1970];
//            if(msgTime <= receiptTime && ![cell.readReceiptLabel.text isEqualToString:TUILocalizableString(Read)]) {
//                cell.readReceiptLabel.text = TUILocalizableString(Read);
//            }
//        }
//    }
}
- (void)LoadNav
{
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    lable.textColor = [UIColor colorWithHexString:@"#333333"];
    lable.font = [UIFont boldSystemFontOfSize:22];
    lable.text = @"消息";
    UIBarButtonItem *leftLable = [[UIBarButtonItem alloc]initWithCustomView:lable];
    self.navigationItem.leftBarButtonItem=leftLable;

    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [moreButton setImage:[UIImage imageNamed:@"消息_more"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(rightBarButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *moreItem = [[UIBarButtonItem alloc] initWithCustomView:moreButton];
    self.navigationItem.rightBarButtonItem = moreItem;
    
    self.view.backgroundColor = [UIColor  whiteColor];
    
//    UINavigationBar *navigationBar = self.navigationController.navigationBar;
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
//    [navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//     //此处使底部线条失效
//     [navigationBar setShadowImage:[UIImage new]];
   
    self.automaticallyAdjustsScrollViewInsets = YES;
   
}
/**
 *推送默认跳转
 */
- (void)pushToChatViewController:(NSString *)groupID userID:(NSString *)userID {
    if ([userID isEqualToString:@"xxzs10001"]) {
        CSSystemMessageVC *vc  = [[ CSSystemMessageVC alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        liaotianshiViewController *chat = [[liaotianshiViewController alloc] init];
        TUIConversationCellData *conversationData = [[TUIConversationCellData alloc] init];
        conversationData.groupID = groupID;
        conversationData.userID = userID;
        chat.conversationData = conversationData;
        [self.navigationController pushViewController:chat animated:YES];
    }
    
}
/**
 *对导航栏右侧的按钮（即视图右上角按钮）进行初始化，创建对应的popView
 */
- (void)rightBarButtonClick:(UIButton *)rightBarButton
{
    NSMutableArray *menus = [NSMutableArray array];
    TPopCellData *friend = [[TPopCellData alloc] init];
    
    
    friend.image = @"消息_添加";
    friend.title = @"添加好友";
    [menus addObject:friend];

    TPopCellData *group3 = [[TPopCellData alloc] init];
    group3.image = @"消息_扫一扫";
    group3.title = @"扫一扫";
    [menus addObject:group3];

    TPopCellData *group = [[TPopCellData alloc] init];
    group.image = @"消息_发起群聊";
    group.title = @"发起群聊";
    [menus addObject:group];

    TPopCellData *room = [[TPopCellData alloc] init];
    room.image = @"消息_加入群聊";
    room.title = @"加入群聊";
    [menus addObject:room];
   

    CGFloat height = [TPopCell getHeight] * menus.count + TPopView_Arrow_Size.height;
    CGFloat orginY = StatusBar_Height + NavBar_Height;
    CSPopView *popView = [[CSPopView alloc] initWithFrame:CGRectMake(Screen_Width - 155, orginY, 145, height)];
    
    UIColor *color = [UIColor blackColor];
    popView.tableView.backgroundColor = [color colorWithAlphaComponent:0.7];
    
    CGRect frameInNaviView = [self.navigationController.view convertRect:rightBarButton.frame fromView:rightBarButton.superview];
    popView.arrowPoint = CGPointMake(frameInNaviView.origin.x + frameInNaviView.size.width * 0.5, orginY);
    popView.delegate = self;
    [popView setData:menus];
    [popView showInWindow:self.view.window];
}

/**
 *点击了popView中具体某一行后的响应函数，popView初始化请参照上述 rightBarButtonClick: 函数
 */
- (void)popView:(TPopView *)popView didSelectRowAtIndex:(NSInteger)index
{
     
    if(index == 0){
        //添加好友
        CSAddFriendViewController *add = [[CSAddFriendViewController alloc] init];
        add.hidesBottomBarWhenPushed = YES;
        add.segIndex = 0;
        [self.navigationController pushViewController:add animated:YES];
    }
    else if(index == 1){
//        //扫一扫
        CSRichScanViewController *vc  =[[CSRichScanViewController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];


        return;
    } else if(index == 2){
        //创建群聊
        CSContactSelectController *vc = [CSContactSelectController new];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        vc.finishBlock = ^(NSArray<TCommonContactSelectCellData *> *array) {
           
           
            [self addGroup:GroupType_Public addOption:V2TIM_GROUP_ADD_AUTH withContacts:array];
            [TCUtil report:Action_Createpublicgrp actionSub:@"" code:@(0) msg:@"createpublicgrp"];
        };
        return;
    } else if(index == 3){
//        //添加群聊
        CSAddFriendViewController *add = [[CSAddFriendViewController alloc] init];
        add.hidesBottomBarWhenPushed = YES;
        add.segIndex = 1;
        [self.navigationController pushViewController:add animated:YES];
//        };
        return;
    }
    else {
        return;
    }
}

/**
 *创建讨论组、群聊、聊天室的函数
 *groupType:创建的具体类型 Private--讨论组  Public--群聊 ChatRoom--聊天室
 *addOption:创建后加群时的选项          TIM_GROUP_ADD_FORBID       禁止任何人加群
                                     TIM_GROUP_ADD_AUTH        加群需要管理员审批
                                     TIM_GROUP_ADD_ANY         任何人可以加群
 *withContacts:群成员的信息数组。数组内每一个元素分别包含了对应成员的头像、ID等信息。具体信息可参照 TCommonContactSelectCellData 定义
 */
- (void)addGroup:(NSString *)groupType addOption:(V2TIMGroupAddOpt)addOption withContacts:(NSArray<TCommonContactSelectCellData *>  *)contacts
{
    __weak typeof(self) weakSelf = self;
    NSString *loginUser = [[V2TIMManager sharedInstance] getLoginUser];
    [[V2TIMManager sharedInstance] getUsersInfo:@[loginUser] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
        NSString *showName = loginUser;
        if (infoList.firstObject.nickName.length > 0) {
            showName = infoList.firstObject.nickName;
        }
        NSMutableString *groupName = [NSMutableString stringWithString:showName];
        NSMutableArray *members = [NSMutableArray array];
        //遍历contacts，初始化群组成员信息、群组名称信息
        for (TCommonContactSelectCellData *item in contacts) {
            V2TIMCreateGroupMemberInfo *member = [[V2TIMCreateGroupMemberInfo alloc] init];
            member.userID = item.identifier;
            member.role = V2TIM_GROUP_MEMBER_ROLE_MEMBER;
            [groupName appendFormat:@"、%@", item.title];
            [members addObject:member];
        }

        //群组名称默认长度不超过10，如有需求可在此更改，但可能会出现UI上的显示bug
        if ([groupName length] > 10) {
            groupName = [groupName substringToIndex:10].mutableCopy;
        }

        V2TIMGroupInfo *info = [[V2TIMGroupInfo alloc] init];
        info.groupName = groupName;
        info.groupType = groupType;
        if(![info.groupType isEqualToString:GroupType_Work]){
            info.groupAddOpt = addOption;
        }
        NSMutableArray *arr = [[NSMutableArray alloc]init];
        for (int i= 0; i<contacts.count; i++) {
            TCommonContactSelectCellData *data =contacts[i];
            
            NSString *userid =data.identifier;
            [arr addObject:userid];
        }
        NSString *tempStr = [arr componentsJoinedByString:@","];
        [SVProgressHUD showWithStatus:@"创建中..."];
        NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunchuangjian];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:tempStr forKey:@"haoyouid"];
        [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
            if ([code isEqualToString:@"0"]) {
                [SVProgressHUD showSuccessWithStatus:@"创建成功"];
                NSDictionary *data = dict[@"data"];
                
                
                //创建成功后，在群内推送创建成功的信息
               
                NSString *content = nil;
                if([info.groupType isEqualToString:GroupType_Work]) {
                    content = NSLocalizedString(@"ChatsCreatePrivateGroupTips", nil); // @"创建讨论组";
                } else if([info.groupType isEqualToString:GroupType_Public]){
                    content = NSLocalizedString(@"ChatsCreateGroupTips", nil); // @"创建群聊";
                } else if([info.groupType isEqualToString:GroupType_Meeting]) {
                    content = NSLocalizedString(@"ChatsCreateChatRoomTips", nil); // @"创建聊天室";
                } else {
                    content = NSLocalizedString(@"ChatsCreateDefaultTips", nil); // @"创建群组";
                }
//                NSDictionary *dic = @{@"version": @(GroupCreate_Version),@"businessID": GroupCreate,@"opUser":showName,@"content":content};
//                NSData *data1= [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
//                V2TIMMessage *msg = [[V2TIMManager sharedInstance] createCustomMessage:data1];
//                [[V2TIMManager sharedInstance] sendMessage:msg receiver:nil groupID:data[@"txquncode"] priority:V2TIM_PRIORITY_DEFAULT onlineUserOnly:NO offlinePushInfo:nil progress:nil succ:nil fail:nil];

                
                 //创建成功后，默认跳转到群组对应的聊天界面
                 TUIConversationCellData *cellData = [[TUIConversationCellData alloc] init];
                 cellData.groupID = data[@"txquncode"];
                 cellData.title = data[@"qunming"];
                 liaotianshiViewController *chat = [[liaotianshiViewController alloc] init];
                 chat.conversationData = cellData;
                 chat.hidesBottomBarWhenPushed = YES;
                 [weakSelf.navigationController pushViewController:chat animated:YES];
                NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
                [tempArray removeObjectAtIndex:tempArray.count-2];
                weakSelf.navigationController.viewControllers = tempArray;

            }
            else
            {
                [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
            }
        } error:^(NSString *error) {
            [SVProgressHUD  dismiss];
        }];
        
    } fail:^(int code, NSString *msg) {
        // to do
    }];
}
/**
 *在消息列表内，点击了某一具体会话后的响应函数
 */
#pragma  mark -- 跳转会话聊天室
- (void)conversationListController:(TUIConversationListController *)conversationController didSelectConversation:(TUIConversationCell *)conversation
{
    if ([conversation.convData.userID isEqualToString:@"xxzs10001"]) {
        CSSystemMessageVC *vc  = [[ CSSystemMessageVC alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        
//        conversation.convData.unreadCount=0;
//        [conversation fillWithData:conversation.convData];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        liaotianshiViewController *chat = [[liaotianshiViewController alloc] init];
        chat.conversationData = conversation.convData;
        chat.hidesBottomBarWhenPushed = YES;
        if (conversation.convData.unreadCount>0) {
           
        }
        [self.navigationController pushViewController:chat animated:YES];
        
        if ([conversation.convData.groupID isEqualToString:@"im_demo_admin"] || [conversation.convData.userID isEqualToString:@"im_demo_admin"]) {
            [TCUtil report:Action_Clickhelper actionSub:@"" code:@(0) msg:@"clickhelper"];
        }
        if ([conversation.convData.groupID isEqualToString:@"@TGS#33NKXK5FK"] || [conversation.convData.userID isEqualToString:@"@TGS#33NKXK5FK"]) {
            [TCUtil report:Action_Clickdefaultgrp actionSub:@"" code:@(0) msg:@"clickdefaultgrp"];
        }
    }
    
}

- (void)onReceiveRESTCustomData:(NSString *)groupID data:(NSData *)data
{
    NSLog(@"1111");
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"-------成功释放");
}
@end
