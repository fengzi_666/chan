//
//  CSSystemMessageCell.m
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "CSSystemMessageCell.h"

@implementation CSSystemMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.myView.layer.masksToBounds = YES;
    self.myView.layer.cornerRadius = 10;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
