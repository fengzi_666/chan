//
//  CSSystemMessageDetailsVC.m
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "CSSystemMessageDetailsVC.h"

@interface CSSystemMessageDetailsVC ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation CSSystemMessageDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNav];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = _model.biaoti;
    NSString *chongzhishuoming = _model.message;
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[chongzhishuoming dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.textView.attributedText = attrStr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
