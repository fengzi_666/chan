//
//  CSSystenMessageListModel.h
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSSystenMessageListModel : NSObject
@property (nonatomic ,copy) NSString *biaoti;
@property (nonatomic ,copy) NSString *createdate;
@property (nonatomic ,copy) NSString *listid;
@property (nonatomic ,copy) NSString *message;
@property (nonatomic ,copy) NSString *jianjie;
- (instancetype)initWithDic:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
