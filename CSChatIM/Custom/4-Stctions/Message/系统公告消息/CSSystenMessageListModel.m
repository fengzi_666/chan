//
//  CSSystenMessageListModel.m
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "CSSystenMessageListModel.h"

@implementation CSSystenMessageListModel
- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
//        这里是取出字典里买你的value并且赋值
        self.biaoti = dic[@"biaoti"];
        self.createdate = dic[@"createdate"];
        self.listid = [NSString stringWithFormat:@"%@",dic[@"id"]];
        self.message = dic[@"message"];
        self.jianjie = dic[@"jianjie"];
    }
    return self;
}
@end
