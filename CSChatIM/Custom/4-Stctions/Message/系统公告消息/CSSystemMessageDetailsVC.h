//
//  CSSystemMessageDetailsVC.h
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "BaseViewController.h"
#import "CSSystenMessageListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSSystemMessageDetailsVC : BaseViewController
@property (nonatomic ,strong) CSSystenMessageListModel *model;
@end

NS_ASSUME_NONNULL_END
