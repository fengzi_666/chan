//
//  CSSystemMessageVC.m
//  CSChatIM
//
//  Created by Apple on 2021/1/12.
//

#import "CSSystemMessageVC.h"
#import "CSSystemMessageCell.h"
#import "CSSystenMessageListModel.h"
#import "CSSystemMessageDetailsVC.h"
#import "BaseshenghuoViewController.h"
@interface CSSystemMessageVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@property (nonatomic,assign) NSInteger pageno;

@end
static NSString *cellid = @"cellid";
@implementation CSSystemMessageVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"系统通知";
    
    [self setNav];
    
    [self LoadTableView];
    
    [self LoadFoodView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRevokeMessage:) name:TUIKitNotification_TIMMessageRevokeListener object:nil];
   
}
- (void)onRevokeMessage:(NSNotification *)notification
{
    NSString *msgID = notification.object;
    TUIMessageCellData *uiMsg = nil;
//    for (uiMsg in _uiMsgs) {
//        if ([uiMsg.msgID isEqualToString:msgID]) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self revokeMsg:uiMsg];
//            });
//            break;
//        }
//    }
}
//已读回执
- (void) didRecvMessageReceipts:(NSNotification *)noti{
    NSArray *receiptsArray = noti.object;
    if(!receiptsArray.count){
        NSLog(@"Receipt Data Error");
        return;
    }
    V2TIMMessageReceipt *receipt = receiptsArray[0];
    time_t receiptTime = receipt.timestamp;
//    if(receipt.userID.length > 0 && [receipt.userID isEqualToString:self.conversationData.userID]){
//        //性能优化
//        for(int i = 0;i < _uiMsgs.count;i++){
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_uiMsgs.count - 1 - i inSection:0];
//            TUIMessageCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            //通过回调时间戳判定当前的未读是否需要改为已读
//            time_t msgTime = [cell.messageData.innerMessage.timestamp timeIntervalSince1970];
//            if(msgTime <= receiptTime && ![cell.readReceiptLabel.text isEqualToString:TUILocalizableString(Read)]) {
//                cell.readReceiptLabel.text = TUILocalizableString(Read);
//            }
//        }
//    }
}
- (void)LoadFoodView
{
    UIButton *foodBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [foodBtn setTitle:@"联系客服" forState:UIControlStateNormal];
    [foodBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    foodBtn.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
    foodBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [foodBtn addTarget:self action:@selector(foodBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:foodBtn];
    [foodBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-15);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(44);
    }];
    
}
- (void)foodBtnClick
{
    BaseshenghuoViewController *vc = [[BaseshenghuoViewController alloc]init];
    vc.webUrl = @"http://www.4399.com";
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)LoadRequstData
{
    [SVProgressHUD showWithStatus:@"查询中"];
     NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appxitongxiaoxi];
     NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
      [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
         NSLog(@"%@",dict);
          [SVProgressHUD dismiss];
          NSDictionary *data  =dict[@"data"];
         NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
         if ([code isEqualToString:@"0"]) {
             NSArray *list =data[@"list"];
             NSMutableArray *dataArr = [[NSMutableArray alloc]init];
            for (int i= 0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSSystenMessageListModel *model = [[CSSystenMessageListModel alloc]initWithDic:dic];
                [dataArr addObject:model];
            }
            [self.myTableView.mj_header endRefreshing];
            if (_pageno==1) {
                [_dataArr removeAllObjects];
                [_dataArr addObjectsFromArray:dataArr];
                _myTableView.emptyView.hidden = YES;
                [self.myTableView.mj_footer resetNoMoreData];
            }
            else
            {
                if (dataArr.count==0) {
                    [_myTableView.mj_footer endRefreshingWithNoMoreData];
                }
                else{
                    [_dataArr addObjectsFromArray:dataArr];
                 [self.myTableView.mj_footer endRefreshing];
                }
            }
        }
        else
        {
            [self.myTableView.mj_header endRefreshing];
            if ([dict[@"msg"] isEqualToString:@"没有更多数据了！"]) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
                [self.myTableView.mj_footer endRefreshing];
            }
        }
         [_myTableView reloadData];
        
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
}
- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-StatusBarHeight-44) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    _myTableView.estimatedRowHeight = 200;
    [_myTableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
       if (@available(iOS 11.0, *)) {
           _myTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
      } else {
           self.automaticallyAdjustsScrollViewInsets = NO;
       }
    [_myTableView registerNib:[UINib nibWithNibName:@"CSSystemMessageCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
   
    //这种上拉刷新footer在tableview的底部
  
    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        self.dataArr =[NSMutableArray array];
        [self LoadRequstData];
    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequstData];
    }];
    
    [_myTableView.mj_header beginRefreshing];
    [self.view addSubview:_myTableView];
    
  
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 176;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSSystemMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    CSSystenMessageListModel *model = _dataArr [indexPath.row];
    cell.titleLable.text = model.biaoti;
   
        
    cell.timeLable.text = model.createdate;
    cell.contentLable.text = model.jianjie;
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSSystemMessageDetailsVC *vc = [[CSSystemMessageDetailsVC alloc]init];
    CSSystenMessageListModel *model = _dataArr [indexPath.row];
    vc.model = model;
    [self.navigationController pushViewController: vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
