//
//  CSPopView.m
//  CSChatIM
//
//  Created by Apple on 2020/11/25.
//

#import "CSPopView.h"
#import "TPopCell.h"
#import "THeader.h"
#import "UIColor+TUIDarkMode.h"
#import "RACmetamacros.h"
@interface TPopView ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *data;
@end


@implementation CSPopView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    return self;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPopCell *cell = [tableView dequeueReusableCellWithIdentifier:TPopCell_ReuseId];
    if(!cell){
        cell = [[TPopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TPopCell_ReuseId];
    }
    cell.title.textColor = [UIColor whiteColor];
    [cell setData:self.data[indexPath.row]];
    if(indexPath.row == self.data.count - 1){
        cell.separatorInset = UIEdgeInsetsMake(0, self.bounds.size.width, 0, 0);
    }
    return cell;
}




@end
