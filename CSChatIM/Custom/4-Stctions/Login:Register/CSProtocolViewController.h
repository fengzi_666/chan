//
//  CSProtocolViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/15.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSProtocolViewController : BaseViewController
@property (nonatomic ,copy) NSString *text;
@end

NS_ASSUME_NONNULL_END
