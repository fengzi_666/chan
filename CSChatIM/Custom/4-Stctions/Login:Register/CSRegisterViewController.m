//
//  CSRegisterViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "CSRegisterViewController.h"
#import "CSProtocolViewController.h"
@interface CSRegisterViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UITextField *moblieTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UITextField *psdTF;
@property (weak, nonatomic) IBOutlet UIButton *regsiterBtn;

@property (weak, nonatomic) IBOutlet UIView *mobileView;
@property (weak, nonatomic) IBOutlet UIView *codeView;

@property (weak, nonatomic) IBOutlet UIView *psdView;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (nonatomic ,strong) NSString *yinsixieyi;
@property (nonatomic ,strong) NSString *yonghuxieyi;

@property (nonatomic ,strong) NSString *selectType;


@end

@implementation CSRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _selectType = @"1";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    self.navigationItem.title = @"注册";
    
    [self LoadFoodView];
    
    [self LoadRequsetAPP];
    
     //此处使底部线条失效
    self.mobileView.layer.masksToBounds = YES;
    self.mobileView.layer.cornerRadius = 5;
    self.codeView.layer.masksToBounds = YES;
    self.codeView.layer.cornerRadius = 5;
    self.psdView.layer.masksToBounds = YES;
    self.psdView.layer.cornerRadius = 5;
    self.regsiterBtn.layer.masksToBounds = YES;
    self.regsiterBtn.layer.cornerRadius = 5;
    self.codeBtn.layer.masksToBounds = YES;
    self.codeBtn.layer.cornerRadius = 5;
    
    self.moblieTF.delegate = self;
    self.psdTF.delegate = self;
    self.codeTF.delegate = self;
    _moblieTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _psdTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_moblieTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [_psdTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [_codeTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    
}

- (void)leftClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)loginBtn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)codeClick:(UIButton *)sender {
    NSString *mobileRegex = TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    
    if ([_moblieTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号码"];
        return;
    }
    if (!_moblieTF.text || ![mobilePredicate evaluateWithObject:_moblieTF.text]){
        [SVProgressHUD showInfoWithStatus:@"请输入正确的手机号"];
        return;
    }
    [SVProgressHUD showWithStatus:@"发送中..."];
    
    [sender startCountDownTime:60 withCountDownBlock:^{
        NSLog(@"开始倒计时");
        //此处发送验证码等操作
        //................
        [self LoadRequestCode];

    }];
}
- (void)LoadRequestCode
{
    NSDictionary *dict = @{@"loginname":_moblieTF.text};
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sendRegisterSMS];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
        
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}
- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];

    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
- (IBAction)registerBtnClick:(UIButton *)sender {
    NSString *mobileRegex = TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    
    if ([_moblieTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入手机号码"];
        return;
    }
    if (!_moblieTF.text || ![mobilePredicate evaluateWithObject:_moblieTF.text]){
        [SVProgressHUD showInfoWithStatus:@"请输入正确的手机号"];
        return;
    }
    if ([_codeTF.text isEqualToString:@""])
    {
        [SVProgressHUD showInfoWithStatus:@"验证码为空"];
        return;
    }
    if (_codeTF.text.length < 4)
    {
        [SVProgressHUD showInfoWithStatus:@"请输入正确的验证码"];
        return;
    }
    if ([_psdTF.text isEqualToString:@""]) {
        [SVProgressHUD showInfoWithStatus:@"密码不能为空"];
        return;
    }
    if (_psdTF.text.length < TXWordsNum_PayPassword) {
        [SVProgressHUD showInfoWithStatus:@"密码不可以少于6位"];
        return;
    }
    if (_psdTF.text.length >TXWordsNum_Password) {
        [SVProgressHUD showInfoWithStatus:@"密码不可以多于12位"];
        return;
    }
    if ([_selectType isEqualToString:@"2"]) {
        
        [SVProgressHUD showInfoWithStatus:@"请选同意《用户协议》和《隐私协议》"];
        return;
    }
   //注册接口
    [self LoadRequestRegister];
    
}
- (void)LoadRequestRegister
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_moblieTF.text forKey:@"loginname"];
    [dict setValue:_psdTF.text forKey:@"password"];
    [dict setValue:_codeTF.text forKey:@"code"];
    [SVProgressHUD showWithStatus:@"注册中..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_register];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"注册成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
       
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - textfield action
- (void)textFieldChange:(UITextField*)textField{
    NSString *mobileRegex =TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    if (textField == _moblieTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Phone textField:_moblieTF];
        
    }
    if (textField == _codeTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Code textField:_codeTF];
        
    }
    if (textField == _psdTF)
    {
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Password textField:_psdTF];
        if([mobilePredicate evaluateWithObject:_moblieTF.text] ) {
    
        }
    }
  
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _moblieTF) {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Phone textField:textField replacementString:string];
    }
    else if (textField == _psdTF)
    {
        NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL isRange = [jiaohuanqiValidate limitMaxLength:TXWordsNum_Password textField:textField replacementString:string];
        if (isRange) {
            textField.text = updatedString;
            [self textFieldChange:_psdTF];
            return NO;
        }
        [self textFieldChange:_psdTF];
        return isRange;
    }
 
    else if (textField == _codeTF)
    {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Code textField:textField replacementString:string];
    }
    return YES;
}
- (void)LoadRequsetAPP
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apppeizhi];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *peizhi = data[@"peizhi"];
            _yinsixieyi = peizhi[@"yinsixieyi"];
            _yonghuxieyi = peizhi[@"yonghuxieyi"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"<#msg#>"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)LoadFoodView
{
    UIView *foodView = [[UIView alloc]init];
    foodView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:foodView];
    [foodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    UITextView *textView = [[UITextView alloc]init];
    textView.textAlignment = NSTextAlignmentCenter;
    textView.backgroundColor = [UIColor whiteColor];
    textView.text = @"阅读并同意《用户协议》和《隐私协议》";

    textView.font = [UIFont systemFontOfSize:13];

    [foodView addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(foodView.mas_bottom).offset(-15);
        make.centerX.mas_equalTo(foodView.mas_centerX);
        
    }];
    UIButton * selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectBtn setImage:[UIImage imageNamed:@"登录_选中"] forState:UIControlStateSelected];
    [selectBtn setImage:[UIImage imageNamed:@"登录_未选中"] forState:UIControlStateNormal];
    [selectBtn addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectBtn setSelected:YES];
    [foodView addSubview:selectBtn];
    [selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(foodView.mas_bottom).offset(-20);
        make.right.mas_equalTo(textView.mas_left).offset(-11);
    }];

    // 字体的行间距

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

    paragraphStyle.lineSpacing= 1;

    NSDictionary*attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13],
                            NSParagraphStyleAttributeName:paragraphStyle
                                
    };

    //设置富文本点击

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textView.text attributes:attributes];

    [attributedString addAttribute:NSLinkAttributeName value:@"yonghuxieyi://"
                             range:[[attributedString string]rangeOfString:@"《用户协议》"]];
    [attributedString addAttribute:NSLinkAttributeName
                              value:@"yinsixieyi://"
                             range:[[attributedString string]rangeOfString:@"《隐私协议》"]];

    //设置字体

    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(6,6)];

    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(12,6)];

    //设置整体颜色

    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#333333"] range:NSMakeRange(0,textView.text.length)];

    textView.attributedText= attributedString;
     //设置被点击字体颜色

    textView.linkTextAttributes = @{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#32965A"]};

    textView.delegate=self;

    //必须禁止输入，否则点击将弹出输入键盘

    textView.editable=NO;

    textView.scrollEnabled=NO;

    //代理方法的实现



}
#pragma mark 富文本点击事件

- (BOOL)textView:(UITextView*)textView shouldInteractWithURL:(NSURL*)URL inRange:(NSRange)characterRange {

    if ([[URL scheme] isEqualToString:@"yonghuxieyi"]) {

        NSLog(@"富文本点击 服务条款");
        CSProtocolViewController *vc =[[CSProtocolViewController alloc]init];
        vc.text = _yonghuxieyi;
        vc.navigationItem.title = @"用户协议";
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if([[URL scheme] isEqualToString:@"yinsixieyi"]) {

    NSLog(@"富文本点击 隐私政策");
        CSProtocolViewController *vc =[[CSProtocolViewController alloc]init];
        vc.text = _yinsixieyi;
        vc.navigationItem.title = @"隐私协议";
        [self.navigationController pushViewController:vc animated:YES];
        
    
}
    return YES;

}
- (void)selectBtnClick:(UIButton *)sender
{
    if (sender.selected == YES) {
        NSLog(@"111111");
        [sender setImage:[UIImage imageNamed:@"登录_未选中"] forState:UIControlStateNormal];
        sender.selected = NO;
        _selectType = @"2";
    }
    else{
        NSLog(@"22222");
        [sender setImage:[UIImage imageNamed:@"登录_选中"] forState:UIControlStateNormal];
        sender.selected = YES;
        _selectType = @"1";
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
