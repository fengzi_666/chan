//
//  CSLoginViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "CSLoginViewController.h"
#import "CSRegisterViewController.h"
#import "CSRetrievePsdViewController.h"
#import "helloTabBarController.h"
#import "shawanyiyaUserSig.h"
#import "SFHFKeychainUtils.h"
#import "CSProtocolViewController.h"
#import "CSRedCodeView.h"
@interface CSLoginViewController ()<UITextViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UITextField *mobileTF;
@property (weak, nonatomic) IBOutlet UITextField *psdTF;
@property (weak, nonatomic) IBOutlet UIButton *forgetPsdBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIView *mobileView;
@property (weak, nonatomic) IBOutlet UIView *psdView;
@property (nonatomic ,strong) helloTabBarController *mainTBC;
@property (nonatomic ,strong) NSString *yinsixieyi;
@property (nonatomic ,strong) NSString *yonghuxieyi;
@property (nonatomic ,strong)  CSRedCodeView *foodView;
@property (nonatomic ,strong) NSString *selectType;
@end

@implementation CSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self setNav];
    _selectType = @"1";
    self.mobileTF.delegate = self;
    self.psdTF.delegate = self;
    self.navigationItem.title = @"登录";
    
    self.mobileView.layer.masksToBounds = YES;
    self.mobileView.layer.cornerRadius = 5;
    
    self.psdView.layer.masksToBounds = YES;
    self.psdView.layer.cornerRadius = 5;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.layer.cornerRadius = 5;
   
    _mobileTF.clearButtonMode = UITextFieldViewModeWhileEditing;

    _psdTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_mobileTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [_psdTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];

    
    
    [self LoadFoodView];
    
    [self LoadRequsetAPP];
    
}
- (void)LoadRequsetAPP
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apppeizhi];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *peizhi = data[@"peizhi"];
            _yinsixieyi = peizhi[@"yinsixieyi"];
            _yonghuxieyi = peizhi[@"yonghuxieyi"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"<#msg#>"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (IBAction)regiserBtnClick:(UIButton *)sender {
    CSRegisterViewController  *vc = [[CSRegisterViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
  
}
//忘记密码
- (IBAction)forgetPsdClick:(UIButton *)sender {
    CSRetrievePsdViewController *vc = [[CSRetrievePsdViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)loginBtnClick:(UIButton *)sender {
    if ([_selectType isEqualToString:@"2"]) {
        
        [SVProgressHUD showInfoWithStatus:@"请先同意《用户协议》和《隐私协议》"];
        return;
    }
   
    [SVProgressHUD showWithStatus:@"登录中..."];
    
    [self LoadRequestLogin];


}
- (NSString*)UUID {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *userName = infoDict[@"CFBundleName"];
    NSString *serviceName = infoDict[@"CFBundleIdentifier"];
    NSString *UUID = [SFHFKeychainUtils getPasswordForUsername:userName andServiceName:serviceName error:nil];
    if (UUID.length==0) {
        UUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SFHFKeychainUtils storeUsername:userName andPassword:UUID forServiceName:serviceName updateExisting:1 error:nil];
        
    }
    
    return UUID;

}

- (void)LoadRequestLogin
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_login];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_mobileTF.text forKey:@"loginname"];
    [dict setValue:_psdTF.text forKey:@"password"];
    NSString *uuid= [self UUID];
    [dict setValue:uuid forKey:@"phonecode"];
    __weak typeof(self) weakSelf = self;
    [longwangRequestManage postBaseWithURL1:url param:dict requestHead:[self UUID] success:^(NSDictionary *dict) {
       
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *login = data[@"tengxuncode"];
            NSString *userSig = data[@"userSig"];
            NSString *token = data[@"token"];
            [[UserDefaultManager shareInstance]SavaLoginToken:token];
            [[UserDefaultManager shareInstance]SavaLoginLoginAccount:login];
            [[UserDefaultManager shareInstance]SavaLoginLoginMobile:_mobileTF.text];
            [self LoadTXRequestLogin:login userSig:userSig];
            [SVProgressHUD showSuccessWithStatus:@"登录成功"];
        }
        if ([code isEqualToString:@"1009"]) {
            //
           
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
            
            [self.view endEditing:YES];
            CSRedCodeView *foodView = [[CSRedCodeView alloc]init];
            weakSelf.foodView = foodView;
            
            foodView.backgroundColor = [UIColor whiteColor];
            [foodView.codeBtn addTarget:self action:@selector(codeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [foodView.quedingBtn addTarget:self action:@selector(quedingClick:) forControlEvents:UIControlEventTouchUpInside];
            [foodView.deleteBtn addTarget:self action:@selector(deletaBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            foodView.mobileTF.text = _mobileTF.text;
            foodView.mobileTF.enabled=NO;
            [weakSelf.view addSubview:foodView];
            self.foodView  = foodView;
            
            [foodView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(weakSelf.view.mas_bottom);
                make.height.mas_equalTo(258);
                make.right.mas_equalTo(weakSelf.view.mas_right);
                make.left.mas_equalTo(weakSelf.view.mas_left);
            }];
            
            NSNotificationCenter * center=[NSNotificationCenter defaultCenter];
            [center addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
            //            [[[UIApplication  sharedApplication ]keyWindow ] addSubview : foodView ] ;
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
        NSLog(@"%@",dict);
        
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)LoadTXRequestLogin:(NSString *)login userSig:(NSString *)userSig
{
        [[TUIKit sharedInstance] login:login userSig:userSig succ:^{
            NSLog(@"-----> 登录成功");
            [[TUILocalStorage sharedInstance] saveLogin:login  withAppId:SDKAPPID withUserSig:userSig];
            
            _mainTBC = [[helloTabBarController alloc]init];
            [[AppDelegate shareAppDelegate].window makeKeyAndVisible];
    
            [AppDelegate shareAppDelegate].window.rootViewController = [[helloTabBarController alloc]init];
            [AppDelegate shareAppDelegate].window.backgroundColor = [UIColor whiteColor];
    
        } fail:^(int code, NSString *msg) {
    //        NSLog(@"登录失败-----%@",msg);
    
        }];
}
- (void)deletaBtnClick:(UIButton *)sender
{
    [self.foodView removeFromSuperview];
//    [self.redPsdPopView removeFromSuperview];
}
- (void)codeBtnClick :(UIButton *)sender
{
    [sender startCountDownTime:60 withCountDownBlock:^{
        NSLog(@"开始倒计时");
        //此处发送验证码等操作
        //................
        [self LoadRequestCode];
        
    }];
}
- (void)LoadRequestCode
{
//    [SVProgressHUD showWithStatus:@"发送中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sendLoginSMS];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_foodView.mobileTF.text forKey:@"loginname"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"发送成功"];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
- (void)quedingClick :(UIButton *)sender
{
    if ([_foodView.codeTF.text isEqualToString:@""]) {
        
        [SVProgressHUD showInfoWithStatus:@"请输入验证码"];
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_loginYzm];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_mobileTF.text forKey:@"loginname"];
    [dict setValue:_psdTF.text forKey:@"password"];
    NSString *uuid= [self UUID];
    [dict setValue:uuid forKey:@"phonecode"];
    [dict setValue:_foodView.codeTF.text forKey:@"code"];
    __weak typeof(self) weakSelf = self;
    [longwangRequestManage postBaseWithURL1:url param:dict requestHead:[self UUID] success:^(NSDictionary *dict) {
       
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSString *login = data[@"tengxuncode"];
            NSString *userSig = data[@"userSig"];
            NSString *token = data[@"token"];
            [[UserDefaultManager shareInstance]SavaLoginToken:token];
            [[UserDefaultManager shareInstance]SavaLoginLoginAccount:login];
            [[UserDefaultManager shareInstance]SavaLoginLoginMobile:_mobileTF.text];
            [self LoadTXRequestLogin:login userSig:userSig];
            [SVProgressHUD showSuccessWithStatus:@"登录成功"];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
        NSLog(@"%@",dict);
        
    } error:^(NSString *error) {
        [SVProgressHUD dismiss];
    }];
 
}
-(void)keyboardWillChange:(NSNotification *)note
{
        //获取键盘弹起时的frame值
        CGRect rect=[note.userInfo[UIKeyboardFrameEndUserInfoKey]CGRectValue];
        //获取键盘弹起时的Y值
        CGFloat keyboardY=rect.origin.y;
        //计算view要上移动多少就是键盘的y值减去屏幕的高度就能得到需要向上移动多少，最后y值变回和屏幕高度一样就又回来了，注意这里不能说我之前比如说变了-100，
         //要想变回来变成正100那是错了，要回到原点才是对的。
        CGFloat transformY=keyboardY-KScreenHeight;
        self.foodView.transform=CGAffineTransformMakeTranslation(0, transformY);

}
#pragma mark - textfield action
- (void)textFieldChange:(UITextField*)textField{
    NSString *mobileRegex =TXLength_Mobile;
    NSPredicate *mobilePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    if (textField == _mobileTF) {
        //        _loginButton.userInteractionEnabled = NO;
        
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Phone textField:_mobileTF];
        
    }
    if (textField == _psdTF)
    {
        [jiaohuanqiValidate restoreValidTextLength:TXWordsNum_Password textField:_psdTF];
        if([mobilePredicate evaluateWithObject:_mobileTF.text] ) {
            
        }
    }
  
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _mobileTF) {
        return [jiaohuanqiValidate limitMaxLength:TXWordsNum_Phone textField:textField replacementString:string];
    }
    else if (textField == _psdTF)
    {
        NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL isRange = [jiaohuanqiValidate limitMaxLength:TXWordsNum_Password textField:textField replacementString:string];
        if (isRange) {
            textField.text = updatedString;
            [self textFieldChange:_psdTF];
            return NO;
        }
        [self textFieldChange:_psdTF];
        return isRange;
    }
    
    return YES;
}
- (void)LoadFoodView
{
    UIView *foodView = [[UIView alloc]init];
    foodView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:foodView];
    [foodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    UITextView *textView = [[UITextView alloc]init];
    textView.textAlignment = NSTextAlignmentCenter;
    textView.backgroundColor = [UIColor whiteColor];
    textView.text = @"阅读并同意《用户协议》和《隐私协议》";

    textView.font = [UIFont systemFontOfSize:13];

    [foodView addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(foodView.mas_bottom).offset(-15);
        make.centerX.mas_equalTo(foodView.mas_centerX);
        
    }];
    UIButton * selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [selectBtn setImage:[UIImage imageNamed:@"登录_选中"] forState:UIControlStateSelected];
    [selectBtn setImage:[UIImage imageNamed:@"登录_未选中"] forState:UIControlStateNormal];
    [selectBtn addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectBtn setSelected:YES];
    [foodView addSubview:selectBtn];
    [selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(foodView.mas_bottom).offset(-20);
        make.right.mas_equalTo(textView.mas_left).offset(-11);
    }];

    // 字体的行间距

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

    paragraphStyle.lineSpacing= 1;

    NSDictionary*attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:13],
                            NSParagraphStyleAttributeName:paragraphStyle
                                
    };

    //设置富文本点击

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textView.text attributes:attributes];

    [attributedString addAttribute:NSLinkAttributeName value:@"yonghuxieyi://"
                             range:[[attributedString string]rangeOfString:@"《用户协议》"]];
    [attributedString addAttribute:NSLinkAttributeName
                              value:@"yinsixieyi://"
                             range:[[attributedString string]rangeOfString:@"《隐私协议》"]];

    //设置字体

    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(6,6)];

    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(12,6)];

    //设置整体颜色

    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#333333"] range:NSMakeRange(0,textView.text.length)];

    textView.attributedText= attributedString;
     //设置被点击字体颜色

    textView.linkTextAttributes = @{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#32965A"]};

    textView.delegate=self;

    //必须禁止输入，否则点击将弹出输入键盘

    textView.editable=NO;

    textView.scrollEnabled=NO;

    //代理方法的实现



}
#pragma mark 富文本点击事件

- (BOOL)textView:(UITextView*)textView shouldInteractWithURL:(NSURL*)URL inRange:(NSRange)characterRange {

    if ([[URL scheme] isEqualToString:@"yonghuxieyi"]) {

        NSLog(@"富文本点击 服务条款");
        CSProtocolViewController *vc =[[CSProtocolViewController alloc]init];
        vc.text = _yonghuxieyi;
        vc.navigationItem.title = @"用户协议";
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if([[URL scheme] isEqualToString:@"yinsixieyi"]) {

    NSLog(@"富文本点击 隐私政策");
        CSProtocolViewController *vc =[[CSProtocolViewController alloc]init];
        vc.text = _yinsixieyi;
        vc.navigationItem.title = @"隐私协议";
        [self.navigationController pushViewController:vc animated:YES];
        
    
}
    return YES;

}
- (void)selectBtnClick:(UIButton *)sender
{
    if (sender.selected == YES) {
        NSLog(@"111111");
        [sender setImage:[UIImage imageNamed:@"登录_未选中"] forState:UIControlStateNormal];
        sender.selected = NO;
        _selectType = @"2";
    }
    else{
        NSLog(@"22222");
        [sender setImage:[UIImage imageNamed:@"登录_选中"] forState:UIControlStateNormal];
        sender.selected = YES;
        _selectType = @"1";
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
