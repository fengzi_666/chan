//
//  CSProtocolViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/15.
//

#import "CSProtocolViewController.h"

@interface CSProtocolViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation CSProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNav];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[_text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

    _textView.attributedText = attrStr;
   
    
    self.view.backgroundColor = [UIColor whiteColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
