//
//  CSUIGroupConversationListController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "BaseViewController.h"
#import "TUIConversationCell.h"
#import "TUIGroupConversationListViewModel.h"
NS_ASSUME_NONNULL_BEGIN
/**
 * 【模块名称】群组列表界面（TUIGroupConversationListController）
 * 【功能说明】负责拉取用户的所在的群组信息，并在界面中显示。
 *  用户可以通过群列表界面查看自己所在的所有群，群组展示顺序按首字母开头从 A 到 Z 展示，特殊符号的群名在最后显示。
 */
@interface CSUIGroupConversationListController : BaseViewController

/**
 *  群列表的 TableView
 *  本界面通过 tableView 展示用户所在的所有群组。同时 tableView 能够实现对用户交互操作的响应。
 */
@property (nonatomic, strong) UITableView *tableView;

/**
 *  群列表界面的视图模型。
 *  视图模型负责通过 IM SDK 提供的接口拉取群列表数据并进行加载，方便页面对群列表进行展示。
 */
@property (nonatomic, strong) TUIGroupConversationListViewModel *viewModel;
@end

NS_ASSUME_NONNULL_END
