//
//  CSLinkmanViewController.h
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "BaseViewController.h"

#import "TUIContactController.h"
#import "THeader.h"
#import "TUIKit.h"
#import "NSString+TUICommon.h"
#import "TUIFriendProfileControllerServiceProtocol.h"
#import "TCServiceManager.h"
#import "ReactiveObjC.h"
#import "MMLayout/UIView+MMLayout.h"
#import "TUIBlackListController.h"
#import "TUINewFriendViewController.h"
#import "TUIConversationListController.h"
#import "TUIChatController.h"
#import "TUIGroupConversationListController.h"
#import "TUIContactActionCell.h"
#import "UIColor+TUIDarkMode.h"
#import "NSBundle+TUIKIT.h"
NS_ASSUME_NONNULL_BEGIN
@import ImSDK;

#define kContactCellReuseId @"ContactCellReuseId"
#define kContactActionCellReuseId @"ContactActionCellReuseId"
@interface CSLinkmanViewController :BaseViewController
/**
 *  消息列表界面的视图模型
 *  视图模型负责通过 IM SDK 的接口，拉取好友列表、好友请求等信息并将其加载，以便客户端的进一步处理。
 *  详细信息请参考 Section\Contact\ViewModel\TContactViewModel.h
 */
@property (nonatomic) TContactViewModel *viewModel;

@end

NS_ASSUME_NONNULL_END
