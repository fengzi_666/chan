//
//  CSUserDataHeadView.h
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSUserDataHeadView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *IDLable;
@property (weak, nonatomic) IBOutlet UILabel *signatureLable;

@end

NS_ASSUME_NONNULL_END
