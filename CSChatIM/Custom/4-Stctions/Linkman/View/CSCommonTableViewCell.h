//
//  CSCommonTableViewCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import "TCommonCell.h"
#import "TCommonPendencyCellData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSCommonTableViewCell : TCommonTableViewCell
@property UIImageView *avatarView;
@property UILabel *titleLabel;
@property UILabel *addSourceLabel;
@property UILabel *addWordingLabel;
@property UIButton *agreeButton;
@property (nonatomic ,strong) UILabel *line;
@property (nonatomic ,strong) NSIndexPath *index;
@property (nonatomic) TCommonPendencyCellData *pendencyData;

- (void)fillWithData:(TCommonPendencyCellData *)pendencyData;
@end

NS_ASSUME_NONNULL_END
