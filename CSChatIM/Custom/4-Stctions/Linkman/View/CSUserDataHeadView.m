//
//  CSUserDataHeadView.m
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "CSUserDataHeadView.h"

@implementation CSUserDataHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle]loadNibNamed:@"CSUserDataHeadView" owner:nil options:nil][0];
        self.frame =frame;
        self.headImage.layer.masksToBounds = YES;
        self.headImage.layer.cornerRadius = 30.5;
        self.backgroundColor = [UIColor whiteColor];
    
       
    }
    return self;
}

@end
