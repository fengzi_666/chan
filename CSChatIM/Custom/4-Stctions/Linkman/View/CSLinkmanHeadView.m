//
//  CSLinkmanHeadView.m
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "CSLinkmanHeadView.h"

@implementation CSLinkmanHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.lable = [[UILabel alloc]init];
       
        self.lable.font = [UIFont boldSystemFontOfSize:15];
        self.lable.textColor = [UIColor colorWithHexString:@"#333333"];
        [self addSubview:self.lable];
        [self.lable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(13);
            make.centerY.mas_equalTo(self);
        }];
        
        self.imageView = [[UIImageView alloc]init];
        [self addSubview:self.imageView];
//        self.imageView.image = [UIImage imageNamed:@"下拉箭头"];
//        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.mas_equalTo(self.mas_right).offset(-12);
//            make.centerY.mas_equalTo(self.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(12, 7));
//        }];
    }
    return self;
}
@end
