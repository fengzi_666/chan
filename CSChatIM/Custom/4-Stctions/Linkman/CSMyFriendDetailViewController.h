//
//  CSMyFriendDetailViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/16.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMyFriendDetailViewController : BaseViewController
@property (nonatomic ,copy) NSString *userid;
@end

NS_ASSUME_NONNULL_END
