//
//  CSLinkmanViewController.m
//  CSChatIM
//
//  Created by 梁亚飞 on 2020/11/16.
//

#import "CSLinkmanViewController.h"
#import "CSAddFriendViewController.h"
#import "TPopCell.h"
#import "TPopView.h"
#import "CSLinkmanHeadView.h"
#import "CSNewFriendViewController.h"
#import "CSUIGroupConversationListController.h"
#import "CSUserDataViewController.h"
#import "TUIGroupPendencyController.h"
#import "CSGroupNotificationVC.h"
#import "TUIGroupPendencyViewModel.h"
#import "TUIGroupPendencyCellData.h"
#import "CSMyFriendSearchViewController.h"
#import "TConversationListViewModel.h"
@interface CSLinkmanViewController ()<TPopViewDelegate,UITableViewDelegate,UITableViewDataSource,TUIConversationListControllerDelegate,UISearchBarDelegate>
@property NSArray<TUIContactActionCellData *> *firstGroupData;
@property (nonatomic ,strong) TUIGroupPendencyViewModel *groupModel;
@property (nonatomic ,copy) NSArray *groupList;
@property (nonatomic, strong) TConversationListViewModel *megModel;
@property (nonatomic ,assign) NSInteger qun;
@property (nonatomic ,assign) NSInteger geren;
@property (nonatomic ,copy) NSString *friendStr;
@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CSLinkmanViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //获取群组通知
    [[V2TIMManager sharedInstance] getGroupApplicationList:^(V2TIMGroupApplicationResult *result) {
       
        NSMutableArray *list = @[].mutableCopy;
        for (V2TIMGroupApplication *item in result.applicationList) {
            if (item.handleStatus == V2TIM_GROUP_APPLICATION_HANDLE_STATUS_UNHANDLED) {
                TUIGroupPendencyCellData *data = [[TUIGroupPendencyCellData alloc] initWithPendency:item];
                [list addObject:data];
            }
        }
        NSLog(@"%@",list);
        _groupList = list;
        [_tableView reloadData];
      
       
    } fail:nil];
//    [[NSNotificationCenter defaultCenter]
//    addObserver:self selector:@selector(getNotificationAction:) name:@"ThisIsANoticafication" object:nil];
    _megModel = [TConversationListViewModel new];
    [RACObserve(self.megModel, dataList) subscribeNext:^(id  _Nullable x) {
     
        if (self.megModel.dataList.count>0) {
            self.tableView.emptyView.hidden = YES;
            
        }
        [[V2TIMManager sharedInstance] getConversationList:0 count:INT_MAX succ:^(NSArray<V2TIMConversation *> *list, uint64_t lastTS, BOOL isFinished) {
          
            for (int i = 0; i<list.count; i++) {
                V2TIMConversation *con = list[i];
              
                if ([con.showName isEqualToString:@"administrator"]) {
                    NSString *text =  con.lastMessage.textElem.text;
                    NSLog(@"%@",text);
                    if (text!=nil) {
                        NSData *jsonData = [text dataUsingEncoding:NSUTF8StringEncoding];
                        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                        _qun =[dic[@"qunnum"] integerValue];
                       _geren =[dic[@"geren"] integerValue];
                        
                        
                        if (_geren>0) {
                            self.firstGroupData[0].readNum = _geren;
                        }
                        else
                        {
                            self.firstGroupData[0].readNum = [@"" integerValue];
                        }

                        if (_qun>0) {
                            self.firstGroupData[1].readNum = _qun;
                        }
                        else
                        {
                            self.firstGroupData[1].readNum = [@"" integerValue];;
                        }
                    }
                    
                    
                }
             
            }
            
           
        } fail:^(int code, NSString *msg) {
            // 拉取会话列表失败
        }];
        self.tableView.frame = CGRectMake(0, 0, KScreenWidth,
                                          KScreenHeight);
        [self.tableView reloadData];
    }];
    [[V2TIMManager sharedInstance]getFriendList:^(NSArray<V2TIMFriendInfo *> *infoList) {
      
        _friendStr =[NSString stringWithFormat:@"我的好友  (%lu)",(unsigned long)infoList.count];
        [_tableView reloadData];
        
        } fail:^(int code, NSString *desc) {
            
        }];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self LoadNav];
    [self loadSearcView];
    [self LoadTableView];
   
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendListChanged) name:TUIKitNotification_onFriendListAdded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendListChanged) name:TUIKitNotification_onFriendListDeleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendListChanged) name:TUIKitNotification_onFriendInfoUpdate object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendApplicationListChanged) name:TUIKitNotification_onFriendApplicationListAdded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendApplicationListChanged) name:TUIKitNotification_onFriendApplicationListDeleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFriendApplicationListChanged) name:TUIKitNotification_onFriendApplicationListRead object:nil];
  
    
    @weakify(self)
    [RACObserve(self.viewModel, isLoadFinished) subscribeNext:^(id finished) {
        @strongify(self)
        if ([(NSNumber *)finished boolValue]) {
            [self.tableView reloadData];
        }
    }];
//    [RACObserve(self.viewModel, pendencyCnt) subscribeNext:^(NSNumber *x) {
//        self.firstGroupData[0].readNum = [x integerValue];
//
//    }];
    [_viewModel loadContacts];
   
}
- (void)getNotificationAction:(NSNotification *)notification{
    NSDictionary * infoDic = [notification object];
    // 这样就得到了我们在发送通知时候传入的字典了
  
   
}
- (void)LoadTableView
{
    NSMutableArray *list = @[].mutableCopy;
    [list addObject:({
        TUIContactActionCellData *data = [[TUIContactActionCellData alloc] init];
        data.icon = [UIImage imageNamed:@"新的朋友"];
        data.title = TUILocalizableString(TUIKitContactsNewFriends); // @"新的联系人";
        data.cselector = @selector(onAddNewFriend:);
        data.readNum = _geren;
        data;
    })];
    [list addObject:({
        TUIContactActionCellData *data = [[TUIContactActionCellData alloc] init];
        data.icon = [UIImage imageNamed:@"群通知"];
        data.title = @"群通知"; // @"群通知";
        data.cselector = @selector(onAddNewGroup:);
        data.readNum = _qun;
        data;
    })];
    [list addObject:({
        TUIContactActionCellData *data = [[TUIContactActionCellData alloc] init];
        data.icon = [UIImage imageNamed:@"我的群聊"];
        data.title = @"我的群聊"; // 我的群聊
        data.cselector = @selector(onGroupConversation:);
        data;
    })];
  
    self.firstGroupData = [NSArray arrayWithArray:list];


    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,100, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.sectionHeaderHeight = 0.1f;
    _tableView.sectionFooterHeight = 0.1f;
    _tableView.separatorStyle = UITableViewCellAccessoryNone;
    [_tableView setSectionIndexBackgroundColor:[UIColor clearColor]];
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, 8, 0);
    [_tableView setSectionIndexColor:[UIColor darkGrayColor]];
    [_tableView setBackgroundColor:self.view.backgroundColor];
  
    [self.view addSubview:_tableView];
     
    //cell无数据时，不显示间隔线
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableView setTableFooterView:v];
    _tableView.separatorInset = UIEdgeInsetsMake(0, 58, 0, 0);
    [_tableView registerClass:[TCommonContactCell class] forCellReuseIdentifier:kContactCellReuseId];
    [_tableView registerClass:[TUIContactActionCell class] forCellReuseIdentifier:kContactActionCellReuseId];
   
}
//搜索
- (void)searchTop:(UITapGestureRecognizer *)gesture
{
    CSMyFriendSearchViewController *vc = [[CSMyFriendSearchViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)loadSearcView
{
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 52)];
    myView.backgroundColor = [UIColor whiteColor];
    myView.userInteractionEnabled = YES;
    [self.view addSubview:myView];
    UIView *searchView = [[UIView alloc]init];
    searchView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    searchView.layer.masksToBounds = YES;
    searchView.layer.cornerRadius = 5;
    searchView.userInteractionEnabled = YES;
    [myView addSubview:searchView];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchTop:)];
    [searchView addGestureRecognizer:tapGesture];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(3);
        make.height.mas_equalTo(32);
        make.left.mas_equalTo(myView.mas_left).offset(12);
        make.right.mas_equalTo(myView.mas_right).offset(-12);
    }];
    UIImageView *searchImage = [[UIImageView alloc]init];
    searchImage.image = [UIImage imageNamed:@"search"];
    [searchView addSubview:searchImage];
    [searchImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(14, 14));
        make.centerY.mas_equalTo(searchView.mas_centerY);
    }];
    
    UILabel *searchLable = [[UILabel alloc]init];
    searchLable.text = @"搜索";
    searchLable.textColor = [UIColor colorWithHexString:@"#999999"];
    searchLable.font = [UIFont systemFontOfSize:14];
    [searchView addSubview:searchLable];
    [searchLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchImage.mas_right).offset(5);
        make.centerY.mas_equalTo(searchImage.mas_centerY);
    }];
    
}

- (TContactViewModel *)viewModel
{
    if (_viewModel == nil) {
        _viewModel = [TContactViewModel new];
    }
    return _viewModel;
}


- (void)onFriendListChanged {
    [_viewModel loadContacts];
}

- (void)onFriendApplicationListChanged {
    [_viewModel loadFriendApplication];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return self.viewModel.groupList.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.firstGroupData.count;
    } else {
        NSString *group = self.viewModel.groupList[section-1];
        NSArray *list = self.viewModel.dataDict[group];
        return list.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return nil;

    if (section==1) {
        CSLinkmanHeadView *headView = [[CSLinkmanHeadView alloc]
                                       initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
        headView.backgroundColor = [UIColor whiteColor];
        headView.lable.text = _friendStr;
        return headView;
    }

    return nil;
  
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section==0) {
        UIView *foodView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 10)];
        foodView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
        return foodView;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0.1f;
    if (section == 1)
        return 50;
    return 0.1f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.1f;
}
//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//    NSMutableArray *array = [NSMutableArray arrayWithObject:@""];
//    [array addObjectsFromArray:self.viewModel.groupList];
//    return array;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        TUIContactActionCell *cell = [tableView dequeueReusableCellWithIdentifier:kContactActionCellReuseId forIndexPath:indexPath];
        [cell fillWithData:self.firstGroupData[indexPath.row]];
//        if ([cell.titleLabel.text isEqualToString:@"群通知"]) {
//            [cell.unRead setNum:_groupList.count];
//        }

        //可以在此处修改，也可以在对应cell的初始化中进行修改。用户可以灵活的根据自己的使用需求进行设置。
        cell.changeColorWhenTouched = YES;
        return cell;
    } else {
        TCommonContactCell *cell = [tableView dequeueReusableCellWithIdentifier:kContactCellReuseId forIndexPath:indexPath];
        NSString *group = self.viewModel.groupList[indexPath.section-1];
        NSArray *list = self.viewModel.dataDict[group];
        TCommonContactCellData *data = list[indexPath.row];
        data.cselector = @selector(onSelectFriend:);
        [cell fillWithData:data];
        [cell.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(67);
            make.bottom.mas_equalTo(cell.contentView.mas_bottom);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
        }];

        //可以在此处修改，也可以在对应cell的初始化中进行修改。用户可以灵活的根据自己的使用需求进行设置。
        cell.changeColorWhenTouched = YES;
        _tableView.frame = CGRectMake(0, 52, KScreenWidth, KScreenHeight-64-52);
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}


#pragma mark -- 点击查看用户详情
- (void)onSelectFriend:(TCommonContactCell *)cell
{
    TCommonContactCellData *data = cell.contactData;
    [[V2TIMManager sharedInstance] getFriendsInfo:@[data.friendProfile.userID] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
        V2TIMFriendInfoResult *result = resultList.firstObject;
        CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
        vc.friendProfile = data.friendProfile;
        vc.result = result;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
      
    } fail:^(int code, NSString *msg) {
       
    }];

}
#pragma mark --新的联系人
- (void)onAddNewFriend:(TCommonTableViewCell *)cell
{
    CSNewFriendViewController *vc = CSNewFriendViewController.new;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    [self.viewModel clearApplicationCnt];
}
#pragma mark --我的群聊
- (void)onGroupConversation:(TCommonTableViewCell *)cell
{
    CSUIGroupConversationListController *vc = CSUIGroupConversationListController.new;
    vc.hidesBottomBarWhenPushed = YES;
    vc.title = TUILocalizableString(TUIKitContactsGroupChats); // @"群聊";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark --群通知
- (void)onAddNewGroup:(TCommonTableViewCell *)cell
{
    CSGroupNotificationVC *vc = [[CSGroupNotificationVC alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    [self.viewModel clearApplicationCnt];
}
- (void)onBlackList:(TCommonContactCell *)cell
{
    TUIBlackListController *vc = TUIBlackListController.new;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)conversationListController:(TUIConversationListController *)conversationController didSelectConversation:(TUIConversationCell *)conversation;
{
    TUIChatController *chat = [[TUIChatController alloc] initWithConversation:conversation.convData];
    chat.title = conversation.convData.title;
    [self.navigationController pushViewController:chat animated:YES];
}


- (void)runSelector:(SEL)selector withObject:(id)object{
    if([self respondsToSelector:selector]){
        //因为 TCommonCell中写了 [vc performSelector:self.data.cselector withObject:self]，所以此处不管有无参数，和父类逻辑保持一致进行传参，防止意外情况
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL, id) = (void *)imp;
        func(self, selector, object);
    }

}
- (void)LoadNav
{
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    lable.textColor = [UIColor colorWithHexString:@"#333333"];
    lable.font = [UIFont boldSystemFontOfSize:22];
    lable.text = @"联系人";
    UIBarButtonItem *leftLable = [[UIBarButtonItem alloc]initWithCustomView:lable];
    self.navigationItem.leftBarButtonItem=leftLable;

    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [moreButton setImage:[UIImage imageNamed:@"联系人_添加好友"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(rightBarButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *moreItem = [[UIBarButtonItem alloc] initWithCustomView:moreButton];
    self.navigationItem.rightBarButtonItem = moreItem;
    
    self.view.backgroundColor = [UIColor  whiteColor];
  
  
   
}
- (void)rightBarButtonClick:(UIButton *)rightBarButton
{
    NSMutableArray *menus = [NSMutableArray array];
    TPopCellData *friend = [[TPopCellData alloc] init];
    friend.image = TUIKitResource(@"new_friend");
    friend.title = NSLocalizedString(@"ContactsAddFriends", nil); //@"添加好友";
    [menus addObject:friend];

    TPopCellData *group = [[TPopCellData alloc] init];
    group.image = TUIKitResource(@"add_group");
    group.title = NSLocalizedString(@"ContactsJoinGroup", nil);//@"添加群组";
    [menus addObject:group];

    CGFloat height = [TPopCell getHeight] * menus.count + TPopView_Arrow_Size.height;
    CGFloat orginY = StatusBar_Height + NavBar_Height;
    TPopView *popView = [[TPopView alloc] initWithFrame:CGRectMake(Screen_Width - 140, orginY, 130, height)];
    CGRect frameInNaviView = [self.navigationController.view convertRect:rightBarButton.frame fromView:rightBarButton.superview];
    popView.arrowPoint = CGPointMake(frameInNaviView.origin.x + frameInNaviView.size.width * 0.5, orginY);
    popView.delegate = self;
    [popView setData:menus];
    [popView showInWindow:self.view.window];
}

- (void)popView:(TPopView *)popView didSelectRowAtIndex:(NSInteger)index
{
    if(index == 0){
        //添加好友
        //添加好友
        CSAddFriendViewController *add = [[CSAddFriendViewController alloc] init];
        add.hidesBottomBarWhenPushed = YES;
        add.segIndex = 0;
        [self.navigationController pushViewController:add animated:YES];
    }
    if(index == 1){
        //添加群组
        CSAddFriendViewController *add = [[CSAddFriendViewController alloc] init];
        add.hidesBottomBarWhenPushed = YES;
        add.segIndex = 1;
        [self.navigationController pushViewController:add animated:YES];
//        };
        return;    }
}
#pragma mark --  好友列表搜索
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
   
    NSString *inputStr = searchBar.text ;
    NSLog(@"serach==== %@", inputStr);
//    [[V2TIMManager sharedInstance] getUsersInfo:@[inputStr] succ:^(NSArray<V2TIMUserFullInfo *> *infoList) {
//        NSLog(@"%@",infoList);
//        _listArr =infoList;
//        [_myTableView reloadData];
//    } fail:^(int code, NSString *msg) {
//
//    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
