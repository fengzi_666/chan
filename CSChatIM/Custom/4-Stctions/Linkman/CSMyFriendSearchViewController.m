//
//  CSMyFriendSearchViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/16.
//

#import "CSMyFriendSearchViewController.h"
#import "CSSearchTableViewCell.h"
#import <SDWebImage/SDWebImage.h>
#import "V2TIMManager.h"
#import "CSApplyAddFrindViewController.h"
#import "CSAddFriendListModel.h"
#import "CSAddGoupListModel.h"
#import "CSGroupInfoVC.h"
#import "CSMyFriendDetailViewController.h"
#import "CSUserDataViewController.h"
@interface CSMyFriendSearchViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic ,strong) UITableView *myTableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
//是否加入群聊
@property (nonatomic ,copy) NSString *state;
@property (nonatomic,assign) NSInteger pageno;
@property (nonatomic ,copy) NSString *searchStr;

@end
static NSString *cellid = @"cellid";
@implementation CSMyFriendSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor  = [UIColor whiteColor];
    
    _listArr = [[NSMutableArray alloc]init];
    
    self.navigationItem.title = @"搜索好友";
    
    [self setNav];
    
    [self loadSearcView];
    
    [self LoadTableView];
    _pageno =1;
}
- (void)loadSearcView
{
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 52)];
    myView.backgroundColor = [UIColor whiteColor];
    myView.userInteractionEnabled = YES;
    [self.view addSubview:myView];
    UIView *searchView = [[UIView alloc]init];
    searchView.backgroundColor = [UIColor whiteColor];
    searchView.layer.masksToBounds = YES;
    searchView.layer.cornerRadius = 5;
    searchView.userInteractionEnabled = YES;
    [myView addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(3);
        make.height.mas_equalTo(32);
        make.left.mas_equalTo(myView.mas_left).offset(12);
        make.right.mas_equalTo(myView.mas_right).offset(-12);
    }];
   
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    searchBar.frame = CGRectMake(0, 0, KScreenWidth-24, 32); // 设置位置和大小
//    oneSearchBar.keyboardType = UIKeyboardTypeEmailAddress; // 设置弹出键盘的类型
    [searchBar setImage:[UIImage imageNamed:@"search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    if (@available(iOS 13.0, *)) {
        UITextField *txfSearchField = (UITextField*)searchBar.searchTextField;
        txfSearchField.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    } else {
        // Fallback on earlier versions
        NSArray *subviewArr = self.view.subviews;
        for(int i = 0; i < subviewArr.count ; i++) {
            UIView *viewSub = [subviewArr objectAtIndex:i];
            NSArray *arrSub = viewSub.subviews;
            for (int j = 0; j < arrSub.count ; j ++) {
                id tempId = [arrSub objectAtIndex:j];
                if([tempId isKindOfClass:[UITextField class]]) {
                    UITextField *searchFiled = (UITextField *)tempId;
                    searchFiled.font = [UIFont systemFontOfSize:14];
                }
            }
        }
      
     
       
    }
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault; // 设置UISearchBar的样式
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
//    searchBar.showsCancelButton = YES;
//    searchBar.tintColor = [UIColor redColor]; // 设置UISearchBar的颜色 使用clearColor就是去掉背景
    searchBar.placeholder = @"搜索"; // 设置提示文字
    [searchView addSubview:searchBar];
    
}
- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 52, KScreenWidth, KScreenHeight-52) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSSearchTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
    [self.view addSubview:_myTableView];
    _myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        self.listArr =[NSMutableArray array];
        [self LoadRequestSearch :_searchStr];
    }];
    self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequestSearch :_searchStr];
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CSSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    CSAddFriendListModel *info = _listArr[indexPath.row];
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:info.touxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = info.nicheng;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSAddFriendListModel *info = _listArr[indexPath.row];
    [[V2TIMManager sharedInstance] getFriendsInfo:@[info.tengxuncode] succ:^(NSArray<V2TIMFriendInfoResult *> *resultList) {
        V2TIMFriendInfoResult *result = resultList.firstObject;
        CSUserDataViewController *vc = [[CSUserDataViewController alloc]init];
        vc.friendProfile = result.friendInfo;
        vc.result = result;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
      
    } fail:^(int code, NSString *msg) {
       
    }];
   
     
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //查找用户
    _searchStr = searchBar.text;
    _pageno =1;
        [self LoadRequestSearch:_searchStr];
    
}

- (void)LoadRequestSearch :(NSString  *)str
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_haoyoulist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
  
    [dict setValue:str forKey:@"zhanghaoornicheng"];
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
   
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
       
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data =dict[@"data"];
            NSArray *list = data[@"list"];
            NSMutableArray *arr = [NSMutableArray array];
            for (int i=0; i<list.count; i++) {
                NSDictionary *dic = list[i];
                CSAddFriendListModel *model = [[CSAddFriendListModel alloc]initWithDic:dic];
                [arr addObject:model];
            }
            [_listArr addObjectsFromArray:arr];
           
            [self.myTableView.mj_header endRefreshing];
            if (_pageno ==1) {
                [_listArr removeAllObjects];
                [_listArr addObjectsFromArray:arr];
                
                [self.myTableView.mj_footer resetNoMoreData];
            }
            else
            {
                [_listArr addObjectsFromArray:arr];
                [self.myTableView.mj_footer endRefreshing];
            }
            if (arr.count==0) {
                [_myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            [_myTableView reloadData];
        }
       
    else
    {
        [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
