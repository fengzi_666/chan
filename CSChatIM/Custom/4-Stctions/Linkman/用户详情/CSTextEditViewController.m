//
//  CSTextEditViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "CSTextEditViewController.h"
#import "THeader.h"
#import "UIColor+TUIDarkMode.h"
#import "NSBundle+TUIKIT.h"
@interface TXTextField : UITextField
@property int margin;
@end


@implementation TXTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    int margin = self.margin;
    CGRect inset = CGRectMake(bounds.origin.x + margin, bounds.origin.y, bounds.size.width - margin, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    int margin = self.margin;
    CGRect inset = CGRectMake(bounds.origin.x + margin, bounds.origin.y, bounds.size.width - margin, bounds.size.height);
    return inset;
}

@end

@interface CSTextEditViewController ()

@end

@implementation CSTextEditViewController

// MLeaksFinder 会对这个类误报，这里需要关闭一下

- (BOOL)willDealloc {
    return NO;
}

- (instancetype)initWithText:(NSString *)text;
{
    if (self = [super init]) {
        _textValue = text;
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNav];
    self.navigationItem.title = @"修改备注";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:TUILocalizableString(Save) style:UIBarButtonItemStylePlain target:self action:@selector(onSave)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor d_colorWithColorLight:TController_Background_Color dark:TController_Background_Color_Dark];

    _inputTextField = [[TXTextField alloc] initWithFrame:CGRectZero];
    _inputTextField.placeholder = @"请修改备注";
    _inputTextField.text = [self.textValue stringByTrimmingCharactersInSet:
                                           [NSCharacterSet illegalCharacterSet]];
    [(TXTextField *)_inputTextField setMargin:10];
    _inputTextField.backgroundColor = [UIColor d_colorWithColorLight:TCell_Nomal dark:TCell_Nomal_Dark];
    _inputTextField.frame = CGRectMake(0, 10, self.view.frame.size.width, 40);
    _inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.view addSubview:_inputTextField];
}


- (void)onSave
{
    [SVProgressHUD showWithStatus:@""];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_updatebeizhu];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_userid forKey:@"haoyouid"];
    [dict setValue:_inputTextField.text forKey:@"beizhu"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"修改成功"];
            self.textValue = [self.inputTextField.text stringByTrimmingCharactersInSet:
                              [NSCharacterSet illegalCharacterSet]];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
   
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
