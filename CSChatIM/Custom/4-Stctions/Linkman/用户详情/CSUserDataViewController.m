//
//  CSUserDataViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "CSUserDataViewController.h"
#import "CSUserDataHeadView.h"
#import "TCommonTextCell.h"
#import "TCommonSwitchCell.h"
#import "liaotianshiViewController.h"
#import "TTextEditController.h"
#import "NSObject+RACPropertySubscribing.h"
#import "RACSignal.h"
#import "CSTextEditViewController.h"
#import "CSAddGroupManViewController.h"
//#import "TXMainCircleViewController.h"
@interface CSUserDataViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong)UITableView *myTableView;
@property NSArray<NSArray *> *dataList;
@property (nonatomic ,copy) NSString *jiaqunlaiyuan;

@property V2TIMUserFullInfo *userFullInfo;
@end

@implementation CSUserDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    self.navigationItem.title = @"用户详情";
    [[V2TIMManager sharedInstance] getBlackList:^(NSArray<V2TIMFriendInfo *> *infoList) {
        for (V2TIMFriendInfo *friend in infoList) {
            if ([friend.userID isEqualToString:self.friendProfile.userID])
            {
               
                break;
            }
        }
        [self loadData];
    } fail:nil];

    self.userFullInfo = self.friendProfile.userFullInfo;
    
    [self loadData];
    [self LoadTableView];
   
    [self LoadRequestData];
}
- (void)LoadRequestData
{

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunYaoQingInFo];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:_groupId forKey:@"quntxcode"];
    [dict setValue:self.userFullInfo.userID forKey:@"tengxuncode"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSDictionary *user = data[@"user"];

            _jiaqunlaiyuan =user[@"nicheng"];
            [self loadData];
        }
        else
        {
//            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
/**
 *初始化视图显示数据
 */
- (void)loadData
{
    NSMutableArray *list = @[].mutableCopy;
   

    [list addObject:({
        NSMutableArray *inlist = @[].mutableCopy;
        [inlist addObject:({
            TCommonTextCellData *data = TCommonTextCellData.new;
            data.key = NSLocalizedString(@"ProfileAlia", nil); // @"备注名";
            data.value = self.friendProfile.friendRemark;
            if (data.value.length == 0)
            {
                data.value = NSLocalizedString(@"None", nil); // @"无";
            }
            data.showAccessory = YES;
            data.cselector = @selector(onChangeRemark:);
            data.reuseId = @"TextCell";
            data;
        })];
      
       
        inlist;
    })];

    [list addObject:({
        NSMutableArray *inlist = @[].mutableCopy;
        [inlist addObject:({
            TCommonSwitchCellData *data = TCommonSwitchCellData.new;
            data.title = @"置顶聊天"; // @"置顶聊天";
            if ([[[TUILocalStorage sharedInstance] topConversationList] containsObject:[NSString stringWithFormat:@"c2c_%@",self.friendProfile.userID]]) {
                data.on = YES;
            }
            
            data.cswitchSelector =  @selector(onTopMostChat:);
            data.reuseId = @"SwitchCell";
            data;
        })];
        
        
        if ([_istype isEqualToString:@"1"]) {
            [inlist addObject:({
                TCommonTextCellData *data1 = TCommonTextCellData.new;
                data1.key = @"加群来源"; // @"备注名";
                data1.value =[NSString stringWithFormat:@"%@邀请进群", _jiaqunlaiyuan];
                if (data1.value.length == 0)
                {
                    data1.value = @""; // @"无";
                }
//                data1.cselector = @selector(addGroupClick:);
//                data1.showAccessory = YES;
                data1.reuseId = @"TextCell";
                data1;
            })];
            
        }
//        if ( _result.relation == V2TIM_FRIEND_RELATION_TYPE_BOTH_WAY ) {
//            [inlist addObject:({
//                TCommonTextCellData *data = TCommonTextCellData.new;
//                data.key = @"朋友圈"; // @"备注名";
//                data.value = @"";
//
//                data.showAccessory = YES;
//                data.cselector = @selector(mainClick);
//                data.reuseId = @"TextCell";
//                data;
//            })];
//        }
      
        
        [inlist addObject:({

            TCommonTextCellData *data1 = TCommonTextCellData.new;
            data1.key = @"删除好友"; // @"备注名";
            if (data1.value.length == 0)
            {
                data1.value = @""; // @"无";
            }
            data1.showAccessory = NO;
            data1.cselector = @selector(onDeleteFriend:);
            data1.reuseId = @"TextCell";
            data1;
        })];
        inlist;
    })];
    [list addObject:({
        NSMutableArray *inlist = @[].mutableCopy;
        [inlist addObject:({
            TUIButtonCellData *data = TUIButtonCellData.new;
            data.title = NSLocalizedString(@"ProfileSendMessages", nil); // @"发送消息";
            data.style = ButtonBule;
            data.cbuttonSelector = @selector(onSendMessage:);
            data.reuseId = @"ButtonCell";
            data;
        })];
        
        inlist;
    })];

    self.dataList = list;
    [self.myTableView reloadData];
}
- (void)addGroupClick:(TCommonTextCell *)cell
{
    NSLog(@"加群来源");
    CSAddGroupManViewController *vc = [[CSAddGroupManViewController alloc]init];
    vc.quntxcode = _groupId;
    vc.tengxuncode = self.userFullInfo.userID;
    [self.navigationController pushViewController: vc animated:YES];
}
/**
 *点击 修改备注 按钮后所执行的函数。包含数据的获取与请求回调
 */
- (void)onChangeRemark:(TCommonTextCell *)cell
{
    CSTextEditViewController *vc = [[CSTextEditViewController alloc] initWithText:self.friendProfile.friendRemark];
    vc.title = NSLocalizedString(@"ProfileEditAlia", nil); // @"修改备注";
    vc.textValue = self.friendProfile.friendRemark;
    NSDictionary *dict = _userFullInfo.customInfo;
    vc.userid =[[ NSString alloc] initWithData:dict[@"userid"] encoding:NSUTF8StringEncoding];
    [self.navigationController pushViewController:vc animated:YES];

//    @weakify(self)

    [[RACObserve(vc, textValue) skip:1] subscribeNext:^(NSString *value) {
//        @strongify(self)
//        self.modified = YES;
        self.friendProfile.friendRemark = value;
        [[V2TIMManager sharedInstance] setFriendInfo:self.friendProfile succ:^{
            [self loadData];;
        } fail:nil];
    }];

}
//#pragma mark -- 朋友圈入口
//- (void)mainClick
//{
//    NSDictionary *dict = self.userFullInfo.customInfo;
//    NSString *userid = [[ NSString alloc]initWithData:dict[@"userid"] encoding:NSUTF8StringEncoding];
//    NSString *touxiang = self.userFullInfo.faceURL;
//    NSString *name = self.userFullInfo.nickName;
//
//    TXMainCircleViewController *vc = [[TXMainCircleViewController alloc]init];
//    vc.userid = userid;
//    vc.headImage =touxiang;
//    vc.name = name;
//    [self.navigationController pushViewController:vc animated:YES];
//}
/**
 *点击 删除好友 后执行的函数，包括好友信息获取和请求回调
 */
- (void)onDeleteFriend:(id)sender
{
    NSLog(@"删除好友");
    
    [SVProgressHUD showWithStatus:@""];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apphaoyoudel];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSDictionary *customInfo = _userFullInfo.customInfo;
    NSString *userid =[[ NSString alloc] initWithData:customInfo[@"userid"] encoding:NSUTF8StringEncoding];
    [dict setValue:userid forKey:@"haoyouid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [SVProgressHUD showSuccessWithStatus:@"删除成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
   
  
}

/**
 *点击 发送消息 后执行的函数，默认跳转到对应好友的聊天界面
 */
- (void)onSendMessage:(id)sender
{
    TUIConversationCellData *data = [[TUIConversationCellData alloc] init];
    data.conversationID = [NSString stringWithFormat:@"c2c_%@",self.userFullInfo.userID];
    data.userID = self.friendProfile.userID;
    data.title = [self.friendProfile.userFullInfo showName];
    liaotianshiViewController *chat = [[liaotianshiViewController alloc] init];
    chat.conversationData = data;
    [self.navigationController pushViewController:chat animated:YES];
}

/**
 *操作 置顶 开关后执行的函数，将对应好友添加/移除置顶队列
 */
- (void)onTopMostChat:(TCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        [[TUILocalStorage sharedInstance] addTopConversation:[NSString stringWithFormat:@"c2c_%@",self.friendProfile.userID]];
    } else {
        [[TUILocalStorage sharedInstance] removeTopConversation:[NSString stringWithFormat:@"c2c_%@",self.friendProfile.userID]];
    }
}

- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [self.myTableView registerClass:[TCommonTextCell class] forCellReuseIdentifier:@"TextCell"];
    [self.myTableView registerClass:[TCommonSwitchCell class] forCellReuseIdentifier:@"SwitchCell"];
    [self.myTableView registerClass:[TUIProfileCardCell class] forCellReuseIdentifier:@"CardCell"];
    [self.myTableView registerClass:[TUIButtonCell class] forCellReuseIdentifier:@"ButtonCell"];
    [self.view addSubview:_myTableView];
    
    CSUserDataHeadView *headView = [[CSUserDataHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 92)];
    [headView.headImage sd_setImageWithURL:[NSURL URLWithString:self.userFullInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    headView.nameLable.text = self.userFullInfo.nickName;
    NSDictionary *dict =self.userFullInfo.customInfo;
    NSString *idname = [[ NSString alloc] initWithData:dict[@"usercode"] encoding:NSUTF8StringEncoding];
    headView.IDLable.text = [NSString stringWithFormat:@"ID:  %@",idname];
    headView.signatureLable.text = [NSString stringWithFormat:@"个性签名:  %@",self.userFullInfo.selfSignature];

    _myTableView.tableHeaderView.height = 92;
    _myTableView.tableHeaderView = headView;

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataList.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList[section].count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSObject *data = self.dataList[indexPath.section][indexPath.row];
   if([data isKindOfClass:[TUIButtonCellData class]]){
        TUIButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ButtonCell"];
        if(!cell){
            cell = [[TUIButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ButtonCell"];
            
        }
      
        [cell fillWithData:(TUIButtonCellData *)data];
       [cell.button setBackgroundColor:[UIColor colorWithHexString:@"#32965A"]];
       
        return cell;

    }  else if([data isKindOfClass:[TCommonTextCellData class]]) {
        TCommonTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextCell" forIndexPath:indexPath];
        
      
        [cell fillWithData:(TCommonTextCellData *)data];
        if ([cell.keyLabel.text isEqualToString:@"删除好友"]) {
            cell.keyLabel.textColor = [UIColor redColor];
        }
        return cell;

    }  else if([data isKindOfClass:[TCommonSwitchCellData class]]) {
        TCommonSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell" forIndexPath:indexPath];
        [cell fillWithData:(TCommonSwitchCellData *)data];
        cell.switcher.onTintColor = [UIColor colorWithHexString:@"#32965A"];
        return cell;
    }

    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

/*

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
