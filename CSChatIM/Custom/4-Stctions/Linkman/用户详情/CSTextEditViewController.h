//
//  CSTextEditViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class CSTextEditViewController;
@interface CSTextEditViewController : BaseViewController
@property UITextField *inputTextField;
@property NSString *textValue;
@property (nonatomic ,copy) NSString *userid;

- (instancetype)initWithText:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
