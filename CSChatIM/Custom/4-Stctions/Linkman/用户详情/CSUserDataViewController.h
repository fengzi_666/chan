//
//  CSUserDataViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSUserDataViewController : BaseViewController<TUIFriendProfileControllerServiceProtocol>
@property V2TIMFriendInfo *friendProfile;
@property V2TIMFriendInfoResult *result ;
@property (nonatomic ,copy) NSString *groupId;
@property (nonatomic ,copy) NSString *istype;
@property (nonatomic ,copy) NSString *msgid;
@end

NS_ASSUME_NONNULL_END
