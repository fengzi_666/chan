//
//  CSNewFriendListModel.h
//  CSChatIM
//
//  Created by Apple on 2021/1/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSNewFriendListModel : NSObject
@property (nonatomic ,copy) NSString *haoyoucode;
@property (nonatomic ,copy) NSString *haoyouid;
@property (nonatomic ,copy) NSString *haoyounicheng;
@property (nonatomic ,copy) NSString *haoyouphone;
@property (nonatomic ,copy) NSString *haoyoutouxiang;
@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *message;
@property (nonatomic ,copy) NSString *shenqingdate;
@property (nonatomic ,assign) NSInteger state;

@end

NS_ASSUME_NONNULL_END
