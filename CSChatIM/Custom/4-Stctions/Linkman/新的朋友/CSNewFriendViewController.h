//
//  CSNewFriendViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/11/30.
//

#import "BaseViewController.h"
#import "TContactViewModel.h"
NS_ASSUME_NONNULL_BEGIN
/**
 * 【模块名称】好友申请界面（TUINewFriendViewController）
 * 【功能说明】负责拉取好友申请信息，并在界面中显示。
 *  通过本界面，您可以查看自己收到的好友请求，并进行同意/拒绝申请的操作。
 */
@interface CSNewFriendViewController : BaseViewController
@property (nonatomic ,strong)TContactViewModel *model;
@end

NS_ASSUME_NONNULL_END
