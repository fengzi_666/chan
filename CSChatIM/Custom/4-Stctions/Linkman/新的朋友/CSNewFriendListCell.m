//
//  CSNewFriendListCell.m
//  CSChatIM
//
//  Created by Apple on 2021/1/15.
//

#import "CSNewFriendListCell.h"

@implementation CSNewFriendListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.height/2;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)agree:(UIButton *)sender {
   
    if ([self.delegate respondsToSelector:@selector(TXNewFriendListClick:sender:)]) {
        [self.delegate TXNewFriendListClick:self sender:sender];
    }
}

@end
