//
//  CSNewFriendListCell.h
//  CSChatIM
//
//  Created by Apple on 2021/1/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class CSNewFriendListCell;
@protocol CSNewFriendListCellDelegete <NSObject>

//修饰协议方法
//@optional--协议方法可实现也可不实现
//@required--协议方法必须实现
@optional

//即协议方法（点击按钮后要做的事情）
- (void)TXNewFriendListClick:(CSNewFriendListCell *)cell sender:(UIButton *)sender;

@end
@interface CSNewFriendListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *msgLable;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (nonatomic ,strong) NSIndexPath *index;
@property (nonatomic, weak) id <CSNewFriendListCellDelegete> delegate;
@end

NS_ASSUME_NONNULL_END
