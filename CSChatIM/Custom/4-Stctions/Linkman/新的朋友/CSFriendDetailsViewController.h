//
//  CSFriendDetailsViewController.h
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import "BaseViewController.h"
#import "TUINewFriendViewModel.h"
#import "CSNewFriendListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSFriendDetailsViewController : BaseViewController
@property V2TIMUserFullInfo *userFullInfo;
@property TUINewFriendViewModel *viewModel;
@property TCommonPendencyCellData *pendency;
@property (nonatomic ,strong) CSNewFriendListModel *model ;
@end

NS_ASSUME_NONNULL_END
