//
//  CSFriendDetailsViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/4.
//

#import "CSFriendDetailsViewController.h"
#import "TCommonPendencyCellData.h"
#import "CSLinkmanViewController.h"
@interface CSFriendDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *IDlable;
@property (weak, nonatomic) IBOutlet UILabel *msgLable;

@end

@implementation CSFriendDetailsViewController
{
    TCommonPendencyCellData *_pendency;
}
@synthesize pendency = _pendency;
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"好友申请";
    
    [self setNav];
    
    NSLog(@"%@",_userFullInfo);
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:_model.haoyoutouxiang] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    self.headImage.layer.masksToBounds= YES;
    self.headImage.layer.cornerRadius = 28.5;
    self.nameLable.text = _model.haoyounicheng;
   
    self.IDlable.text =[NSString stringWithFormat:@"ID:  %@",_model.haoyoucode];
    self.msgLable.text = [NSString stringWithFormat:@"对方留言:  %@",_model.message];
    
}
//拒绝
- (IBAction)refuseBtnClick:(UIButton *)sender {
    [self onRejectFriend];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apphaoyoushengqing];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];

    [dict setValue:_model.id forKey:@"shenqingid"];
    [dict setValue:@"3" forKey:@"state"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [self onAgreeFriend];
            [self jumpVC];
            [SVProgressHUD showSuccessWithStatus:@"已拒绝"];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}

//同意
- (IBAction)consent:(UIButton *)sender {
    
    [SVProgressHUD showWithStatus:@"请稍后..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apphaoyoushengqing];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];

    [dict setValue:_model.id forKey:@"shenqingid"];
    [dict setValue:@"2" forKey:@"state"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [self onAgreeFriend];
            [self jumpVC];
            [SVProgressHUD showSuccessWithStatus:@"添加好友成功"];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
/**
 *  点击 同意(好友) 按钮后执行的函数
 */
- (void)onAgreeFriend
{
    [self.pendency agree];
}
- (void)onRejectFriend
{
    [self.pendency reject];
}
//跳转指定控制器
- (void)jumpVC
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSLinkmanViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
