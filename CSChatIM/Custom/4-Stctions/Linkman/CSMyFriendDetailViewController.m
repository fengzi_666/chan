//
//  CSMyFriendDetailViewController.m
//  CSChatIM
//
//  Created by Apple on 2020/12/16.
//

#import "CSMyFriendDetailViewController.h"
#import "CSUserDataHeadView.h"
#import "TCommonTextCell.h"
#import "TCommonSwitchCell.h"
#import "liaotianshiViewController.h"
#import "TTextEditController.h"
#import "NSObject+RACPropertySubscribing.h"
#import "RACSignal.h"
#import "CSTextEditViewController.h"
#import "CSAddGroupManViewController.h"
@interface CSMyFriendDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong)UITableView *myTableView;
@property NSArray<NSArray *> *dataList;

@property V2TIMUserFullInfo *userFullInfo;
@property V2TIMFriendInfo *friendProfile;
@end

@implementation CSMyFriendDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    self.navigationItem.title = @"用户详情";
   
    [[V2TIMManager sharedInstance]getFriendsInfo:@[_userid] succ:^(NSArray<V2TIMFriendInfo *> *resultList) {
        V2TIMUserFullInfo *info = resultList.firstObject.userFullInfo;
        self.friendProfile =resultList.firstObject;
        self.userFullInfo = info;
        [self loadData];
        [self LoadTableView];
        } fail:^(int code, NSString *desc) {
            
        }];
}
/**
 *初始化视图显示数据
 */
- (void)loadData
{
    NSMutableArray *list = @[].mutableCopy;
   

    [list addObject:({
        NSMutableArray *inlist = @[].mutableCopy;
        [inlist addObject:({
            TCommonTextCellData *data = TCommonTextCellData.new;
            data.key = NSLocalizedString(@"ProfileAlia", nil); // @"备注名";
            data.value = self.friendProfile.friendRemark;
            if (data.value.length == 0)
            {
                data.value = NSLocalizedString(@"None", nil); // @"无";
            }
            data.showAccessory = YES;
            data.cselector = @selector(onChangeRemark:);
            data.reuseId = @"TextCell";
            data;
        })];
      
       
        inlist;
    })];

    [list addObject:({
        NSMutableArray *inlist = @[].mutableCopy;
        [inlist addObject:({
            TCommonSwitchCellData *data = TCommonSwitchCellData.new;
            data.title = @"置顶聊天"; // @"置顶聊天";
            if ([[[TUILocalStorage sharedInstance] topConversationList] containsObject:[NSString stringWithFormat:@"c2c_%@",self.friendProfile.userID]]) {
                data.on = YES;
            }
            
            data.cswitchSelector =  @selector(onTopMostChat:);
            data.reuseId = @"SwitchCell";
            data;
        })];
        
        [inlist addObject:({

            TCommonTextCellData *data1 = TCommonTextCellData.new;
            data1.key = @"删除好友"; // @"备注名";
            data1.value = self.friendProfile.friendRemark;
            if (data1.value.length == 0)
            {
                data1.value = @""; // @"无";
            }
            data1.showAccessory = YES;
            data1.cselector = @selector(onDeleteFriend:);
            data1.reuseId = @"TextCell";
            data1;
        })];
        inlist;
    })];
    [list addObject:({
        NSMutableArray *inlist = @[].mutableCopy;
        [inlist addObject:({
            TUIButtonCellData *data = TUIButtonCellData.new;
            data.title = NSLocalizedString(@"ProfileSendMessages", nil); // @"发送消息";
            data.style = ButtonBule;
            data.cbuttonSelector = @selector(onSendMessage:);
            data.reuseId = @"ButtonCell";
            data;
        })];
        
        inlist;
    })];

    self.dataList = list;
    [self.myTableView reloadData];
}

/**
 *点击 修改备注 按钮后所执行的函数。包含数据的获取与请求回调
 */
- (void)onChangeRemark:(TCommonTextCell *)cell
{
    CSTextEditViewController *vc = [[CSTextEditViewController alloc] initWithText:self.friendProfile.friendRemark];
    vc.title = NSLocalizedString(@"ProfileEditAlia", nil); // @"修改备注";
    vc.textValue = self.friendProfile.friendRemark;
    NSDictionary *dict = _userFullInfo.customInfo;
    vc.userid =[[ NSString alloc] initWithData:dict[@"userid"] encoding:NSUTF8StringEncoding];
    [self.navigationController pushViewController:vc animated:YES];

//    @weakify(self)

    [[RACObserve(vc, textValue) skip:1] subscribeNext:^(NSString *value) {
//        @strongify(self)
//        self.modified = YES;
        self.friendProfile.friendRemark = value;
        [[V2TIMManager sharedInstance] setFriendInfo:self.friendProfile succ:^{
            [self loadData];;
        } fail:nil];
    }];

}

/**
 *点击 删除好友 后执行的函数，包括好友信息获取和请求回调
 */
- (void)onDeleteFriend:(id)sender
{
    NSLog(@"删除好友");
    
    [SVProgressHUD showWithStatus:@""];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_apphaoyoudel];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSDictionary *customInfo = _userFullInfo.customInfo;
    NSString *userid =[[ NSString alloc] initWithData:customInfo[@"userid"] encoding:NSUTF8StringEncoding];
    [dict setValue:userid forKey:@"haoyouid"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
//            [SVProgressHUD showSuccessWithStatus:@"删除成功"];
            [[V2TIMManager sharedInstance] deleteFromFriendList:@[self.friendProfile.userID] deleteType:V2TIM_FRIEND_TYPE_BOTH succ:^(NSArray<V2TIMFriendOperationResult *> *resultList) {

                [self.navigationController popViewControllerAnimated:YES];
            } fail:nil];
        }
        else
        {
            [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
   
}

/**
 *点击 发送消息 后执行的函数，默认跳转到对应好友的聊天界面
 */
- (void)onSendMessage:(id)sender
{
    TUIConversationCellData *data = [[TUIConversationCellData alloc] init];
    data.conversationID = [NSString stringWithFormat:@"c2c_%@",self.userFullInfo.userID];
    data.userID = self.friendProfile.userID;
    data.title = [self.friendProfile.userFullInfo showName];
    liaotianshiViewController *chat = [[liaotianshiViewController alloc] init];
    chat.conversationData = data;
    [self.navigationController pushViewController:chat animated:YES];
}

/**
 *操作 置顶 开关后执行的函数，将对应好友添加/移除置顶队列
 */
- (void)onTopMostChat:(TCommonSwitchCell *)cell
{
    if (cell.switcher.on) {
        [[TUILocalStorage sharedInstance] addTopConversation:[NSString stringWithFormat:@"c2c_%@",self.friendProfile.userID]];
    } else {
        [[TUILocalStorage sharedInstance] removeTopConversation:[NSString stringWithFormat:@"c2c_%@",self.friendProfile.userID]];
    }
}

- (void)LoadTableView
{
   
    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.sectionHeaderHeight = 0.f;
    _myTableView.sectionFooterHeight = 0.f;
    _myTableView.separatorStyle = UITableViewCellAccessoryNone;
    _myTableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    
    [self.myTableView registerClass:[TCommonTextCell class] forCellReuseIdentifier:@"TextCell"];
    [self.myTableView registerClass:[TCommonSwitchCell class] forCellReuseIdentifier:@"SwitchCell"];
    [self.myTableView registerClass:[TUIProfileCardCell class] forCellReuseIdentifier:@"CardCell"];
    [self.myTableView registerClass:[TUIButtonCell class] forCellReuseIdentifier:@"ButtonCell"];
    [self.view addSubview:_myTableView];
    
    CSUserDataHeadView *headView = [[CSUserDataHeadView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 92)];
    [headView.headImage sd_setImageWithURL:[NSURL URLWithString:self.userFullInfo.faceURL] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    headView.nameLable.text = self.userFullInfo.nickName;
    NSDictionary *dict =self.userFullInfo.customInfo;
    NSString *idname = [[ NSString alloc] initWithData:dict[@"usercode"] encoding:NSUTF8StringEncoding];
    headView.IDLable.text = [NSString stringWithFormat:@"ID:  %@",idname];
    headView.signatureLable.text = [NSString stringWithFormat:@"个性签名:  %@",self.userFullInfo.selfSignature];

    _myTableView.tableHeaderView.height = 92;
    _myTableView.tableHeaderView = headView;

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataList.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList[section].count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSObject *data = self.dataList[indexPath.section][indexPath.row];
   if([data isKindOfClass:[TUIButtonCellData class]]){
        TUIButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ButtonCell"];
        if(!cell){
            cell = [[TUIButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ButtonCell"];
            
        }
      
        [cell fillWithData:(TUIButtonCellData *)data];
       [cell.button setBackgroundColor:[UIColor colorWithHexString:@"#32965A"]];
       
        return cell;

    }  else if([data isKindOfClass:[TCommonTextCellData class]]) {
        TCommonTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextCell" forIndexPath:indexPath];
        
      
        [cell fillWithData:(TCommonTextCellData *)data];
        if ([cell.keyLabel.text isEqualToString:@"删除好友"]) {
            cell.keyLabel.textColor = [UIColor redColor];
        }
        return cell;

    }  else if([data isKindOfClass:[TCommonSwitchCellData class]]) {
        TCommonSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell" forIndexPath:indexPath];
        [cell fillWithData:(TCommonSwitchCellData *)data];
        return cell;
    }

    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

/*

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
