//
//  CSGroupNotificationDetailsVC.m
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import "CSGroupNotificationDetailsVC.h"
#import "CSLinkmanViewController.h"
@interface CSGroupNotificationDetailsVC ()
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *subName;
@property (weak, nonatomic) IBOutlet UILabel *msgLable;

@end

@implementation CSGroupNotificationDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"申请入群";
    
    [self setNav];
    
    NSLog(@"%@",_data);
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:_model.qunpic] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    self.headImage.layer.masksToBounds= YES;
    self.headImage.layer.cornerRadius = 28.5;
    self.nameLable.text = _model.nicheng;
    self.subName.text =[NSString stringWithFormat:@"ID:  %@",_model.usercode];
    
    self.msgLable.text = [NSString stringWithFormat:@"对方留言:  %@",_model.message];
}
//同意
- (IBAction)agreeBtnClick:(UIButton *)sender {
    [SVProgressHUD showWithStatus:@"请稍后..."];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunshenqingchuli];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:_model.id forKey:@"shenqingid"];
    [dict setValue:@"2" forKey:@"state"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            [_data accept];
            [SVProgressHUD showSuccessWithStatus:@"加群成功"];
            [self jumpVC];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
            [_data accept];
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
}
//拒绝
- (IBAction)refuse:(UIButton *)sender {

    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunshenqingchuli];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:_model.id forKey:@"shenqingid"];
    [dict setValue:@"3" forKey:@"state"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        if ([code isEqualToString:@"0"]) {
            
            [SVProgressHUD showSuccessWithStatus:@"已拒绝"];
            [self jumpVC];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [SVProgressHUD showInfoWithStatus:dict[@"msg"]];
            
        }
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
    }];
           
      
}
//跳转指定控制器
- (void)jumpVC
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSLinkmanViewController class]]) {
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
