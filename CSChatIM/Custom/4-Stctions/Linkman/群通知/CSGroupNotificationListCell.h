//
//  CSGroupNotificationListCell.h
//  CSChatIM
//
//  Created by Apple on 2020/12/5.
//

#import "TCommonCell.h"
#import "TUIGroupPendencyCellData.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSGroupNotificationListCell : TCommonTableViewCell
@property UIImageView *avatarView;
@property UILabel *titleLabel;
@property UILabel *addSourceLabel;
@property UILabel *addWordingLabel;
@property UIButton *agreeButton;
@property (nonatomic ,strong) UILabel *line;
@property (nonatomic ,strong) NSIndexPath *index;
@property (nonatomic) TUIGroupPendencyCellData *pendencyData;

- (void)fillWithData1:(TUIGroupPendencyCellData *)pendencyData;
@end

NS_ASSUME_NONNULL_END
