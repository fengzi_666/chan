//
//  CSGroupNotificationVC.m
//  CSChatIM
//
//  Created by Apple on 2020/12/5.
//

#import "CSGroupNotificationVC.h"
#import "TUINewFriendViewModel.h"
#import "ReactiveObjC.h"
#import "MMLayout/UIView+MMLayout.h"
#import "TUIUserProfileControllerServiceProtocol.h"
#import "TCServiceManager.h"
#import "Toast/Toast.h"
#import "UIColor+TUIDarkMode.h"
#import "THeader.h"
#import "NSBundle+TUIKIT.h"
#import "CSCommonTableViewCell.h"
#import "CSFriendDetailsViewController.h"
#import "TUIGroupPendencyCellData.h"
#import "CSGroupNotificationListCell.h"
#import "CSGroupNotificationDetailsVC.h"
#import "V2TIMManager.h"
#import "CSNewFriendListCell.h"
#import "CSGroupNotifucationListModel.h"
@import ImSDK;

@interface CSGroupNotificationVC ()<UITableViewDelegate,UITableViewDataSource,CSNewFriendListCellDelegete>
@property UITableView *tableView;
@property UIButton  *moreBtn;
@property TUINewFriendViewModel *viewModel;
@property (nonatomic ,strong) NSArray *groupList;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,assign) NSInteger pageno;
@end
static NSString *cellid = @"cellid";
@implementation CSGroupNotificationVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //获取群组通知
    [[V2TIMManager sharedInstance] getGroupApplicationList:^(V2TIMGroupApplicationResult *result) {
       
        NSMutableArray *list = @[].mutableCopy;
        for (V2TIMGroupApplication *item in result.applicationList) {
            if (item.handleStatus == V2TIM_GROUP_APPLICATION_HANDLE_STATUS_UNHANDLED) {
                TUIGroupPendencyCellData *data = [[TUIGroupPendencyCellData alloc] initWithPendency:item];
                [list addObject:data];
                if (list.count>0) {
                    self.tableView.emptyView.hidden = YES;
                }
                
            }
        }
        NSLog(@"%@",list);
        _groupList = list;
        [_tableView reloadData];
       
    } fail:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    _dataArr = [[NSMutableArray alloc]init];
    self.title = @"群通知"; //
    self.view.backgroundColor = [UIColor d_colorWithColorLight:TController_Background_Color dark:TController_Background_Color_Dark];
  
    [self LoadTableView];
    
}
- (void)LoadTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.sectionHeaderHeight = 0.f;
    _tableView.sectionFooterHeight = 0.f;
    _tableView.separatorStyle = UITableViewCellAccessoryNone;
    
    _tableView.backgroundColor = [UIColor colorWithHexString:@"#f4f4f4"];
    [_tableView registerNib:[UINib nibWithNibName:@"CSNewFriendListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellid];
//    self.tableView.allowsMultipleSelectionDuringEditing = NO;
//    _tableView.separatorInset = UIEdgeInsetsMake(0, 94, 0, 0);
//    _tableView.backgroundColor = self.view.backgroundColor;
//    _tableView.separatorStyle = UITableViewCellAccessoryNone;
    [_tableView addEmptyViewWithImageName:@"空界面占位图" title:@""];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageno =1;
        
        [_dataArr removeAllObjects];
        [self LoadRequestDta];
    }];
    _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageno ++;
        [self LoadRequestDta];
    }];
    
    [_tableView.mj_header beginRefreshing];
    [self.view addSubview:_tableView];
    
}
- (void)LoadRequestDta
{
    [SVProgressHUD showWithStatus:@"查询中"];
    NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_appshengqinglist];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
   
    [dict setValue:[NSString stringWithFormat:@"%ld",_pageno] forKey:@"pageNo"];
    [dict setValue:@"1" forKey:@"leixing"];
    [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
        [SVProgressHUD dismiss];
        NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
        [self.tableView.mj_header endRefreshing];
        if ([code isEqualToString:@"0"]) {
            NSDictionary *data = dict[@"data"];
            NSArray *list = data[@"list"];
            [_dataArr addObjectsFromArray:[CSGroupNotifucationListModel mj_objectArrayWithKeyValuesArray:list]];
            
            if ([data[@"next"] intValue]<0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self.tableView.mj_footer endRefreshing];
            }
            if (_dataArr.count>0) {
                _tableView.emptyView.hidden=YES;
            }
            else
            {
                _tableView.emptyView.hidden=NO;
            }
        }
        [_tableView reloadData];
                
    } error:^(NSString *error) {
        [SVProgressHUD  dismiss];
        self.pageno--;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSNewFriendListCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellid];
    CSGroupNotifucationListModel *model = _dataArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.index =indexPath;
    [cell.headImage sd_setImageWithURL:[NSURL URLWithString:model.userpic] placeholderImage:[UIImage imageNamed:@"头像占位"]];
    cell.nameLable.text = model.nicheng;
    if ([NSString isBlankString:model.message]) {
        model.message = @"";
    }
    cell.msgLable.text = [NSString stringWithFormat:@"对方留言:  %@",model.message];
    cell.delegate = self;
    if (model.state ==1) {
        cell.agreeBtn.backgroundColor = [UIColor colorWithHexString:@"#32965A"];
        [cell.agreeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.agreeBtn setTitle:@"同意" forState:UIControlStateNormal];
        cell.agreeBtn.userInteractionEnabled = YES;
        cell.agreeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    if (model.state ==2) {
        cell.agreeBtn.backgroundColor = [UIColor clearColor];
        [cell.agreeBtn setTitleColor:[UIColor colorWithHexString:@"#999999"]
                            forState:UIControlStateNormal];
        [cell.agreeBtn setTitle:@"已同意" forState:UIControlStateNormal];
        cell.agreeBtn.userInteractionEnabled = NO;
        cell.agreeBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    }
    if (model.state ==3) {
        cell.agreeBtn.backgroundColor = [UIColor clearColor];
        [cell.agreeBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        cell.agreeBtn.userInteractionEnabled = NO;
        [cell.agreeBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
        cell.agreeBtn.titleLabel.font = [UIFont systemFontOfSize:11];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSGroupNotifucationListModel *model = _dataArr[indexPath.row];
    if (model.state==1) {
        
        CSGroupNotificationDetailsVC *vc = [[CSGroupNotificationDetailsVC alloc]init];
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
    NSLog(@"11");
}
- (void)TXNewFriendListClick:(CSNewFriendListCell *)cell sender:(UIButton *)sender
{
    CSGroupNotifucationListModel *model = _dataArr[cell.index.row];
    [SVProgressHUD showWithStatus:@"请稍后..."];
    if (model.state==1) {
        NSString *url = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_qunshenqingchuli];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];

        [dict setValue:model.id forKey:@"shenqingid"];
        [dict setValue:@"2" forKey:@"state"];
        [longwangRequestManage postBaseWithURL:url param:dict success:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            NSString *code = [NSString stringWithFormat:@"%@",dict[@"code"]];
            if ([code isEqualToString:@"0"]) {
                [SVProgressHUD showSuccessWithStatus:@"已同意"];
                cell.agreeBtn.backgroundColor = [UIColor clearColor];
                [cell.agreeBtn setTitleColor:[UIColor colorWithHexString:@"#999999"]
                                    forState:UIControlStateNormal];
                [cell.agreeBtn setTitle:@"已同意" forState:UIControlStateNormal];
                cell.agreeBtn.userInteractionEnabled = NO;
                cell.agreeBtn.titleLabel.font = [UIFont systemFontOfSize:11];
                
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:dict[@"msg"]];
            }
        } error:^(NSString *error) {
            [SVProgressHUD  dismiss];
        }];
    }
    if (model.state==2) {
        
    }
    if (model.state==3) {
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
