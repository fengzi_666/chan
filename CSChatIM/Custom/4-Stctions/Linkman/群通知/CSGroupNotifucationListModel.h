//
//  CSGroupNotifucationListModel.h
//  CSChatIM
//
//  Created by Apple on 2021/1/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGroupNotifucationListModel : NSObject
@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *message;
@property (nonatomic ,copy) NSString *nicheng;
@property (nonatomic ,copy) NSString *qunid;
@property (nonatomic ,copy) NSString *qunming;
@property (nonatomic ,copy) NSString *qunpic;
@property (nonatomic ,copy) NSString *shenqingdate;
@property (nonatomic ,assign) NSInteger state;
@property (nonatomic ,copy) NSString *usercode;
@property (nonatomic ,copy) NSString *userid;
@property (nonatomic ,copy) NSString *userphone;
@property (nonatomic ,copy) NSString *userpic;
@end

NS_ASSUME_NONNULL_END
