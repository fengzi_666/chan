//
//  CSGroupNotificationDetailsVC.h
//  CSChatIM
//
//  Created by Apple on 2020/12/7.
//

#import "BaseViewController.h"
#import "TUIGroupPendencyCellData.h"
#import "CSGroupNotifucationListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSGroupNotificationDetailsVC : BaseViewController
@property (nonatomic ,strong) TUIGroupPendencyCellData *data;
@property V2TIMUserFullInfo *userFullInfo;
@property (nonatomic ,strong) CSGroupNotifucationListModel *model;
@end

NS_ASSUME_NONNULL_END
