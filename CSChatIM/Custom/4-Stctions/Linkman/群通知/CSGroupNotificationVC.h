//
//  CSGroupNotificationVC.h
//  CSChatIM
//
//  Created by Apple on 2020/12/5.
//

#import "BaseViewController.h"
#import "TContactViewModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSGroupNotificationVC : BaseViewController
@property (nonatomic ,strong)TContactViewModel *model;
@end

NS_ASSUME_NONNULL_END
