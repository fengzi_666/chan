//
//  helloTabBarController.m
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#import "helloTabBarController.h"
#import "yujianniNavViewController.h"
#import "AppDelegate.h"
#import "CSMainViewController.h"
#import "xinxipingtaiViewController.h"
#import "CSLinkmanViewController.h"

#import "CSLoginViewController.h"
#import "TConversationListViewModel.h"
#import "CSShopViewController.h"
#import "CSLiveViewController.h"
@interface helloTabBarController ()<UITabBarControllerDelegate>
@property (nonatomic,weak)UIButton *plus;
@property (nonatomic,weak)UIImageView *text;
@property (nonatomic,weak)UIImageView *ablum;
@property (nonatomic,weak)UIImageView *camera;
@property (nonatomic,weak)UIImageView *sign;
@property (nonatomic,weak)UIImageView *comment;
@property (nonatomic,weak)UIImageView *more;
@property (nonatomic ,strong) CSLinkmanViewController *tool;

@property (nonatomic, strong) TConversationListViewModel *viewModel;

@end

@implementation helloTabBarController
+ (instancetype)instance{
    AppDelegate *delegete = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *vc = delegete.window.rootViewController;
    if ([vc isKindOfClass:[helloTabBarController class]]) {
        return (helloTabBarController *)vc;
    }else{
        return nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Do any additional setup after loading the view.
    [AppDelegate shareAppDelegate].suoyoudeNetworkDetection = [[suoyoudeNetworkDetection alloc] init];
    self.delegate = self;
    [self LoadNavController];
    
    
    //    [[NSNotificationCenter defaultCenter]
    //    addObserver:self selector:@selector(getNotificationAction:) name:@"ThisIsANoticafication" object:nil];
    _viewModel = [TConversationListViewModel new];
    __weak typeof(self) weakSelf = self;
    [RACObserve(self.viewModel, dataList) subscribeNext:^(id  _Nullable x) {
        
        
            
            [[V2TIMManager sharedInstance] getConversationList:0 count:INT_MAX succ:^(NSArray<V2TIMConversation *> *list, uint64_t lastTS, BOOL isFinished) {
                
                for (int i = 0; i<list.count; i++) {
                    V2TIMConversation *con = list[i];
                    
                    if ([con.showName isEqualToString:@"administrator"]) {
                        NSString *text =  con.lastMessage.textElem.text;
                        NSLog(@"%@",text);
                        if (text!=nil) {
                            NSData *jsonData = [text dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                            NSInteger qunnum =[dic[@"qunnum"] integerValue];
                            NSInteger geren =[dic[@"geren"] integerValue];
                            NSInteger num=   qunnum +geren;
                            if (num>0) {
                                [_tool.tabBarItem setBadgeValue:[NSString stringWithFormat:@"%lu",(unsigned long)num]];
                                
                            }
                            else
                            {
                                [weakSelf.tool.tabBarItem setBadgeValue:nil];
                            }
                        }
                       
                        
                    }
                    
                }
                
                
            } fail:^(int code, NSString *msg) {
                // 拉取会话列表失败
            }];
        
        
    }];
    
    
    [self loadRequestGPSData];
}

- (void)loadRequestGPSData
{
    NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
    if ([def objectForKey:@"lat"]==nil||[[def objectForKey:@"lat"] isEqual:@""]) {
        return;
    }
    NSDictionary *dics =@{
        @"lon":[def objectForKey:@"lon"]?:@"",
        @"lat":[def objectForKey:@"lat"]?:@"",
        @"shi":[def objectForKey:@"locCity"]?:@""
    };
    [zhengtaiHttpRequest SRPost:MDAPPURL_userUpdateWeiZhi view:self parameters:dics success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, id  _Nullable status) {
        
    } erronrous:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        
    }];
    
    [zhengtaiHttpRequest SRPost:MDAPPURL_homeStrikeUpAConversation view:self parameters:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, id  _Nullable status) {
        
    } erronrous:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        
    }];
}
- (void)getNotificationAction:(NSNotification *)notification{
    NSDictionary * infoDic = [notification object];
    // 这样就得到了我们在发送通知时候传入的字典了
    NSInteger qunnum =[infoDic[@"qunnum"] integerValue];
    NSInteger geren =[infoDic[@"geren"] integerValue];
    NSInteger num=   qunnum +geren;
    
    
}
- (void)LoadNavController
{
    
    xinxipingtaiViewController *home = [[xinxipingtaiViewController alloc]init];
    [self addChildViewController:home title:@"消息" image:@"消息" selImage:@"消息_select"];
    
    CSLinkmanViewController *tool = [[CSLinkmanViewController alloc] init];
    _tool = tool;
    [self addChildViewController:tool title:@"联系人" image:@"联系人" selImage:@"联系人_select"];
    
    CSLiveViewController *Strategy = [[CSLiveViewController alloc] init];
    [self addChildViewController:Strategy title:@"发现" image:@"发现" selImage:@"发发现_select"];
    
    CSMainViewController *main = [[CSMainViewController alloc] init];
    [self addChildViewController:main title:@"我的" image:@"我的" selImage:@"我的_select"];
   
    
}
#pragma mark - 添加子控制器
-(void)addChildViewController:(UIViewController *)childVc  title:(NSString *)title image:(NSString *)image selImage:(NSString *)selImage {
    
    //设置未选中的TabBarItem的字体颜色、大小
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    attrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#999999"];
    //设置选中了的TabBarItem的字体颜色、大小
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    selectedAttrs[NSForegroundColorAttributeName] =  [UIColor colorWithHexString:@"#32965A"];
    UITabBarItem *item = [UITabBarItem appearance];
    if (@available(iOS 13.0, *)) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor colorWithHexString:@"#999999"]];
    }
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    static NSInteger index = 0;
    //设置子控制器的TabBarButton属性
    childVc.tabBarItem.title = title;
    childVc.tabBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.tag = index;
    index++;
    //让子控制器包装一个导航控制器
    yujianniNavViewController *nav = [[yujianniNavViewController alloc]initWithRootViewController:childVc];
    [self addChildViewController:nav];
}

+ (void)initialize
{
    
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSLog(@"点击的item:%ld title:%@", item.tag, item.title);
    //    tabBar.backgroundColor = [UIColor redColor];
}
#pragma mark - 按钮出来动画
-(UIImageView *)btnAnimateWithFrame:(CGRect)frame imageName:(NSString *)imageName text:(NSString *)text animateFrame:(CGRect)aniFrame delay:(CGFloat)delay {
    UIImageView *btnContainer = [[UIImageView alloc]init];
    btnContainer.frame  = frame;
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    [btnContainer addSubview:image];
    UILabel *word = [[UILabel alloc]initWithFrame:CGRectMake(0, 75, 71, 25)];
    [word setText:text];
    [word setTextAlignment:NSTextAlignmentCenter];
    [word setFont:[UIFont systemFontOfSize:15]];
    [word setTextColor:[UIColor grayColor]];
    [btnContainer addSubview:word];
    
    [UIView animateWithDuration:0.5 delay:delay usingSpringWithDamping:0.6 initialSpringVelocity:0.05 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        btnContainer.frame  = aniFrame;
    } completion:^(BOOL finished) {
    }];
    return btnContainer;
}
//设置按钮方法
-(void)setAction:(UIImageView *)imageView action:(SEL)action{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:action];
    imageView.userInteractionEnabled = YES;
    [imageView addGestureRecognizer:gesture];
}
#pragma mark - 消失
-(void)tapAction:(UITapGestureRecognizer *)tap{
    [self closeClick];
}
//跳转
-(void)compose {
    [self closeClick];
}
#pragma mark - 关闭动画
-(void)btnCloseAnimateWithFrame:(CGRect)rect delay:(CGFloat)delay btnView:(UIImageView *)btnView{
    [UIView animateWithDuration:0.3 delay:delay usingSpringWithDamping:0.6 initialSpringVelocity:0.05 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        btnView.frame  = rect;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - 关闭按钮
-(void)closeClick {
    
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    //判断用户是否登陆。token无效
    if (![LFUserManager sharedInstance].isUserLogin ) {
        NSLog(@"%@",viewController.tabBarItem.title);
        NSString *titles =viewController.tabBarItem.title;
        //        if ([titles isEqualToString:@"我的"]) {
        ////            CSLoginViewController *vc = [CSLoginViewController new];
        ////            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
        ////            [self presentViewController:nav animated:YES completion:nil];
        //            //这里的NO是关键,如果是这个tabBarItem,就不要让他点击进去
        //            return NO;
        //        }
        
        
    }
    
    //    [viewController.navigationController popToViewController:[viewController.navigationController.viewControllers objectAtIndex:0] animated:YES];
    //    [self setupUnreadSocialCount];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
