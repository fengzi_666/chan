//
//  helloTabBarController.h
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface helloTabBarController : UITabBarController
+ (instancetype)instance;
@property (nonatomic ,strong) UIView *stateBar;
@end
