//
//  BaseViewController.m
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/6.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#import "BaseViewController.h"
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>

@interface BaseViewController ()


@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    

    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav白色"] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNav
{
//    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
//    UINavigationBar *navigationBar = self.navigationController.navigationBar;
//    [navigationBar setBackgroundImage:[UIImage imageNamed:@"nav白色"] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//     //此处使底部线条失效
//     [navigationBar setShadowImage:[UIImage new]];
    
    
   
    [self setBackButtonItemWhite];

}
//-(void)setRedNav
//{
////    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//
////    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
////     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
//
//
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    UIColor *color = [UIColor colorWithHexString:@"#ffffff"];
//
//   [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed:@"nav.png"] forBarMetrics:UIBarMetricsDefault];
////    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"#07c3b0"];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17], NSForegroundColorAttributeName:color}] ;
//    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithHexString:@"#07c3b0"]];;
//
//    [self setBackButtonItemWhite];
//
//
//}
//设置navigationBar样式，白色
- (void)setBackButtonItemWhite {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(backBarButtonClick)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    }
-(void)backBarButtonClick{
    [self.navigationController popViewControllerAnimated:YES ];
}


@end
