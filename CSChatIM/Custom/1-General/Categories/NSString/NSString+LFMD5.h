//
//  NSString+LFMD5.h
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LFMD5)
/**
 *  字符串的MD5处理
 *
 *  @return MD5处理后的字符串
 */
- (NSString *)LFMD5Digest;

@end
