//
//  NSString+LFAdd.h
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LFAdd)

//如果是@"",null,(null),NULL,(NULL),就返回NO
+(BOOL)isStringEffective:(NSString *)str;
//nsdata转换字符串
+ (NSString *)hexStringFromString:(NSData *)data;
//转换钱保留两位 0.00
+ (NSString *)formatDecimalNumber:(NSString *)string;
//时间戳转换时间
+ (NSString *)dateToString:(NSString *)date;
//非空判断
+ (BOOL) isBlankString:(NSString *)string ;
@end
