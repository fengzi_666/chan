//
//  NSString+LFAdd.m
//  LiveFitness
//
//  Created by Lucas on 16/8/4.
//  Copyright © 2016年 bluemobi. All rights reserved.
//

#import "NSString+LFAdd.h"

@implementation NSString (LFAdd)

//判断字符串是否有效
+(BOOL)isStringEffective:(NSString *)str
{
    BOOL retVal = YES;
    if ([str isKindOfClass:[NSNull class]]) {
        return NO;
    }
    if([str isEqualToString:@"null"]||[str isEqualToString:@"(null)"]||[str isEqualToString:@""]||[str isEqualToString:@"NULL"]||[str isEqualToString:@"(NULL)"]||!str){
        retVal = NO;
    }
    return retVal;
}
+ (NSString *)hexStringFromString:(NSData *)data{
    Byte *bytes = (Byte *)[data bytes];
    //下面是Byte转换为16进制。
    NSString *hexStr=@"";
    for(int i=0;i<[data length];i++){
        NSString *newHexStr = [NSString stringWithFormat:@"%x",bytes[i]&0xff];///16进制数
        if([newHexStr length]==1)
            hexStr = [NSString stringWithFormat:@"%@0%@",hexStr,newHexStr];
        else
            hexStr = [NSString stringWithFormat:@"%@%@",hexStr,newHexStr];
    }
    return hexStr;
}
+ (NSString *)formatDecimalNumber:(NSString *)string {
 if (!string || string.length == 0) {
 return string;
 }
 
 NSNumber *number = @([string doubleValue]);
 NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
 formatter.numberStyle = kCFNumberFormatterDecimalStyle;
 formatter.positiveFormat = @"0.00";
 
 NSString *amountString = [formatter stringFromNumber:number];
 return amountString;
}
+ (NSString *)dateToString:(NSString *)date {
   // 初始化时间格式控制器
   NSDateFormatter *matter = [[NSDateFormatter alloc] init];
   // 设置设计格式
   [matter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
   // 进行转换
   NSTimeInterval time = [date doubleValue] + 28800;
   NSDate * Date = [NSDate dateWithTimeIntervalSince1970:time];
   NSString *dateStr = [matter stringFromDate:Date];
   return dateStr;
}
+ (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}
@end
