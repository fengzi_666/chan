//
//  UITableView+WFEmpty.m
//  WFEmptyTableView
//
//  Created by wanawt on 2016/11/11.
//  Copyright © 2016年 wanawt. All rights reserved.
//

#import "UITableView+WFEmpty.h"
#import <objc/runtime.h>

static char UITableViewEmptyView;

@implementation UITableView (WFEmpty)

@dynamic emptyView;

- (UIView *)emptyView
{
    return objc_getAssociatedObject(self, &UITableViewEmptyView);
}

- (void)setEmptyView:(UIView *)emptyView
{
    [self willChangeValueForKey:@"HJEmptyView"];
    objc_setAssociatedObject(self, &UITableViewEmptyView,
                             emptyView,
                             OBJC_ASSOCIATION_ASSIGN);
    [self didChangeValueForKey:@"HJEmptyView"];
}
- (void)addTitleEmptyViewWithImageName:(NSString *)title button:(NSString *)tapTitel
{
    if (!self.emptyView)
    {
        CGRect frame = CGRectMake(0,30, self.frame.size.width, self.frame.size.height);
       
        NSString* text = title;
        
        UIView* noMessageView = [[UIView alloc] initWithFrame:frame];
        noMessageView.backgroundColor = [UIColor clearColor];
        [self addSubview:noMessageView];
    
        UILabel *noInfoLabel = [[UILabel alloc] init];
        noInfoLabel.textAlignment = NSTextAlignmentCenter;
        noInfoLabel.textColor = [UIColor lightGrayColor];
        noInfoLabel.text = text;
        noInfoLabel.backgroundColor = [UIColor clearColor];
        noInfoLabel.font = [UIFont systemFontOfSize:14];
        [noMessageView addSubview:noInfoLabel];
        [noInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX).offset(-30);
            make.centerY.mas_equalTo(self.mas_centerY).offset(-100);
            make.height.mas_equalTo(14);
        }];
        UILabel *noInfoLabel1 = [[UILabel alloc] init];
        noInfoLabel1.textAlignment = NSTextAlignmentCenter;
        noInfoLabel1.textColor = [UIColor colorWithHexString:@"#1bc8b5"];
        noInfoLabel1.text = tapTitel;
        noInfoLabel1.backgroundColor = [UIColor clearColor];
        noInfoLabel1.font = [UIFont systemFontOfSize:14];
        NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:noInfoLabel1.text attributes:attribtDic];
        noInfoLabel1.attributedText = attribtStr;
        [noMessageView addSubview:noInfoLabel1];
        [noInfoLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(noInfoLabel.mas_right).offset(10);
            make.bottom.mas_equalTo(noInfoLabel.mas_bottom);
            make.height.mas_equalTo(15);
        }];
        UITapGestureRecognizer *backImage1Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backImage1TapClick:)];
        noInfoLabel1.userInteractionEnabled = YES;
        [noInfoLabel1 addGestureRecognizer:backImage1Tap];
        
        self.emptyView = noMessageView;
        self.emptyView.userInteractionEnabled = YES;
    }
}
- (void)TXaddTitleEmptyViewWithImageName:(NSString *)title img:(NSString *)image button:(NSString *)tapTitel
{
    if (!self.emptyView)
    {
        CGRect frame = CGRectMake(0,30, self.frame.size.width, self.frame.size.height);
       
        NSString* text = title;
        
        UIView* noMessageView = [[UIView alloc] initWithFrame:frame];
        noMessageView.backgroundColor = [UIColor clearColor];
        [self addSubview:noMessageView];
        
          UIImageView *carImageView = [[UIImageView alloc] init];
           [carImageView setImage:[UIImage imageNamed:image]];
           [noMessageView addSubview:carImageView];
        [carImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.top.mas_equalTo(noMessageView.mas_top).offset(172);
                   make.centerX.mas_equalTo(noMessageView.mas_centerX);
                   make.left.mas_equalTo(noMessageView.mas_left);
                   make.left.mas_equalTo(noMessageView.mas_right);
               }];
        
        UILabel *noInfoLabel = [[UILabel alloc] init];
        noInfoLabel.textAlignment = NSTextAlignmentCenter;
        noInfoLabel.textColor = [UIColor lightGrayColor];
        noInfoLabel.text = text;
        noInfoLabel.backgroundColor = [UIColor colorWithHexString:@"#656565"];
        noInfoLabel.font = [UIFont systemFontOfSize:14];
        [noMessageView addSubview:noInfoLabel];
        [noInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(carImageView.mas_bottom).offset(-17);
            make.centerX.mas_equalTo(carImageView.mas_centerX);
        }];
        
        
       
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:tapTitel forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        btn.layer.borderWidth=1;
        btn.layer.borderColor = [UIColor redColor].CGColor;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [noMessageView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(carImageView.mas_bottom).offset(116);
            make.centerX.mas_equalTo(carImageView.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(280, 47));
        }];
       
        
        self.emptyView = noMessageView;
        self.emptyView.userInteractionEnabled = YES;
    }
}
- (void)btnClick: (UIButton *)sender
{
    UIButton *btn = sender;
    NSLog(@"1213123");
    self.str=@"111";
    if ([self.delegater respondsToSelector:@selector(HitTapClick:)]) {
        [self.delegater HitTapClick:self.str];
    }
    
    
}
-(void)addNewEmptyViewWithImageName:(NSString*)imageName title:(NSString*)title button:(NSString*)tapTitel
{
    if (!self.emptyView)
    {
        CGRect frame = CGRectMake(0,30, self.frame.size.width, self.frame.size.height);
        UIImage* image = [UIImage imageNamed:imageName];
        NSString* text = title;
        
        UIView* noMessageView = [[UIView alloc] initWithFrame:frame];
        noMessageView.backgroundColor = [UIColor clearColor];
        [self addSubview:noMessageView];
        UIImageView *carImageView = [[UIImageView alloc] init];
        [carImageView setImage:image];
        [noMessageView addSubview:carImageView];
        
        [carImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(noMessageView.mas_top).offset(100);
            make.centerX.mas_equalTo(noMessageView.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(180, 155));
        }];
        
        UILabel *noInfoLabel = [[UILabel alloc] init];
        noInfoLabel.textAlignment = NSTextAlignmentCenter;
        noInfoLabel.textColor = [UIColor lightGrayColor];
        noInfoLabel.text = text;
        noInfoLabel.backgroundColor = [UIColor clearColor];
        noInfoLabel.font = [UIFont systemFontOfSize:14];
        [noMessageView addSubview:noInfoLabel];
        [noInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(carImageView.mas_bottom).offset(40);
            make.centerX.mas_equalTo(carImageView.mas_centerX).offset(-25);
            make.height.mas_equalTo(15);
        }];
        UILabel *noInfoLabel1 = [[UILabel alloc] init];
        noInfoLabel1.textAlignment = NSTextAlignmentCenter;
        noInfoLabel1.textColor = [UIColor lightGrayColor];
        noInfoLabel1.text = tapTitel;
        noInfoLabel1.backgroundColor = [UIColor clearColor];
        noInfoLabel1.font = [UIFont systemFontOfSize:14];
        [noMessageView addSubview:noInfoLabel1];
        [noInfoLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(noInfoLabel.mas_right).offset(10);
            make.bottom.mas_equalTo(noInfoLabel.mas_bottom);
            make.height.mas_equalTo(15);
        }];
        
        
        self.emptyView = noMessageView;
        self.emptyView.userInteractionEnabled = YES;
    }
}
-(void)addEmptyViewWithImageName:(NSString*)imageName title:(NSString*)title
{
    if (!self.emptyView)
    {
        CGRect frame = CGRectMake(0,30, self.frame.size.width, self.frame.size.height);
        UIImage* image = [UIImage imageNamed:imageName];
        NSString* text = title;
        
        UIView* noMessageView = [[UIView alloc] initWithFrame:frame];
        noMessageView.backgroundColor = [UIColor clearColor];
        noMessageView.userInteractionEnabled = YES;
         [self addSubview:noMessageView];
        UIImageView *carImageView = [[UIImageView alloc] init];
        [carImageView setImage:image];
        carImageView.userInteractionEnabled = YES;
        [noMessageView addSubview:carImageView];
       
        [carImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.mas_equalTo(noMessageView.mas_centerY);
            make.top.mas_equalTo(noMessageView.mas_top).offset(100);
            make.centerX.mas_equalTo(noMessageView.mas_centerX);
//            make.size.mas_equalTo(CGSizeMake(180, 155));
        }];
        
        UILabel *noInfoLabel = [[UILabel alloc] init];
        noInfoLabel.textAlignment = NSTextAlignmentCenter;
        noInfoLabel.textColor = [UIColor lightGrayColor];
        noInfoLabel.text = text;
        noInfoLabel.backgroundColor = [UIColor clearColor];
        noInfoLabel.font = [UIFont systemFontOfSize:14];
        [noMessageView addSubview:noInfoLabel];
        [noInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(carImageView.mas_bottom).offset(40);
            make.centerX.mas_equalTo(carImageView.mas_centerX);
            make.height.mas_equalTo(15);
        }];
     
        
        self.emptyView = noMessageView;
        self.emptyView.userInteractionEnabled = YES;
    }
    
}
-(void)addEmptyTalebleViewImageName:(NSString*)imageName title:(NSString*)title
{
    if (!self.emptyView)
        {
            CGRect frame = CGRectMake(0,30, self.frame.size.width, self.frame.size.height);
            UIImage* image = [UIImage imageNamed:imageName];
            NSString* text = title;
            
            UIView* noMessageView = [[UIView alloc] initWithFrame:frame];
            noMessageView.backgroundColor = [UIColor clearColor];
            noMessageView.userInteractionEnabled = YES;
             [self addSubview:noMessageView];
            UIImageView *carImageView = [[UIImageView alloc] init];
            [carImageView setImage:image];
            carImageView.userInteractionEnabled = YES;
            [noMessageView addSubview:carImageView];
           
            [carImageView mas_makeConstraints:^(MASConstraintMaker *make) {
    //            make.centerY.mas_equalTo(noMessageView.mas_centerY);
                make.top.mas_equalTo(noMessageView.mas_top).offset(100);
                make.centerX.mas_equalTo(noMessageView.mas_centerX);
    //            make.size.mas_equalTo(CGSizeMake(180, 155));
            }];
            
            UILabel *noInfoLabel = [[UILabel alloc] init];
            noInfoLabel.textAlignment = NSTextAlignmentCenter;
            noInfoLabel.textColor = [UIColor lightGrayColor];
            noInfoLabel.text = text;
            noInfoLabel.backgroundColor = [UIColor clearColor];
            noInfoLabel.font = [UIFont systemFontOfSize:14];
            [noMessageView addSubview:noInfoLabel];
            [noInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(carImageView.mas_bottom);
                make.centerX.mas_equalTo(carImageView.mas_centerX);
                make.height.mas_equalTo(15);
            }];
         
            
            self.emptyView = noMessageView;
            self.emptyView.userInteractionEnabled = YES;
        }
}
-(void)addEmptyTalebleViewImageName1:(NSString*)imageName title:(NSString*)title centenY:(int)y frame:(CGRect)frame
{
    if (!self.emptyView)
        {
            
            UIImage* image = [UIImage imageNamed:imageName];
            NSString* text = title;
            
            UIView* noMessageView = [[UIView alloc] initWithFrame:frame];
            noMessageView.backgroundColor = [UIColor clearColor];
            noMessageView.userInteractionEnabled = YES;
             [self addSubview:noMessageView];
            UIImageView *carImageView = [[UIImageView alloc] init];
            [carImageView setImage:image];
            carImageView.userInteractionEnabled = YES;
            [noMessageView addSubview:carImageView];
           
            [carImageView mas_makeConstraints:^(MASConstraintMaker *make) {
    //            make.centerY.mas_equalTo(noMessageView.mas_centerY);
//                make.top.mas_equalTo(noMessageView.mas_top).offset(100);
                make.centerX.mas_equalTo(noMessageView.mas_centerX);
                make.centerY.mas_equalTo(noMessageView.mas_centerY).offset(y);
    //            make.size.mas_equalTo(CGSizeMake(180, 155));
            }];
            
            UILabel *noInfoLabel = [[UILabel alloc] init];
            noInfoLabel.textAlignment = NSTextAlignmentCenter;
            noInfoLabel.textColor = [UIColor lightGrayColor];
            noInfoLabel.text = text;
            noInfoLabel.backgroundColor = [UIColor clearColor];
            noInfoLabel.font = [UIFont systemFontOfSize:14];
            [noMessageView addSubview:noInfoLabel];
            [noInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(carImageView.mas_bottom);
                make.centerX.mas_equalTo(carImageView.mas_centerX);
                make.height.mas_equalTo(15);
            }];
         
            
            self.emptyView = noMessageView;
            self.emptyView.userInteractionEnabled = YES;
        }
}
@end
