//
//  UITableView+WFEmpty.h
//  WFEmptyTableView
//
//  Created by wanawt on 2016/11/11.
//  Copyright © 2016年 wanawt. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UITableView;
@protocol HitTapClickDelegate <NSObject>
@optional
- (void)HitTapClick:(NSString *)sender;

@end
@interface UITableView (WFEmpty)

@property (nonatomic, strong, readonly) UIView *emptyView;
@property (nonatomic,weak)id<HitTapClickDelegate>delegater;

@property (nonatomic ,strong) NSString *str;

-(void)addEmptyViewWithImageName:(NSString*)imageName title:(NSString*)title;


-(void)addEmptyTalebleViewImageName:(NSString*)imageName title:(NSString*)title;
-(void)addEmptyTalebleViewImageName1:(NSString*)imageName title:(NSString*)title centenY:(int)y frame:(CGRect)frame;

-(void)addNewEmptyViewWithImageName:(NSString*)imageName title:(NSString*)title button:(NSString*)tapTitel;

-(void)addTitleEmptyViewWithImageName:(NSString*)title button:(NSString*)tapTitel;

-(void)TXaddTitleEmptyViewWithImageName:(NSString*)title img:(NSString *)image button:(NSString*)tapTitel;
@end
