//
//  longwangRequestManage.m
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/21.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#import "longwangRequestManage.h"
#import "YHUtils.h"
#import "AppDelegate.h"
#import "CSLoginViewController.h"
#import "yujianniNavViewController.h"
#import "AppDelegate.h"
#import "NSString+LFMD5.h"

static  longwangRequestManage *_service;

@interface  longwangRequestManage()
@property (nonatomic ,strong) NSMutableString *md5Str;
@end
@implementation longwangRequestManage
+(id)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _service = [[self alloc] init];
    });
    return _service;
}
-(void)manager
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
}
//网络状态error信息
- (NSString *)errorStaetCodeResponse:(NSURLSessionDataTask* )task
{
    NSHTTPURLResponse *respinse = (NSHTTPURLResponse *)task.response;
    NSInteger statusCode = respinse.statusCode;

    if (statusCode == 404) {
         [SVProgressHUD dismiss];
        return @"服务器资源缺失";
    }else if (statusCode == 500){
        return @"服务器异常";
    }
    else if (statusCode == 400){
        return @"服务器异常";
    }
    else if (statusCode == 401){
        return @"服务器异常";
    }
    else if (statusCode == 403){
        return @"服务器异常";
    }
    else if (statusCode == 404){
        return @"服务器异常";
    }
    else if (statusCode == 1011){

        return @"服务器异常";
    }
    return @"服务器异常";

}

-(void)DataGETURL:(NSString *)url params:(NSDictionary *)params CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
{
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // 网络异常判断 开始修改时，判断网络情况
    BOOL isBadNet = [AFNetworkReachabilityManager sharedManager].isReachable;
    NSLog(@"当前网络状态：%@", (isBadNet ? @"不可用" : @"可用"));
    if (isBadNet)
    {
        NSLog(@"网络断了哦，请检查网络再试");
       
    }
    NSLog(@"---请求参数%@",params);
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:url parameters:params headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:NULL];
        NSLog(@"%@",dict);
        aBlock(dict);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (aError) {
            NSString *errorString = [self errorStaetCodeResponse:task];
            aError(errorString);
            [SVProgressHUD showErrorWithStatus:@"网络异常"];
        }
    }];
    
}
-(void)DataPOSTURL:(NSString *)url params:(NSDictionary *)params CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
{
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if ([AppDelegate shareAppDelegate].suoyoudeNetworkDetection.isConnectionAvailable == NO) {
        NSLog(@"~~~~~~~网络异常请检查网络");
        return;
    }
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    //设置请求超时时间/
    manager.requestSerializer.timeoutInterval = 10.0f;
    
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    

    [manager POST:url parameters:params headers:nil  progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"%@",responseObject);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:NULL];
        NSLog(@"%@",dict);
        aBlock(dict);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----网络请求错误%@",error);
        if (aError) {
            NSString *errorString = [self errorStaetCodeResponse:task];
            aError(errorString);
            [SVProgressHUD showErrorWithStatus:@"网络超时！"];
        }
    }];
    
}
#pragma mark-- 注册发送验证码
- (void)LoadRequestSendRegisterSMS:(NSString *)loginname   CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
     [params setValue:loginname  forKey:@"loginname"];

    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sendRegisterSMS];
    [self DataPOSTURL:urlString params:params CompletionBlock:aBlock error:aError];
}

//- (void)LoadDataversionshow:(NSString *)appType   CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:appType  forKey:@"appType"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_versionshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//- (void)LoadDataFindSchoolOffset:(NSString *)regionId fname:(NSString *)fname  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//     [params setValue:regionId  forKey:@"regionId"];
//    [params setValue:fname  forKey:@"fname"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lschoolIndex];
//
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}

+(void)postBossDemoWithUrl:(NSString*)url param:(NSString*)param  success:(void(^)(NSDictionary *dict))success fail:(void (^)(NSError *error))fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];//不设置会报-1016或者会有编码问题
     manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
    manager.responseSerializer = [AFHTTPResponseSerializer serializer]; //不设置会报 error 3840
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json",@"text/json", @"text/javascript",@"text/html",@"text/plain",nil]];
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url  parameters:nil error: nil];
     [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     NSData *body = [param dataUsingEncoding:NSUTF8StringEncoding];
     [request setHTTPBody:body];
     //发起请求
    [manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
            
        } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
            
        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            success(dic);
        }];
    
    
   
}
+(void)postBossDemoWithUrl1:(NSString*)url param:(NSString*)param  token:(NSString *)token success:(void(^)(NSDictionary *dict))success fail:(void (^)(NSError *error))fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];//不设置会报-1016或者会有编码问题
     manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
    manager.responseSerializer = [AFHTTPResponseSerializer serializer]; //不设置会报 error 3840
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json",@"text/json", @"text/javascript",@"text/html",@"text/plain",nil]];
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url  parameters:nil error: nil];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:token forHTTPHeaderField:@"x-auth-token"];
     NSData *body = [param dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:body];
    //发起请求
   [manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
           
       } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
           
       } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
           NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
           success(dic);
       }];
    
    
   
}



+(void)postBaseWithURL1:(NSString *)url param:(NSDictionary *)param requestHead:(NSString *)requestHead success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
       manager.requestSerializer.timeoutInterval = 20;
       manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
   
        [manager.requestSerializer setValue:requestHead forHTTPHeaderField:@"PhoneCode"];
    
    
      NSMutableArray *Arr= [[NSMutableArray alloc]init];
      NSString *time = [self getCurrentTimestamp];
      NSString *appstr= [NSString stringWithFormat:@"jyz_app8x72141i285f81e58a6c13x85a9eaa58%@",time];
      NSString *aoostrMd5 = [appstr LFMD5Digest];
      [Arr addObject:[NSString stringWithFormat:@"1=%@",time]];
      [manager.requestSerializer setValue:time forHTTPHeaderField:@"timeStamp"];
      [manager.requestSerializer setValue:aoostrMd5 forHTTPHeaderField:@"signature"];
      NSMutableDictionary *md5dic= [[NSMutableDictionary alloc]init];
      [md5dic setValue:time forKey:@"1"];
      [param enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
          NSString *string=@"";//结果字符串
          NSString *string1=key;
          NSString *string2=obj;//已存在的字符串，需要将string1和string2连接起来
          string =  [string stringByAppendingFormat:@"%@=%@" ,string1, string2];
          [Arr addObject:string];
          [md5dic setValue:obj forKey:key];
          

      }];
      if (Arr.count >0) {
          
          NSArray *result = [Arr sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
              NSLog(@"%@~%@",obj1,obj2); // 3~4 2~1 3~1 3~2
              return [obj1 compare:obj2]; // 升序
          }];
          NSMutableArray *arr1 = [[NSMutableArray alloc]init];
          for (int i =0; i<result.count; i++) {
              NSString *str = result[i];
              [arr1 addObject:str];
          }
          [arr1 addObject:@"key=x5df2b4x5s32c5f6"];
        
          NSString *text = [arr1 componentsJoinedByString:@"&"];
          NSString *str =  [text LFMD5Digest];

          [manager.requestSerializer setValue:str forHTTPHeaderField:@"signKey"];
      }
      else
      {
          [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"signKey"];
      }
       [manager POST:url parameters:md5dic headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
           
       } progress:^(NSProgress * _Nonnull uploadProgress) {
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
          
           NSLog(@"上传成功------%@",responseObject);
           success(responseObject);
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           NSLog(@"----网络请求错误%@",error);
           if (aError) {
//               NSString *errorStr =[self errorStaetCodeResponse:task];
//               NSString *errorString = [self errorStaetCodeResponse:task];
//               aError(errorString);
//               [SVProgressHUD showErrorWithStatus:@"网络异常"];
              
               NSLog(@"上传失败------%@",error);
           }
           
       }];
}
+(void)postBaseWithURL:(NSString *)url param:(NSDictionary *)param  success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    if (token.length!=0) {
        [manager.requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    }
   
  
    NSMutableArray *Arr= [[NSMutableArray alloc]init];
    NSString *time = [self getCurrentTimestamp];
    NSString *appstr= [NSString stringWithFormat:@"jyz_app8x72141i285f81e58a6c13x85a9eaa58%@",time];
    NSString *aoostrMd5 = [appstr LFMD5Digest];
    [Arr addObject:[NSString stringWithFormat:@"1=%@",time]];
    [manager.requestSerializer setValue:time forHTTPHeaderField:@"timeStamp"];
    [manager.requestSerializer setValue:aoostrMd5 forHTTPHeaderField:@"signature"];
    NSMutableDictionary *md5dic= [[NSMutableDictionary alloc]init];
    [md5dic setValue:time forKey:@"1"];
    [param enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        NSString *string=@"";//结果字符串
        NSString *string1=key;
        NSString *string2=obj;//已存在的字符串，需要将string1和string2连接起来
        string =  [string stringByAppendingFormat:@"%@=%@" ,string1, string2];
        [Arr addObject:string];
        [md5dic setValue:obj forKey:key];
        

    }];
    if (Arr.count >0) {
        
        NSArray *result = [Arr sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
            NSLog(@"%@~%@",obj1,obj2); // 3~4 2~1 3~1 3~2
            return [obj1 compare:obj2]; // 升序
        }];
        NSMutableArray *arr1 = [[NSMutableArray alloc]init];
        for (int i =0; i<result.count; i++) {
            NSString *str = result[i];
            [arr1 addObject:str];
        }
        [arr1 addObject:@"key=x5df2b4x5s32c5f6"];
      
        NSString *text = [arr1 componentsJoinedByString:@"&"];
        NSString *str =  [text LFMD5Digest];

        [manager.requestSerializer setValue:str forHTTPHeaderField:@"signKey"];
    }
    else
    {
        [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"signKey"];
    }
//
    [manager POST:url parameters:md5dic headers:nil
constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
          
           NSLog(@"上传成功------%@",responseObject);
           NSString *code = [NSString stringWithFormat: @"%@",responseObject[@"code"]];
           if ([code isEqualToString:@"1001"]) {
               [SVProgressHUD showInfoWithStatus:responseObject[@"msg"]];
               [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
           }
           success(responseObject);
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           NSLog(@"----网络请求错误%@",error);
           if (aError) {
//               NSString *errorStr =[self errorStaetCodeResponse:task];
//               NSString *errorString = [self errorStaetCodeResponse:task];
//               aError(errorString);
               [SVProgressHUD showErrorWithStatus:@"网络异常,请检查网络"];
              
               NSLog(@"上传失败------%@",error);
           }
           
       }];
}

//获取当前时间的时间戳
+(NSString *)getCurrentTimestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0]; // 获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970]*1000;// *1000 是精确到毫秒(13位),不乘就是精确到秒(10位)
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

#pragma mark -- 发送红包
+(void)postBaseRedWithURL:(NSString *)url param:(NSDictionary *)param uuid:(NSString *)uuid success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.requestSerializer.timeoutInterval = 20;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    if (token.length!=0) {
        [manager.requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    }
  
      NSMutableArray *Arr= [[NSMutableArray alloc]init];
      NSString *time = [self getCurrentTimestamp];
      NSString *appstr= [NSString stringWithFormat:@"jyz_app8x72141i285f81e58a6c13x85a9eaa58%@",time];
      NSString *aoostrMd5 = [appstr LFMD5Digest];
      [Arr addObject:[NSString stringWithFormat:@"1=%@",time]];
      [manager.requestSerializer setValue:time forHTTPHeaderField:@"timeStamp"];
      [manager.requestSerializer setValue:aoostrMd5 forHTTPHeaderField:@"signature"];
      [manager.requestSerializer setValue:uuid forHTTPHeaderField:@"PhoneCode"];
      NSMutableDictionary *md5dic= [[NSMutableDictionary alloc]init];
      [md5dic setValue:time forKey:@"1"];
      [param enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
          NSString *string=@"";//结果字符串
          NSString *string1=key;
          NSString *string2=obj;//已存在的字符串，需要将string1和string2连接起来
          string =  [string stringByAppendingFormat:@"%@=%@" ,string1, string2];
          [Arr addObject:string];
          [md5dic setValue:obj forKey:key];
          

      }];
      if (Arr.count >0) {
          NSArray *result = [Arr sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
              NSLog(@"%@~%@",obj1,obj2); // 3~4 2~1 3~1 3~2
              return [obj1 compare:obj2]; // 升序
          }];
          NSMutableArray *arr1 = [[NSMutableArray alloc]init];
          for (int i =0; i<result.count; i++) {
              NSString *str = result[i];
              [arr1 addObject:str];
          }
          [arr1 addObject:@"key=x5df2b4x5s32c5f6"];
        
          NSString *text = [arr1 componentsJoinedByString:@"&"];
          NSString *str =  [text LFMD5Digest];

          [manager.requestSerializer setValue:str forHTTPHeaderField:@"signKey"];
      }
      else
      {
          [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"signKey"];
      }
//
    [manager POST:url parameters:md5dic headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
          
           NSLog(@"上传成功------%@",responseObject);
           NSString *code = [NSString stringWithFormat: @"%@",responseObject[@"code"]];
           if ([code isEqualToString:@"1001"]) {
               [SVProgressHUD showInfoWithStatus:responseObject[@"msg"]];
               [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
           }
           success(responseObject);
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           NSLog(@"----网络请求错误%@",error);
           if (aError) {
//               NSString *errorStr =[self errorStaetCodeResponse:task];
//               NSString *errorString = [self errorStaetCodeResponse:task];
//               aError(errorString);
//               [SVProgressHUD showErrorWithStatus:@"网络异常"];
              
               NSLog(@"上传失败------%@",error);
           }
           
       }];
}
+ (void)postBaseImageWithURL:(NSString *)url param:(NSDictionary *)param image:(UIImage *)image success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = 20;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"multipart/form-data", @"application/json", @"text/html", @"image/jpeg", @"image/png", @"application/octet-stream", @"text/json", nil];
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    
    
    
      NSMutableArray *Arr= [[NSMutableArray alloc]init];
      NSString *time = [self getCurrentTimestamp];
      NSString *appstr= [NSString stringWithFormat:@"jyz_app8x72141i285f81e58a6c13x85a9eaa58%@",time];
      NSString *aoostrMd5 = [appstr LFMD5Digest];
      [Arr addObject:[NSString stringWithFormat:@"1=%@",time]];
      [manager.requestSerializer setValue:time forHTTPHeaderField:@"timeStamp"];
      [manager.requestSerializer setValue:aoostrMd5 forHTTPHeaderField:@"signature"];
      NSMutableDictionary *md5dic= [[NSMutableDictionary alloc]init];
      [md5dic setValue:time forKey:@"1"];
      [param enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
          NSString *string=@"";//结果字符串
          NSString *string1=key;
          NSString *string2=obj;//已存在的字符串，需要将string1和string2连接起来
          string =  [string stringByAppendingFormat:@"%@=%@" ,string1, string2];
          [Arr addObject:string];
          [md5dic setValue:obj forKey:key];
          

      }];
      if (Arr.count >0) {
          
          NSArray *result = [Arr sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
              NSLog(@"%@~%@",obj1,obj2); // 3~4 2~1 3~1 3~2
              return [obj1 compare:obj2]; // 升序
          }];
          NSMutableArray *arr1 = [[NSMutableArray alloc]init];
          for (int i =0; i<result.count; i++) {
              NSString *str = result[i];
              [arr1 addObject:str];
          }
          [arr1 addObject:@"key=x5df2b4x5s32c5f6"];
        
          NSString *text = [arr1 componentsJoinedByString:@"&"];
          NSString *str =  [text LFMD5Digest];

          [manager.requestSerializer setValue:str forHTTPHeaderField:@"signKey"];
      }
      else
      {
          [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"signKey"];
      }
    [manager POST:url parameters:md5dic headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        [formData appendPartWithFileData :imageData name:@"touxiang" fileName:@"Filedate.png" mimeType:@"image/png"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSLog(@"上传成功------%@",responseObject);
        NSString *code = [NSString stringWithFormat: @"%@",responseObject[@"code"]];
        if ([code isEqualToString:@"1001"]) {
            [SVProgressHUD showInfoWithStatus:responseObject[@"msg"]];
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        }
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----网络请求错误%@",error);
        if (aError) {
            //               NSString *errorStr =[self errorStaetCodeResponse:task];
            //               NSString *errorString = [self errorStaetCodeResponse:task];
            //               aError(errorString);
            //               [SVProgressHUD showErrorWithStatus:@"网络异常"];
            
            NSLog(@"上传失败------%@",error);
        }
        
    }];
}




//#pragma mark-- 获取学校详情
//- (void)LoadDatalschoolshow:(NSString *)school  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:school  forKey:@"id"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lschoolshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取直播列表
//- (void)LoadDataFindLivemxOffset:(NSString *)offset max:(NSString *)max liveId:(NSString *)liveid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:offset  forKey:@"offset"];
//    [params setValue:max  forKey:@"max"];
//    [params setValue:liveid  forKey:@"id"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_llivemx];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//
//}
//- (void)LoadDataFindZnews:(NSString *)schoolId CompletionBlock:(void (^)(NSDictionary *))aBlock error:(FailureBlock)aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:schoolId  forKey:@"schoolId"];
//
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_znews];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//
//#pragma mark-- 一周食谱列表
//- (void)LoadDataLsampleorderAppindex:(NSString *)schoolid  classId:(NSString *)classId fname:(NSString *)fname  fdateTime:(NSString *)fdateTime CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:schoolid  forKey:@"schoolId"];
//    [params setValue:classId  forKey:@"lclassId"];
//    [params setValue:fname  forKey:@"fname"];
//    [params setValue:fdateTime  forKey:@"fdateTime"];
//    NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lsampleorderAppindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//
//}
//
//#pragma mark-- 查询育儿天地主类
//- (void)LoadDataPTotalClassifyCompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//
//
//
//      NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_PTotalClassify];
//      [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//
//
//}
//#pragma mark-- 查询育儿天地2级分类
//- (void)LoadDataPClassifyAppindex:(NSString *)listid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//     NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:listid  forKey:@"ptotalClassifyId"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_PClassifyAppindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//
//#pragma mark-- 查询育儿天地推荐列表
//- (void)LoadDataPFileRecommendCompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_PFileRecommend];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//
//#pragma mark-- 查询育儿天地推荐列表
//- (void)LoadDataPClassifyShow:(NSString *)showid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:showid  forKey:@"id"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_PFileRecommend];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查询育儿天地全部分类
//- (void)LoadDataPFileindex:(NSString *)max offset:(NSString *)offset totalClassifyId:(NSString *)totalClassifyId classifyId:(NSString *)classifyId fleave:(NSString *)fleave CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:max  forKey:@"max"];
//    [params setValue:offset  forKey:@"offset"];
//    [params setValue:totalClassifyId  forKey:@"totalClassifyId"];
//    [params setValue:classifyId  forKey:@"classifyId"];
//    [params setValue:fleave  forKey:@"fleave"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_PFileindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查询育儿天地详情
//- (void)LoadDataPFileshow:(NSString *)showid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:showid  forKey:@"id"];
////   NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
////        [params setValue:token  forKey:@"x-auth-token"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_PFileshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 轮播图
//- (void)LoadDatalpictureappindex:(NSString *)showid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:showid  forKey:@"schoolId"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lpictureappindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取资讯详情
//- (void)LoadDataznewsshow:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:listid  forKey:@"id"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_znewsshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取全部地区
//- (void)LoadDataregionpublicapptree:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//       [params setValue:listid  forKey:@"id"];
//       NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_regionpublicapptree];
//       [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark--------------------------------直播---------------------------
//#pragma mark-- 直播列表
//- (void)LoadDataLivemxAppindex :(NSString *)max offset:(NSString *)offset ftype:(NSString *)ftype lschoolId:(NSString *)lshoolId CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:offset  forKey:@"offset"];
//    [params setValue:max  forKey:@"max"];
//    [params setValue:lshoolId  forKey:@"lschoolId"];
//    [params setValue:ftype  forKey:@"ftype"];
////    NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
////    [params setValue:token  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_LivemxAppindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 直播详情
//- (void)LoadDatallivemxshow :(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:listid  forKey:@"id"];
//
//    NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_llivemxshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//
//#pragma mark-- 增加直播观看数量
//- (void)LoadDatallivemxaddReadNum :(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError{
//
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//       [params setValue:listid  forKey:@"id"];
//
//       NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//       [params setValue:token  forKey:@"x-auth-token"];
//
//       NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_llivemxaddReadNum];
//       [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 教学名师类别
//- (void)LoadDataAppindex:(NSString *)schoolid  offset:(NSString *)offset max:(NSString *)max  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//     NSMutableDictionary *params = [NSMutableDictionary dictionary];
////       [params setValue:offset  forKey:@"offset"];
////       [params setValue:max  forKey:@"max"];
//       [params setValue:schoolid  forKey:@"schoolId"];
//       NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//       [params setValue:token  forKey:@"x-auth-token"];
//
//       NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_gappindex];
//       [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 教学名师详情
//- (void)LoadDataTearchShow:(NSString *)schoolid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:schoolid  forKey:@"id"];
//    NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_teacherShow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 根据学校获取分班
//- (void)LoadDataGetClassesBySchool:(NSString *)schoolid   CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:schoolid  forKey:@"lshcoolId"];
//    NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token  forKey:@"x-auth-token"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_getClassesBySchool];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 注册
//- (void)LoadRegister:(NSString *)telephone  WithPassword:(NSString *)password WithvCode:(NSString *)vCode  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//     NSString *passwordDes = [YHUtils md5HexDigest:password];
//    [params setValue:passwordDes  forKey:@"password"];
//    [params setValue:vCode  forKey:@"vCode"];
//
//    [params setValue:telephone  forKey:@"telephone"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_addUser];
//    [self DataPOSTURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取验证码
//- (void)LoadDatagetVcode4Register:(NSString *)telephone  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:telephone  forKey:@"telephone"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_sendSMS];
//    [self DataPOSTURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 修改个人信息
//- (void)LoadRegisterSave:(NSString *)name fsex:(NSString *)fsex fqy:(NSString *)fqy paw:(NSString *)password CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:name  forKey:@"fname"];
//    [params setValue:fsex  forKey:@"fsex"];
//    [params setValue:fqy  forKey:@"fqy"];
//    [params setValue:password  forKey:@"password"];
//    NSString *token = [LFUserManager sharedInstance].curUserInfo.access_token;
//     [params setValue:token  forKey:@"x-auth-token"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_save];
//    [self DataPOSTURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 显示个人信息
//- (void)LoadRegistershow:(NSString *)token  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_show];
//    [self DataPOSTURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取报名进度
//- (void)LoadDataLapplyUserList:(NSString *)offset max:(NSString *)max  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:offset  forKey:@"offset"];
//    [params setValue:max  forKey:@"max"];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:[LFUserManager sharedInstance].curUserInfo.userId forKey:@"userId"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lapplyUserList];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取我的宝宝详情
//- (void)LoadDatalstudentshow:(NSString *)listid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//       NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//       [params setValue:token1  forKey:@"x-auth-token"];
//       [params setValue:listid  forKey:@"id"];
//       NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lstudentshow];
//       [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取报名进度详情
//- (void)LoadDatalapplyShow:(NSString *)userid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:userid forKey:@"id"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lapplyShow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//
//#pragma mark-- 查询绑定宝宝
//- (void)LoadDatalmyclassmybabyCompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lmyclassmybaby];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查询绑定进度列表
//- (void)LoadDatalmyclassappindex:(NSString *)userid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lmyclassappindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 获取绑定进度详情
//- (void)LoadDatalmyclassshow:(NSString *)userid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:userid forKey:@"id"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lmyclassshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 切换宝宝
//- (void)LoadDatalmyclasschoose:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:listid  forKey:@"studentId"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lmyclasschoose];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查看宝宝详情
//- (void)LoadDatalclassshow:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//       NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//       [params setValue:token1  forKey:@"x-auth-token"];
//       [params setValue:listid  forKey:@"id"];
//       NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lclassshow];
//       [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//
//
//#pragma mark--------------------------------成长日记---------------------------
//#pragma mark-- 查询当前选中宝宝
//- (void)LoadDatallmyclassfindchoose:(NSString *)userid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:userid  forKey:@"id"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lmyclassfindchoose];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查询动态列表
//- (void)LoadDataldynamicindex:(NSString *)max offset:(NSString *)offset lclassId:(NSString*)lclassId CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:max  forKey:@"max"];
//    [params setValue:offset  forKey:@"offset"];
//    [params setValue:lclassId  forKey:@"lclassId"];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_ldynamicindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查询动态详情
//- (void)LoadDataldynamicshow:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//   NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:listid  forKey:@"id"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_ldynamicshow];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}
//#pragma mark-- 查询动态评论列表
//- (void)LoadDatalcommentindex:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSString *token1 = [LFUserManager sharedInstance].curUserInfo.access_token;
//    [params setValue:token1  forKey:@"x-auth-token"];
//    [params setValue:listid  forKey:@"dynamicId"];
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",CSURL_BaseUrl,CSURL_lcommentappindex];
//    [self DataGETURL:urlString params:params CompletionBlock:aBlock error:aError];
//}

@end
