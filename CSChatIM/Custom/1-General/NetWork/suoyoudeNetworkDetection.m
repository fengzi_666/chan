//
//  suoyoudeNetworkDetection.m
//  ZhiShiFenQi
//
//  Created by 殷洛晴 on 2017/9/8.
//  Copyright © 2017年 王恒. All rights reserved.
//

#import "suoyoudeNetworkDetection.h"
#import "Reachability.h"

@implementation suoyoudeNetworkDetection
-(BOOL) isConnectionAvailable{
    
    BOOL isExistenceNetwork = YES;
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
            isExistenceNetwork = NO;
            self.networkName = @"notReachable";
            //NSLog(@"notReachable");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork = YES;
            self.networkName = @"WIFI";
            //NSLog(@"WIFI");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork = YES;
            self.networkName = @"3G/4G";
            //NSLog(@"3G");
            break;
            
    }
    
    if (!isExistenceNetwork) {
        
        if ([NSThread isMainThread]) {
//            [[ZSStagesAppDelegate SharedAppDelegate].dialogFactory ShowAlert:@"网络未连接，请检查你的网络！"];
            [SVProgressHUD showErrorWithStatus:@"网络异常请检查网络！"];
            //wangheng
        }
        else
        {
//            [[ZSStagesAppDelegate SharedAppDelegate].dialogFactory performSelectorOnMainThread:@selector(ShowAlert:) withObject:@"网络未连接，请检查你的网络！" waitUntilDone:false];
            [SVProgressHUD showErrorWithStatus:@"网络异常请检查网络！"];
            //wangheng
        }
        
        return NO;
    }
    
    return isExistenceNetwork;
}


@end
