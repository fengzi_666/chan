//
//  longwangRequestManage.h
//  TXPrattWhitney
//
//  Created by 梁亚飞 on 2018/6/21.
//  Copyright © 2018年 梁亚飞. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^FailureBlock)(NSString *error);

@interface longwangRequestManage : NSObject

+(instancetype) shareInstance;

+(void)postBossDemoWithUrl:(NSString*)url param:(NSString*)param  success:(void(^)(NSDictionary *dict))success fail:(void (^)(NSError *error))fail;

+(void)postBossDemoWithUrl1:(NSString*)url param:(NSString*)param token:(NSString *)token success:(void(^)(NSDictionary *dict))success fail:(void (^)(NSError *error))fail;
+(void)getBossDemoWithUrl:(NSString*)url param:(NSString*)param  success:(void(^)(NSDictionary *dict))success fail:(void (^)(NSError *error))fail;





//form表单参数上传
#pragma mark -- 常用fore表单参数上传
+(void)postBaseWithURL:(NSString *)url param:(NSDictionary *)param  success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError;
+(void)postBaseWithURL1:(NSString *)url param:(NSDictionary *)param requestHead:(NSString *)requestHead success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError;
#pragma mark -- 上传图片
+(void)postBaseImageWithURL:(NSString *)url param:(NSDictionary *)param image:(UIImage *)image success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError;

#pragma mark -- 发送红包
+(void)postBaseRedWithURL:(NSString *)url param:(NSDictionary *)param uuid:(NSString *)uuid success:(void(^)(NSDictionary *dict))success error:(FailureBlock) aError;




-(void)DataPOSTURL:(NSString *)url params:(NSDictionary *)params CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;

#pragma mark-- 注册发送验证码
- (void)LoadRequestSendRegisterSMS:(NSString *)loginname   CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
+(NSString *)getCurrentTimestamp ;






























#pragma mark-- 获取APP版本信息
- (void)LoadDataversionshow:(NSString *)loginname   CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 获取学校列表
- (void)LoadDataFindSchoolOffset:(NSString *)regionId fname:(NSString *)fname  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 获取学校详情
- (void)LoadDatalschoolshow:(NSString *)school  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 获取直播列表
- (void)LoadDataFindLivemxOffset:(NSString *)offset max:(NSString *)max liveId:(NSString *)liveid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 获取资讯列表
- (void)LoadDataFindZnews:(NSString *)schoolId  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 教学名师列表
- (void)LoadDataAppindex:(NSString *)schoolid  offset:(NSString *)offset max:(NSString *)max  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;


#pragma mark-- 教学名师详情
- (void)LoadDataTearchShow:(NSString *)schoolid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 根据学校获取分班
- (void)LoadDataGetClassesBySchool:(NSString *)schoolid   CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 一周食谱列表
- (void)LoadDataLsampleorderAppindex:(NSString *)schoolid  classId:(NSString *)classId fname:(NSString *)fname  fdateTime:(NSString *)fdateTime CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 查询育儿天地主类
- (void)LoadDataPTotalClassifyCompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 查询育儿天地2级分类

- (void)LoadDataPClassifyAppindex:(NSString *)listid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;

#pragma mark-- 查询育儿天地推荐列表
- (void)LoadDataPFileRecommendCompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;

#pragma mark-- 查询育儿天地推荐列表
- (void)LoadDataPClassifyShow:(NSString *)showid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 查询育儿天地全部分类
- (void)LoadDataPFileindex:(NSString *)max offset:(NSString *)offset totalClassifyId:(NSString *)totalClassifyId classifyId:(NSString *)classifyId fleave:(NSString *)fleave CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 查询育儿天地详情
- (void)LoadDataPFileshow:(NSString *)showid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 轮播图
- (void)LoadDatalpictureappindex:(NSString *)showid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 获取资讯详情
- (void)LoadDataznewsshow:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;

#pragma mark-- 获取学校全部地区
- (void)LoadDataregionpublicapptree:(NSString *)listid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;


#pragma mark--------------------------------直播---------------------------
#pragma mark-- 直播列表
- (void)LoadDataLivemxAppindex :(NSString *)max offset:(NSString *)offset ftype:(NSString *)ftype lschoolId:(NSString *)lshoolId CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 直播详情
- (void)LoadDatallivemxshow :(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;
#pragma mark-- 增加直播观看数量
- (void)LoadDatallivemxaddReadNum :(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;

#pragma mark--------------------------------登陆注册忘记密码---------------------------
#pragma mark-- 注册
- (void)LoadRegister:(NSString *)telephone  WithPassword:(NSString *)password WithvCode:(NSString *)vCode  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError;

#pragma mark-- 发送验证码
- (void)LoadDatagetVcode4Register:(NSString *)telephone  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark--------------------------------个人中心---------------------------
#pragma mark-- 修改个人信息
- (void)LoadRegisterSave:(NSString *)name fsex:(NSString *)fsex fqy:(NSString *)fqy paw:(NSString *)password CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 显示个人信息
- (void)LoadRegistershow:(NSString *)token  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 获取报名进度
- (void)LoadDataLapplyUserList:(NSString *)offset max:(NSString *)max  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 获取我的宝宝详情
- (void)LoadDatalstudentshow:(NSString *)listid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 获取报名进度详情
- (void)LoadDatalapplyShow:(NSString *)userid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 查询绑定宝宝
- (void)LoadDatalmyclassmybabyCompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 查询绑定进度列表
- (void)LoadDatalmyclassappindex:(NSString *)userid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 获取报名进度详情
- (void)LoadDatalmyclassshow:(NSString *)userid  CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 切换宝宝
- (void)LoadDatalmyclasschoose:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 查看宝宝详情
- (void)LoadDatalclassshow:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;


#pragma mark--------------------------------成长日记---------------------------
#pragma mark-- 查询当前选中宝宝
- (void)LoadDatallmyclassfindchoose:(NSString *)userid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

#pragma mark-- 查询动态列表
- (void)LoadDataldynamicindex:(NSString *)max offset:(NSString *)offset lclassId:(NSString*)lclassId CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 查询动态详情
- (void)LoadDataldynamicshow:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;
#pragma mark-- 查询动态评论列表
- (void)LoadDatalcommentindex:(NSString *)listid CompletionBlock:(void (^) (NSDictionary * dict))aBlock  error:(FailureBlock) aError ;

@end
