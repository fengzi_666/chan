//
//  suoyoudeNetworkDetection.h
//  ZhiShiFenQi
//
//  Created by 殷洛晴 on 2017/9/8.
//  Copyright © 2017年 王恒. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface suoyoudeNetworkDetection : NSObject{
    
}

@property (nonatomic, strong) NSString *networkName;

-(BOOL) isConnectionAvailable;

@end
