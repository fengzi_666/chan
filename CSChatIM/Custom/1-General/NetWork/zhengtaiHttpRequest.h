//
//  IMHttpRequest.h
//  IM
//
//  Created by Apple on 2020/8/1.
//  Copyright © 2020 IM. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

@interface zhengtaiHttpRequest : AFHTTPSessionManager
typedef void(^Success)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject);
typedef void(^PageSuccess)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject, id _Nullable responsePage);
typedef void(^Failure)(NSURLSessionDataTask * _Nullable task, id _Nullable status);
typedef void(^Error)(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error);

typedef void(^HttWWrogress)(NSProgress * _Nonnull uploadProgress);

+ (instancetype _Nullable )sharedManager;

+(NSURLSessionDataTask * _Nullable)SRPost:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view
                               parameters:(id _Nullable)parameters success:(Success _Nullable )success
                                  failure:(Failure _Nullable )failure
                                erronrous:(Error _Nullable)erroneous;


//图片上传
+(NSURLSessionDataTask * _Nullable)uploadWithURL:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view
parameters:(id _Nullable)parameters image:(NSArray <UIImage *> *)image imageName:(NSArray *_Nullable)nameArr uploadProgress:(HttWWrogress _Nullable)uploadProgress success:(Success _Nullable )success
   failure:(Failure _Nullable )failure
 erronrous:(Error _Nullable)erroneous;

//其他文件上传
+(NSURLSessionDataTask * _Nullable)uploadMPWithURL:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view
parameters:(id _Nullable)parameters file:(NSURL *)file fileName:(NSString *)filename uploadProgress:(HttWWrogress _Nullable)uploadProgres success:(Success _Nullable )success
   failure:(Failure _Nullable )failure
 erronrous:(Error _Nullable)erroneous;


//MP3上传
+(NSURLSessionDataTask * _Nullable)uploadMP3WithURL:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view
parameters:(id _Nullable)parameters file:(NSData *)file uploadProgress:(HttWWrogress _Nullable)uploadProgres success:(Success _Nullable )success
   failure:(Failure _Nullable )failure
 erronrous:(Error _Nullable)erroneous;

@end

//发送短信验证码接口
static  NSString * _Nonnull  const  MDAPPURL_LoginSendCode= @"app/zhuce/sendCode";
//图形验证码接口
static  NSString * _Nonnull  const  MDAPPURL_LoginVercode= @"app/zhuce/vercode";
//注册
static  NSString * _Nonnull  const  MDAPPURL_LoginZhuce= @"app/zhuce/zhuce";
//登录
static  NSString * _Nonnull  const  MDAPPURL_LoginLogin= @"app/login/login";

//找回密码
static  NSString * _Nonnull  const  MDAPPURL_LoginZhaohui= @"app/user/zhaohui";

//上传视频认证接口
static  NSString * _Nonnull  const  MDAPPURL_RenzhengSaveshipin= @"app/renzheng/saveshipin";
//获取视频认证说明接口
static  NSString * _Nonnull  const  MDAPPURL_RenzhengGetShipin= @"app/renzheng/getShipin";
//获取实名认证说明
static  NSString * _Nonnull  const  MDAPPURL_RenzhengGetShiming= @"app/renzheng/getShiming";

//编辑头像接口
static  NSString * _Nonnull  const  MDAPPURL_GerenUppic= @"app/geren/uppic";
//修改昵称接口
static  NSString * _Nonnull  const  MDAPPURL_GerenUpnick= @"app/geren/upnick";
//修改个性签名接口
static  NSString * _Nonnull  const  MDAPPURL_GerenUpmysign= @"app/geren/upmysign";

//个人信息
static  NSString * _Nonnull  const  MDAPPURL_usermessageGetMessage= @"app/usermessage/getMessage";
//编辑生日接口
static  NSString * _Nonnull  const  MDAPPURL_gerenUpbirthday= @"app/geren/upbirthday";
//获取省市列表信息接口
static  NSString * _Nonnull  const  MDAPPURL_gerenSslist= @"app/geren/sslist";

//编辑地址接口
static  NSString * _Nonnull  const  MDAPPURL_gerenSaveaddress= @"app/geren/saveaddress";

//用户签到操作
static  NSString * _Nonnull  const  MDAPPURL_taskQiandao= @"apptask/qiandao";

//编辑形象照片信息接口
static  NSString * _Nonnull  const  MDAPPURL_gerenUpxingxiang= @"app/geren/upxingxiang";
//删除形象照片接口
static  NSString * _Nonnull  const  MDAPPURL_gerenDelxxpic= @"app/geren/delxxpic";

//编辑性别接口
static  NSString * _Nonnull  const  MDAPPURL_gerenUpsex= @"app/geren/upsex";


//发布动态
static  NSString * _Nonnull  const  MDAPPURL_ongtaiAddDongtai= @"app/dongtai/addDongtai";
//删除该动态
static  NSString * _Nonnull  const  MDAPPURL_dongtaiDelDongtai= @"appdongtai/del";
//获取热门动态
static  NSString * _Nonnull  const  MDAPPURL_dynamicList= @"app/dynamic/list";
//查看某个用户的动态列表
static  NSString * _Nonnull  const  MDAPPURL_userGetTdongtai= @"app/user/getTdongtai";

//动态点赞/取消点赞
static  NSString * _Nonnull  const  MDAPPURL_dongtaiUpdateDongtaiLike= @"appdongtai/UpdateDongTaiDianZan";

//查看某个动态的评论列表
static  NSString * _Nonnull  const  MDAPPURL_dongtaiGetDpinglunList= @"appdongtai/getDongtaiPingLun";

//查看某个评论的回复列表
static  NSString * _Nonnull  const  MDAPPURL_dongtaiGetDhuifuList= @"appdongtai/getDongtaiPingLunHuiFu";
//对动态进行评论
static  NSString * _Nonnull  const  MDAPPURL_dongtaiDynamicallyComment= @"appdongtai/UpdateDongTaiPingLun";
//对评论进行回复
static  NSString * _Nonnull  const  MDAPPURL_dongtaiReplyToComments= @"appdongtai/pingLunHuiFu";

//用户关注与粉丝 操作接口
static  NSString * _Nonnull  const  MDAPPURL_guanzhuandfensiGuanzhu= @"appguanzhuandfensi/guanzhu";
//获取关注列表
static  NSString * _Nonnull  const  MDAPPURL_guanzhuandfensiGuanzhulist= @"appguanzhuandfensi/guanzhulist";

//获取粉丝列表
static  NSString * _Nonnull  const  MDAPPURL_fensilsit= @"appguanzhuandfensi/fensilsit";

//实名认证接口
static  NSString * _Nonnull  const  MDAPPURL_renzhengSave= @"app/renzheng/save";
//获取任务列表
static  NSString * _Nonnull  const  MDAPPURL_taskTasklist= @"apptask/tasklist";
//领取任务奖励
static  NSString * _Nonnull  const  MDAPPURL_taskLingqu= @"apptask/lingqu";

//获取钱包
static  NSString * _Nonnull  const  MDAPPURL_walletList= @"app/wallet/list";
//获取钻石充值金额列表
static  NSString * _Nonnull  const  MDAPPURL_chongzhiGetRechargeAmountList= @"app/chongzhi/getRechargeAmountList";

//申请提现
static  NSString * _Nonnull  const  MDAPPURL_cashwithdrawalAddCash= @"app/cashwithdrawal/addCash";

//获取我的背包接口
static  NSString * _Nonnull  const  MDAPPURL_giftnumGetMeKnapsack= @"app/giftnum/getMeKnapsack";

//查看某个用户的信息
static  NSString * _Nonnull  const  MDAPPURL_userLookTinfo= @"app/user/lookTinfo";
//获取某个动态详情
static  NSString * _Nonnull  const  MDAPPURL_dongtaiGetDongtaiDetail= @"appdongtai/getDongtaiDetail";

//轮播图
static  NSString * _Nonnull  const  MDAPPURL_bannerGetBanner= @"app/banner/getBanner";

//获取首页系统公告
static  NSString * _Nonnull  const  MDAPPURL_messageList= @"app/message/list";
//获取首页视频直播
static  NSString * _Nonnull  const  MDAPPURL_homeSplist= @"app/home/splist";

//评论点赞/取消点赞
static  NSString * _Nonnull  const  MDAPPURL_dongtaiUpdatePinglunLike= @"app/dongtai/updatePinglunLike";

//评论评论点赞/取消点赞
static  NSString * _Nonnull  const  MDAPPURL_dongtaipinglunUpdatePinglunLike= @"appdongtai/UpdateDongTaiPingLunDianZan";
//获取附近人列表接口
static  NSString * _Nonnull  const  MDAPPURL_appfujinrenList= @"appfujinren/list";
//更新用户经纬度
static  NSString * _Nonnull  const  MDAPPURL_userUpdateWeiZhi= @"app/user/updateWeiZhi";

//获取拉黑名单
static  NSString * _Nonnull  const  MDAPPURL_applaheiUserlist= @"applahei/userlist";
//用户拉黑
static  NSString * _Nonnull  const  MDAPPURL_applaheiUserlahei= @"applahei/userlahei";

//用户附近开关 接口
static  NSString * _Nonnull  const  MDAPPURL_appshezhiFujin= @"appshezhi/fujin";

//礼物特效开关
static  NSString * _Nonnull  const  MDAPPURL_appshezhiLiwu= @"appshezhi/liwu";

//我的团队
static  NSString * _Nonnull  const  MDAPPURL_yaoqingTuandui= @"app/yaoqing/tuandui";
//获取贵族等级列表
static  NSString * _Nonnull  const  MDAPPURL_nobilityList= @"app/nobility/list";
//获取商城头饰
static  NSString * _Nonnull  const  MDAPPURL_shopGetHeadwear= @"app/shop/getHeadwear";
//获取商城的坐骑
static  NSString * _Nonnull  const  MDAPPURL_shopGetMounts= @"app/shop/getMounts";
//购买坐骑和头饰
static  NSString * _Nonnull  const  MDAPPURL_shopBuyMountsOrHeadwear= @"app/shop/buyMountsOrHeadwear";

//获取自己的头饰和坐骑
static  NSString * _Nonnull  const  MDAPPURL_shopGetMyHeadwear= @"app/shop/getMyHeadwear";
//默认坐骑或者头饰
static  NSString * _Nonnull  const  MDAPPURL_shopDressupDefault= @"app/shop/dressupDefault";

//取消默认
static  NSString * _Nonnull  const  MDAPPURL_shopCancelDefault= @"app/shop/cancelDefault";


//获取房间分类
static  NSString * _Nonnull  const  MDAPPURL_homeGetRoomAllCat= @"app/home/getRoomAllCat";
//根据房间分类分页获取房间
static  NSString * _Nonnull  const  MDAPPURL_homeGetRoomByCat= @"app/home/getRoomByCat";
//获取收藏的房间
static  NSString * _Nonnull  const  MDAPPURL_homeGetRoomCollection= @"app/home/getRoomCollection";
//账户明细 （充值、兑换、 提现）//1充值 2提现 3兑换
static  NSString * _Nonnull  const  MDAPPURL_mingxiCaifulist= @"app/mingxi/caifulist";
//账户明细（收礼、送礼）类型 //1收礼 2送礼
static  NSString * _Nonnull  const  MDAPPURL_mingxiLiwulist= @"app/mingxi/liwulist";

//修改密码接口
static  NSString * _Nonnull  const  MDAPPURL_userXiugai= @"app/user/xiugai";

//意见反馈
static  NSString * _Nonnull  const  MDAPPURL_appshezhiYijian= @"appshezhi/yijian";

//联系我们
static  NSString * _Nonnull  const  MDAPPURL_appshezhiLianxiwomen= @"appshezhi/lianxiwomen";
//帮助列表
static  NSString * _Nonnull  const  MDAPPURL_appshezhiHelplist= @"appshezhi/helplist";

//获取语音房间背景图片列表
static  NSString * _Nonnull  const  MDAPPURL_roomGetYuyinRoomImgList= @"app/room/getYuyinRoomImgList";
//上传自定义房间背景
static  NSString * _Nonnull  const  MDAPPURL_roomUproombj= @"app/room/uproombj";
//删除房间自定义背景图
static  NSString * _Nonnull  const  MDAPPURL_roomDelroomimg= @"app/room/delroomimg";
//开启直播
static  NSString * _Nonnull  const  MDAPPURL_roomStartLiveBroadcast= @"app/room/startLiveBroadcast";

//获取房间详情
static  NSString * _Nonnull  const  MDAPPURL_roomGetRoomDetail= @"app/room/getRoomDetail";
//获取UserSig
static  NSString * _Nonnull  const  MDAPPURL_txGetUserSig= @"app/tx/getUserSig";

//更改房间封面
static  NSString * _Nonnull  const  MDAPPURL_roomUproomimg= @"app/room/uproomimg";
//更改房间标签类型
static  NSString * _Nonnull  const  MDAPPURL_roomUproomtype= @"app/room/uproomtype";
//更改房间标题
static  NSString * _Nonnull  const  MDAPPURL_roomUproomname= @"app/room/uproomname";
//更改房间背景
static  NSString * _Nonnull  const  MDAPPURL_roomUproombeijing= @"app/room/uproombeijing";
//设置房间公告
static  NSString * _Nonnull  const  MDAPPURL_roomkauiguanGonggao= @"app/roomkauiguan/gonggao";
//加入粉丝团
static  NSString * _Nonnull  const  MDAPPURL_roomJoinTheFanClub= @"app/room/joinTheFanClub";

//根据腾讯code查看某个用户的信息
static  NSString * _Nonnull  const  MDAPPURL_userTxCodelookTinfo= @"app/user/txCodelookTinfo";

//获取房间管理员列表
static  NSString * _Nonnull  const  MDAPPURL_roomGetTheListOfRoomManagers= @"app/room/getTheListOfRoomManagers";
//礼物&粉丝团礼物列表接口
static  NSString * _Nonnull  const  MDAPPURL_giftnumGift= @"app/giftnum/gift";

//房间送礼
static  NSString * _Nonnull  const  MDAPPURL_songliRoomsl= @"app/songli/roomsl";

//获取举报类型
static  NSString * _Nonnull  const  MDAPPURL_feedbackList= @"app/feedback/list";

//添加举报信息
static  NSString * _Nonnull  const  MDAPPURL_feedbackAddFeedback= @"app/feedback/addFeedback";
//添加举报信息
static  NSString * _Nonnull  const  MDAPPURL_appfensituanList= @"appfensituan/list";

//获取房间成员
static  NSString * _Nonnull  const  MDAPPURL_txGetAListOfRoomMembers= @"app/tx/getAListOfRoomMembers";

//增加房间管理员
static  NSString * _Nonnull  const  MDAPPURL_roomAddRoomAdministrator= @"app/room/addRoomAdministrator";

//移除房间管理员
static  NSString * _Nonnull  const  MDAPPURL_roomRemoveRoomManager= @"app/room/removeRoomManager";

//房间拉黑
static  NSString * _Nonnull  const  MDAPPURL_applaheiRoomlahei= @"applahei/roomlahei";

//获取房间拉黑列表
static  NSString * _Nonnull  const  MDAPPURL_applaheiRoomlist= @"applahei/roomlist";
//获取房间禁言列表
static  NSString * _Nonnull  const  MDAPPURL_jinyanRoomlist= @"app/jinyan/roomlist";
//取消房间拉黑
static  NSString * _Nonnull  const  MDAPPURL_applaheiQxroomlahei= @"applahei/qxroomlahei";

//开闭麦克风更新腾讯资料字段
static  NSString * _Nonnull  const  MDAPPURL_txUpdateTencentProfileField= @"app/tx/updateTencentProfileField";

//1对1送礼
static  NSString * _Nonnull  const  MDAPPURL_songliSongli= @"app/songli/songli";

//房间送礼【背包礼物】
static  NSString * _Nonnull  const  MDAPPURL_songliBeibaoroomsl= @"app/songli/beibaoroomsl";
//1对1送礼【背包礼物】
static  NSString * _Nonnull  const  MDAPPURL_songliBeibaosongli= @"app/songli/beibaosongli";

//1对1送礼
//static  NSString * _Nonnull  const  MDAPPURL_songliSongli= @"app/songli/songli";

//获取房间实时在线人数
static  NSString * _Nonnull  const  MDAPPURL_txGetOnlineNumberOfRoom= @"app/tx/getOnlineNumberOfRoom";

//获取首页情感专家
static  NSString * _Nonnull  const  MDAPPURL_homeGetEmotionExpert= @"app/home/getEmotionExpert";

//查看某个用户的礼物墙
static  NSString * _Nonnull  const  MDAPPURL_userGetTgiftQiang= @"app/user/getTgiftQiang";
//用户取消拉黑
static  NSString * _Nonnull  const  MDAPPURL_applaheiQxuserlahei= @"applahei/qxuserlahei";

//系统消息接口
static  NSString * _Nonnull  const  MDAPPURL_appsystemXiaoxi= @"appsystem/xiaoxi";
//获取平台活动列表
static  NSString * _Nonnull  const  MDAPPURL_pingtaiPingtaiList= @"app/pingtai/pingtaiList";
//获取平台活动详情
static  NSString * _Nonnull  const  MDAPPURL_pingtaiGetPingtai= @"app/pingtai/getPingtai";

//贵族开通
static  NSString * _Nonnull  const  MDAPPURL_nobilityNoblesOpen= @"app/nobility/noblesOpen";

//查看邀请信息
static  NSString * _Nonnull  const  MDAPPURL_yaoqingYaoqing= @"app/yaoqing/yaoqing";
//1v1视频匹配
static  NSString * _Nonnull  const  MDAPPURL_yiduiyishipinAction= @"app/yiduiyishipin/action";

//判断用户是否在视频匹配
static  NSString * _Nonnull  const  MDAPPURL_yiduiyishipinWhetherToMatchInTheVideo= @"app/yiduiyishipin/whetherToMatchInTheVideo";

//房间收藏/取消收藏
static  NSString * _Nonnull  const  MDAPPURL_roomUpdateRoomCollect= @"app/room/updateRoomCollect";
//获取标签
static  NSString * _Nonnull  const  MDAPPURL_userGetBiaoqian= @"app/user/getBiaoqian";
//修改标签
static  NSString * _Nonnull  const  MDAPPURL_gerenUpBiaoqian= @"app/geren/upBiaoqian";

//获取职业数组
static  NSString * _Nonnull  const  MDAPPURL_userGetZhiYe= @"app/user/getZhiYe";
//修改职业
static  NSString * _Nonnull  const  MDAPPURL_gerenUpzhiye= @"app/geren/upzhiye";
//获取年收入数组
static  NSString * _Nonnull  const  MDAPPURL_userGetNianShouRu= @"app/user/getNianShouRu";

//获取体重数组
static  NSString * _Nonnull  const  MDAPPURL_userGetTizhong= @"app/user/getTizhong";

//获取身高数组
static  NSString * _Nonnull  const  MDAPPURL_userGetShengao= @"app/user/getShengao";

//修改年收入
static  NSString * _Nonnull  const  MDAPPURL_gerenUpnianshouru= @"app/geren/upnianshouru";

//修改体重
static  NSString * _Nonnull  const  MDAPPURL_gerenUptizhong= @"app/geren/uptizhong";

//修改身高
static  NSString * _Nonnull  const  MDAPPURL_gerenUpshengao= @"app/geren/upshengao";
//音视频聊天扣费【音视频聊天之后调用】
static  NSString * _Nonnull  const  MDAPPURL_yiduiyishipinAudioVideoChatFees= @"app/yiduiyishipin/audioVideoChatFees";

//音视频聊天余额是否充足【音视频聊天之前调用】
static  NSString * _Nonnull  const  MDAPPURL_yiduiyishipinIsAudioVideoChatTerm= @"app/yiduiyishipin/isAudioVideoChatTerm";

//不看ta的动态
static  NSString * _Nonnull  const  MDAPPURL_applaheiNoLookUdongtai= @"applahei/noLookUdongtai";
//取消不看ta的动态
static  NSString * _Nonnull  const  MDAPPURL_applaheiQuxiaoNoLookUdongtai= @"applahei/quxiaoNoLookUdongtai";

//钻石兑换
static  NSString * _Nonnull  const  MDAPPURL_walletDuihuanDiamond= @"app/wallet/duihuanDiamond";

//第一次进入 完善基础资料
static  NSString * _Nonnull  const  MDAPPURL_loginCompleteMaterial= @"app/login/completeMaterial";

//是否可以发送文字消息
static  NSString * _Nonnull  const  MDAPPURL_homeIsSendTextMessage= @"app/home/isSendTextMessage";
//微信充值
static  NSString * _Nonnull  const  MDAPPURL_wechatDiamondRecharge= @"api/wechat/diamondRecharge";
//音视频免费时长明细
static  NSString * _Nonnull  const  MDAPPURL_mingxiFreetimelist= @"app/mingxi/freetimelist";
//支付宝充值
static  NSString * _Nonnull  const  MDAPPURL_alipayDiamondRecharge= @"api/alipay/diamondRecharge";
//保存发送文字消息次数
static  NSString * _Nonnull  const  MDAPPURL_homeUpdateSendTextMessage= @"app/home/updateSendTextMessage";
//保礼物数量列表接口
static  NSString * _Nonnull  const  MDAPPURL_giftnumGiftnum= @"app/giftnum/giftnum";

//房间搜索
static  NSString * _Nonnull  const  MDAPPURL_homeSearchRoom= @"app/home/searchRoom";

//用户搜索
static  NSString * _Nonnull  const  MDAPPURL_homeSearchUser= @"app/home/searchUser";

//房间禁言
static  NSString * _Nonnull  const  MDAPPURL_jinyanRoomjinyan= @"app/jinyan/roomjinyan";

//取消房间禁言
static  NSString * _Nonnull  const  MDAPPURL_jinyanCanceljinyan =@"app/jinyan/canceljinyan";

//获取平台活动和系统消息最新一条
static  NSString * _Nonnull  const  MDAPPURL_pingtaiGetPingtaiAndXitong =@"app/pingtai/getPingtaiAndXitong";

//隐私协议接口
static  NSString * _Nonnull  const  MDAPPURL_setupYinsixieyi =@"app/setup/yinsixieyi";

//用户协议
static  NSString * _Nonnull  const  MDAPPURL_setupYonghuxieyi =@"app/setup/yonghuxieyi";
//搭讪
static  NSString * _Nonnull  const  MDAPPURL_homeStrikeUpAConversation =@"app/home/strikeUpAConversation";
//领取搭讪奖励
static  NSString * _Nonnull  const  MDAPPURL_homeLingquDashanJiangli =@"app/home/lingquDashanJiangli";
NS_ASSUME_NONNULL_END
