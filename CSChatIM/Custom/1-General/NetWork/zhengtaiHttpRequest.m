//
//  IMHttpRequest.m
//  IM
//
//  Created by Apple on 2020/8/1.
//  Copyright © 2020 IM. All rights reserved.
//

#import "zhengtaiHttpRequest.h"
#import "tuziHttpModel.h"
//#import "IMLoginUserModel.h"
#import "CSLoginViewController.h"
//#import "HBDNavigationController.h"
//#import "MDUserInfoToken.h"
#import "NSString+LFMD5.h"

@implementation zhengtaiHttpRequest
+ (instancetype _Nullable )sharedManager{
    static zhengtaiHttpRequest *_requestManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _requestManager = [[zhengtaiHttpRequest alloc] initWithBaseURL:[NSURL URLWithString:CSURL_BaseUrl]];
    });
    
    return _requestManager;
}


+(NSURLSessionDataTask * _Nullable)SRPost:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view parameters:(id _Nullable)parameters success:(Success _Nullable )success failure:(Failure _Nullable )failure erronrous:(Error _Nullable)erroneous{
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    if (token.length!=0) {
        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    }
//    NSLog(@"这个是传递参数%@-----%@",parameters,[MDUserInfoToken shareInstance].token);
    [zhengtaiHttpRequest sharedManager].responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableArray *Arr= [[NSMutableArray alloc]init];
    NSString *time = [self getCurrentTimestamp];
    NSString *appstr= [NSString stringWithFormat:@"jyz_app8x72141i285f81e58a6c13x85a9eaa58%@",time];
    NSString *aoostrMd5 = [appstr LFMD5Digest];
    [Arr addObject:[NSString stringWithFormat:@"1=%@",time]];
    [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:time forHTTPHeaderField:@"timeStamp"];
    [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:aoostrMd5 forHTTPHeaderField:@"signature"];
    NSMutableDictionary *md5dic= [[NSMutableDictionary alloc]init];
    [md5dic setValue:time forKey:@"1"];
    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        NSString *string=@"";//结果字符串
        NSString *string1=key;
        NSString *string2=obj;//已存在的字符串，需要将string1和string2连接起来
        string =  [string stringByAppendingFormat:@"%@=%@" ,string1, string2];
        [Arr addObject:string];
        [md5dic setValue:obj forKey:key];
        

    }];
    if (Arr.count >0) {
        
        NSArray *result = [Arr sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
            NSLog(@"%@~%@",obj1,obj2); // 3~4 2~1 3~1 3~2
            return [obj1 compare:obj2]; // 升序
        }];
        NSMutableArray *arr1 = [[NSMutableArray alloc]init];
        for (int i =0; i<result.count; i++) {
            NSString *str = result[i];
            [arr1 addObject:str];
        }
        [arr1 addObject:@"key=x5df2b4x5s32c5f6"];
      
        NSString *text = [arr1 componentsJoinedByString:@"&"];
        NSString *str =  [text LFMD5Digest];

        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:str forHTTPHeaderField:@"signKey"];
    }
    else
    {
        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:@"" forHTTPHeaderField:@"signKey"];
    }
    return [[zhengtaiHttpRequest sharedManager] POST:urlString parameters:md5dic headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //        NSLog(@"---%@",responseObject.mj_)
        tuziHttpModel *model =[tuziHttpModel mj_objectWithKeyValues:responseObject];
        if (model.code==0) {
            //            [XNHUD dismiss];
            if ([urlString isEqualToString:MDAPPURL_LoginVercode]) {
                success(task,responseObject);
            }else{
                if ([responseObject isKindOfClass:[NSData class]] ) {
                    responseObject =[self dictionaryForJsonData:responseObject];
                }
                NSLog(@"这个是---%@数据%@",urlString,responseObject);
                success(task,responseObject);
            }
            
        }
        else if (model.code==1001){
            //登录过期
            [SVProgressHUD showInfoWithStatus:@"登录时间已过期"];
            NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
            [def removeObjectForKey:@"MD_UserToken"];
            //            [IMLoginUserModel deleteKeychainValue:@"token"];
            UIWindow *window;
            if (@available(iOS 13.0, *)) {
                window = [UIApplication sharedApplication].windows[0];
            } else {
                window = [UIApplication sharedApplication].delegate.window;
            }
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        }else{
            if (model.msg.length>1) {
                if (![model.msg isEqualToString:@"正在匹配中"]) {
                    [SVProgressHUD showInfoWithStatus:model.msg];
                }
                
            }
            failure(task,model.msg);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---%@",error);
        erroneous(task, error);
    }];
}
//获取当前时间的时间戳
+(NSString *)getCurrentTimestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0]; // 获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970]*1000;// *1000 是精确到毫秒(13位),不乘就是精确到秒(10位)
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

+(NSURLSessionDataTask * _Nullable)uploadWithURL:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view parameters:(id _Nullable)parameters image:(NSArray <UIImage *> *)image imageName:(NSArray *_Nullable)nameArr  uploadProgress:(HttWWrogress _Nullable)uploadProgres success:(Success _Nullable )success
                                         failure:(Failure _Nullable )failure
                                       erronrous:(Error _Nullable)erroneous{
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    if (token.length!=0) {
        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    }
//    NSLog(@"这个是传递参数%@-----%@",parameters,[MDUserInfoToken shareInstance].token);
    NSMutableArray *Arr= [[NSMutableArray alloc]init];
    NSString *time = [self getCurrentTimestamp];
    NSString *appstr= [NSString stringWithFormat:@"jyz_app8x72141i285f81e58a6c13x85a9eaa58%@",time];
    NSString *aoostrMd5 = [appstr LFMD5Digest];
    [Arr addObject:[NSString stringWithFormat:@"1=%@",time]];
    [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:time forHTTPHeaderField:@"timeStamp"];
    [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:aoostrMd5 forHTTPHeaderField:@"signature"];
    NSMutableDictionary *md5dic= [[NSMutableDictionary alloc]init];
    [md5dic setValue:time forKey:@"1"];
    [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        NSString *string=@"";//结果字符串
        NSString *string1=key;
        NSString *string2=obj;//已存在的字符串，需要将string1和string2连接起来
        string =  [string stringByAppendingFormat:@"%@=%@" ,string1, string2];
        [Arr addObject:string];
        [md5dic setValue:obj forKey:key];
        

    }];
    if (Arr.count >0) {
        
        NSArray *result = [Arr sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
            NSLog(@"%@~%@",obj1,obj2); // 3~4 2~1 3~1 3~2
            return [obj1 compare:obj2]; // 升序
        }];
        NSMutableArray *arr1 = [[NSMutableArray alloc]init];
        for (int i =0; i<result.count; i++) {
            NSString *str = result[i];
            [arr1 addObject:str];
        }
        [arr1 addObject:@"key=x5df2b4x5s32c5f6"];
      
        NSString *text = [arr1 componentsJoinedByString:@"&"];
        NSString *str =  [text LFMD5Digest];

        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:str forHTTPHeaderField:@"signKey"];
    }
    else
    {
        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:@"" forHTTPHeaderField:@"signKey"];
    }
    return [[zhengtaiHttpRequest sharedManager] POST:urlString parameters:md5dic headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i < image.count; i++) {
            NSData *imageData = UIImageJPEGRepresentation(image[i], 0.5);
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            // 设置时间格式
            [formatter setDateFormat:@"yyyyMMddHHmmss"];
            NSString *dateString = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString  stringWithFormat:@"%@.jpg", dateString];
            NSString *name =@"image";
            if (nameArr.count>0) {
                name =nameArr[i];
            }
            [formData appendPartWithFileData:imageData name:name fileName:fileName mimeType:@"image/jpg"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        uploadProgres(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        tuziHttpModel *model =[tuziHttpModel mj_objectWithKeyValues:responseObject];
        if (model.code==0) {
            //            [XNHUD dismiss];
            if ([responseObject isKindOfClass:[NSData class]] ) {
                responseObject =[self dictionaryForJsonData:responseObject];
            }
            NSLog(@"这个是数据%@",responseObject);
            success(task,responseObject);
        }else if (model.code==1001){
            NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
            [def removeObjectForKey:@"MD_UserToken"];
            //登录过期
            [SVProgressHUD showInfoWithStatus:@"登录时间已过期"];
            //            [IMLoginUserModel deleteKeychainValue:@"token"];
            UIWindow *window;
            if (@available(iOS 13.0, *)) {
                window = [UIApplication sharedApplication].windows[0];
            } else {
                window = [UIApplication sharedApplication].delegate.window;
            }
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        }else{
            if (model.msg.length>1) {
                [SVProgressHUD showInfoWithStatus:model.msg];
            }
            failure(task,model.msg);
        }
        NSLog(@"这个是数据%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task,error);
    }];
}
//上传其他文件
+(NSURLSessionDataTask * _Nullable)uploadMPWithURL:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view
                                        parameters:(id _Nullable)parameters file:(NSURL *)file fileName:(NSString *)filename uploadProgress:(HttWWrogress _Nullable)uploadProgres success:(Success _Nullable )success
                                           failure:(Failure _Nullable )failure
                                         erronrous:(Error _Nullable)erroneous{
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    if (token.length!=0) {
        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    }
//    NSLog(@"这个是传递参数%@-----%@",parameters,[MDUserInfoToken shareInstance].token);
    return [[zhengtaiHttpRequest sharedManager] POST:urlString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data = [NSData dataWithContentsOfURL:file];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmm";
        NSString *fileStr = [formatter stringFromDate:[NSDate date]];
        NSString *name = [NSString stringWithFormat:@"%@.mp4",fileStr];
        [formData appendPartWithFileData:data
                                    name:filename
                                fileName:name
                                mimeType:@"mp4"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        uploadProgres(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        tuziHttpModel *model =[tuziHttpModel mj_objectWithKeyValues:responseObject];
        if (model.code==0) {
            //            [XNHUD dismiss];
            if ([responseObject isKindOfClass:[NSData class]] ) {
                responseObject =[self dictionaryForJsonData:responseObject];
            }
            NSLog(@"这个是数据%@",responseObject);
            success(task,responseObject);
        }else if (model.code==1001){
            NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
            [def removeObjectForKey:@"MD_UserToken"];
            //登录过期
            [SVProgressHUD showInfoWithStatus:@"登录时间已过期"];
            //               [IMLoginUserModel deleteKeychainValue:@"token"];
            UIWindow *window;
            if (@available(iOS 13.0, *)) {
                window = [UIApplication sharedApplication].windows[0];
            } else {
                window = [UIApplication sharedApplication].delegate.window;
            }
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        }else{
            if (model.msg.length>1) {
                [SVProgressHUD showInfoWithStatus:model.msg];
            }
            failure(task,model.msg);
        }
        NSLog(@"这个是数据%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task,error);
    }];
}

+(NSURLSessionDataTask * _Nullable)uploadMP3WithURL:(NSString *_Nullable)urlString view:(UIViewController *_Nullable)view
                                         parameters:(id _Nullable)parameters file:(NSData *)file uploadProgress:(HttWWrogress _Nullable)uploadProgres success:(Success _Nullable )success
                                            failure:(Failure _Nullable )failure
                                          erronrous:(Error _Nullable)erroneous{
    NSString *token = [UserDefaultManager shareInstance].GetLoginToken;
    if (token.length!=0) {
        [[zhengtaiHttpRequest sharedManager].requestSerializer setValue:token forHTTPHeaderField:@"Token"];
    }
    NSLog(@"这个是传递参数%@",parameters);
    return [[zhengtaiHttpRequest sharedManager] POST:urlString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmm";
        NSString *fileStr = [formatter stringFromDate:[NSDate date]];
        NSString *name = [NSString stringWithFormat:@"%@.mp3",fileStr];
        [formData appendPartWithFileData:file
                                    name:@"mp3"
                                fileName:name
                                mimeType:@"mp3"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        uploadProgres(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        tuziHttpModel *model =[tuziHttpModel mj_objectWithKeyValues:responseObject];
        if (model.code==0) {
            //            [XNHUD dismiss];
            success(task,responseObject);
        }else if (model.code==1001){
            NSUserDefaults *def =[NSUserDefaults standardUserDefaults];
            [def removeObjectForKey:@"MD_UserToken"];
            //登录过期
            [SVProgressHUD showInfoWithStatus:@"登录时间已过期"];
            //                 [IMLoginUserModel deleteKeychainValue:@"token"];
            UIWindow *window;
            if (@available(iOS 13.0, *)) {
                window = [UIApplication sharedApplication].windows[0];
            } else {
                window = [UIApplication sharedApplication].delegate.window;
            }
            [AppDelegate shareAppDelegate].window.rootViewController = [[yujianniNavViewController alloc]initWithRootViewController:[[CSLoginViewController alloc]init]];
        }else{
            if (model.msg.length>1) {
                [SVProgressHUD showInfoWithStatus:model.msg];
            }
            failure(task,model.msg);
        }
        NSLog(@"这个是数据%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task,error);
    }];
}

+ (NSDictionary *)dictionaryForJsonData:(NSData *)jsonData

{
    
    if (![jsonData isKindOfClass:[NSData class]] || jsonData.length < 1) {
        
        return nil;
        
    }
    
    id jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
    
    if (![jsonObj isKindOfClass:[NSDictionary class]]) {
        
        return nil;
        
    }
    
    return [NSDictionary dictionaryWithDictionary:(NSDictionary *)jsonObj];
    
}
@end
