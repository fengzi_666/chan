//
//  IMHttpModel.h
//  IM
//
//  Created by Apple on 2020/8/4.
//  Copyright © 2020 IM. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface tuziHttpModel : NSObject

@property (nonatomic,strong) NSString *msg;
//code是0接口成功
@property (nonatomic,assign) NSInteger code;
@end

NS_ASSUME_NONNULL_END
